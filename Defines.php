<?php
/**
 * Created by PhpStorm.
 * User: Gabriel PHP
 * Date: 09/09/2017
 * Time: 23:49
 */
define('LI_PROTOCOLO', 'http://');
/*
 * Enable tester ambient for online devs
 */
define('TESTONLINE', false);
define('DEBUG', 'teste');

/**
 * ENV
 * DESENVOLVIMENTO
 * PRODUCAO
 */
define('ENV', 'PRODUCAO');
define('ENV_DESENVOLVIMENTO', FALSE);
define('ENV_PRODUCAO', TRUE);

switch (ENV) {
    case ENV_DESENVOLVIMENTO:
        error_reporting(E_ALL);
        break;
    case ENV_PRODUCAO:
        error_reporting(0);
        ini_set('display_errors', 0);
        break;
    default:
        exit('The application environment is not set correctly.');
}
//Verify ambient
if (ENV_DESENVOLVIMENTO == true && TESTONLINE == false) {
    //Base Url
    $LI_BASE_PATH = '/cavok';
    $LI_BASE_URL = LI_PROTOCOLO . 'localhost' . $LI_BASE_PATH;
    //DB CONNECT
    $user = 'root';
    $password = '';
    $database = 'cavok';
} elseif (ENV == 'PRODUCAO' || TESTONLINE == true) {
    //Base Url
    $LI_BASE_PATH = '';
    $LI_BASE_URL = LI_PROTOCOLO . 'cavokdrone.com.br' . $LI_BASE_PATH;
    //DB CONNECT
    $user = 'root';
    $password = '';
    $database = 'cavok';
} else {
    die('You may not has any valid ENV! Select "DESENVOLVIMENTO or PRODUCAO for enable any functionality on this system."');
}
/*
 * @description Criando url Base
 */
define('LI_BASE_PATH', $LI_BASE_PATH);
define('LI_BASE_URL', $LI_BASE_URL);

/*
 *@description Conexao com banco de dados
 */
define("LI_CONNECT_DBTYPE", "mysql");
define("LI_CONNECT_HOST", '127.0.0.1');//"107.180.51.80"
define("LI_CONNECT_PORT", "");
define("LI_CONNECT_USER", $user);
define("LI_CONNECT_PASSWORD", $password);
define("LI_CONNECT_DATABASE", $database);

/*
 * @description Separando diretorios
 */
//Controladores
define("LI_DIR_CONTROLLERS", "src/Controllers/");
define("LI_NAMESPACE_CONTROLLERS", "Controllers\\");
//Controladores Admingit s
define("LI_DIR_CONTROLLERS_ADMIN", "src/Controllers/Admin/");
define("LI_NAMESPACE_CONTROLLERS_ADMIN", "Controllers\\Admin\\");
//Modelos
define("LI_DIR_MODELS", "src/Models/");
define("LI_NAMESPACE_MODELS", "Models\\");
//Visões
define("LI_DIR_VIEWS", "src/Views/");
define("LI_NAMESPACE_VIEWS", "Views\\");
//Modulos
define("LI_DIR_MODULOS", "src/Modulos/");
define("LI_NAMESPACE_MODULOS", "Modulos\\");
//Gadgets
define("LI_DIR_GADGETS", "src/Modulos/Gadgets/");
define("LI_NAMESPACE_GADGETS", "Modulos\\Gadgets\\");
//Gerenciador
define('LI_DIR_GERENCIADOR', 'src/Assets/Gerenciador/');
/**
 * Padrao Layout
 */
define('LAYOUT', 'AutoDesenvolve');
// Padrao de Imagens
define("LI_IMAGES_CACHES", "src/Assets/Images/Caches/");
define("LI_IMAGES_DEFAULT", "src/Assets/Images/Default/");
define("LI_IMAGES_DEFAULT_LAYOUT", LI_IMAGES_DEFAULT . 'Layout/');
define("LI_IMAGES_DEFAULT_LAYOUT_ADMIN", LI_IMAGES_DEFAULT . 'Admin/');
define("LI_MANAGEMENT", 'src/Assets/Images/Management/');



/**
 * Padrão Assets
 */
define('LI_ASSETS_ADMIN', 'src/Assets/Admin/Layout/' . LAYOUT . '/');

/*
 * @description Administracao Config
 *
 */
define('LI_BASE_ADMIN_URL', LI_BASE_URL . '/Admin');
define('LI_LAYOUT_ADMIN', 'src/Views/Admin/Layout/' . LAYOUT . '/');

/*
 * @description Extensao de arquivo padrao utilizada
 */
define('EXT', '.php');

/**
 * Tipos
 */
define('APP_MOEDA', 'R$ ');

/**
 * Compactacao
 */
define('GZIP', true);

/**
 * Erros
 */
define('DISPLAY_ERRORS', true);
