<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 17/11/2016
 * Time: 00:12
 */

namespace Core\DB;

use Illuminate\Database\Capsule\Manager as Capsule;

class CapsuleGetter
{

    /**
     * @return \Illuminate\Database\Schema\Builder
     */
    public function schema()
    {
        return Capsule::schema();
    }

    /**
     * @param $table
     * @return \Illuminate\Database\Query\Builder
     */
    public function table($table)
    {
        return Capsule::table($table);
    }

    /**
     * @return \Illuminate\Database\Connection
     */
    public function connection()
    {
        return Capsule::connection();
    }

}