<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 05/11/2016
 * Time: 09:51
 */

namespace Core\Modelos;

use Core\Modelos\Modelagem as ModelagemDriver;
use Illuminate\Database\Capsule\Manager as Capsule;

class  ModelagemDb
{

	public $error = '';

	/**
	 * @var array
	 */
	private $where = [];

	/**
	 * @var
	 */
	private $order;

	/**
	 * @var array
	 */
	private $order_by = [];

	/**
	 * @var
	 */
	private $group_by = '';

	/**
	 * @var
	 */
	private $join = [];

	/**
	 * @var array
	 */
	private $having = [];

	/**
	 * @var
	 */
	private $limit;

	/**
	 * @var ModelagemDriver
	 */
	private $LI_Model;

	/**
	 * @var bool
	 */
	private $select = false;

	/**
	 * @var array
	 */
	private $options = [];

	/**
	 * @var (int)
	 */
	private $lastInsertId;

	public function __construct()
	{
		$this->setLI_Model();
	}

	/**
	 *
	 */
	public function setLI_Model()
	{
		$this->LI_Model = new ModelagemDriver();
	}

	/**
	 * @return ModelagemDriver
	 */
	public function getLI_Model()
	{
		return $this->LI_Model;
	}

	/**
	 * @param $select
	 * @return $this
	 */
	public function select($select)
	{
		$this->select = $select;

		return $this;
	}

	private function getSelect()
	{
		return $this->select;
	}

	/**
	 * @param      $key
	 * @param null $value
	 * @param bool $scape
	 * @return $this
	 */
	public function where($key, $value = null, $scape = false)
	{
		if ($scape)
		{
			$this->where[] = ['method' => 'where', 'field' => $key, 'value' => $value];
		} else
		{
			$this->where[] = ['method' => 'where', 'field' => $key, 'value' => '"' . $value . '"'];
		}

		return $this;
	}

	/**
	 * @param        $key
	 * @param null   $value
	 * @param string $type
	 * @return $this
	 */
	public function like($key, $value = null, $type = 'both')
	{
		$this->where[] = ['method' => 'where', 'field' => $key, 'value' => $value, 'like' => $type];

		return $this;
	}

	/**
	 * @param       $key
	 * @param array $value
	 * @return $this
	 */
	public function where_in($key, $value = [])
	{
		$this->where[] = ['method' => 'where', 'field' => $key, 'value' => $value, 'in' => true];

		return $this;
	}

	/**
	 * @return string
	 */
	private function getWhere($scape = true)
	{
		$where = '';
		if (count($this->where))
		{
			$where = 'WHERE ';
		}
		foreach ($this->where as $field)
		{
			$mystring = $field['field'];
			$findme = ' ';
			$pos = strpos($mystring, $findme);
			if ($pos === false)
			{
				$comparison = ' = ';
			} else
			{
				$comparison = '';
			}
			if (isset($field['like']))
			{
				$where .= $field['field'] . ' LIKE ' . "'%" . $field['value'] . "%'" . ' AND ';
			} elseif (isset($field['in']))
			{
				$where .= $field['field'] . " IN ('" . implode("','", $field['value']) . "')";
			} else
			{
				$where .= $field['field'] . $comparison . $field['value'] . ' AND ';
			}

		}
		if (count($this->where))
		{
			$where = substr($where, 0, -5);
		}

		return $where;
	}

	/**
	 * @param        $table
	 * @param        $on
	 * @param string $tipo
	 * @return $this
	 */
	public function join($table, $on, $tipo = 'INNER')
	{
		$this->join[] = "$tipo JOIN $table ON $on ";

		return $this;
	}

	/**
	 * @return string
	 */
	private function getJoin()
	{
		return implode(' ', $this->join);
	}

	/**
	 * @param $key
	 * @param $value
	 * @return $this
	 *
	 * @by DB
	 */
	public function order($key, $value)
	{
		$this->order = ['key' => $key, 'value' => $value];

		return $this;
	}

	/**
	 * @return string
	 */
	private function getOrder()
	{
		$order = '';
		if (count($this->order))
		{
			$order = 'ORDER BY ' . $this->order['key'] . ' ' . $this->order['value'];
		}

		return $order;
	}

	/**
	 * @param $start
	 * @param $end
	 * @return $this
	 *
	 * @by DB
	 */
	public function limit($start, $end = false)
	{
		if ($end)
		{
			$this->limit = ['inicio' => $start, 'fim' => $end];
		} else
		{
			$this->limit = ['inicio' => $start];
		}

		return $this;
	}

	/**
	 * @return string
	 */
	private function getLimit()
	{
		$limit = '';

		if (is_array($this->limit) && count($this->limit))
		{
			if (isset($this->limit['fim']))
			{
				$limit = 'LIMIT ' . $this->limit['inicio'] . ', ' . $this->limit['fim'];

				return $limit;
			} else
			{
				$limit = isset($this->limit['inicio']) ? 'LIMIT ' . $this->limit['inicio'] : '';

				return $limit;
			}
		}
	}

	/**
	 * @param $field
	 * @return $this
	 */
	public function group_by($field)
	{
		$this->group_by = ' GROUP BY ' . $field;

		return $this;
	}

	/**
	 * @return mixed
	 */
	private function getGroupBy()
	{
		return $this->group_by;
	}

	/**
	 * @param $field
	 * @param $value
	 * @return $this
	 */
	public function order_by($field, $value)
	{
		if (is_string($this->order_by))
		{
			$this->order_by = [];
		}
		array_push($this->order_by, $field . ' ' . $value);

		return $this;
	}

	/**
	 * @return mixed
	 */
	private function getOrderBy()
	{
		if (is_string($this->order_by) && strlen($this->order_by) <= 1)
		{
			return '';
		}
		if (is_string($this->order_by))
		{
			return ' ORDER BY ' . $this->order_by;
		}
		if (!count($this->order_by))
		{
			return '';
		}

		return 'ORDER BY ' . implode(', ', $this->order_by);
	}

	/**
	 * @param $operators
	 * @return $this
	 */
	public function having($operators)
	{
		$this->having[] = $operators;

		return $this;
	}

	/**
	 * @return mixed|string
	 */
	private function getHaving()
	{
		if (!count($this->having))
		{
			return '';
		}
		$having = ' HAVING ' . implode(', ', $this->having);

		return $having;
	}

	/**
	 * @param $table
	 * @return ModelagemDb
	 */
	public function get($table)
	{
		$where = $this->getWhere();
		$orderBy = $this->getOrderBy();
		$groupBy = $this->getGroupBy();
		$limit = $this->getLimit();
		$join = $this->getJoin();
		$having = $this->getHaving();
		$select = $this->getSelect() ? $this->getSelect() : '*';
		$this->options = [
			'table'    => $table,
			'select'   => $select,
			'join'     => $join,
			'where'    => $where,
			'order_by' => $orderBy,
			'group_by' => $groupBy,
			'limit'    => $limit,
			'having'   => $having,
		];
		$this->unsetOptions();

		return $this;
	}

	/**
	 * @return array
	 */
	public function result($showSql = false, $exit = true)
	{
		$sql = $this->getSql();
		if ($showSql)
		{
			if ($exit)
			{
				show_array($sql, true);
			} else
			{
				show_array($sql, false);
			}
		}
		$this->options = [];

		return $this->query($sql);
	}

	/**
	 * @return object
	 */
	public function row($showSql = false, $returnSql = false)
	{
		$this->limit(1);
		$sql = $this->getSql();
		$this->unsetOptions();
		$this->options = [];
		if ($showSql)
		{
			echo $sql;
			exit;
		}
		if ($returnSql)
		{
			return $sql;
		}

		return current($this->query($sql));
	}

	/**
	 * @param string $tipo
	 * @return array|string
	 */
	public function getSql($tipo = 'SELECT')
	{
		$result = $this->getLI_Model()
			->get($this->options['table'],
				$tipo . ' ' . $this->options['select'],
				$this->options['join'],
				$this->options['where'],
				$this->options['order_by'],
				$this->options['group_by'],
				$this->options['limit'],
				(isset($this->options['having']) ? $this->options['having'] : ''),
				true
			);
		$this->options = [];

		return $result;
	}

	/**
	 *
	 */
	private function unsetOptions()
	{
		$this->where = [];
		$this->order = '';
		$this->order_by = [];
		$this->join = [];
		$this->limit = '';
		$this->group_by = '';
		$this->select = false;
		$this->having = [];
	}

	public function resetDb()
	{
		$this->unsetOptions();

		return $this;
	}

	/**
	 * @param            $tabela
	 * @param            $dados
	 * @param bool|false $params
	 * @return int
	 */
	public function insert($tabela, $dados, $params = false)
	{
		$campos = [];
		if ($params)
		{
			$campos = $dados;
		}
		if (!$params)
		{
			foreach ($dados as $key => $value)
			{
				$campos[ $key ] = $key;
				$params[ $key ] = $value;
			}
		}
		$lastInsertId = $this->getLI_Model()->insert($tabela, $campos, $params);
		$this->setLastInsertId($lastInsertId);

		return $this->getLastInsertId();
	}

	/**
	 * @param $lastInsertId
	 */
	private function setLastInsertId($lastInsertId)
	{
		$this->lastInsertId = $lastInsertId;
	}

	/**
	 * @return int
	 */
	public function getLastInsertId()
	{
		return (int)$this->lastInsertId;
	}

	/**
	 * @param $tabela
	 * @param $dados
	 * @param $params
	 * @return int
	 * @by DB
	 */
	public function update($tabela, $dados, $params = false)
	{
		$campos = [];
		if ($params)
		{
			$campos = $dados;
		}
		if (!$params)
		{
			foreach ($dados as $key => $value)
			{
				$campos[ $key ] = $key;
				$params[ $key ] = $value;
			}
		}

		$rowCount = $this->getLI_Model()->update($tabela, $campos, $params);
		$this->error = $this->getLI_Model()->getError();

		return $rowCount;
	}

	/**
	 * @param      $table
	 * @param bool $execute
	 * @param bool $showSql
	 * @return bool
	 */
	public function delete($table, $execute = true, $showSql = false)
	{
		$where = $this->getWhere();
		$orderBy = $this->getOrderBy();
		$groupBy = $this->getGroupBy();
		$limit = $this->getLimit();
		$join = $this->getJoin();
		$select = " ";
		$this->options = [
			'table'    => $table,
			'select'   => $select,
			'join'     => $join,
			'where'    => $where,
			'order_by' => $orderBy,
			'group_by' => $groupBy,
			'limit'    => $limit,
		];
		$this->unsetOptions();
		$sql = $this->getSql('DELETE');
		if ($execute)
		{
			$this->query($sql);
		}
		if ($showSql)
		{
			echo '<pre>';
			print_r($sql);
			echo '<br>';
			echo '</pre>';
		}

		return $execute;

	}

	public function query($sql)
	{
		return $this->getLI_Model()->getLI_Database()->selectDB($sql);
	}

	public function Capsule()
	{
		$capsule = new Capsule;

		$capsule->addConnection([
			'driver'    => LI_CONNECT_DBTYPE,
			'host'      => LI_CONNECT_HOST,
			'database'  => LI_CONNECT_DATABASE,
			'username'  => LI_CONNECT_USER,
			'password'  => LI_CONNECT_PASSWORD,
			'port'      => LI_CONNECT_PORT,
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		]);
		$capsule->setAsGlobal();
		$capsule->bootEloquent();

		return new \Core\DB\CapsuleGetter();
	}

}