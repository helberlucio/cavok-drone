<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 05/11/2016
 * Time: 09:45
 */

namespace Core\Modelos;


use Aplicacao\Ferramentas\Identificadores;
use Core\DB\Conexao as ConexaoDB;

class Modelagem
{

    /**
     * @var ConexaoDB
     */
    protected $LI_Database;

    private $sql = '';
    private $error = '';

    public function __construct()
    {
        $this->setLI_Database();
    }

    public function setLI_Database()
    {
        $this->LI_Database = new ConexaoDB();
    }

    /**
     * @return ConexaoDB
     */
    public function getLI_Database()
    {
        return $this->LI_Database;
    }

    private function setSql($table, $select = '*', $join = '', $where = '', $orderBy = '', $groupBy, $limit = '', $having = '', $offset = '')
    {
        $sql = "{$select} FROM {$table} {$join} {$where} {$groupBy} {$having} {$orderBy} {$limit} ";
        $this->sql = $sql;
    }

    private function getSql()
    {
        return $this->sql;
    }

    public function getError() {
        return $this->error;
    }

    public function get($table, $select = '', $join = '', $where = '', $orderBy, $groupBy, $limit = '', $having = '', $justSql = false)
    {
        $this->setSql($table, $select, $join, $where, $orderBy, $groupBy, $limit, $having);
        if ($justSql) {
            return $this->getSql();
        }
        $resultObj = $this->getLI_Database()->selectDB($this->getSql());

        return $resultObj;
    }

    /**
     * @param $tabela
     * @param $campos
     * @param $params
     * @return int
     *
     * @by db
     */
    public function insert($tabela, $campos, $params)
    {
        $camposTabela = '';
        foreach ($campos as $campo) {
            $camposTabela .= '`'.$campo.'`, ';
        }
        $camposTabela = substr($camposTabela, 0,-2);
        $fields = '';
        foreach ($campos as $campo) {
            $fields .= ", :".$campo;
        }
        $contaLeng = strlen($fields);
        $fields = substr($fields, 2, $contaLeng);

        $this->sintaxy = "INSERT INTO {$tabela} ({$camposTabela}) VALUES ({$fields})";

        $lastId = $this->getLI_Database()->insertDB($this->sintaxy, $params);
        return $lastId;
    }


    /**
     * @param $tabela
     * @param $campos
     * @param $params
     * @return int
     * @by DB
     */
    public function update($tabela, $campos, $params)
    {
        $linkscampos = '';
        foreach ($campos as $campo) {
            $linkscampos .= '`'.$campo.'`' . "=:" . $campo . ", ";
        }
        $contafields = strlen($linkscampos) - 2;
        $linkscampos = substr($linkscampos, 0, $contafields);
        $primaryKey = Identificadores::chavePrimaria($tabela);
        $whereParametter =  $primaryKey. '=:' . $primaryKey;
        $sql = "UPDATE {$tabela} SET {$linkscampos} WHERE {$whereParametter}";
        try {
            $rowCount = $this->getLI_Database()->updateDB($sql, $params);
            return $rowCount;
        } catch (\Exception $ex) {
            $this->error = $ex->getMessage();
            return 0;
        }
    }

    /**
     * @param $tabela
     * @param $params
     *
     * @by DB
     */
    public function delete($tabela, $params)
    {
        foreach ($params as $key => $value) {
        }
        $sql = "DELETE FROM {$tabela} WHERE {$key}=:{$key}";
        $this->getLI_Database()->deleteDB($sql, $params);
    }



}

