<script>
    function base_url($url) {
        if (!(typeof $url !== 'undefined')) {
            $url = '/';
        }
        return '<?php echo \Aplicacao\Url::Base();?>' + $url;
    }

    function current_url() {
        return '<?php echo \Aplicacao\Url::Current();?>';
    }


    var width = $(window).width();
    var fontLevel = 0;
    var upFontSizeLimit, downFontSizeLimit;

    function getCurrentWidthForSize() {
        width = $(window).width();
        downFontSizeLimit = 10;
        if (width < 1320 && width >= 1024) {
            upFontSizeLimit = 12;
        } else {
            upFontSizeLimit = 15;
        }
    }

    function upFontSize() {
        getCurrentWidthForSize();
        if (fontLevel < 2) {
            $('p, span, a, h1, h2, h3, h4, h5, h6, li').each(function () {
                var size = parseInt($(this).css('font-size'));
                if (size < upFontSizeLimit) {
                    $(this).css('font-size', size + 1);
                }
            });
            fontLevel += 1;
        }
    }

    function downFontSize() {
        getCurrentWidthForSize();
        if (fontLevel > -2) {
            $('p, span, a, h1, h2, h3, h4, h5, h6, li').each(function () {
                var size = parseInt($(this).css('font-size'));
                if (size > downFontSizeLimit) {
                    $(this).css('font-size', size - 1);
                }
            });
            fontLevel -= 1;
        }
    }

    String.prototype.replaceAll = function (search, replacement) {
        var target = this;
        return target.split(search).join(replacement);
    };
    function formatarDinheiro($valor) {
        return $valor.toLocaleString('pt-br', {minimumFractionDigits: 2});
    }
</script>