"use strict";window.elFinderSupportVer1=function(upload){var self=this;this.upload=upload||'auto';this.init=function(fm){this.fm=fm;this.fm.parseUploadData=function(text){var data;if(!$.trim(text)){return{error:['errResponse','errDataEmpty']}}
try{data=$.parseJSON(text)}catch(e){return{error:['errResponse','errDataNotJSON']}}
return self.normalize('upload',data)}}
this.send=function(opts){var self=this,fm=this.fm,dfrd=$.Deferred(),cmd=opts.data.cmd,args=[],_opts={},data,xhr;dfrd.abort=function(){xhr.state()=='pending'&&xhr.abort()}
switch(cmd){case 'open':opts.data.tree=1;break;case 'parents':case 'tree':return dfrd.resolve({tree:[]});break;case 'get':opts.data.cmd='read';opts.data.current=fm.file(opts.data.target).phash;break;case 'put':opts.data.cmd='edit';opts.data.current=fm.file(opts.data.target).phash;break;case 'archive':case 'rm':opts.data.current=fm.file(opts.data.targets[0]).phash;break;case 'extract':case 'rename':case 'resize':opts.data.current=fm.file(opts.data.target).phash;break;case 'duplicate':_opts=$.extend(!0,{},opts);$.each(opts.data.targets,function(i,hash){$.ajax($.extend(_opts,{data:{cmd:'duplicate',target:hash,current:fm.file(hash).phash}})).error(function(error){fm.error(fm.res('error','connect'))}).done(function(data){data=self.normalize('duplicate',data);if(data.error){fm.error(data.error)}else if(data.added){fm.trigger('add',{added:data.added})}})});return dfrd.resolve({})
break;case 'mkdir':case 'mkfile':opts.data.current=opts.data.target;break;case 'paste':opts.data.current=opts.data.dst
break;case 'size':return dfrd.resolve({error:fm.res('error','cmdsupport')});break;case 'search':return dfrd.resolve({error:fm.res('error','cmdsupport')});break;case 'file':opts.data.cmd='open';opts.data.current=fm.file(opts.data.target).phash;break}
xhr=$.ajax(opts).fail(function(error){dfrd.reject(error)}).done(function(raw){data=self.normalize(cmd,raw);if(cmd=='paste'&&!data.error){fm.sync();dfrd.resolve({})}else{dfrd.resolve(data)}})
return dfrd}
this.normalize=function(cmd,data){var self=this,fm=this.fm,files={},filter=function(file){return file&&file.hash&&file.name&&file.mime?file:null},phash,diff,isCwd;if((cmd=='tmb'||cmd=='get')){return data}
if(cmd=='upload'&&data.error&&data.cwd){data.warning=$.extend({},data.error);data.error=!1}
if(data.error){return data}
if(cmd=='put'){phash=fm.file(data.target.hash).phash;return{changed:[this.normalizeFile(data.target,phash)]}}
phash=data.cwd.hash;isCwd=(phash==fm.cwd().hash);if(data.tree){$.each(this.normalizeTree(data.tree),function(i,file){files[file.hash]=file})}
$.each(data.cdc||[],function(i,file){var hash=file.hash;if(files[hash]){files[hash].date=file.date;files[hash].locked=file.hash==phash?!0:file.rm===void(0)?!1:!file.rm}else{files[hash]=self.normalizeFile(file,phash,data.tmb)}});if(!data.tree){$.each(fm.files(),function(hash,file){if(!files[hash]&&file.phash!=phash&&file.mime=='directory'){files[hash]=file}})}
if(cmd=='open'){return{cwd:files[phash]||this.normalizeFile(data.cwd),files:$.map(files,function(f){return f}),options:self.normalizeOptions(data),init:!!data.params,debug:data.debug}}
diff=isCwd?fm.diff($.map(files,filter)):{added:$.map(files,filter)};return $.extend({current:data.cwd.hash,error:data.error,warning:data.warning,options:{tmb:!!data.tmb}},diff)}
this.normalizeTree=function(root){var self=this,result=[],traverse=function(dirs,phash){var i,dir;for(i=0;i<dirs.length;i++){dir=dirs[i];result.push(self.normalizeFile(dir,phash))
dir.dirs.length&&traverse(dir.dirs,dir.hash)}};traverse([root]);return result}
this.normalizeFile=function(file,phash,tmb){var mime=file.mime||'directory',size=mime=='directory'&&!file.linkTo?0:file.size,info={url:file.url,hash:file.hash,phash:phash,name:file.name,mime:mime,date:file.date||'unknown',size:size,read:file.read,write:file.write,locked:!phash?!0:file.rm===void(0)?!1:!file.rm};if(file.mime=='application/x-empty'||file.mime=='inode/x-empty'){info.mime='text/plain'}
if(file.linkTo){info.alias=file.linkTo}
if(file.linkTo){info.linkTo=file.linkTo}
if(file.tmb){info.tmb=file.tmb}else if(info.mime.indexOf('image/')===0&&tmb){info.tmb=1}
if(file.dirs&&file.dirs.length){info.dirs=!0}
if(file.dim){info.dim=file.dim}
if(file.resize){info.resize=file.resize}
return info}
this.normalizeOptions=function(data){var opts={path:data.cwd.rel,disabled:$.merge((data.disabled||[]),['search','netmount','zipdl']),tmb:!!data.tmb,copyOverwrite:!0};if(data.params){opts.api=1;opts.url=data.params.url;opts.archivers={create:data.params.archives||[],extract:data.params.extract||[]}}
if(opts.path.indexOf('/')!==-1){opts.separator='/'}else if(opts.path.indexOf('\\')!==-1){opts.separator='\\'}
return opts}}