function disableLink() {
    $('a.disableLink').click(function () {
        return !1
    })
}

function retira_acentos(str) {
    var com_acento = "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝŔÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿŕ";
    var sem_acento = "AAAAAAACEEEEIIIIDNOOOOOOUUUUYRsBaaaaaaaceeeeiiiionoooooouuuuybyr";
    var novastr = "";
    for (i = 0; i < str.length; i++) {
        var troca = !1;
        for (var a = 0; a < com_acento.length; a++) {
            if (str.substr(i, 1) === com_acento.substr(a, 1)) {
                novastr += sem_acento.substr(a, 1);
                troca = !0;
                break
            }
        }
        if (troca === !1) {
            novastr += str.substr(i, 1)
        }
    }
    return novastr
}

function str_pos($needle, $string) {
    if (typeof $needle !== 'undefined') {
        $needle = retira_acentos($needle.toLowerCase())
    }
    if (typeof $string !== 'undefined') {
        $string = retira_acentos($string.toLowerCase())
    }
    var n = $string.indexOf($needle);
    if (n > -1) {
        if (n === 0) {
            n = 1
        }
        return n
    } else {
        return !1
    }
}

function hitEnterToTabInput() {
    $('input').keypress(function (e) {
        if (e.keyCode === 13) {
            var nextInput = !1;
            var $this = $(this);
            $.each($('input'), function (key, val) {
                if (nextInput === !0) {
                    nextInput = !1;
                    val.focus();
                    return
                }
                if ($this[0] === val) {
                    nextInput = !0
                }
            });
            return !1
        }
    })
}

$(function () {
    hitEnterToTabInput();
    disableLink()
});

function confirm2($options) {
    var title = 'Confirmação de ação';
    var content = 'Deseja realmente executar essa ação ?';
    var type = 'red';
    var confirm_title = 'Sim';
    var cancel_title = 'Não';
    var confirm_class = '';
    var cancel_class = '';
    var confirm_keys = [];
    var cancel_keys = [];
    var confirm_function_callback = function () {
    };
    var cancel_function_callback = function () {
    };
    if (typeof $options.title !== 'undefined') {
        title = $options.title
    }
    if (typeof $options.content !== 'undefined') {
        content = $options.content
    }
    if (typeof $options.type !== 'undefined') {
        type = $options.type
    }
    if (typeof $options.buttons !== 'undefined' && typeof $options.buttons.confirm_title !== 'undefined') {
        confirm_title = $options.buttons.confirm_title
    }
    if (typeof $options.buttons !== 'undefined' && typeof $options.buttons.cancel_title !== 'undefined') {
        cancel_title = $options.buttons.cancel_title
    }
    if (typeof $options.buttons !== 'undefined' && typeof $options.buttons.confirm_class !== 'undefined') {
        confirm_class = $options.buttons.confirm_class
    }
    if (typeof $options.buttons !== 'undefined' && typeof $options.buttons.cancel_class !== 'undefined') {
        cancel_class = $options.buttons.cancel_class
    }
    if (typeof $options.buttons !== 'undefined' && typeof $options.buttons.confirm_keys !== 'undefined') {
        confirm_keys = $options.buttons.confirm_keys
    }
    if (typeof $options.buttons !== 'undefined' && typeof $options.buttons.cancel_keys !== 'undefined') {
        cancel_keys = $options.buttons.cancel_keys
    }
    if (typeof $options.buttons !== 'undefined' && typeof $options.buttons.confirm_function_callback !== 'undefined') {
        confirm_function_callback = $options.buttons.confirm_function_callback
    }
    if (typeof $options.buttons !== 'undefined' && typeof $options.buttons.cancel_function_callback !== 'undefined') {
        cancel_function_callback = $options.buttons.cancel_function_callback
    }
    var options = {
        title: title,
        content: content,
        type: type,
        buttons: {
            confirm_title: confirm_title,
            cancel_title: cancel_title,
            confirm_class: confirm_class,
            cancel_class: cancel_class,
            confirm_keys: confirm_keys,
            cancel_keys: cancel_keys,
            confirm_function_callback: confirm_function_callback,
            cancel_function_callback: cancel_function_callback
        }
    };
    $.confirm({
        title: options.title,
        content: options.content,
        type: options.type,
        typeAnimated: !0,
        buttons: {
            confirm: {
                text: options.buttons.confirm_title,
                btnClass: options.buttons.confirm_class,
                keys: options.buttons.confirm_keys,
                action: function () {
                    options.buttons.confirm_function_callback()
                }
            },
            cancel: {
                text: options.buttons.cancel_title,
                btnClass: options.buttons.cancel_class,
                keys: options.buttons.cancel_keys,
                action: function () {
                    options.buttons.cancel_function_callback()
                }
            }
        }
    })
}

class AutoCompleteAPI {
    constructor($id, $dataApi, $replaceId, $replaceValue, $response) {
        this.prototypeClass = {
            inputIdActived: '',
            urlApiActived: '',
            replaceIdActived: '',
            replaceValueActived: '',
            replaceResponseActived: '',
            search: !0,
            triggerFirstResult: !1,
            lastValueSearched: [],
            init: function ($id, $dataApi, $replaceId, $replaceValue, $response) {
                console.log('===AutoCompleteAPI===');
                console.log($id);
                console.log('======');
                console.log($dataApi);
                this.hideOnOutClick();
                var $thisClass = this;
                var typingTimer;
                var doneTypingInterval = 600;
                var $input = $('#' + $id);
                var nameInput = $input.attr('name');
                var placeholder = $input.attr('placeholder');
                var classes = $input.attr('class');
                var id = $input.attr('id');
                var dataId = $input.attr('data-id');
                var dataValue = $input.attr('data-value');
                var ngModel = '';
                if (typeof $input.attr('ng-model') !== "undefined") {
                    ngModel = 'ng-model="' + $input.attr('ng-model') + '"';
                }
                console.log('_-_-_-_-_-_-_-_-_-_-_-_-__-_-_-_-_-_-_-_-_-_-_-_-__-_-_-_-_-_-_-_-_-_-_-_-_');
                console.log(nameInput);
                console.log('_-_-_-_-_-_-_-_-_-_-_-_-__-_-_-_-_-_-_-_-_-_-_-_-__-_-_-_-_-_-_-_-_-_-_-_-_');
                if (nameInput == 'undefined') {
                    console.log('input name not founded');
                    return false;
                }
                $input.removeAttr('name');
                $input.before('<div class="input-group autocompleteField">' + '<input ' + ngModel + ' autocomplete="off" type="text" class="' + classes + '" placeholder="' + placeholder + '" name="' +
                    nameInput.replace('[', '').replace(']', '') +
                    '-fake" id="' + $id + '">' + '<span class="buttonSearchDefault-' + $id + ' input-group-addon btn-primary" id="basic-addon2">' + '<i class="fa fa-chevron-down" aria-hidden="true"></i>' + '<i style="display:none;font-size: 10px;" class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom" aria-hidden="true"></i>' + '</span></div>' + '<input type="text" hidden name="' + nameInput + '" id="realinput-' + $id + '" class="realinput-' + $id + '">');
                $input.remove();
                var $input = $('#' + $id);
                this.moveSelectedItem($id);
                $('.buttonSearchDefault-' + $id).click(function () {
                    $input.focus();
                    setTimeout(function () {
                        $thisClass.inputIdActived = $id;
                        $thisClass.urlApiActived = $dataApi;
                        $thisClass.replaceIdActived = $replaceId;
                        $thisClass.replaceValueActived = $replaceValue;
                        $thisClass.replaceResponseActived = $response;
                        $thisClass.search = !1;
                        $thisClass.doneTyping()
                    }, 90)
                });
                $('#' + $id).click(function () {
                    setTimeout(function () {
                        $thisClass.inputIdActived = $id;
                        $thisClass.urlApiActived = $dataApi;
                        $thisClass.replaceIdActived = $replaceId;
                        $thisClass.replaceValueActived = $replaceValue;
                        $thisClass.replaceResponseActived = $response;
                        $thisClass.search = !1;
                        $thisClass.doneTyping()
                    }, 90)
                });
                $('#' + $id).focus(function () {
                    setTimeout(function () {
                        $thisClass.inputIdActived = $id;
                        $thisClass.urlApiActived = $dataApi;
                        $thisClass.replaceIdActived = $replaceId;
                        $thisClass.replaceValueActived = $replaceValue;
                        $thisClass.replaceResponseActived = $response;
                        $thisClass.search = !1;
                        $thisClass.doneTyping()
                    }, 90)
                });
                $input.on('keyup', function () {
                    $('.buttonSearchDefault-' + $id + ' .fa.fa-chevron-down').hide();
                    $('.buttonSearchDefault-' + $id + ' .fa.fa-spinner.fa-pulse').show();
                    clearTimeout(typingTimer);
                    $thisClass.inputIdActived = $id;
                    $thisClass.urlApiActived = $dataApi;
                    $thisClass.replaceIdActived = $replaceId;
                    $thisClass.replaceValueActived = $replaceValue;
                    $thisClass.replaceResponseActived = $response;
                    $thisClass.search = !0;
                    if ($input.val() === '') {
                        return
                    }
                    typingTimer = setTimeout(function () {
                        $thisClass.doneTyping()
                    }, doneTypingInterval)
                });
                $input.on('keydown', function () {
                    clearTimeout(typingTimer)
                });
                if (typeof dataValue === 'undefined' && typeof dataId !== 'undefined') {
                    if (!$.isArray($dataApi)) {
                        dataValue = dataId;
                        $input.val(dataValue);
                        $('.buttonSearchDefault-' + $id + ' .fa.fa-chevron-down').hide();
                        $('.buttonSearchDefault-' + $id + ' .fa.fa-spinner.fa-pulse').show();
                        clearTimeout(typingTimer);
                        $thisClass.inputIdActived = $id;
                        $thisClass.urlApiActived = $dataApi;
                        $thisClass.replaceIdActived = $replaceId;
                        $thisClass.replaceValueActived = $replaceValue;
                        $thisClass.replaceResponseActived = $response;
                        $thisClass.search = !0;
                        $thisClass.triggerFirstResult = !0;
                        $thisClass.doneTyping()
                    } else if ($.isArray($dataApi)) {
                        if (typeof $replaceId === 'undefined') {
                            $replaceId = 'id'
                        }
                        if (typeof $replaceValue === 'undefined') {
                            $replaceValue = 'value'
                        }
                        var MatchResults = !1;
                        $.each($dataApi, function (key, val) {
                            dataId = dataId;
                            val[$replaceId] = val[$replaceId];
                            if (val[$replaceId] == dataId) {
                                MatchResults = !0;
                                dataValue = val[$replaceValue];
                                return
                            }
                        });
                        if (MatchResults === !0) {
                            MatchResults = !1;
                            console.log($id, dataValue);
                            $input.val(dataValue);
                            $('#realinput-' + $id).val(dataId).trigger('change')
                        }
                    }
                }
            },
            doneTyping: function () {
                this.autocompleteFieldApi(this.inputIdActived, this.urlApiActived, this.replaceIdActived, this.replaceValueActived, this.replaceResponseActived)
            },
            autocompleteFieldApi: function ($id, $dataApi, $replaceId, $replaceValue, $responseApi) {
                var $thisClass = this;
                var field = $('input#' + $id);
                if (field.val() === this.lastValueSearched[$id] && this.lastValueSearched[$id] !== '') {
                    $('#autocompleteBoxForApi-' + $id).show();
                    $('.buttonSearchDefault-' + $id + ' .fa.fa-chevron-down').show();
                    $('.buttonSearchDefault-' + $id + ' .fa.fa-spinner.fa-pulse').hide();
                    return
                }
                this.lastValueSearched[$id] = field.val();
                var id = 'id';
                var value = 'value';
                var responseApi = 'response';
                if (typeof $replaceId !== 'undefined') {
                    id = $replaceId
                }
                if (typeof $replaceValue !== 'undefined') {
                    value = $replaceValue
                }
                if (typeof $responseApi !== 'undefined') {
                    responseApi = $responseApi
                }
                if ($responseApi === '') {
                    responseApi = !1
                }
                var htmlAutoComplete = '';
                if ($.isArray($dataApi)) {
                    htmlAutoComplete = '<div class="autocompleteBoxForApi col-md-12" id="autocompleteBoxForApi-' + $id + '">';
                    htmlAutoComplete += '<div class="autocompleteBoxForApi-results" id="autocompleteBoxForApi-results-' + $id + '">';
                    htmlAutoComplete += '<ul role="tree">';
                    $.each($dataApi, function (key, val) {

                        var searchMask = field.val();
                        var regEx = new RegExp(searchMask, "ig");
                        var replaceMask = '<b style="background: yellow;">' + field.val() + '</b>';
                        if (str_pos(field.val(), val[value]) && field.val().length >= 2) {
                            htmlAutoComplete += '<li data-value="' + val[id] + '" data-text="' + val[value] + '">';
                            htmlAutoComplete += '<a href="javascript:void(0);">';
                            htmlAutoComplete += val[value].replace(regEx, replaceMask);
                            htmlAutoComplete += '</a>';
                            htmlAutoComplete += '</li>'
                        } else {
                            if ($dataApi.length <= 20 || $thisClass.search === !1) {
                                htmlAutoComplete += '<li data-value="' + val[id] + '" data-text="' + val[value] + '">';
                                htmlAutoComplete += '<a href="javascript:void(0);">';
                                htmlAutoComplete += val[value].replace(regEx, replaceMask);
                                htmlAutoComplete += '</a>';
                                htmlAutoComplete += '</li>'
                            }
                        }
                    });
                    htmlAutoComplete += '</ul>';
                    htmlAutoComplete += '</div>';
                    htmlAutoComplete += '</div>';
                    $thisClass.appendResponses(field, $id, htmlAutoComplete)
                } else if ($dataApi.length > 10 && $thisClass.search === !0) {
                    $.ajax({
                        url: $dataApi,
                        data: {"value": field.val()},
                        headers: {"token": token},
                        type: "GET",
                        success: function (response) {
                            var dataResults = response[responseApi];
                            if (responseApi === !1) {
                                dataResults = response
                            }
                            var mensageResponse = 'Resultados abaixo';
                            if (response.msg !== 'undefined') {
                                mensageResponse = response.msg
                            }
                            htmlAutoComplete = '<div class="autocompleteBoxForApi col-md-12" id="autocompleteBoxForApi-' + $id + '">';
                            htmlAutoComplete += '<div class="autocompleteBoxForApi-results" id="autocompleteBoxForApi-results-' + $id + '">';
                            htmlAutoComplete += '<ul role="tree">';
                            if ($thisClass.triggerFirstResult === !0) {
                                if ($.isArray(dataResults) && typeof dataResults[0] !== 'undefined') {
                                    var valu = dataResults[0];
                                    $('#realinput-' + $id).val(valu[id]);
                                    $('#' + $id).val(valu[value]);
                                    $('.buttonSearchDefault-' + $id + ' .fa.fa-chevron-down').show();
                                    $('.buttonSearchDefault-' + $id + ' .fa.fa-spinner.fa-pulse').hide()
                                }
                                $thisClass.triggerFirstResult = !1;
                                return
                            }
                            $.each(dataResults, function (key, val) {
                                var searchMask = field.val();
                                var regEx = new RegExp(searchMask, "ig");
                                var replaceMask = '<b style="background: yellow;">' + field.val() + '</b>';
                                if (str_pos(field.val(), val[value]) && field.val().length > 1) {
                                    htmlAutoComplete += '<li data-suggest-resource data-value="' + val[id] + '" data-text="' + val[value] + '">';
                                    htmlAutoComplete += '<a href="javascript:void(0);">';
                                    htmlAutoComplete += val[value].replace(regEx, replaceMask);
                                    htmlAutoComplete += '</a>';
                                    htmlAutoComplete += '</li>'
                                } else if (field.val().length > 1) {
                                    htmlAutoComplete += '<li data-value="' + val[id] + '" data-text="' + val[value] + '">';
                                    htmlAutoComplete += '<a href="javascript:void(0);">';
                                    htmlAutoComplete += val[value].replace(regEx, replaceMask);
                                    htmlAutoComplete += '</a>';
                                    htmlAutoComplete += '</li>'
                                }
                            });
                            htmlAutoComplete += '</ul>';
                            htmlAutoComplete += '</div>';
                            htmlAutoComplete += '</div>';
                            $thisClass.appendResponses(field, $id, htmlAutoComplete)
                        },
                        error: function (response) {
                            $('.btn').removeAttrs('disabled');
                            if (typeof response.responseJSON !== 'undefined' && typeof response.responseJSON.msg !== 'undefined') {
                                $('.callbackplace-' + $id).html('<div class="alert alert-danger">' + response.responseJSON.msg + '</div>');
                                setTimeout(function () {
                                    $('.callbackplace-' + $id).html()
                                }, 4500)
                            }
                        }
                    })
                } else if ($thisClass.search === !1) {
                    htmlAutoComplete = '<div class="autocompleteBoxForApi col-md-12" id="autocompleteBoxForApi-' + $id + '">';
                    htmlAutoComplete += '<div class="autocompleteBoxForApi-results" id="autocompleteBoxForApi-results-' + $id + '">';
                    htmlAutoComplete += '<ul role="tree">';
                    htmlAutoComplete += '<li data-value="0" data-text="Comece a digitar para autocompletar">';
                    htmlAutoComplete += '<a href="javascript:void(0);">';
                    htmlAutoComplete += 'Comece a digitar para autocompletar';
                    htmlAutoComplete += '</a>';
                    htmlAutoComplete += '</li>';
                    $thisClass.appendResponses(field, $id, htmlAutoComplete)
                }
            },
            appendResponses: function ($field, $id, $htmlAutoComplete) {
                var $thisClass = this;
                $('#autocompleteBoxForApi-' + $id).remove();
                $field.after($htmlAutoComplete);
                $('#autocompleteBoxForApi-results-' + $id + ' ul li').click(function () {
                    var value = $(this).attr('data-value');
                    var text = $(this).attr('data-text');
                    $('#realinput-' + $id).val(value).trigger('change');
                    $field.val(text);
                    $thisClass.hideAutoCompleteList()
                });
                var inputWidth = $field.width();
                if (inputWidth < 80) {
                    inputWidth = 80
                }
                $('#autocompleteBoxForApi-results-' + $id).width((inputWidth + 63));
                $('.buttonSearchDefault-' + $id + ' .fa.fa-chevron-down').show();
                $('.buttonSearchDefault-' + $id + ' .fa.fa-spinner.fa-pulse').hide()
            },
            moveSelectedItem: function ($id) {
                var $thisClass = this;
                $('#' + $id).keydown(function (e) {
                    var $listItems = $('#autocompleteBoxForApi-results-' + $id + ' ul li');
                    var key = e.keyCode, $selected = $listItems.filter('.active'), $current;
                    if (key === 13) {
                        if (!$selected.length) {
                            $current = $listItems.eq(0)
                        } else {
                            $current = $selected
                        }
                        var value = $current.attr('data-value');
                        var text = $current.attr('data-text');
                        $('#realinput-' + $id).val(value).trigger('change');
                        $('#' + $id).val(text);
                        $thisClass.hideAutoCompleteList();
                        return !1
                    }
                    if (key !== 40 && key !== 38) {
                        return
                    }
                    $listItems.removeClass('active');
                    if (key === 40) {
                        if (!$selected.length || $selected.is(':last-child')) {
                            $current = $listItems.eq(0)
                        } else {
                            $current = $selected.next()
                        }
                    } else if (key === 38) {
                        if (!$selected.length || $selected.is(':first-child')) {
                            $current = $listItems.last()
                        } else {
                            $current = $selected.prev()
                        }
                    }
                    $current.addClass('active')
                })
            },
            hideOnOutClick: function () {
                var $thisClass = this;
                $(window).click(function () {
                    $thisClass.hideAutoCompleteList()
                })
            },
            hideAutoCompleteList: function () {
                $('.autocompleteBoxForApi').hide()
            }
        };
        this.prototypeClass.init($id, $dataApi, $replaceId, $replaceValue, $response)
    }
}

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

$(function () {

});
