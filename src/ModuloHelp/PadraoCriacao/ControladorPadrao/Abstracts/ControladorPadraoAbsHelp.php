<?php

/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 17/02/2017
 * Time: 11:33
 */

namespace ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts;

use Aplicacao\Url;
use Core\Modelos\Carregamentos;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\ControladorPadrao\Interfaces\ControladorPadraoInterfHelp;
use Modulos\AdminAutoDesenvolve\Escritorios\Models\Constantes\Escritorios;
use Modulos\AdminAutoDesenvolve\Escritorios\Models\Repositorios\AdminAutoDesenvolveEscritoriosRP;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsUsuariosEntidade;


class ControladorPadraoAbsHelp implements ControladorPadraoInterfHelp
{

    public $db;
    public $load;

    public $escritorios = array();
    public $admin = true;

    public function __construct()
    {
        $this->db = new ModelagemDb();
        $this->load = new Carregamentos();
        if (empty($this->escritorios)) {
            $this->escritorios = array(
                Escritorios::emissao_de_notas_fiscais => 'Emissão de notas fiscais',
                Escritorios::compras => 'Compras',
                Escritorios::vendas => 'Vendas',
                Escritorios::frente_de_caixa => 'Frente de caixa',
                Escritorios::fluxo_de_caixa => 'Fluxo de caixa',
                Escritorios::estoque => 'Estoque',
                Escritorios::recurssos_humanos => 'Recurssos humanos',
                Escritorios::cobranca => 'Cobrança',
                Escritorios::contabilidade => 'Contabilidade',
                Escritorios::gerenciar_produtos => 'Gerenciar produtos',
                Escritorios::configuracoes_gerais => 'Configurações gerais',
                Escritorios::estatisticas_da_loja => 'Estatísticas da loja',
                Escritorios::gerenciar_balanco_de_estoque => 'Gerenciar balanço de estoque'
            );
        }
        $this->verificarEscritorio();
    }

    /**
     * @param string $view
     * @param array $data
     * @param bool $admin
     * @return string
     */
    public function index($view = 'index', $data = array(), $admin = false)
    {
        $class = get_class($this);
        if (!$class) {
            return 'erro';
        }
        $namescpaneEmodulo = explode('\\', $class);
        if (!is_array($namescpaneEmodulo)) {
            return 'erro';
        }
        $emodulo = '';
        $grupoModulo = '';
        $nomeModulo = '';
        foreach ($namescpaneEmodulo as $key => $modulo) {
            if ($key == 1) {
                $grupoModulo = $modulo;
            }
            if ($key == 2) {
                $nomeModulo = $modulo;
            }
            if ($key >= 3) {
                break;
            }
            $emodulo .= $modulo . '\\';
        }
        eval('echo \\' . $emodulo . 'Models\\Repositorios\\' . $grupoModulo . $nomeModulo . 'RP::getIndexPage($view, $data, $admin);');
    }

    public function modulo()
    {
        // TODO: Implement modulo() method.
    }

    public function verificarEscritorio()
    {
        $escritoriosRP = (AdminAutoDesenvolveEscritoriosRP::getEscritorioAtual());
        if (!isset($this->escritorios[$escritoriosRP->getId()])) {
            if (sessao('admin') instanceof CmsUsuariosEntidade) {
                if (sessao('admin')->getId() && $this->admin) {
                    Url::Redirecionar(Url::Admin());
                }
            }
        }
    }

}