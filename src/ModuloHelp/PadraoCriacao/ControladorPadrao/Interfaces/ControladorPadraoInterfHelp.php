<?php

/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 17/02/2017
 * Time: 11:31
 */

namespace ModuloHelp\PadraoCriacao\ControladorPadrao\Interfaces;

interface ControladorPadraoInterfHelp
{
    public function index();

    public function modulo();
}