<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Gabriel
 * Date: 10/02/2017
 * Time: 09:26
 */

namespace ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts;


use Aplicacao\Conversao;
use Aplicacao\Ferramentas\Identificadores;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Interfaces\EntidadePadraoIntfHelp;

abstract class EntidadePadraoAbsHelp implements EntidadePadraoIntfHelp
{

    public static $TABELA = '';
    public static $PREFIXO = '';
    public static $NAMESPACE_FILE = '';
    public static $FILE_NAME = '';

    public $error = '';

    public function __construct()
    {
        self::$TABELA = 'POLIFORFISMTHIS';
        self::$PREFIXO = 'POLIFORFISMTHIS';
        self::$NAMESPACE_FILE = 'POLIFORFISMTHIS';
        self::$FILE_NAME = 'POLIFORFISMTHIS';
    }

    /**
     * @return bool
     */
    public function getPkField()
    {
        return Identificadores::chavePrimaria(self::$TABELA);
    }

    /**
     * @param ModelagemDb $objectFilter
     * @return $this
     */
    public function getRow(ModelagemDb $objectFilter, $showSql = false)
    {
        $obj = $objectFilter->get(self::$TABELA)->row($showSql);
        $thisClass = "\\" . self::$NAMESPACE_FILE . "\\" . self::$FILE_NAME;
        $classe = Conversao::objetoParaClasse($obj, new $thisClass);
        return $classe;
    }

    /**
     * @param ModelagemDb $objectFilter
     * @return $this[]
     */
    public function getRows(ModelagemDb $objectFilter, $showSql = false)
    {

        $classe = array();
        $objects = $objectFilter->get(self::$TABELA)->result($showSql);
        $thisClass = "\\" . self::$NAMESPACE_FILE . "\\" . self::$FILE_NAME;

        foreach ($objects as $row) {
            $classe[] = Conversao::objetoParaClasse($row, new $thisClass);
        }
        return $classe;
    }

    /**
     * @param bool $debug
     * @return $this
     */
    public function update($debug = false)
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::$NAMESPACE_FILE . "\\" . self::$FILE_NAME;
        $data = GenericRepository::createSaveData($thisClass, $this);
        if ($debug) {
            print_r(self::$TABELA);
            print_r($data);
        }
        if (!$db->update(self::$TABELA, $data)) {
            $thisClassInstancia = (new $thisClass);
            foreach ($data as $atributo => $valor) {
                if (!isset($thisClassInstancia->{$atributo})) {
                    continue;
                }
                $thisClassInstancia->{$atributo} = $valor;
            }
            $this->error = $db->error;
        }
        if($debug){
            show_array($this, true);
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function delete()
    {
        $db = new ModelagemDb();
        $getterPkFiel = 'get' . Conversao::underscoreToCamelCase($this->getPkField());
        $data[$this->getPkField()] = $this->{$getterPkFiel}();
        return $db->where($this->getPkField(), $this->{$getterPkFiel}())->delete(self::$TABELA, true, false);
    }

    /**
     * @return int
     */
    public function insert($debug = false, $returnDebug = false)
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::$NAMESPACE_FILE . "\\" . self::$FILE_NAME;
        if(method_exists($thisClass, 'setInsertData') && method_exists($thisClass, 'getInsertData')){
            if(!$this->getInsertData()){
                $this->setInsertData(date('Y-m-d H:i:s'));
            }
        }
        $data = GenericRepository::createSaveData($thisClass, $this);
        if ($debug) {
            if($returnDebug){
                return $data;
            }
            print_r(self::$TABELA);
            print_r($data);
            exit;
        }
        return $db->insert(self::$TABELA, $data);
    }
}