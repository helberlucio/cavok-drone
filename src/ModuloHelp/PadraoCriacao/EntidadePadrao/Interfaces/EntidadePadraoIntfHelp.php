<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 10/02/2017
 * Time: 09:27
 */

namespace ModuloHelp\PadraoCriacao\EntidadePadrao\Interfaces;

use Core\Modelos\ModelagemDb;
interface EntidadePadraoIntfHelp
{
    public function __construct();

    public function getRow(ModelagemDb $db);

    public function getRows(ModelagemDb $db);

    public function Insert();

    public function Update();

    public function Delete();
}