<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Gabriel
 * Date: 10/02/2017
 * Time: 09:26
 */

namespace ModuloHelp\PadraoCriacao\ModelRegraDeNegocio\Abstracts;


use Aplicacao\Conversao;
use Aplicacao\Ferramentas\Identificadores;
use Aplicacao\GenericRepository;
use Aplicacao\Url;
use Core\Modelos\Carregamentos;
use Core\Modelos\ModelagemDb;
use League\Flysystem\Exception;
use ModuloHelp\PadraoCriacao\ModelRegraDeNegocio\Interfaces\ModelRegraDeNegocioPadraoIntfHelp;
use Modulos\AdminAutoDesenvolve\Escritorios\Models\Constantes\Escritorios;
use Modulos\AdminAutoDesenvolve\Escritorios\Models\Repositorios\AdminAutoDesenvolveEscritoriosRP;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsUsuariosEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\EmailAvisoEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\ServidorEnvioEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\WebsiteGeralEntidade;
use Modulos\Envio\Email\Models\RegrasDeNegocio\EnvioMailer;

abstract class ModelRegraDeNegocioPadraoAbsHelp implements ModelRegraDeNegocioPadraoIntfHelp
{

    public $load;
    public $db;
    public $viewName = '';
    public $viewNameHtml = '';

    public function __construct()
    {
        $this->db = new ModelagemDb();
        $this->load = new Carregamentos();
    }

    public function getModulo($viewName = 'index', $data = array(), $admin = false, $minifyHtml = false, $viewDeAtualizacao = false)
    {
        $class = get_class($this);
        if (!$class) {
            return 'erro';
        }
        $namescpaneEmodulo = explode('\\', $class);

        if (!is_array($namescpaneEmodulo)) {
            return 'erro';
        }
        $emodulo = '';
        foreach ($namescpaneEmodulo as $key => $modulo) {
            if ($key >= 3) {
                break;
            }
            $emodulo .= $modulo . '\\';
        }
        if ($admin) {
            return $this->load->AdminView($viewName, $data, true, $emodulo, $minifyHtml);
        }

        $this->viewNameHtml = $this->identificarNomeDaViewHtml();
        $this->viewName = $this->identificarNomeDaView($viewName, $data, $emodulo);

        if ($viewDeAtualizacao && strlen($this->viewName) >= 5) {
            $atualizado = $this->verificarAtualizacaoDaViewDoModulo($emodulo);
            if (!$atualizado) {
                if ($this->atualizarViewDoModulo($viewName, $data, $emodulo, $minifyHtml)) {
                    return $this->getHtmlView(false, $emodulo);
                }
                throw new Exception('Não foi possivel atualizar o modulo', 500);
            } else {
                return $this->getHtmlView(false, $emodulo);
            }
        }
        $minifyHtml = false;
        return $this->load->view($viewName, $data, true, $emodulo, $minifyHtml);
    }


    public function identificarNomeDaViewHtml()
    {
        return $this->viewNameHtml = Url::Title($_SERVER['QUERY_STRING']);
    }

    public function identificarNomeDaView($viewName, $data, $moduloName)
    {
        $moduloDir = 'src/' . str_replace('\\', '/', $moduloName) . 'Views/';
        if (isset($data['pagina'])) {
            if (!file_exists($moduloDir . $data['pagina'] . '_view.php')) {
                return false;
            }
            return $data['pagina'];
        }
        if (!file_exists($moduloDir . $viewName . '_view.php')) {
            return false;
        }
        return $viewName;
    }

    private function verificarAtualizacaoDaViewDoModulo($moduloName)
    {
        if (!$moduloName) {
            return true;
        }

        $moduloDir = 'src/' . str_replace('\\', '/', $moduloName) . 'Views/';
        if (!is_dir($moduloDir)) {
            throw new Exception('Diretorio view nao existe no modulo: ' . $moduloName, 500);
        }

        $fileViewHtml = $moduloDir . $this->viewNameHtml . '_view.html';
        if (!is_file($fileViewHtml)) {
            return false;
        }

        $fileUltimaAtualizacao = $moduloDir . 'ultima_atualizacao.html';
        if (!is_file($fileUltimaAtualizacao)) {
            return false;
        }

        $fileUltimaAtualizacaoGeral = str_replace('\\', '/', realpath('./')) . '/ultima_atualizacao_geral.html';

        if (!is_file($fileUltimaAtualizacaoGeral)) {
            file_put_contents($fileUltimaAtualizacaoGeral, '#i' . date('Y-m-d H:i:s') . '#f', LOCK_EX);
        }

        $ultimaAtualizacaoDoModulo = file_get_contents($fileUltimaAtualizacao);
        $ultimaAtualizacaoGeral = file_get_contents($fileUltimaAtualizacaoGeral);

        $dataDaUltimaAtualizacaoDoModulo = substr($ultimaAtualizacaoDoModulo, (strpos('#i', $ultimaAtualizacaoDoModulo) + 2), 19);
        $dataDaUltimaAtualizacaoGeral = substr($ultimaAtualizacaoGeral, (strpos('#i', $ultimaAtualizacaoGeral) + 2), 19);

        $stampAtualizacaoDoModulo = strtotime($dataDaUltimaAtualizacaoDoModulo);
        $stampAtualizacaoGeral = strtotime($dataDaUltimaAtualizacaoGeral);

        if ($stampAtualizacaoDoModulo < $stampAtualizacaoGeral) {
            return false;
        }
        return true;
    }

    private function atualizarViewDoModulo($viewName, $data, $moduloName, $minifyHtml)
    {
        $moduloDir = 'src/' . str_replace('\\', '/', $moduloName) . 'Views/';
        $teste = array('Dir'=>$moduloDir, 'file'=>$this->viewNameHtml . '_view.html');
        //show_array($teste);
        $fileViewHtml = $moduloDir . $this->viewNameHtml . '_view.html';
        $fileUltimaAtualizacao = $moduloDir . 'ultima_atualizacao.html';
        file_put_contents($fileUltimaAtualizacao, '#i' . date('Y-m-d H:i:s') . '#f', LOCK_EX);
        $viewCode = $this->load->view($viewName, $data, true, $moduloName, $minifyHtml);
        file_put_contents($fileViewHtml, $viewCode, LOCK_EX);
        return true;
    }

    public function getHtmlView($viewName = false, $moduloName)
    {
        if ($viewName !== false) {
            $this->viewNameHtml = $viewName;
        }
        $fileViewHtml = 'src/' . str_replace('\\', '/', $moduloName) . 'Views/' . $this->viewNameHtml . '_view.html';
        $view = file_get_contents($fileViewHtml);
        return $view;
    }

    public function enviarEmail($dados = array(), $assunto = 'Contato do site', $body = null, $emailsExtras = array(), $enviarSomenteExtras = false, $anexos = array())
    {
        //Iniciar dependenciais gerais
        $websiteGeral = (new WebsiteGeralEntidade())->getRow($this->db);
        $mailsAlerta = (new EmailAvisoEntidade())->getRows($this->db);
        $mailsAlerta = $enviarSomenteExtras ? [] : $mailsAlerta;
        //passa pelos emails extras verificando se cada um é uma instancia
        foreach ($emailsExtras as $emailExtra) {
            if (!$emailExtra instanceof EmailAvisoEntidade) {
                continue;
            }
            $mailsAlerta = array_merge($mailsAlerta, array($emailExtra));
        }
        //Carrega dados do servidor
        $dadosServidorEntidade = (new ServidorEnvioEntidade())->getRow($this->db);
        //instancia Classe do mailer para envio dos emaiils
        $mailer = new EnvioMailer();
        //Seta o array dos dados do servidor
        $dadosDoServidor = array(
            'seguranca' => 'ssl',
            'host' => $dadosServidorEntidade->getHost(),
            'porta' => $dadosServidorEntidade->getPorta(),
            'username' => $dadosServidorEntidade->getEmail(),
            'password' => $dadosServidorEntidade->getSenha(),
            'from' => $dadosServidorEntidade->getEmail(),
            'nome_from' => $websiteGeral->getEmpresa(),
            'reply' => $dadosServidorEntidade->getEmail()
        );
        //Seta o bomdia / boa tarde / boa noite;
        $inicioBody = 'Bom dia ';
        if (date('H') >= 12) {
            $inicioBody = 'Boa tarde ';
        }
        if (date('H') >= 19) {
            $inicioBody = 'Boa noite ';
        }
        //Se não tem body, cria um padrao de acordo com os dados passados
        if (!$body) {
            $body = '<img style="max-width:250px;" src="' . Url::Base() . $websiteGeral->getLogo() . '" alt="' . $websiteGeral->getEmpresa() . '"><br><br>';
            $body .= '<b>' . $inicioBody . '</b> #nome#<br>';
            foreach ($dados as $key => $dado) {
                $body .= '<b>' . $key . ': </b>' . $dado . '<br>';
            }
        }
        $retornosDeEnvios = array();
        foreach ($mailsAlerta as $mailAlerta) {
            $retornosDeEnvios[] = $mailer->enviarEmailSMTP(
                $dadosDoServidor,
                $mailAlerta->getEmail(),
                $mailAlerta->getNome(),
                $assunto,
                str_replace('#nome#', $mailAlerta->getNome(), $body),
                $anexos);
        }
        return $retornosDeEnvios;
    }
}