<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 10/02/2017
 * Time: 09:27
 */

namespace ModuloHelp\PadraoCriacao\ModelRegraDeNegocio\Interfaces;

interface ModelRegraDeNegocioPadraoIntfHelp
{
    public function __construct();

    public function getModulo();

}