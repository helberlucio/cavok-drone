<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Gabriel
 * Date: 10/02/2017
 * Time: 09:26
 */

namespace ModuloHelp\PadraoCriacao\RepositorioPadrao\Abstracts;


use Aplicacao\Conversao;
use ModuloHelp\PadraoCriacao\ModelGadgets\Abstracts\ModelGadgetsPadraoAbsHelp;
use ModuloHelp\PadraoCriacao\ModelRegraDeNegocio\Abstracts\ModelRegraDeNegocioPadraoAbsHelp;
use ModuloHelp\PadraoCriacao\RepositorioPadrao\Interfaces\RepositorioPadraoIntfHelp;

abstract class RepositorioPadraoAbsHelp implements RepositorioPadraoIntfHelp
{
    public function getModulo($view = 'index', $data = array(), $admin = false)
    {
        $class = get_class($this);
        if (!$class) {
            return 'erro';
        }
        $namespaneEmodulo = explode('\\', $class);
        if (!is_array($namespaneEmodulo)) {
            return 'erro';
        }
        $namespaceModulo = '';
        $nomeDoModuloString = '';
        foreach ($namespaneEmodulo as $key => $modulo) {
            if ($key >= 1 && $key < 3) {
                $nomeDoModuloString .= $modulo . '_';
            }
            if ($key >= 4) {
                break;
            }
            $namespaceModulo .= $modulo . '\\';
        }
        $modulo = '\\' . $namespaceModulo . 'RegrasDeNegocio\\' . Conversao::underscoreToCamelCase($nomeDoModuloString) . 'RN';
        if (!class_exists($modulo)) {
            return 'Classe nao existe: ' . $modulo;
        }
        $RN = new $modulo();
        if (!method_exists($RN, 'getModulo')) {
            return 'Metodo getModulo nao existe';
        }
        /**
         * @var $RN ModelRegraDeNegocioPadraoAbsHelp
         */
        return $RN->getModulo($view, $data, $admin);
    }
}