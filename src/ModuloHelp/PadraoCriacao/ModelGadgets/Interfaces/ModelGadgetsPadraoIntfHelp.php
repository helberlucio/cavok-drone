<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 10/02/2017
 * Time: 09:27
 */

namespace ModuloHelp\PadraoCriacao\ModelGadgets\Interfaces;

interface ModelGadgetsPadraoIntfHelp
{
    public function __construct();

    public function getGadget();
}