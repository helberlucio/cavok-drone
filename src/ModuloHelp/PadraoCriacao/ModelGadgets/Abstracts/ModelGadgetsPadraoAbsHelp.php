<?php

/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 08/07/2017
 * Time: 15:54
 */
namespace ModuloHelp\PadraoCriacao\ModelGadgets\Abstracts;

use Core\Modelos\Carregamentos;
use ModuloHelp\PadraoCriacao\ModelGadgets\Interfaces\ModelGadgetsPadraoIntfHelp;
use Modulos\AdminAutoDesenvolve\Escritorios\Models\Constantes\Escritorios;

class ModelGadgetsPadraoAbsHelp implements ModelGadgetsPadraoIntfHelp
{

    public $db;
    public $load;
    public $enable_gadget = false;
    public $order = 0;
    public $ecritorios = array();

    public function __construct()
    {
        $this->db = new \Core\Modelos\ModelagemDb();
        $this->load = new Carregamentos();
        $this->ecritorios = array(
            Escritorios::emissao_de_notas_fiscais=>'Emissão de notas fiscais',
            Escritorios::compras=>'Compras',
            Escritorios::vendas=>'Vendas',
            Escritorios::frente_de_caixa=>'Frente de caixa',
            Escritorios::fluxo_de_caixa=>'Fluxo de caixa',
            Escritorios::estoque=>'Estoque',
            Escritorios::recurssos_humanos=>'Recurssos humanos',
            Escritorios::cobranca=>'Cobrança',
            Escritorios::contabilidade=>'Contabilidade',
            Escritorios::gerenciador_site=>'Gerênciador de site',
            Escritorios::tarefas=>'Tarefas',
            );
    }

    public function getGadget($view = 'index', $data = array(), $admin = true)
    {
        $prefixoView = 'Gadgets/';
        $view = $prefixoView . $view;

        if (isset($data['pagina'])) {
            $data['pagina'] = $prefixoView.$data['pagina'];
        }

        $class = get_class($this);
        if (!$class) {
            return 'erro';
        }
        $namescpaneEmodulo = explode('\\', $class);
        if (!is_array($namescpaneEmodulo)) {
            return 'erro';
        }
        $emodulo = '';
        foreach ($namescpaneEmodulo as $key => $modulo) {
            if ($key >= 3) {
                break;
            }
            $emodulo .= $modulo . '\\';
        }
        if ($admin) {
            return $this->load->AdminView($view, $data, true, $emodulo);
        }
        return $this->load->view($view, $data, true, $emodulo);
    }

}