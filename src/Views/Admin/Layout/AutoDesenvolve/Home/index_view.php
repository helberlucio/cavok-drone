<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Ariza
 * Date: 05/11/2016
 * Time: 10:00
 */
if (!isset($data)) {
    $data = array();
}
?>
<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="pt-br" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <?php $this->load->AdminView('Home/head', array()); ?>
    <?php $this->load->AdminView('Home/custom_layout', array()); ?>
</head>
<body ng-app="AutoDesenvolcerAPP">
<?php
$this->load->AdminView('Home/loading', array());
$this->load->AdminView('Home/header', $data);
$this->load->AdminView('Home/menu', $data);

if (isset($pagina)) {
    unset($data['pagina']);
    if (isset($moduloRouteLI)) {
        $data['moduloRouteLIToOpen'] = $moduloRouteLI;
    }

    $data['admin_page_to_open'] = $pagina;
    $this->load->AdminView('Home/abertura_pagina', $data);
}
if (!isset($pagina)) {
    echo '<code>Nenhuma view foi inicializada</code>';
}
$this->load->AdminView('Home/footer', $data);
?>
<script>
    $(function () {
        var windowWidth = $(window).width();
        if (windowWidth > 1000) {
            medirTamanhoAlturaMenu();
            medirTamanhoLarguraMenu(
                <?php echo
                !isset($_SESSION['admin_escritorio_atual'])
                ||
                !$_SESSION['admin_escritorio_atual']
                    ? 0 :
                    220;?>);
        }
    });

    function voltarPagina() {
        window.history.back();
    }

</script>
<!-- ################ Scripts de fluxo de caixa ################################# --->
<script type='text/javascript'>
    function medirTamanhoAlturaMenu() {
        var windowHeight = ($(window).height());
        var aberturaDeTela = $('.aberturaDeTela');
        aberturaDeTela.css({'min-height': windowHeight + 'px'});
        var PanelHeight = aberturaDeTela.height();
        $('.menu-left-autodesenvolve').css({'min-height': PanelHeight});
    }

    function medirTamanhoLarguraMenu($valorDiferenciacao) {
        var widthDaTela = $(window).width();
        var widthDoMenu = (($valorDiferenciacao / widthDaTela) * 100);
        $('.aberturaDeTela').css({'min-width': ((100 - widthDoMenu) - 0.03) + '%'});
        $('.informacoes-header-autodesenvolve').css({'min-width': (100 - widthDoMenu) + '%'});
        $('.menu-left-autodesenvolve').css({'max-width': widthDoMenu + '%'});
        $('.logo-header-autodesenvolve').css({'max-width': widthDoMenu + '%'});

    }

    var optionSet1 = {
        startDate: moment('<?php echo date('01/m/Y');?>', 'DD/MM/YYYY'),
        endDate: moment('<?php echo date('t/m/Y');?>', 'DD/MM/YYYY'),
        showDropdowns: true,
        opens: 'left',
        format: 'DD/MM/YYYY',
        ranges: {
            'Este mês': [moment().startOf('month'), moment().endOf('month')],
            'Próximo mês': [moment().add(1, 'month').startOf('month'), moment().add(1, 'month').endOf('month')],
            'Mês passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'Hoje': [moment(), moment()],
            'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Amanhã': [moment().add(1, 'days'), moment().subtract(1, 'days')],
            'Últimos 3 meses': [moment().subtract(3, 'month'), moment().subtract(0, 'month')],
            'Próximos 3 meses': [moment().add(0, 'month'), moment().add(3, 'month')],
            'Últimos 7 dias': [moment().subtract(6, 'days'), moment()],
            'Próximos 7 dias': [moment().add(7, 'days'), moment()]
        },
        locale: {
            applyLabel: 'Aplicar',
            cancelLabel: 'Limpar',
            fromLabel: 'Data início',
            toLabel: 'Data Fim',
            customRangeLabel: 'Personalizado',
            daysOfWeek: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            firstDay: 1
        }
    };
    $(function () {
        if ($(window).width() > 768) {
            $('.filtraData').daterangepicker(optionSet1);

            $('.filtraData').on('show.daterangepicker', function (ev, picker) {
                $('.daterangepicker').addClass("show-calendar");
            });

            $('.filtraData').on('cancel.daterangepicker', function (ev, picker) {
                $('#filtraData').val("");
            });
        }
        $(".filtraData").mask("99/99/9999 - 99/99/9999");

        $('#filtraCategoria-btn-recarregar').click(function () {
            closeTheIFrameImDoneReload('filtraCategoria');
        });
        $('#filtraCategoria-btn-group .dropdown-toggle').click(function () {
            var cod = $('#filtraCategoria').val();
            if (!cod) {
                $('#filtraCategoria-btn-editar').closest('li').hide();
            } else {
                $('#filtraCategoria-btn-editar').closest('li').show();
                var url = $('#filtraCategoria-btn-editar').attr('data-url');
                $('#filtraCategoria-btn-editar').attr('href', url + '?cod=' + cod + '&tema=branco');
            }
        });

        $('#filtraCliente-btn-recarregar').click(function () {
            closeTheIFrameImDoneReload('filtraCliente');
        });
        $('#filtraCliente-btn-group .dropdown-toggle').click(function () {
            var cod = $('#filtraCliente').val();
            if (!cod) {
                $('#filtraCliente-btn-editar').closest('li').hide();
            } else {
                $('#filtraCliente-btn-editar').closest('li').show();
                var url = $('#filtraCliente-btn-editar').attr('data-url');
                $('#filtraCliente-btn-editar').attr('href', url + '?cod=' + cod + '&tema=branco');
            }
        });
        $.fn.select2.defaults.set("theme", "bootstrap");
        $.fn.select2.defaults.set("width", "style");
        $('.select2').select2();
    });
    // Fluxo de caixa
    $.metadata.setType('attr', 'validate');

    function niceLink() {
        $('a').click(function () {
            var href = $(this).attr('href');
            var classes = $(this).attr('class');
            classes = classes.split(' ');
            var isOutsideLink = true;
            $.each(classes, function (key, value) {
                if (value === 'disableLink') {
                    isOutsideLink = false;
                }
            });
            if (href === '#') {
                return true;
            }
            if (href === '') {
                isOutsideLink = false;
            }
            if (href === 'javascript:void(0)') {
                isOutsideLink = false;
            }
            if (isOutsideLink === true) {
                goToNiceLink(href);
                return false;
            }
            return false;
        });
    }

    function goToNiceLink($href) {
        $('.load-gear').fadeIn();
        $('body').css({opacity: '0.5'});
        location = $href;
        setTimeout(function () {
            $('.load-gear').fadeOut();
            $('body').css({opacity: '1'});
        }, 3000);
    }

    $(function () {
        $('#filtraCategoria-btn-group').hide();
        $('#field-filtraCategoria').removeClass('group-insercao-dinamica');

        $('#filtraCliente-btn-group').hide();
        $('#field-filtraCliente').removeClass('group-insercao-dinamica');

    });
    // END Fluxo de caixa

</script>


</body>
<!-- END BODY -->
</html>