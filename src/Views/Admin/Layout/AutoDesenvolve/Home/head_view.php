<?php
$configuracao = (new \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\WebsiteGeralEntidade())->getRow($this->db);
?>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<meta charset="UTF-8">
<meta name="description" content="CMS Auto Desenvolver - Gabriel Ariza">
<meta name="author" content="Gabriel Ariza Gomes de Castro">
<title>[Admin] <?= $configuracao->getEmpresa() ?></title>
<link rel="manifest" href="<?php echo \Aplicacao\Url::Base('manifest.json');?>">
<link rel="stylesheet"
      href="<?php echo \Aplicacao\Url::Base(); ?>src/Assets/Admin/Layout/AutoDesenvolve/Plugins/Angular/angular-material.min.css">

<link rel="stylesheet" type="text/css" href="<?php echo \Aplicacao\Url::Base(); ?>src/Assets/Admin/Layout/AutoDesenvolve/Plugins/font-awesome.min.css">
<link rel="icon" type="text/css" href="<?php echo \Aplicacao\Url::Base($configuracao->getFavicon()); ?>"/>

<link href='https://fonts.googleapis.com/css?family=Passion+One:400,700,900&subset=latin,latin-ext' rel='stylesheet'
      type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700italic,700,500italic,500,400italic,300italic,300'
      rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

<link rel="stylesheet" type="text/css"
      href="<?php echo \Aplicacao\Url::Base('src/Assets/js/plugins/colorpicker/spectrum/css/spectrum.css'); ?>">
<?php
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Padrao/Components', 'css', false);
?>
<!-- Angular libraries -->
<script src="<?php echo \Aplicacao\Url::Base(); ?>src/Assets/Admin/Layout/AutoDesenvolve/Plugins/Angular/angular.min.js"></script>
<script src="<?php echo \Aplicacao\Url::Base(); ?>src/Assets/Admin/Layout/AutoDesenvolve/Plugins/Angular/angular-animate.min.js"></script>
<script src="<?php echo \Aplicacao\Url::Base(); ?>src/Assets/Admin/Layout/AutoDesenvolve/Plugins/Angular/angular-route.min.js"></script>
<script src="<?php echo \Aplicacao\Url::Base(); ?>src/Assets/Admin/Layout/AutoDesenvolve/Plugins/Angular/angular-aria.min.js"></script>
<script src="<?php echo \Aplicacao\Url::Base(); ?>src/Assets/Admin/Layout/AutoDesenvolve/Plugins/Angular/angular-messages.min.js"></script>
<script src="<?php echo \Aplicacao\Url::Base(); ?>src/Assets/Admin/Layout/AutoDesenvolve/Plugins/Angular/angular-material.min.js"></script>
<script src="<?php echo \Aplicacao\Url::Base(); ?>src/Assets/Admin/Layout/AutoDesenvolve/Plugins/Angular/docs.js"></script>
<script src="<?php echo \Aplicacao\Url::Base(); ?>src/Assets/Admin/Layout/AutoDesenvolve/Plugins/Angular/svg-assets-cache.js"></script>
<script src="<?php echo Aplicacao\Url::Base(); ?>src/Assets/Admin/Layout/AutoDesenvolve/Aplicacoes/Angular/app.js" type="text/javascript"></script>

<script src="<?php echo \Aplicacao\Url::Base(); ?>src/Assets/Admin/Layout/AutoDesenvolve/Plugins/jquery.min.js"></script>
<script src="<?php echo \Aplicacao\Url::Base(); ?>src/Assets/Admin/Layout/AutoDesenvolve/Plugins/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo \Aplicacao\Url::Base(); ?>src/Assets/Admin/Layout/AutoDesenvolve/Plugins/jquery.dataTables.min.css">
<script src="<?php echo \Aplicacao\Url::Base(); ?>src/Assets/Admin/Layout/AutoDesenvolve/Plugins/ckeditor5/build/ckeditor.js"></script>

<link href="<?php echo \Aplicacao\Url::Base(); ?>src/Assets/Admin/Layout/AutoDesenvolve/Plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo \Aplicacao\Url::Base(); ?>src/Assets/Admin/Layout/AutoDesenvolve/Plugins/bootstrap-toastr/toastr.js" type="text/javascript"></script>
<?php include 'src/Assets/js/phphelper.php' ?>
<link rel="stylesheet" href="<?php echo Aplicacao\Url::Base('src/Assets/util/lightbox/src/css/lightbox.css'); ?>">
<script src="<?php echo Aplicacao\Url::Base('src/Assets/util/lightbox/src/js/lightbox.js'); ?>"></script>
<link rel="stylesheet" href="<?php echo \Aplicacao\Url::Base(); ?>src/Assets/Admin/Layout/AutoDesenvolve/Plugins/bootstrap.min.css">
<script src="<?php echo \Aplicacao\Url::Base(); ?>src/Assets/Admin/Layout/AutoDesenvolve/Plugins/bootstrap.min.js"></script>
<?php
echo '<script>var token = "'.sessao('admin')->getToken().'";</script>';
Aplicacao\Ferramentas\GetAssets::JS('plugins/colorpicker/spectrum/js/spectrum',null,null,false);
Aplicacao\Ferramentas\GetAssets::JS('plugins/colorpicker/spectrum/js/jquery.spectrum-pt-br',null,null,false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Padrao/Default', 'js', false);

//Datepicker
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/bootstrap-datepicker/js/bootstrap-datepicker.min', 'js', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min', 'js', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/bootstrap-datepicker/css/bootstrap-datepicker.min', 'css', false);

\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Padrao/Layout', 'css');

// Presets fluxo de caixa
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/theme','css', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/theme-custom','css', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/magnific-popup','css', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/default','css', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/jquery.click-calendario-1.0','css', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/autoform','css', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/select2.min','css', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/select2-bootstrap.min','css', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/daterangepicker-bs3','css', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/jquery-confirm.min','css', false);

\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/modernizr','js', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/nanoscroller','js', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/jquery.quicksearch','js', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/jquery.magnific-popup.min','js', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/admin-scripts','js', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/jquery.validate.min','js', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/jquery.metadata','js', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/jquery.maskedinput-1.4.min','js', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/jquery.click-calendario-1.0','js', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/jquery.maskMoney','js', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/moment-with-locales','js', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/select2.full.min','js', false);
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/daterangepicker','js', false);
//https://craftpip.github.io/jquery-confirm/
\Aplicacao\Ferramentas\GetAssets::LayoutAdmin('Plugins/fluxodecaixa/jquery-confirm.min','js', false);
?>
