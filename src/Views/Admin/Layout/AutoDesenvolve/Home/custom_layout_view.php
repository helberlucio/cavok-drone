<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 10/04/2017
 * Time: 20:50
 */
$stiloAdmin = (new \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysDesignAdmin())->getRow($this->db);
?>
<style type="text/css">
    /*Core do topo*/
    .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu > li.heading > h3, .page-sidebar .page-sidebar-menu > li.heading > h3 {
        color: <?php echo $stiloAdmin->getCorTopo();?> !important;

    }

    .page-header.navbar, .page-footer, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .sidebar-search .input-group .form-control, .page-sidebar .sidebar-search .input-group .form-control, .input-group-btn {
        background-color: <?php echo $stiloAdmin->getCorTopo();?> !important;
    }

    /*Cor do menu ativo*/
    .page-header.navbar .top-menu .navbar-nav > li.dropdown .dropdown-toggle:hover, .page-header.navbar .top-menu .navbar-nav > li.dropdown.open .dropdown-toggle, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu > li:hover > a, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu > li.open > a, .page-sidebar .page-sidebar-menu > li:hover > a, .page-sidebar .page-sidebar-menu > li.open > a {
        background-color: <?php echo $stiloAdmin->getCorMenuHover();?> !important;
    }

    .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu .sub-menu > li:hover > a, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu .sub-menu > li.open > a, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu .sub-menu > li.active > a, .page-sidebar .page-sidebar-menu .sub-menu > li:hover > a, .page-sidebar .page-sidebar-menu .sub-menu > li.open > a, .page-sidebar .page-sidebar-menu .sub-menu > li.active > a {
        background-color: rgba(0, 0, 0, 0.4) !important;
    }

    /*Cor do pai do menu ativo*/
    .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu > li.active > a, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu > li.active.open > a, .page-sidebar .page-sidebar-menu > li.active > a, .page-sidebar .page-sidebar-menu > li.active.open > a {
        background: <?php echo $stiloAdmin->getCorMenuHover();?> !important;
    }

    /*Cor do corco da barra do menu*/
    .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover, .page-sidebar, .page-content-wrapper {
        background-color: <?php echo $stiloAdmin->getCorBackgroundMenuLateral();?> !important;
    }

    /* Cor da área do sub-menu*/
    .page-sidebar .page-sidebar-menu .sub-menu, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu .sub-menu {
        background: <?php echo $stiloAdmin->getCorDropDown();?> !important;
        margin: 1px 0 !important;
    }

    /* Cor da barra de rolagem*/
    ::-webkit-scrollbar {
        background: <?php echo '#001c2e';//$stiloAdmin->getCorMenuHover();?> !important;
        width: 8px;
        height: 8px;
    }

    ::-webkit-scrollbar-thumb {
        background: <?php echo '#1989EA';//$stiloAdmin->getCorMenuAtivo();?> !important;
        border-radius: 100px;
    }

    /*Border bottom da pesquisa */
    .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .sidebar-search .input-group, .page-sidebar .sidebar-search .input-group {
        border-bottom: 1px solid <?php echo $stiloAdmin->getCorMenuAtivo();?> !important;
    }
</style>



