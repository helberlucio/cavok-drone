<?php
/**
 * Created by PhpStorm.
 * User: Gabriel PHP
 * Date: 25/05/2017
 * Time: 20:25
 * @var $admin_page_to_open string
 * @var $data array
 * @var $moduloRouteLI string
 */

$pagina = $admin_page_to_open;
if (isset($moduloRouteLIToOpen)) {
    $data['moduloRouteLI'] = $moduloRouteLIToOpen;
}
unset($data['admin_page_to_open']);

?>
<div class="col-md-12 aberturaDeTela" style="padding: 0; padding-left: 220px;">
    <div class="col-md-12" style="padding: 0;">
        <?php //echo \Modulos\AdminAutoDesenvolve\BreadCrumb\Models\Repositorios\AdminAutoDesenvolveBreadCrumbRP::getIndexPage(); ?>
    </div>
    <div class="col-md-12" style="min-height:100%;padding: 3px 3px 40px 3px;">
        <?php $this->load->AdminView($pagina, $data); ?>
    </div>
</div>

