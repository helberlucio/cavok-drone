<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 06/11/2016
 * Time: 12:19
 */
use Modulos\AdminAutoDesenvolve\Gadgets\AdminAutoDesenvolveGadgets;

$gadgets = (new AdminAutoDesenvolveGadgets())->getGadgetsFiles();
foreach ($gadgets as $gadget) {
    echo $gadget->getGadget();
}