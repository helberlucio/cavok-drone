<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 06/11/2016
 * Time: 12:57
 * @var $images
 */
use Aplicacao\Url;

\Aplicacao\Ferramentas\GetAssets::JS('jquery');
?>
<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="pt-br">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>ADMINISTRAÇÃO | SISTEMA AUTO DESENVOLVE</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="Login Page to Auto Desenvolve system" name="description"/>
    <meta content="Auto Desenvolve" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<body>
<style>
    body {
        padding: 0;
        margin: 0;
    }

    .background-page {
        transition: 0.5s linear !important;
        background-color: #212121 !important;
        /*background-image: url('<?php echo Url::Base('src/Assets/Images/Default/Admin/login/bg.jpg');?>') !important;*/
        background-position: center center !important;
        background-size: cover !important;
    }

    .login-content-area {
        width: 100%;
        height: 100vh;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .login-info-box {
        height: 489px;
        width: 468px;
    }

    .background-login-area {
        background-image: url('<?php echo Url::Base('src/Assets/Images/Default/Admin/login/mascara_autodesenvolve.png');?>') !important;
        background-position: center center !important;
        background-size: 100% 100% !important;
        margin-left:107px;
    }

    .input-login input {
        border: 1px solid #CCCCCC;
        height: 32px;
        background-color: transparent;
        width: 250px;
        margin-bottom: 15px;
        color: #FFF;
        font-family: 'Open sans', sans-serif;
        font-weight: 400;
        padding-left: 30px;
        max-width: 282px;
        background-size: auto;
        background-repeat: no-repeat;
        background-position: 8px center;

    }

    .login-info-user-pass {
        text-align: center;
        justify-content: center;
        align-items: center;
        width: 363px;
        margin-top: 53px;
    }

    .logo-login {
        width: 100%;
        text-align: center;
        margin-bottom: 32px;
    }

    .input-login {
        width: 100%;
        text-align: center;
    }

    .logo-login img {
        height: 78px;
    }

    .login-info-button {
        width: 427px;
        text-align: right;
        margin-top: 49px;
    }

    .login-button {
        transition: all 0.4s linear;
        width: 130px;
        height: 130px;
        border-radius: 1000px;
        background-color: transparent;
        color: #FFF;
        font-family: 'Open sans', sans-serif;
        border: 2px solid #FFF;
        font-size: 14px;
        cursor: pointer;
    }

    .login-button.button-disable:hover {
        transition: all 0.4s linear;
        background-color: #D14841;
        border-color: #B8312F;
    }

    .button-disable-independent, .button-disable-independent:hover {
        transition: all 0.4s linear !important;
        background-color: #D14841 !important;
        border-color: #B8312F !important;
    }

    .login-button.button-enable {
        transition: all 0.4s linear;
        border-color: #00A885;
    }

    .login-button.button-enable:hover {
        transition: all 0.4s linear;
        background-color: #1ABC9C;
        border-color: #00A885;
    }

    .login-forgot {
        text-align: right;
        width: 305px;
        color: #FFF;
        font-weight: 300;
        font-family: 'Open sans', sans-serif;
        font-size: 12px;
        margin-top: -7px;
    }

    .login-alerts {
        position: absolute;
        width: 250px;
        margin-top: 10px;
        margin-left: 58px;
    }

    .login-error-alert {
        background: #D14841 !important;
        border-bottom: 4px solid #B8312F !important;
        background-image: url('<?php echo Url::Base('src/Assets/Images/Default/icons/flat/alert-danger.png');?>') !important;
        background-repeat: no-repeat !important;
        background-position: 12px center !important;
        background-size: 15px !important;
        padding: 4px 0 4px 40px;
        text-align: left;
        font-family: 'Open sans', sans-serif;
        color: #FFF;
        font-size: 13px;
        font-weight: 500;
        border-radius: 3px;
    }

    .login-success-alert {
        background: #00A885 !important;
        border-bottom: 4px solid #008A6F !important;
        background-image: url('<?php echo Url::Base('src/Assets/Images/Default/icons/flat/alert-success.png');?>') !important;
        background-repeat: no-repeat !important;
        background-position: 12px center !important;
        background-size: 15px !important;
        padding: 4px 0 4px 40px;
        text-align: left;
        font-family: 'Open sans', sans-serif;
        color: #FFF;
        font-size: 13px;
        font-weight: 500;
        border-radius: 3px;

    }

    button:focus, button:active, button:hover, button:after {
        outline: none !important;
    }

    .copyright-area {
        position: absolute;
        bottom: 0;
        font-size: 12px;
        color: #FFF;
        font-family: "Open sans", sans-serif;
        height: 50px;
        width: 100%;
        text-align: center;
        justify-content: center;
        align-items: center;
        display: flex;
        background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.71) 73%, rgba(0, 0, 0, 0.85) 100%);
    }

    .copyright-descricoes {
        display: flex;
        justify-content: center;
        align-items: center;
    }

</style>
<div class="background-page login-content-area">
    <div class="login-info-box background-login-area">
        <form method="post" id="form-login">
            <div class="login-info-user-pass">
                <div class="logo-login">
                    <img src="<?php echo Url::Base('src/Assets/Images/Default/Admin/login/logo.png'); ?>"
                         style="max-width: 100%;">
                </div>
                <div class="input-login">
                    <input type="text" name="user" placeholder="Login"
                           style="background-image: url(<?php echo Url::Base('src/Assets/Images/Default/Admin/login/icons/user.png'); ?>);">
                    <input type="password" name="password" placeholder="Senha"
                           style="background-image: url(<?php echo Url::Base('src/Assets/Images/Default/Admin/login/icons/password.png'); ?>);"
                    >
                </div>
                <div class="login-forgot">
                    Esqueci minha senha
                </div>
                <div class="login-alerts">

                </div>
            </div>
            <div class="login-info-button">
                <button class="login-button button-disable">
                    Acessar
                </button>
            </div>
        </form>
    </div>
    <div class="copyright-area">
        <div class="copyright-descricoes">
            Copyright © <?php echo date('Y'); ?> Auto Desenvolve
        </div>
    </div>
</div>


<script>
    function enableDisableButton() {
        var userArea = $('.input-login input[name="user"]').val();
        var passwordArea = $('.input-login input[name="password"]').val();
        if (!(passwordArea != '' && userArea != '')) {
            $('.login-button').removeClass('button-enable');
            $('.login-button').addClass('button-disable');
        } else {
            $('.login-button').addClass('button-enable');
            $('.login-button').removeClass('button-disable');
        }
    }
    function verificarAcoesDeLogin() {
        setInterval(function () {
            enableDisableButton();
        }, 500);
    }

    function submitForm() {
        $('#form-login').submit(function () {
            var serial = $(this).serialize();
            $.post('<?php echo Url::Base('Admin/Login/autenticar');?>', serial, function (data) {
                var alerta = '';//'<div class="login-success-alert">' + data.mensagem + '</div>';
                if (data.level < 1) {
                    $('.login-button').html('Logado');
                    setTimeout(function () {
                        window.location = "<?php echo Url::Admin('');?>";
                    }, 1000);
                } else {
                    $('.login-button').removeClass('button-enable');
                    $('.login-button').addClass('button-disable-independent');
                    $('.login-button').html(data.mensagem);
                    setTimeout(function () {
                        $('.login-button').removeClass('button-disable-independent');
                        $('.login-button').html('Tentar Novamente');
                    }, 3500);
                    alerta = '';//'<div class="login-error-alert">' + data.mensagem + '</div>';
                }
                $('.login-alerts').empty().append(alerta);
            }, 'json');
            return false;
        });
    }

    function limpar() {
        $('input').val(' ');
        setTimeout(function () {
            $('input').val('');
        }, 300);
    }

    var timeToChangeBackground = 0;
    var timeToWait = 5000;
    function getBackgrounds() {
        $.each(<?php echo $images;?>, function ($key, $val) {
            changeBackground($val);
        });
        setTimeout(function(){
                getBackgrounds();
        }, 10000);
    }

    function changeBackground($imagem) {
        setTimeout(function () {
            console.log($imagem);
            $('.background-page').css({'background-image':' url('+$imagem+')'});
        }, timeToChangeBackground);
        timeToChangeBackground = (timeToChangeBackground + timeToWait);
    }

    $(function () {
        verificarAcoesDeLogin();
        submitForm();
        limpar();
        getBackgrounds();
    });
</script>
</body>
</html>
