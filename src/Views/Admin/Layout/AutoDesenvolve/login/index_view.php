<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 06/11/2016
 * Time: 12:57
 * @var $images
 */

use Aplicacao\Ferramentas\GetAssets;
use Aplicacao\Url;

\Aplicacao\Ferramentas\GetAssets::JS('jquery', null, null, false);
?>
<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="pt-br">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Agile - Admin</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="Login Page to Auto Desenvolve system" name="description"/>
    <meta content="Auto Desenvolve" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <script type="text/javascript" src="<?= GetAssets::Plugins('jquery.min.js') ?>"></script>
    <script type="text/javascript" src="<?= GetAssets::Plugins('bootstrap.bundle.min.js') ?>"></script>
    <?php GetAssets::CSS('login'); ?>
    <link rel="shortcut icon" href="<?= Url::Gerenciador('favicon.png') ?>"/>
</head>
<body class="text-center">
<form class="form-signin bg-white rounded shadow border border-primary" id="form-login" method="post">
    <img class="mb-5 img-fluid" src="<?php echo Url::Gerenciador('logo.png'); ?>">
    <label for="input-user" class="sr-only">Usuário</label>
    <input type="text" id="input-user" name="user" class="form-control" placeholder="Usuário" required autofocus>
    <label for="input-password" class="sr-only">Senha</label>
    <input type="password" id="input-password" name="password" class="form-control" placeholder="Senha" required>
    <button class="btn btn-lg btn-primary btn-block login-button" type="submit">Entrar</button>
</form>
<script>
    function enableDisableButton() {
        var userArea = $('.input-login input[name="user"]').val();
        var passwordArea = $('.input-login input[name="password"]').val();
        if (!(passwordArea != '' && userArea != '')) {
            $('.login-button').removeClass('button-enable');
            $('.login-button').addClass('button-disable');
        } else {
            $('.login-button').addClass('button-enable');
            $('.login-button').removeClass('button-disable');
        }
    }

    function verificarAcoesDeLogin() {
        setInterval(function () {
            enableDisableButton();
        }, 500);
    }

    function submitForm() {
        $('#form-login').submit(function () {
            var serial = $(this).serialize();
            $.post('<?php echo Url::Base('Admin/Login/autenticar');?>', serial, function (data) {
                if (data.level < 1) {
                    $('.login-button').html('Logado');
                    setTimeout(function () {
                        window.location = "<?php echo Url::Admin('');?>";
                    }, 1000);
                } else {
                    $('.login-button').html(data.mensagem);
                    setTimeout(function () {
                        $('.login-button').html('Tentar Novamente');
                    }, 3500);
                }
            }, 'json');
            return false;
        });
    }

    function limpar() {
        $('input').val(' ');
        setTimeout(function () {
            $('input').val('');
        }, 300);
    }

    $(function () {
        verificarAcoesDeLogin();
        submitForm();
        limpar();
    });
</script>
</body>
</html>
