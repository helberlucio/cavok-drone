<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 19/02/2017
 * Time: 23:15
 * @var $institucional \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\InstitucionalPadrao
 * @var $equipe \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\NossaEquipe[]
 */
\Aplicacao\Ferramentas\GetAssets::CSS('ServicosInovattaHomeCarousel', 'Servicos', 'InovattaHomeCarousel');
\Aplicacao\Ferramentas\GetAssets::CSS('plugins/lightbox/lightbox.min');
\Aplicacao\Ferramentas\GetAssets::JS('plugins/lightbox/lightbox.min');
\Aplicacao\Ferramentas\GetAssets::CSS('SobreInovatta', 'Sobre', 'Inovatta');
echo \Modulos\BreadCrumb\Inovatta\Models\Repositorios\BreadCrumbInovattaRP::getIndexPage(\Aplicacao\Ferramentas\GetAssets::IMAGE('bg.jpg', 'Sobre', 'Inovatta'), $institucional->getTitulo(), true, false);
?>
<div class="container-fluid SobreInovatta-fluid">
    <div class="row">
        <div class="col-md-12 SobreInovatta-chamada-pagina">
            <h1>CONFORTO E SOFISTICAÇÃO</h1>
            <p>Ambiente acolhedor pensado exclusivamente no bem-estar dos nossos pacientes.</p>
        </div>
    </div>
</div>
<?php echo \Modulos\BreadCrumb\Inovatta\Models\Repositorios\BreadCrumbInovattaRP::getIndexPage(\Aplicacao\Ferramentas\GetAssets::IMAGE('bg.jpg', 'Sobre', 'Inovatta'), $institucional->getTitulo(), false, true);;?>

<div class="container-fluid" style="margin-top:40px;">
    <div class="row">
        <div class="container">
            <div class="col-md-8" style="padding: 0">
                <div class="col-md-12 SobreInovatta-titulo">
                    <?php echo $institucional->getTitulo();?>
                </div>
                <div class="col-md-12 SobreInovatta-descricao">
                    <?php echo $institucional->getDescricao();?>
                </div>
            </div>
            <div class="col-md-4">
                <img style="max-width: 100%;" src="<?php echo \Aplicacao\Url::Base($institucional->getImagem());?>">
            </div>
        </div>
        <div class="col-md-12 nossaequipe-titulo">
            NOSSA EQUIPE ESTÁ PREPARADA PARA ATENDER VOCÊ!
        </div>
    </div>
</div>
<div class="container-fluid nossaequipe-fluid">
    <div class="row">
        <div class="container">
            <?php
            foreach($equipe as $colaborador) {
                ?>
                <div class="col-md-4 nossaequipe-box">
                    <div class="col-md-12 nossaequipe-imagem">
                        <img src="<?php echo \Aplicacao\Url::Base($colaborador->getImagem()); ?>">
                    </div>
                    <div class="nossaequipe-informacoes col-md-12">
                        Dr.<?php echo ' '.$colaborador->getNome().' - CRO '.$colaborador->getCro();?><br>
                        <?php echo $colaborador->getAreaAtuacao();?>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<?php
echo \Modulos\Newsletter\Ortodente\Models\Repositorios\NewsletterOrtodenteRP::getIndexPage();
echo \Modulos\Mapa\Fullbasic\Models\Repositorios\MapaFullbasicRP::getIndexPage();
?>
<link rel="stylesheet"
      href="<?php echo \Aplicacao\Url::Base('src/Assets/js/plugins/owlcarousel/dist/assets/owl.carousel.css'); ?>">
<script src="<?php echo \Aplicacao\Url::Base('src/Assets/js/plugins/owlcarousel/dist/owl.carousel.js'); ?>"></script>
<script>
    function startCarouselowlcarouselServicosInovattaHomeCarousel() {
        $('.nossaEstruturaCarousel').owlCarousel({
            loop: true,
            margin: 10,
            nav: false,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 4
                },
                1000: {
                    items: 4
                }
            }
        });
        $('.nossaEstruturaCarousel-arrow-left').click(function () {
            $('.nossaEstruturaCarousel').trigger('prev.owl.carousel');
        });
        $('.nossaEstruturaCarousel-arrow-right').click(function () {
            $('.nossaEstruturaCarousel').trigger('next.owl.carousel');
        });
    }

    $(function () {
        startCarouselowlcarouselServicosInovattaHomeCarousel();
    });


</script>

