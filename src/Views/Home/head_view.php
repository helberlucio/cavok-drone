<?php
/**
 * @var $configuracaoGeral WebsiteGeralEntidade
 * @var $page              SysPagesEntidade
 */

use Aplicacao\Ferramentas\GetAssets;
use Aplicacao\Ferramentas\Identificadores;
use Aplicacao\Imagem;
use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysPagesEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\WebsiteGeralEntidade;

if (isset($page) && $page instanceof SysPagesEntidade) {
    $pagina_titulo = $page->getTitulo();
    $pagina_descricao = $page->getDescricao();
    $pagina_palavras_chaves = $page->getPalavrasChaves();
    $pagina_image = $page->getBanner();
}

$pageTitle = $configuracaoGeral->getEmpresa();
$pageTitle .= input_get('re_pagina') ? (" | " . input_get('re_pagina')) : (isset($pagina_titulo) && $pagina_titulo ? " | {$pagina_titulo}" : '');
$pageDescription = input_get('re_descricao') ? input_get('re_descricao') : 'Contato: ' . $configuracaoGeral->getContatoTelefone() . ' - ' . (isset($pagina_descricao) && $pagina_descricao ? $pagina_descricao : $configuracaoGeral->getSobre());
$pageImage = isset($pagina_image) && $pagina_image ? Url::Base() . Imagem::resize($pagina_image, 476, 250, '#212121', 'jpg') : Url::Base() . $configuracaoGeral->getLogo();

$pageKeyWords = array_map('trim', explode(',', $configuracaoGeral->getPalavrasChaves()));
$pageKeyWordsExtra = array_map('trim', explode(',', input_get('re_plalavraChave') ? input_get('re_plalavraChave') : (isset($pagina_palavras_chaves) ? $pagina_palavras_chaves : '')));
$pageKeyWords = implode(',', array_filter(array_merge($pageKeyWords, $pageKeyWordsExtra)));

?>
<title><?= $pageTitle ?></title>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="content-language" content="pt-br">
<meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1">
<meta name="author" content="Nicolas Nascimento">
<meta name="robots" CONTENT="INDEX, FOLLOW">
<meta name="language" content="pt-br"/>
<meta name="geo.region" content="BR"/>
<meta name="copyright" CONTENT="Ligados Digital">
<meta name="keywords" content="<?= $pageKeyWords ?>">
<meta name="description" content="<?= $pageDescription ?>">
<link rel="canonical" content="<?php echo Url::GetUrlRealPorUrlApelido(Url::Current()); ?>">
<meta property="og:site_name" content="<?php echo $configuracaoGeral->getEmpresa(); ?>">
<meta property="og:locale" content="pt_BR">
<meta property="og:image" content="<?= $pageImage ?>">
<meta property="og:image:type" content="jpg">
<meta property="og:image:width" content="476">
<meta property="og:image:height" content="250">
<meta property="og:locale" content="pt_BR"/>
<meta property="og:type" content="article"/>
<meta property="og:title" content="<?= $pageTitle ?>"/>
<meta property="og:description" content="<?= $pageDescription ?>"/>
<meta property="og:url" content="<?= Url::Current(); ?>"/>
<meta property="article:section" content="<?= isset($ogsection) && $ogsection ? $ogsection : 'Post'; ?>"/>
<meta property="article:tag" content="<?= $pageKeyWords; ?>">
<meta property="article:published_time"
      content="<?= isset($publishedTime) ? $publishedTime : Identificadores::ultimaAtualizacaoGeral() ?>">
<meta property="article:modified_time"
      content="<?= isset($publishedModifyedTime) ? $publishedModifyedTime : Identificadores::ultimaAtualizacaoGeral() ?>"/>
<meta name="twitter:description" content="<?= $pageDescription ?>"/>
<meta name="twitter:title" content="<?= $pageTitle ?>"/>
<meta name="twitter:site" content="<?= Url::Base() ?>"/>
<meta name="twitter:image" content="<?= $pageImage ?>"/>
<meta name="twitter:creator" content="@<?= $configuracaoGeral->getEmpresa(); ?>"/>
<meta name="revisit-after" content="7 days">
<meta name="ICBM"
      content="<?= $configuracaoGeral->getLatitudeMapa() . ',' . $configuracaoGeral->getLongitudeMapa() ?>"/>
<meta http-equiv="last-modified" content="<?= Identificadores::ultimaAtualizacaoGeral() ?>"/>
<link rel="apple-touch-icon" href="<?= Url::Base($configuracaoGeral->getFavicon()) ?>"/>
<link rel="shortcut icon" type="image/png" href="<?= Url::Base($configuracaoGeral->getFavicon()) ?>"/>
<link rel="manifest" href="<?= Url::Base('manifest.json') ?>">
<!-- Dependencias -->
<script type="text/javascript" src="<?= GetAssets::Plugins('jquery.min.js') ?>"></script>
<script type="text/javascript" src="<?= GetAssets::Plugins('bootstrap.bundle.min.js') ?>"></script>
<?php
GetAssets::CSS('main');
?>

<link rel="stylesheet" type="text/css"
      href="<?= GetAssets::Plugins('OwlCarousel2/dist/assets/owl.carousel.min.css') ?>"/>
<link rel="stylesheet" type="text/css"
      href="<?= GetAssets::Plugins('OwlCarousel2/dist/assets/owl.theme.default.min.css') ?>"/>
<script type="text/javascript" src="<?= GetAssets::Plugins('OwlCarousel2/dist/owl.carousel.min.js') ?>"></script>

<script type="text/javascript" src="<?= GetAssets::Plugins('fancybox-master/dist/jquery.fancybox.min.js') ?>"></script>
<link rel="stylesheet" type="text/css"
      href="<?= GetAssets::Plugins('fancybox-master/dist/jquery.fancybox.min.css') ?>"/>
<script src="<?= GetAssets::Plugins('sweetalert2/dist/sweetalert2.all.min.js') ?>"></script>
<script src="<?= GetAssets::Plugins('jQuery-Mask-Plugin-master/dist/jquery.mask.min.js') ?>"></script>
<script type="text/javascript" src="<?= GetAssets::Plugins('select2-develop/dist/js/select2.full.min.js') ?>"></script>
<script type="text/javascript" src="<?= GetAssets::Plugins('select2-develop/dist/js/i18n/pt-BR.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?= GetAssets::Plugins('select2-develop/dist/css/select2.min.css') ?>"/>
<link rel="stylesheet" type="text/css"
      href="<?= GetAssets::Plugins('select2-bootstrap4-theme-master/dist/select2-bootstrap4.css') ?>"/>
<script>
    $(function () {
        const SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 0 0000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function (val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };

        $('.mask-telefone').mask(SPMaskBehavior, spOptions);
        $('.mask-cpf').mask('000.000.000-00', {reverse: true});
        $('.mask-number').mask('0#');
        $('.mask-cep').mask('00.000-000', {reverse: true});
        $('.mask-money').mask("#.##0,00", {reverse: true});
    });
</script>
