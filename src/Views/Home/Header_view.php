<?php
/**
 * @var array $configuracaoGeral
 */

use Modulos\Site\Header\Models\Repositorios\SiteHeaderRP;

echo SiteHeaderRP::getIndexPage('index', [
    'menu' => input_get('menu'),
    'configuracaoGeral' => $configuracaoGeral,
]);
