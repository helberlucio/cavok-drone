<?php

use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\WebsiteGeralEntidade;

$data['configuracaoGeral'] = (new WebsiteGeralEntidade())->getRow($this->db);

?>
<!DOCTYPE html>
<html lang="pt-br" class="h-100">
<head><?php $this->load->view('Home/head', $data); ?></head>
<body class="d-flex flex-column h-100" id="pagina-<?= input_get('menu') ? input_get('menu') : 'home' ?>">
<?php
include_once getcwd() . '/src/Assets/js/phphelper.php';
if (isset($pagina)) {
    unset($data['pagina']);
    if (isset($moduloRouteLI)) {
        $data['moduloRouteLI'] = $moduloRouteLI;
    }
    if (!isset($desabilitarHeader) || $desabilitarHeader = false) {
        $this->load->view('Home/Header', ['configuracaoGeral' => $data['configuracaoGeral']]);
    }
    $this->load->view($pagina, $data);
    if (!isset($desabilitarFooter) || $desabilitarFooter = false) {
        $this->load->view('Home/Footer', ['configuracaoGeral' => $data['configuracaoGeral']]);
    }
}
if (!isset($pagina)) {
    echo 'Nenhuma view foi inicializada';
}
?>
</body>
</html>