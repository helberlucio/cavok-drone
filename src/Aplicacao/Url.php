<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 06/11/2016
 * Time: 13:09
 */

namespace Aplicacao;


use Exception;
use Modulos\Criadores\SiteMap\Models\RegrasDeNegocio\CriadoresSiteMapRN;

class Url {

    /**
     * @param null $uris
     * @return string
     */
    public static function Base($uris = null) {
        $url = LI_BASE_URL . '/' . $uris;

        return $url;
    }

    /**
     * @param null $uris
     * @return string
     */
    public static function Admin($uris = null) {
        $url = $uris ? LI_BASE_ADMIN_URL . '/' . $uris : LI_BASE_ADMIN_URL . '/';

        return $url;
    }

    /**
     * @param null $uris
     * @return string
     */
    public static function ModuloAdmin($uris = null) {
        $url = $uris ? LI_BASE_ADMIN_URL . '/Modulo/' . $uris : LI_BASE_ADMIN_URL;

        return $url;
    }

    /**
     * @description Retorna o modulo atual de acordo com a url
     * @param $url
     * @return string
     */
    public static function ModuloAtual($url) {
        $segmentos = (self::Segmento(0) == 'Admin' && self::Segmento(1) == 'Modulo') ||
        (self::Segmento(0) == 'admin' && self::Segmento(1) == 'Modulo')
            ? 3
            :
            (self::Segmento(0) ? 2 : false);
        if (!$segmentos) {
            die('<code>Modulo-nao-detectado</code>');
        }
        $urlModulo = '';
        for ($i = 0; $i <= 3; $i++) {
            $segmento = self::Segmento($i);
            if ($i <= 1 && $segmento == 'Modulo') {
                $segmento = 'Modulo';
            }
            $urlModulo .= '/' . $segmento;
        }

        return Url::Base(substr($urlModulo, 1) . '/' . $url);
    }

    /**
     * @description Retorna o modulo atual de acordo com a url
     * @param $url
     * @return string
     */
    public static function getModuloAtual($url) {
        return self::ModuloAtual($url);
    }

    /**
     * @return string
     */
    public static function Current() {
        $protocolo = LI_PROTOCOLO;
        $pageURL = $protocolo . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];

        return $pageURL;
    }

    /**
     * @param $int
     * @return array|null|string
     */
    public static function Segmento($int = false) {
        $segmentos = explode('/', $_SERVER['QUERY_STRING']);
        if ($int === false) {
            return $segmentos;
        }
        if (isset($segmentos[ $int ])) {
            $segmento = $segmentos[ $int ];
        } else {
            $segmento = null;
        }

        return $segmento;
    }

    /**
     * @param $url
     */
    public static function Redirecionar($url) {
        header('Location:' . $url);
    }

    /**
     * @param null $string
     * @return mixed
     */
    public static function Title($string = null) {
        $string = trim($string);
        $string = strip_tags($string);
        $procurar = ['À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í',
            'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á',
            'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô',
            'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', '}', ']', '°', '+', '(', ')', '*', '#', '@', '!', '#', '$', '%', '¨', ':', '’', '‘', ',', '.', ':', 'º', '/', '|', '?'];
        $substituir = ['a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i',
            'i', 'i', 'd', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 's', 'a', 'a',
            'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o',
            'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', '', '', '', '', '', '', '', '', '', '', ' ', ' ', ' ', ' ', '', '', '', ' ', '', '', '', '-', '', ''];
        $replace = str_replace($procurar, $substituir, $string);
        $replace = str_replace(' ', '-', $replace);
        $replace = str_replace(['-----', '----', '---', '--'], '-', $replace);

        return strtolower($replace);
    }

    private static function updateSiteMap($url_apelido, $url_real, $chave_comparacao) {
        $dir = getcwd();
        $file = $dir . '/url_apelido.json';
        $json = [];
        if (file_exists($file)) {
            $json = json_decode(file_get_contents($file), true);
        }
        $json[ $url_apelido ] = [
            'url_apelido'      => $url_apelido,
            'url_real'         => $url_real,
            'chave_comparacao' => $chave_comparacao,
        ];
        file_put_contents($file, json_encode($json));
        try {
            (new CriadoresSiteMapRN())->gerar(false);
        } catch (Exception $ex) {

        }
    }

    /**
     * @param       $url
     * @param       $newUrl
     * @param array $prefixosBreadCrumb
     * @return mixed
     */
    public static function Apelido($url, $newUrl, $prefixosBreadCrumb = []) {
        $newUrl = (isset($prefixosBreadCrumb) && !empty($prefixosBreadCrumb) ? implode('/', $prefixosBreadCrumb) . '/' : '') . self::Title(strip_tags($newUrl));
        $dirRotas = 'src/Aplicacao/Rotas/';
        if (!is_dir($dirRotas)) {
            mkdir($dirRotas, 0777, true);
        }
        $urlKey = sha1($newUrl);
        $routFile = $dirRotas . $urlKey . '.html';
        if (!is_file($routFile)) {
            file_put_contents($routFile, $url, LOCK_EX);
            @self::updateSiteMap($newUrl, $url, $urlKey);

            return $newUrl;
        }
        $realUrlByFile = file_get_contents($routFile);
        $realUrlByPushedEncrypted = sha1($url);
        $realUrlByFileEncrypted = sha1($realUrlByFile);
        if ($realUrlByPushedEncrypted != $realUrlByFileEncrypted) {
            //It becouse its needs to be updated
            file_put_contents($routFile, $url, LOCK_EX);
            @self::updateSiteMap($newUrl, $url, $urlKey);
        }

        return $newUrl;
    }

    public static function ModuloApelido($modulo, $controlador, $apelido, $prefixosBreadCrumb = []) {
        return Url::Base(
            Url::Apelido(
                forward_static_call_array([$modulo, 'getUrlModulo'], [$controlador]),
                $apelido,
                $prefixosBreadCrumb
            )
        );
    }

    /**
     * @param bool $urlApelido
     * @return bool
     */
    public static function GetUrlRealPorUrlApelido($urlApelido = false) {
        if (!$urlApelido) {
            $urlApelido = $_SERVER['QUERY_STRING'];
        }
        $routFile = 'src/Aplicacao/Rotas/' . sha1($urlApelido) . '.html';
        if (!is_file($routFile)) {
            return $urlApelido;
        }

        return file_get_contents($routFile);
    }

    /**
     * @return string
     */
    public static function VoltarAdmin() {
        $url = \Modulos\AdminAutoDesenvolve\BreadCrumb\Models\Repositorios\AdminAutoDesenvolveBreadCrumbRP::getLastBreadCrumb();

        return isset($url['url']) ? $url['url'] : self::Admin();
    }

    public static function getGetters($excludeGetter = []) {
        $urlGetters = '?';
        $excludeGetters = [];
        if (input_get()) {
            if (!empty($excludeGetter)) {
                foreach ($excludeGetter as $exclude) {
                    $excludeGetters[ $exclude ] = '';
                }
            }
            foreach (input_get() as $key => $value) {
                if (!isset($excludeGetters[ $key ]) && $key != 'url') {
                    $urlGetters .= $key . '=' . $value . '&';
                }
            }
        }
        $urlGetters = substr($urlGetters, 0, -1);

        return $urlGetters;
    }

    public static function Gerenciador($uri = '') {
        return self::Base(LI_DIR_GERENCIADOR . $uri);
    }

}