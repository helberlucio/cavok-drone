<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 29/08/2016
 * Time: 23:20
 */

namespace Aplicacao;


class GenericRepository
{

    /**
     * @param $entityName
     * @param $entityObj
     * @return array
     */
    public static function createSaveData($entityName, $entityObj)
    {
        $data = array();
        $class = new \ReflectionClass(new $entityName());

        $methods = $class->getProperties();
        foreach ($methods as $method) {
            $methodPasCalCase = self::pascalCaseNamingConvention($method->getName(), 'get');
            if (!method_exists($entityObj, $methodPasCalCase)) {
                continue;
            }
            if (is_null($entityObj->{$methodPasCalCase}())) {
                continue;
            }
            if (is_object($entityObj->{$methodPasCalCase}())) {
                $objetoOfClass = $entityObj->{$methodPasCalCase}()->getId();
            } else {
                $objetoOfClass = $entityObj->{$methodPasCalCase}();
            }
            $data[$method->getName()] = $objetoOfClass;
        }
        return $data;
    }

    /**
     * @param $nameToConversion
     * @param string $setOrGet
     * @return string
     */
    private static function pascalCaseNamingConvention($nameToConversion, $setOrGet = 'set')
    {
        $newConverssion = count(explode('_', $nameToConversion)) > 1 ? explode('_', $nameToConversion) : $nameToConversion;
        if (!is_array($newConverssion)) {
            return $setOrGet . ucfirst($newConverssion);
        }
        $newConverssionArrayToString = '';
        foreach ($newConverssion as $value) {
            $newConverssionArrayToString .= ucfirst($value);
        }
        return $setOrGet . $newConverssionArrayToString;
    }

}