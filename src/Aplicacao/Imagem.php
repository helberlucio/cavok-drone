<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 08/11/2016
 * Time: 10:39
 */

namespace Aplicacao;

use Gregwar\Image\Image;

class Imagem
{
    /**
     * @param $image
     * @param null $x
     * @param null $y
     * @param null $background
     * @param string $type
     * @return string
     */
    public static function resize($image, $x = null, $y = null, $background = null, $type = 'jpg')
    {
        $convertWebP = false;
        if (!(strlen($image) > 0)) {
            return '';
        }
        if (!$x || !$y) {
            list($x, $y, $type, $attr) = getimagesize($image);
            switch ($type) {
                case IMAGETYPE_JPEG:
                    $type = 'jpg';
                    break;
                case IMAGETYPE_PNG:
                    $type = 'png';
                    break;
                case IMAGETYPE_GIF:
                    $type = 'gif';
                    break;
                case IMAGETYPE_BMP:
                    $type = 'bmp';
                    break;
                default:
                    $type = 'jpg';
            }
        }
        $imagemName = substr(sha1($image . $x . $y . $background . $type), 0, 8) . '-' . $x . 'X' . $y . '-' . str_replace('#', '', $background);
        $newImage = LI_IMAGES_CACHES . $imagemName . '.' . $type;
        $newWebp = LI_IMAGES_CACHES . $imagemName . '.webp';

        if (file_exists($newWebp)) {
            return $newWebp;
        }

        if (!file_exists($image)) {
            self::converterImagemParaWebp($image, $type, $newWebp);
            $image = self::createImage($image, LI_IMAGES_CACHES . $imagemName . '-saved.' . $type);
            $convertWebP = false;
        }

        $imagem = Image::open($image);
        $imagem->setCacheDir('src/Assets/Images/Caches');
        $imagem->setCacheDirMode(1);
        $imagem->setFallback(null);
        if ($background) {
            $imagem = $imagem
                ->resize($x, $y, $background, true, true, false);
        } else {
            $imagem = $imagem
                ->resize($x, $y, 'transparent', true, true, true);
        }
        $newImage = $imagem->save($newImage);
        if ($convertWebP) {
            $newImage = self::converterImagemParaWebp($newImage, $type, $newWebp);
        }
        return $newImage;
    }

    public static function createImage($arquivo, $newImageName)
    {
        $imageContent = file_get_contents($arquivo);
        if (!(strlen($imageContent) > 100)) {
            return $imageContent;
        }
        file_put_contents($newImageName, $imageContent, LOCK_EX);
        return $newImageName;
    }

    public static function converterImagemParaWebp($arquivo, $type, $newWebp)
    {
    	return $arquivo;
        ini_set('max_execution_time', 60);
        $dadosConverterApi['img'] = $arquivo;
        $dadosConverterApi['type'] = $type;
        $response = curl('http://159.89.191.135/webp_convert.php', 'POST', $dadosConverterApi);
        if ($response['http_code'] == 200) {
            $imgContent = base64_decode($response['response_original']['img_content']);
            file_put_contents($newWebp, $imgContent, LOCK_EX);
        }else{
            return $arquivo;
        }
        if (is_file($newWebp)) {
            return $newWebp;
        }
        return $arquivo;
    }
}