<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 29/08/2016
 * Time: 22:12
 */

namespace Aplicacao;

use Aplicacao\Ferramentas\Identificadores;
use ReflectionClass;
use ReflectionProperty;

class Conversao {

    /**
     * @param $texto
     * @return string
     */
    public static function underscoreToCamelCase($texto) {
        return trim(str_replace(' ', '', ucwords(str_replace('_', ' ', $texto))));
    }

    /**
     * @param $texto
     * @return string
     */
    public static function underscoreToPascalCase($texto) {
        return trim(lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $texto)))));
    }

    /**
     * Converte um texto de pascalCase para under_score
     * @param string $texto
     * @return string
     */
    public static function pascalCaseToUnderscore($texto) {
        $texto = self::removeAcentos($texto);

        return strtolower(
            preg_replace('/([a-z])([A-Z])/', '$1_$2', $texto)
        );
    }

    /**
     * @param $str
     * @return mixed|string
     */
    static function stringPura($str) {
        $str = url_title($str);
        $str = preg_replace('/[^a-z0-9]/i', '_', $str);
        $str = str_replace('-', '_', $str);

        return $str;
    }

    static function separaPalavraPorMaiuscula($str) {
        $str = preg_replace('/(?<!^)([A-Z])/', '-\\1', $str);
        $str = explode('-', $str);
        $str = array_map(function ($str) {
            return ucwords($str);
        }, $str);
        $str = implode(' ', $str);

        return $str;
    }

    /**
     * Remove os acentos de um texto
     *
     * @param string $texto
     * @param bool   $espacoParaUnderscore
     * @return string
     */
    public static function removeAcentos($texto, $espacoParaUnderscore = false) {
        $texto = preg_replace('![áàãâä]+!u', 'a', $texto);
        $texto = preg_replace('![éèêë]+!u', 'e', $texto);
        $texto = preg_replace('![íìîï]+!u', 'i', $texto);
        $texto = preg_replace('![óòõôö]+!u', 'o', $texto);
        $texto = preg_replace('![úùûü]+!u', 'u', $texto);
        $texto = preg_replace('![ç]+!u', 'c', $texto);
        $texto = preg_replace('![ñ]+!u', 'n', $texto);

        if ($espacoParaUnderscore) {
            $texto = str_replace(' ', '_', $texto);
        }

        return $texto;
    }

    /**
     * @param        $campo -- Nome do campo
     * @param string $prefixo
     *
     * @return string
     */
    public static function campoParaGet($campo, $prefixo = '') {
        $funcao = empty($prefixo) ? $campo : preg_replace('/' . $prefixo . '/', '', $campo, 1);

        return 'get' . self::underscoreToCamelCase($funcao);
    }

    /**
     * @param        $campo -- Nome do campo
     * @param string $prefixo
     *
     * @return string
     */
    public static function campoParaSet($campo, $prefixo = '') {
        $funcao = empty($prefixo) ? $campo : preg_replace('/' . $prefixo . '/', '', $campo, 1);

        return 'set' . self::underscoreToCamelCase($funcao);
    }

    /**
     * @param      $objeto
     * @param      $classe
     * @param null $prefixo
     * @return mixed
     */
    public static function objetoParaClasse($objeto, $classe, $prefixo = null) {
        $instancia = new $classe();
        if (null !== $objeto && is_object($objeto) && $objeto) {
            foreach ($objeto as $key => $value) {
                $metodo = self::campoParaSet($key, $prefixo);
                if (method_exists($instancia, $metodo)) {
                    call_user_func([$instancia, $metodo], $value);
                }
            }
        }

        return $instancia;
    }

    /**
     * @param $objeto
     * @param $entidade
     *
     * @return mixed
     */
    public static function objetoParaEntidade($objeto, &$entidade, $teste = false) {
        $classe = get_class($entidade);
        $prefixo = defined("{$classe}::PREFIXO") ? $entidade::PREFIXO : '';
        if ($teste) {
            var_dump($objeto);
        }
        if (null !== $objeto) {
            foreach ($objeto as $key => $value) {
                $metodo = self::campoParaSet($key, $prefixo);
                if (method_exists($entidade, $metodo)) {
                    call_user_func([$entidade, $metodo], $value);
                }
            }
        }

        return $entidade;
    }

    public static function entidadeParaArray($entidade) {
        $className = '';
        $stdArray = $entidade;
        if (is_object($entidade)) {
            $className = get_class($entidade);
            $stdArray = (array)$entidade;
        }
        foreach ($stdArray as $key => $valor) {
            $newKey = trim(str_replace($className, '', $key));
            $stdArray[ $newKey ] = $valor;
            if (is_object($valor) || is_array($valor)) {
                $newValue = self::entidadeParaArray($valor);
                $stdArray[ $newKey ] = $newValue;
            }
            if ($newKey != $key) {
                unset($stdArray[ $key ]);
            }
        }

        return $stdArray;
    }

    /**
     * @param $valor
     * @return float
     */
    public static function formatarDinheiro($valor) {
        return number_format($valor, 2, ',', '.');
    }

    public static function formatarDataBR($data, $horario = false) {
        return date($horario ? 'd/m/Y H:i:s' : 'd/m/Y', strtotime($data));
    }

    /**
     * Transporta os dados de uma Entidade para outra.
     * Exemplo: \Modulos\Compras\Entidades\LojaOrder => \Modulos\MeuModulo\Entidades\LojaOrder
     * Obs: Os objeto já devem estar instanciados.
     * @param $atual
     * @param $destino
     * @return mixed
     */
    public static function entidadeParaEntidade($atual, &$destino) {
        if (is_object($atual) && is_object($destino)) {
            $reflection = new ReflectionClass($atual);
            $props = $reflection->getProperties(ReflectionProperty::IS_PRIVATE);
            foreach ($props as $prop) {
                $metodoGet = self::campoParaGet($prop->getName(), $atual::PREFIXO);
                $metodoSet = self::campoParaSet($prop->getName(), $destino::PREFIXO);
                if (method_exists($atual, $metodoGet) && method_exists($destino, $metodoSet)) {
                    $destino->{$metodoSet}($atual->{$metodoGet}());
                }
            }

            return $destino;
        }

        return null;
    }

    /**
     * @param string $tipo -- Tipo do dado vindo do banco
     *
     * @return string
     */
    public static function tipoDoBancoParaPHP($tipo) {
        $int = ['int', 'tinyint', 'smallint', 'mediumint', 'bigint'];
        $double = ['float', 'double', 'decimal'];
        $string = [
            'date',
            'datetime',
            'timestamp',
            'time',
            'char',
            'varchar',
            'blob',
            'text',
            'tinyblob',
            'tinytext',
            'mediumblob',
            'mediumtext',
            'longblob',
            'longtext',
            'enum',
        ];

        if (in_array($tipo, $int)) {
            return 'int';
        }
        if (in_array($tipo, $double)) {
            return 'double';
        }
        if (in_array($tipo, $string)) {
            return 'string';
        }

        return 'null';
    }

    /**
     * Retorna o tamanho maximo de um campo do banco de dados
     * utilizando o result de `information_schema`.`columns`.
     * Requerimentos minimos: character_maximum_length, data_type, column_type
     *
     * @param $infoTabela
     *
     * @return int
     */
    public static function tamanhoMaximoBanco($infoTabela) {
        if (empty($infoTabela->character_maximum_length)) {
            $tamanho = str_replace([')', '(', 'unsigned', ' ', $infoTabela->data_type], '', $infoTabela->column_type);
            if (strpos($tamanho, ',') !== false) {
                $tamanho = current(explode(',', $tamanho));
            }

            return (int)$tamanho;
        } else {
            return (int)$infoTabela->character_maximum_length;
        }
    }

    /**
     *
     * @param array $array
     * @return object
     */
    public static function arrayParaObjeto($array) {
        return json_decode(json_encode($array));
    }

    /**
     * @param        $array
     * @param string $aspas
     * @return string
     */
    static function arrayParaArrayString($array, $aspas = "'") {
        if (!is_array($array) && !is_object($array)) {
            return 'array()';
        }
        $str = 'array(';
        foreach ($array as $key => $valor) {
            if (Identificadores::eJson($valor)) {
                $valor = json_decode($valor, true);
            }
            if (is_array($valor) || is_object($valor)) {
                $str .= "{$aspas}{$key}{$aspas} => array(";
                foreach ($valor as $key2 => $valor2) {
                    $str .= "{$aspas}{$key2}{$aspas}=>{$aspas}" . $valor2 . "{$aspas},";
                }
                $str = rtrim($str, ',');
                $str .= '),';
            } else {
                if (strpos('_' . $valor, 'array(')) {
                    $str .= "{$aspas}{$key}{$aspas}=>" . $valor . ",";
                    continue;
                } else {
                    if ($valor || is_numeric($valor) || is_bool($valor)) {
                        $str .= "{$aspas}{$key}{$aspas}=>{$aspas}" . $valor . "{$aspas},";
                    }
                }
            }
        }
        $str = rtrim($str, ',');
        $str .= ')';

        return $str;
    }

    /**
     * Varre todos os campos de um array ou objeto recursivamente
     * alterando o valores para os parametros
     * @param array|object $haystack Dados
     * @param mixed        $search   Texto ou Array a procurar
     * @param string       $replace  Texto ou Array a substituir
     * @param bool         $strict   Se passado irá conferir se o valor realmente é uma String
     */
    public static function str_replace_recursive(
        &$haystack, $search, $replace, $strict = false
    ) {
        foreach ($haystack as $key => &$value) {
            if (is_array($value) || is_object($value)) {
                self::str_replace_recursive($value, $search, $replace, $strict);
            } elseif ($strict && is_string($value) && !is_numeric($value)) {
                $value = str_replace($search, $replace, $value);
            } elseif (!$strict) {
                $value = str_replace($search, $replace, $value);
            }
        }
    }

    /**
     * @param $d
     * @return array
     */
    public static function objectToArray($d) {
        if (is_object($d)) {
            $d = get_object_vars($d);
        }
        if (is_array($d)) {
            return array_map(function ($n) {
                return static::objectToArray($n);
            }, $d);
        } else {
            return $d;
        }
    }

    /**
     * Transforma uma cor hexa (Ex: #000000) para rgba (Ex: rgba(0,0,0))
     * @param  $color
     * @param  $opacity
     * @return string
     */
    static function hexaParaRgba($color, $opacity = false) {
        $default = 'rgb(0,0,0)';
        //Return default if no color provided
        if (empty($color))
            return $default;
        //Sanitize $color if "#" is provided
        if ($color[0] == '#') {
            $color = substr($color, 1);
        }
        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
            $hex = [$color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5]];
        } elseif (strlen($color) == 3) {
            $hex = [$color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2]];
        } else {
            return $default;
        }
        //Convert hexadec to rgb
        $rgb = array_map('hexdec', $hex);
        //Check if opacity is set(rgba or rgb)
        if ($opacity) {
            if (abs($opacity) > 1)
                $opacity = 1.0;
            $output = 'rgba(' . implode(",", $rgb) . ',' . $opacity . ')';
        } else {
            $output = 'rgb(' . implode(",", $rgb) . ')';
        }

        //Return rgb(a) color string
        return $output;
    }

    public static function somenteNumeros($texto) {
        return preg_replace('/\D/', '', $texto);
    }

}