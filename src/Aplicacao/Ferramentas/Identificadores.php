<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 09/11/2016
 * Time: 21:18
 */

namespace Aplicacao\Ferramentas;


use Aplicacao\Url;
use Core\Modelos\ModelagemDb;

class Identificadores
{


    public static function ultimaAtualizacaoGeral()
    {
        if(isset($_SESSION['ultima_atualizacao_geral'])){
            return $_SESSION['ultima_atualizacao_geral'];
        }
        $fileUltimaAtualizacaoGeral = str_replace('\\','/', realpath('./')).'/ultima_atualizacao_geral.html';
        if(!is_file($fileUltimaAtualizacaoGeral)){
            $dataAtualizacao = $_SESSION['ultima_atualizacao_geral'] = date('Y-m-d H:i:s');
            file_put_contents($fileUltimaAtualizacaoGeral, '#i'.$dataAtualizacao.'#f', LOCK_EX);
            return $dataAtualizacao;
        }
        return $_SESSION['ultima_atualizacao_geral'] = substr(file_get_contents($fileUltimaAtualizacaoGeral), 2,-2);
    }

    public static function isMobileDevice()
    {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }

    public static function numeros($str)
    {
        preg_match_all('/\d+/', $str, $matches);
        return $matches[0];
    }

    public static function eModulo()
    {
        if (Url::Segmento(0) == 'Modulo' ||
            (Url::Segmento(0) == 'Admin' && Url::Segmento(1) == 'Modulo') ||
            (Url::Segmento(0) == 'admin' && Url::Segmento(1) == 'Modulo')
        ) {
            return true;
        }
        return false;
    }

    /**
     * @param $string
     * @return bool
     */
    public static function eJson($string)
    {
        return is_string($string) && is_array(json_decode(stripslashes($string),
            true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }

    public static function eEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            return true;
        }
        return false;
    }

    /*
     * @return bool
     */
    public static function eCPF($cpf)
    {
        // Verifica se um número foi informado
        if (empty($cpf)) {
            return false;
        }

        // Elimina possivel mascara
        $cpf = @ereg_replace('[^0-9]', '', $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        // Verifica se o numero de digitos informados é igual a 11
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se nenhuma das sequências invalidas abaixo
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999'
        ) {
            return false;
            // Calcula os digitos verificadores para verificar se o
            // CPF é válido
        } else {
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * @param $valor
     * @param array $array
     * @param null $coluna
     * @return bool|mixed
     */
    static function procuraValorEmArray($valor, array $array, $coluna = null)
    {
        if (!$coluna) {
            $key = array_search($valor, $array);
        } else {
            $key = array_search($valor, array_column($array, $coluna));
        }
        return isset($array[$key]) ? $array[$key] : false;
    }

    /**
     * @param $tabela
     * @return bool
     */
    static function tabelaExiste($tabela)
    {
        $db = new ModelagemDb();

        $table = $db->select('`TABLE_NAME` as tabela')
            ->where('`table_schema`', LI_CONNECT_DATABASE)
            ->where('`TABLE_NAME`', $tabela)
            ->get('`information_schema`.`TABLES`')
            ->row();
        return isset($table->tabela);
    }

    /**
     * @param string $tableName
     * @param string $columnName
     * @return bool
     */
    static function tipoColuna($tableName = '', $columnName = '')
    {
        if (empty($tableName) || empty($columnName)) {
            return false;
        }

        $db = new ModelagemDb();
        $columnObj = $db->select('column_name, data_type')
            ->where('table_schema', LI_CONNECT_DATABASE)
            ->where('TABLE_NAME', $tableName)
            ->where('column_name', $columnName)
            ->get('information_schema.columns')
            ->row(false);
        return isset($columnObj->data_type) ? $columnObj->data_type : false;
    }

    /**
     * @param string $tableName
     * @return bool
     */
    static function chavePrimaria($tableName = '')
    {
        if (empty($tableName)) {
            return false;
        }
        $db = new ModelagemDb();
        $indexTableObj = current($db->query('SHOW INDEX FROM ' . $tableName));
        return isset($indexTableObj->Column_name) ? $indexTableObj->Column_name : false;
    }

    public static function getYoutubeVideoIdFromUrl($url)
    {
        if (($pos = strpos($url, '?v=')) !== FALSE) {
            return substr($url, ($pos + 3), 11);
        }
        return false;
    }

    /**
     * @param string $texto
     * @param array $palavras
     * @return array
     */
    public static function getPalavrasSemelhantes($texto = '', $palavras = array())
    {
        if (!is_array($palavras)) {
            $tracos = explode(' ', $palavras);
            $palavras = array();
            foreach ($tracos as $traco) {
                $palavras[] = $traco;
            }
        }
        // Remove line breaks and spaces from stopwords
        $palavras = array_map(function ($x) {
            return trim(strtolower($x));
        }, $palavras);

        // Replace all non-word chars with comma
        $pattern = '/[0-9\W]/';
        $texto = preg_replace($pattern, ',', $texto);

        // Create an array from $text
        $text_array = explode(",", $texto);

        // remove whitespace and lowercase words in $text
        $text_array = array_map(function ($x) {
            return trim(strtolower($x));
        }, $text_array);
        $findedKeys = array();
        foreach ($text_array as $term) {
            if (!in_array($term, $palavras)) {
                continue;
            }
            if (in_array($term, $findedKeys)) {
                continue;
            }
            $findedKeys[] = $term;
        };
        return $findedKeys;
    }

    /**
     * @param $sNeedle
     * @param $aHaystack
     * @return bool
     */
    public static function in_array_like($sNeedle, $aHaystack)
    {
        foreach ($aHaystack as $sKey) {
            if (stripos(strtolower($sKey), strtolower($sNeedle)) !== false) {
                return true;
            }
        }
        return false;
    }


}