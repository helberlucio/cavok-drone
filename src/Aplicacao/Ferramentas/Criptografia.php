<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 11/04/2017
 * Time: 14:56
 */

namespace Aplicacao\Ferramentas;


class Criptografia
{


    /**
     * @var int
     */
    public $chave = 3;

    /**
     * @var string
     */
    public $add_text = '';

    public function __construct()
    {
        $this->add_text = base64_encode(md5(sha1('Auto-Desenvolve')));
        $this->chave = 7;
    }

    public function encriptografar($valor)
    {
        $valor .= $this->add_text;
        $s = strlen($valor) + 1;
        $nw = "";
        $n = $this->chave;
        $nindex = 0;
        for ($x = 1; $x < $s; $x++) {
            $m = $x * $n;
            if ($m > $s) {
                $nindex = $m % $s;
            } else if ($m < $s) {
                $nindex = $m;
            }
            if ($m % $s == 0) {
                $nindex = $x;
            }
            $nw = $nw . $valor[$nindex - 1];
        }
        return $nw;
    }

    public function decriptografar($valor)
    {
        $s = strlen($valor) + 1;
        $nw = "";
        $n = $this->chave;
        for ($y = 1; $y < $s; $y++) {
            $m = $y * $n;
            if ($m % $s == 1) {
                $n = $y;
                break;
            }
        }
        $nindex = 0;
        for ($x = 1; $x < $s; $x++) {
            $m = $x * $n;
            if ($m > $s) {
                $nindex = $m % $s;
            } else if ($m < $s) {
                $nindex = $m;
            }
            if ($m % $s == 0) {
                $nindex = $x;
            }
            $nw = $nw . $valor[$nindex - 1];
        }
        $t = strlen($nw) - strlen($this->add_text);
        return substr($nw, 0, $t);
    }
}