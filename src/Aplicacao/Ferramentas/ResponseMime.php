<?php
/**
 * Created by PhpStorm.
 * User: Gabriel PHP
 * Date: 09/09/2017
 * Time: 23:55
 */

namespace Aplicacao\Ferramentas;


class ResponseMime
{
    const HTML = 'text/html';
    const JSON = 'application/json';
}