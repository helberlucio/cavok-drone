<?php
/**
 * Created by PhpStorm.
 * User: Gabriel PHP
 * Date: 09/09/2017
 * Time: 23:47
 */

namespace Aplicacao\Ferramentas;


class Response extends ResponseCode
{

    /**
     * @param $json
     * @param int $httpCode
     */
    public static function json($json, $httpCode = 200)
    {
        $content = json_encode($json,JSON_PRETTY_PRINT);
        header('content-type: ' . ResponseMime::JSON . '; charset: utf-8');
        http_response_code($httpCode);
        ob_start((GZIP ? 'ob_gzhandler' : null));
        echo $content;
        ob_end_flush();
        $content = null;
        exit();
    }

    public static function printJson($value)
    {
        echo '<pre>';
        echo json_encode($value, JSON_PRETTY_PRINT);
        echo '<pre>';
        exit();
    }

}