<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 09/11/2016
 * Time: 21:14
 */

namespace Aplicacao\Ferramentas;


use Aplicacao\Url;

class GetAssets
{

    /**
     * @param string $arquivoJs
     * @param null $grupoModulo
     * @param null $nomeModulo
     */
    public static function JS($arquivoJs = '', $grupoModulo = null, $nomeModulo = null, $activeDefer = true)
    {
        $url = 'src/Assets/js/' . $arquivoJs . '.js';
        if ($grupoModulo && $nomeModulo) {
            $url = 'src/Modulos/' . $grupoModulo . '/' . $nomeModulo . '/Assets/js/' . $arquivoJs . '.js';
        }
        $defer = '';
        if($activeDefer){
            $defer = 'defer="defer"';
        }
        echo '<script '.$defer.' type="text/javascript" src="' . Url::Base($url) . '"></script>';
    }

    /**
     * @param string $arquivoCss
     * @param null $grupoModulo
     * @param null $nomeModulo
     */
    public static function CSS($arquivoCss = '', $grupoModulo = null, $nomeModulo = null)
    {
        $url = 'src/Assets/css/' . $arquivoCss . '.css';
        if ($grupoModulo && $nomeModulo) {
            $url = 'src/Modulos/' . $grupoModulo . '/' . $nomeModulo . '/Assets/css/' . $arquivoCss . '.css';
        }
        echo '<link rel="stylesheet" type="text/css" href="' . Url::Base($url) . '" />';
    }

    /**
     * @param string $file
     * @param null   $grupoModulo
     * @param null   $nomeModulo
     * @return string
     */
    public static function Plugins($file = '', $grupoModulo = null, $nomeModulo = null)
    {
        $url = 'src/Assets/Plugins/' . $file;
        if ($grupoModulo && $nomeModulo) {
            $url = 'src/Modulos/' . $grupoModulo . '/' . $nomeModulo . '/Assets/Plugins/' . $file;
        }
        return Url::Base($url);
    }

    /**
     * @param string $file
     */
    public static function Utils($file = '')
    {
        $url = 'src/Assets/util/' . $file;
        echo Url::Base($url);
    }


    /**
     * @param string $arquivo
     * @param string $extensao
     */
    public static function LayoutAdmin($arquivo = '', $extensao = 'css', $activeDefer = true)
    {
        $file = LI_ASSETS_ADMIN . $arquivo . '.' . $extensao;
        if (!file_exists($file)) {
            $file = 'Arquivo-nao-encontrado-' . $file;
        }
        if ($extensao == 'css') {
            echo '<link rel="stylesheet" type="text/css" href="' . Url::Base($file) . '"></script>';
            return;
        }
        $defer = '';
        if($activeDefer){
            $defer = 'defer="defer"';
        }
        if ($extensao == 'js') {
            echo '<script '.$defer.' type="text/javascript" src="' . Url::Base($file) . '"></script>';
            return;
        }

        echo 'extensao-nao-encontrada-' . $file;
        return;
    }

    public static function IMAGE($imagem = '', $grupoModulo = null, $nomeModulo = null)
    {
        $url = 'src/Assets/Images/Default/' . $imagem;
        if ($grupoModulo && $nomeModulo) {
            $url = 'src/Modulos/' . $grupoModulo . '/' . $nomeModulo . '/Assets/Images/' . $imagem;
        }
        if (!is_file($url)) {
            return 'src/Assets/Images/Default/imagem-nao-encontrada.jpg?realfile=' . $url;
        }
        return $url;
    }


}