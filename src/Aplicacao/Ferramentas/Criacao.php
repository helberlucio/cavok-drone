<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 10/11/2016
 * Time: 00:21
 */

namespace Aplicacao\Ferramentas;
class Criacao
{

    /**
     * @param string $texto
     * @param int $tabs
     * @param int $enters
     * @return string
     */
    public static function linha($texto = '', $tabs = 0, $enters = 1)
    {
        if (!defined('TAB')) {
            define('TAB', '    ');
        }
        $linha = '';
        for ($i = 0; $i < $tabs; $i++) {
            $linha .= TAB;
        }
        $linha .= $texto;
        for ($i = 0; $i < $enters; $i++) {
            $linha .= PHP_EOL;
        }
        return $linha;
    }

    public static function chave($valorParaEncript)
    {
        $valor = sha1(base64_encode($valorParaEncript).rand(0,9));
        return substr($valor,0,4).'-'.substr($valor, 5,9).'-'.substr($valor, 10,13);
    }

}