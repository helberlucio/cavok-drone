<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 06/11/2016
 * Time: 13:36
 */

namespace Aplicacao;


use Symfony\Component\Finder\SplFileInfo;

class Diretorios
{

    /**
     * @return string
     */
    public static function Controlador()
    {
        return LI_DIR_CONTROLLERS;
    }

    /**
     * @return string
     */
    public static function Model()
    {
        return LI_DIR_MODELS;
    }

    /**
     * @return string
     */
    public static function View()
    {
        return LI_DIR_VIEWS;
    }

    public static function Modulos()
    {
        return LI_DIR_MODULOS;
    }

    /**
     * @return string
     */
    public static function ImagensPadrao()
    {
        return LI_IMAGES_DEFAULT;
    }

    /**
     * @return string
     */
    public static function ImagensPadraoLayout()
    {
        return LI_IMAGES_DEFAULT_LAYOUT;
    }

    public static function ImagensPadraoLayoutAdmin()
    {
        return LI_IMAGES_DEFAULT_LAYOUT_ADMIN;
    }

    /**
     * @return string
     */
    public static function ImagensGerenciador()
    {
        return LI_DIR_GERENCIADOR;
    }

    /**
     * @param string $basePath
     * @param string $iteratorReturn
     * @return array
     */
    public static function Escaniar($basePath = './', $iteratorReturn = 'getRealPath', $apenasDir = false)
    {
        if($apenasDir){
            var_dump(is_dir($basePath));
            var_dump($basePath);
            exit;
        }
        $iterator = new \DirectoryIterator($basePath);

        if ($iterator) {
            foreach ($iterator as $item) {
                if (!$item->isDot()) {
                    if ($item->isFile() && !$apenasDir) {
                        yield array('type' => 'file',
                            'fileDir' => $item->{$iteratorReturn}(),
                            'baseName' => $item->getBasename(),
                            'extension' => $item->getExtension(),
                            'diretorio' => $item->current()->getPath(),
                            'folder_name' =>$item->getPathInfo()->getFileName()
                        );
                    }
                    if ($item->isDir()) {
                        yield array(
                            'type' => 'dir',
                            'fileDir' => $item->getRealPath(),
                            'baseName' => $item->getBasename(),
                            'subFiles' => self::Escaniar($item->getRealPath()),
                            'info' => $item
                        );
                    }
                }
            }
        }
        yield;
    }


    public static function DiretoriosComNiveisDeProfundidade($basePath = 'src/Modulos/', $niveis = 2)
    {
        $diretorios = array();
        $nivel = $niveis;
        $pastasModulos = new \DirectoryIterator($basePath);
        $subpastas = array();
        foreach ($pastasModulos as $pasta) {
            if ($pasta->isDir() && !$pasta->isDot()) {
                if ($nivel > 1) {
                    $nivel = $nivel - 1;
                    $subpastas = self::DiretoriosComNiveisDeProfundidade($pasta->getPath() . '/' . $pasta->getFilename(), $nivel);
                }
                $nivel = $niveis;
                $diretorios[] = array('filename' => $pasta->getFilename(), 'subpastas' => $subpastas);
            }
        }
        return $diretorios;
    }
}