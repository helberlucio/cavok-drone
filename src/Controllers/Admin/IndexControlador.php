<?php

/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 05/11/2016
 * Time: 10:42
 */

namespace Controllers\Admin;

use Core\Controlador\Controlador;
use Modulos\Administravel\Geral\Entidades\Produtos;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysCfopEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysCnaeEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysNcmEntidade;

class IndexControlador extends Controlador
{
    /**
     * @description instanciado assim que executado
     */
    public function index()
    {
        $data['pagina'] = 'Home/corpo';
        $this->load->AdminView('Home/index', $data);
    }

}