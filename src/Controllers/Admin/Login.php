<?php

/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 06/11/2016
 * Time: 13:02
 */

namespace Controllers\Admin;

use Aplicacao\Diretorios;
use Aplicacao\Ferramentas\Ajax;
use Aplicacao\Ferramentas\Response;
use Aplicacao\Ferramentas\ResponseCode;
use Aplicacao\Url;
use Core\Modelos\Autentificacoes;

class Login extends \Core\Controlador\Controlador
{

    public function index()
    {
        if (sessao('admin_id') && sessao('admin')) {
            Url::Redirecionar(Url::Admin());
        }
        $imagesDir = Diretorios::ImagensGerenciador() . 'Login/Backgrounds';
        if (!is_dir($imagesDir)) {
            mkdir($imagesDir, 0777, true);
        }

        $arquivos = Diretorios::Escaniar($imagesDir);
        $imagens = array();
        foreach ($arquivos as $imagem) {
            if (isset($imagem['baseName']) && isset($imagem['type']) && $imagem['type'] == 'file') {
                $imagens[] = Url::Base($imagesDir . '/' . $imagem['baseName']);
            }
        }

        $data['imagens'] = $imagens;
        $data['images'] = json_encode($imagens);
        $data['pagina'] = 'login/index';
        $data['blank'] = '';
        $data['title'] = 'Login área administrativa';

        $this->load->AdminView($data['pagina'], $data, false, false, false);

    }

    public function logout()
    {
        if (sessao('admin')) {
            unset($_SESSION['admin']);
            if (sessao('admin_id')) {
                unset($_SESSION['admin_id']);
            }
        }
        if(isset($_SESSION['admin_escritorio_atual'])){
            unset($_SESSION['admin_escritorio_atual']);
        }
        $this->index();
    }

    public function autenticar()
    {
        $autenticarfAdministrador = (new Autentificacoes())->autenticarAdministrador(input('user'), sha1(input('password')));
        Ajax::retorno($autenticarfAdministrador);
    }
}