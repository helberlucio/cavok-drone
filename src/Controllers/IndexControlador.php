<?php

/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 05/11/2016
 * Time: 10:42
 */

namespace Controllers;

use Core\Controlador\Controlador;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysPagesEntidade;

class IndexControlador extends Controlador {

    private $clumnName = 'testing';

    /**
     * @description instanciado assim que executado
     */
    public function index() {
        $data['page'] = $configs = (new SysPagesEntidade())->getRow($this->db->where('pagina', 'home'));
        $data['pagina'] = 'Home/corpo';

        echo $this->load->view('Home/index', $data, true);
    }

}