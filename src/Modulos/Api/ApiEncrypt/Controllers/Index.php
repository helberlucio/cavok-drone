<?php
namespace Modulos\Api\ApiEncrypt\Controllers;

use Aplicacao\Ferramentas\Ajax;
use Core\Controlador\Controlador;
use Modulos\Api\ApiEncrypt\Models\Repositorios\ApiApiEncryptRP;

class Index extends Controlador{

    public function Sha1($value = '')
    {
        if (input_get('value')) {
            $value = input_get('value');
        }
        Ajax::retorno(array('level'=>1, 'callback'=>sha1($value), 'mensagem'=>'Sh1 Gerado com suceso!'));
    }
}