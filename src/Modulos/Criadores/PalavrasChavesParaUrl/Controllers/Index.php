<?php

namespace Modulos\Criadores\PalavrasChavesParaUrl\Controllers;

use Api\ApiExeption;
use Modulos\Criadores\PalavrasChavesParaUrl\Models\RegrasDeNegocio\CriadoresPalavrasChavesParaUrlRN;

class Index
{

    public $admin = false;

    public function index()
    {
        try{
            (new CriadoresPalavrasChavesParaUrlRN())->criar();
        }catch(ApiExeption $e){
            show_array($e);
        }
    }
}