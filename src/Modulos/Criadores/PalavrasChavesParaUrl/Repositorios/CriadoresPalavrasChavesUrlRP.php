<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 14/06/2018
 * Time: 14:59
 */

namespace Modulos\Criadores\PalavrasChavesParaUrl\Repositorios;


use Modulos\Criadores\PalavrasChavesParaUrl\Models\RegrasDeNegocio\CriadoresPalavrasChavesParaUrlRN;

class CriadoresPalavrasChavesUrlRP
{
    public static function getLinksAsTags()
    {
        return (new CriadoresPalavrasChavesParaUrlRN())->getLinksAsTags();
    }
}