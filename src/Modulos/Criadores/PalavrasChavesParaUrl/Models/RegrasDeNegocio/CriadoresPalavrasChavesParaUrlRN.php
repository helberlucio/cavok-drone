<?php

namespace Modulos\Criadores\PalavrasChavesParaUrl\Models\RegrasDeNegocio;

use Aplicacao\Url;
use ModuloHelp\PadraoCriacao\ModelRegraDeNegocio\Abstracts\ModelRegraDeNegocioPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\PalavraschavesEmpaginasEntidade;

class CriadoresPalavrasChavesParaUrlRN extends ModelRegraDeNegocioPadraoAbsHelp
{

    public function criar()
    {
        $keyWordsToUrl = (new PalavraschavesEmpaginasEntidade())->getRows($this->db);
        foreach ($keyWordsToUrl as $keytou) {
            echo '<a target="_blank" href="' .
                Url::Base() .
                Url::Apelido(
                    $keytou->getUrl() .
                    '?re_pagina=' . urlencode($keytou->getTituloChave()) .
                    '&re_descricao=' . urlencode($keytou->getDescricao()) .
                    '&re_plalavraChave=' . urlencode($keytou->getPalavrasChaves()),
                    Url::Title($keytou->getTituloChave())
                ) . '">' .
                $keytou->getTituloChave() .
                '</a>' .
                '<br>';
        }
    }

    /**
     * @return array
     */
    public function getLinksAsTags()
    {
        $tags = [];
        $keyWordsToUrl = (new PalavraschavesEmpaginasEntidade())->getRows($this->db);
        $keyWordsToUrl[] = (new PalavraschavesEmpaginasEntidade())->setUrl('Modulo/Site/Planos/Index')->setTituloChave('planos e promocoes')->setPalavrasChaves(0)->setDescricao(0)->setPalavrasChaves(0);
        foreach ($keyWordsToUrl as $keytou) {
            $url = $keytou->getUrl() .
                '?re_pagina=' . urlencode($keytou->getTituloChave()) .
                '&re_descricao=' . urlencode($keytou->getDescricao()) .
                '&re_plalavraChave=' . urlencode($keytou->getPalavrasChaves());
            $tags[] = '<a href="' .
                Url::Base() .
                Url::Apelido(
                    $url,
                    Url::Title($keytou->getTituloChave())
                ) . '">' .
                '<div class="tagsFooter" data-id="54">' .
                $keytou->getTituloChave() .
                '</div>' .
                '</a>';
        }
        return $tags;
    }
}
