angularModule.controller('formularioArea', function ($scope, $http) {
    $scope.configuracao = {};
    $scope.configuracao.icone = '';
    $scope.configuracao.modulo = '';
    $scope.configuracao.tabela = '';
    $scope.configuracao.menu = 1;
    $scope.configuracao.layout = 1;
    $scope.configuracao.tipo = 2;
    $scope.submitForm = function () {
        $('.load-gear').fadeIn();
        $http({
            method: 'POST',
            url: base_url('Admin/Modulo/Criadores/ModulosAdministraveis/Index/SalvarModuloAdministravel'),
            data: $scope.configuracao
        }).then(function successCallback(response) {
            $('.load-gear').fadeOut();
            if (response.data.level == 1) {
                toastr.error(response.data.mensagem)
            }
            if (response.data.level == 0) {
                toastr.success(response.data.mensagem);
                location = base_url('Admin/Modulo/Criadores/ModulosAdministraveis/Index/Campos/?tabela=' + $scope.configuracao.tabela)
            } else {
                toastr.error('Ocorreu algum erro! Nada encontrado.')
            }
            toastr.options = {
                "closeButton": !0,
                "debug": !1,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
        }, function errorCallback(response) {
            $('.load-gear').fadeOut()
        })
    }
}).controller('Campos', function ($scope, $http, $timeout, $compile) {
    $scope.configuracao = [];
    $scope.modulos = [];
    $scope.camposFk = [];
    $scope.erros = [];
    $scope.sucesso = [];
    $timeout(function () {
        $('.panel-body').slideDown()
    }, 1000);
    $timeout(function () {
        $scope.init();
        $scope.buscarModulos()
    });
    $scope.incrementOrdem = 1;
    $scope.adicionarCampo = function () {
        $('.load-gear').fadeIn().fadeOut();
        $scope.incrementOrdem = ($scope.incrementOrdem + 1);
        $scope.configuracao.push({
            campo: 'nome_do_campo',
            descricao: 'Nome do Campo',
            tipo: 4,
            caracters: 255,
            obrigatorio: !0,
            plugin: 0,
            listar: !0,
            tabelafk: '',
            campoModulofkid: '',
            campoModulofktext: '',
            camposModuloFk: [],
            opcoesDoCampo: {},
            ordem: $scope.incrementOrdem
        })
    };
    $scope.init = function () {
        $('.load-gear').fadeIn();
        $http({
            method: 'POST',
            url: base_url('Admin/Modulo/Criadores/ModulosAdministraveis/Index/getCmsFieldsFromModuloAJAX'),
            data: {table: tabela}
        }).then(function successCallback(response) {
            $('.load-gear').fadeOut();
            $('.panel-campos-configuracao').slideDown();
            $scope.configuracao = response.data.cmsFields;
            $scope.sucesso = [response.data.mensagem];
            $timeout(function () {
                $scope.sucesso = []
            }, 5000);
            angular.forEach(response.data.cmsFields, function (value, key) {
                if (value.tabelafk) {
                    $scope.pegarCamposDoModulo(value, key)
                }
            })
        }, function errorCallback(response) {
            $('.load-gear').fadeOut()
        })
    };
    $scope.removerCampo = function (item) {
        $('.load-gear').fadeIn();
        $timeout(function () {
            $('.load-gear').fadeOut()
        }, 1000);
        var index = $scope.configuracao.indexOf(item);
        $scope.configuracao.splice(index, 1)
    };
    $scope.buscarModulos = function () {
        $('.load-gear').fadeIn();
        $http({
            method: 'POST',
            url: base_url('Admin/Modulo/Criadores/ModulosAdministraveis/Index/getModulos'),
            data: {getModulos: !0}
        }).then(function successCallback(response) {
            $('.load-gear').fadeOut();
            $scope.modulos = response.data
        }, function errorCallback(response) {
            $('.load-gear').fadeOut()
        })
    };
    $scope.pegarCamposDoModulo = function ($item, index) {
        $('.load-gear').fadeIn();
        $http({
            method: 'POST',
            url: base_url('Admin/Modulo/Criadores/ModulosAdministraveis/Index/getCamposByTabela'),
            data: {tabela_fk: $item.tabelafk}
        }).then(function successCallback(response) {
            $('.load-gear').fadeOut();
            $scope.configuracao[index].camposModuloFk = response.data
        }, function errorCallback(response) {
            $('.load-gear').fadeOut()
        })
    };
    $scope.salvarCampos = function () {
        $scope.configuracao.tabela = tabela;
        $('.load-gear').fadeIn();
        $http({
            method: 'POST',
            url: base_url('Admin/Modulo/Criadores/ModulosAdministraveis/Index/SalvarCampos?metodo=POST'),
            data: {config: $scope.configuracao, tabeleName: tabela}
        }).then(function successCallback(response) {
            $('.load-gear').fadeOut();
            if (response.data.level == 1) {
                $scope.erros = response.data.erros;
                $timeout(function () {
                    $scope.erros = []
                }, 5000)
            }
            if (response.data.level == 0) {
                $scope.sucesso = response.data.sucesso;
                $timeout(function () {
                    location = response.data.redirect
                }, 1500)
            }
        }, function errorCallback(response) {
            $('.load-gear').fadeOut()
        })
    };
    $scope.openConfig = function (index) {
        $('.modal').modal();
        var configuracao = $scope.configuracao[index];
        var campoTipoSelecionado = $scope.configuracao[index].tipo;
        var div = $('.modal-content .modal-body');
        $('.load-gear').fadeIn();
        $http({
            method: 'POST',
            url: base_url('Admin/Modulo/Criadores/ModulosAdministraveis/Index/getConfiguracoesCampoAJAX'),
            data: {config: configuracao, tableName: tabela, indexConfig: index}
        }).then(function successCallback(response) {
            $('.load-gear').fadeOut();
            if (response.data.level == 1) {
                div.html('Não foi encontrado suas configurações');
                $scope.erros = response.data.erros;
                $timeout(function () {
                    $scope.erros = []
                }, 5000)
            }
            if (response.data.level == 0) {
                $timeout(function () {
                    var div = $('.modal-content .modal-body');
                    div.html(response.data.html);
                    $('.modal-title').empty().append(response.data.title);
                    $compile(div.contents())($scope);
                    $scope.$apply()
                });
                $scope.sucesso.push(response.data.sucesso);
                $timeout(function () {
                    $scope.sucesso = []
                }, 3500)
            }
        }, function errorCallback(response) {
            $('.load-gear').fadeOut()
        })
    };
    $scope.adicionarOpcoesDoCampo = function ($opcoesDoCampo, $index) {
        $scope.configuracao[$index].opcoesDoCampo = $opcoesDoCampo;
        console.log($scope.configuracao)
    }
}).controller('ListagemModuloAdministravel', function ($scope, $http, $timeout) {
    $scope.deletarConteudo = function ($event, $conteudoId) {
        if (!window.confirm("Realmente deseja remover este registro?")) {
            return;
        }

        var btn = $($event.currentTarget);

        $('.load-gear').fadeIn();
        $http({
            method: 'POST',
            url: base_url('Admin/Modulo/Criadores/ModulosAdministraveis/Index/DeletarConteudo?metodo=delete&tabela=' + tabela),
            data: {conteudoId: $conteudoId}
        }).then(function successCallback(response) {
            $('.load-gear').fadeOut();
            if (response.data.level == 1) {
                toastr.error(response.data.mensagem)
a            } else {
                btn.parent().parent().fadeOut();
                toastr.success(response.data.mensagem)
            }
            toastr.options = {
                "closeButton": !0,
                "debug": !1,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
        }, function errorCallback(response) {
            $('.load-gear').fadeOut()
        })
    }
}).controller('EditorModuloAdministravel', function ($scope, $http, $timeout) {
}).controller('EditarInserirConteudo', function ($scope, $http, $timeout) {
    $scope.conteudo = {};
    $scope.config = {};
    $timeout(function () {
        $scope.conteudo = camposValoresJson;
        $scope.config.moduloInstancia = moduloInstancia;
        $scope.config.edicao = edicao;
        $scope.config.campoPk = campoPk
    });
    $scope.salvarConteudo = function () {
        $('.load-gear').fadeIn();
        $http({
            method: 'POST',
            url: base_url('Admin/Modulo/Criadores/ModulosAdministraveis/Index/SalvarConteudo?metodo=POST&tabela=' + tabela),
            data: {conteudo: $scope.conteudo, config: $scope.config}
        }).then(function successCallback(response) {
            $('.load-gear').fadeOut();
            if (response.data.level == 1) {
                toastr.error(response.data.mensagem)
            } else {
                toastr.success(response.data.mensagem)
            }
            toastr.options = {
                "closeButton": !0,
                "debug": !1,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
        }, function errorCallback(response) {
            $('.load-gear').fadeOut()
        })
    };
    $scope.atualizarValorConteudoPorInput = function ($inputTextClassName, $campoConteudo, $load, $time) {
        var timeToLoad = 4500;
        if ($time > 0) {
            timeToLoad = $time
        }
        if ($load != 'disable') {
            $('.load-gear').fadeIn()
        }
        var elementInput = document.getElementsByClassName($inputTextClassName);
        $timeout(function () {
            if ($load != 'disable') {
                $('.load-gear').fadeOut()
            }
            var valueFromInput = elementInput[0].value;
            $scope.conteudo[$campoConteudo] = valueFromInput
        }, timeToLoad)
    };
    $scope.atualizarPorInput = function ($inputTextClassName, $campoConteudo) {
        var elementInput = document.getElementsByClassName($inputTextClassName);
        console.log($inputTextClassName);
        console.log($campoConteudo);
        console.log(elementInput);
        console.log(elementInput[0].value);
        $scope[$campoConteudo] = elementInput[0].value
    };
    $scope.atualizarValorConteudo = function ($campoConteudo, $valor) {
        $('.load-gear').fadeIn().fadeOut();
        $scope.conteudo[$campoConteudo] = $valor
    }
});