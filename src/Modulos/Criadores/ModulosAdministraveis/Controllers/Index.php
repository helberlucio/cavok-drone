<?php

namespace Modulos\Criadores\ModulosAdministraveis\Controllers;

use Aplicacao\Ferramentas\Ajax;
use Aplicacao\Ferramentas\Response;
use Core\Controlador\Controlador;
use Modulos\Criadores\ModulosAdministraveis\Models\AdministrarModulo;
use Modulos\Criadores\ModulosAdministraveis\Models\CamposDoModulo;
use Modulos\Criadores\ModulosAdministraveis\Models\CamposRegraDeNegocio\Voucher;
use Modulos\Criadores\ModulosAdministraveis\Models\CriarModuloAdministravel;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsMenu;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModuloLayout;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModuloTipos;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsTipoCampos;
use Modulos\Criadores\ModulosAdministraveis\Models\OpcoesDoCampo\Init;

class Index extends Controlador {

    public function index() {
        $modulos = new CmsModulo();
        $menus = new CmsMenu();
        $tipos = new CmsModuloTipos();
        $layouts = new CmsModuloLayout();
        $data['modulos'] = $modulos->getRows($this->db->order_by('modulo', 'ASC'));
        $data['menus'] = $menus->getRows($this->db->order_by('menu', 'ASC'));
        $data['tipos'] = $tipos->getRows($this->db->order_by('titulo', 'ASC'));
        $data['layouts'] = $layouts->getRows($this->db->order_by('titulo', 'ASC'));
        $data["pagina"] = "index";
        $this->load->AdminView("Home/index", $data);
    }

    public function AdministrarModulo() {

        echo (new AdministrarModulo())->getPaginaAdministrarModulo();
    }

    public function SalvarConteudo() {
        (new AdministrarModulo())->salvarConteudoModuloAdministravel();
    }

    public function DeletarConteudo() {
        (new AdministrarModulo())->DeletarConteudoModuloAdministravel();
    }

    /**
     * @description Salvar os campos do modulo administravel
     */
    public function SalvarCampos() {
        (new CamposDoModulo())->salvarCampos();
    }

    public function getConfiguracoesCampoAJAX() {
        $opcoesDoCampos = new Init();
        $opcoesDoCampoView = $opcoesDoCampos->getOpcoesDoCampoView(input_angular_http('config'), input_angular_http('indexConfig'), input_angular_http('tableName'));
        $status = 0;
        if (!empty($opcoesDoCampos->getErros())) {
            $status = 1;
        }
        Ajax::retorno(['title' => 'Ecolha o plugin', 'html' => $opcoesDoCampoView, 'level' => $status, 'sucesso' => 'Configuracoes do campo iniciadas', 'erros' => json_encode($opcoesDoCampos->getErros())]);
    }

    /**
     * @description Campos
     */
    public function Campos() {
        $data['tipoCampos'] = (new CmsTipoCampos())->getRows($this->db);
        $data['tabela'] = input_get('tabela');
        $data['pagina'] = 'campos';
        $this->load->AdminView('Home/index', $data);
    }

    /**
     * @description Retorna os fields do modulo
     */
    public function getCmsFieldsFromModuloAJAX() {
        $table = input_angular_http('table');
        $cmsFields = new CmsField();
        $cmsModulo = $this->getModuloIdByTable($table);
        if (!$cmsModulo->getId()) {
            Ajax::retorno(['level' => 1, 'mensagem' => 'Modulo Não encontrado pela tabela: ' . $table]);
        }

        $cmsFieldsObj = $cmsFields->getRows($this->db->where('cms_modulo_id', $cmsModulo->getId())->order_by('ordem', 'asc'));
        $fieldsArray = [];
        if (!count($cmsFieldsObj)) {
            $stdObj = new \stdClass();
            $stdObj->campo = 'id';
            $stdObj->descricao = 'ID';
            $stdObj->tipo = 1;
            $stdObj->caracters = 11;
            $stdObj->obrigatorio = true;
            $stdObj->plugin = '';
            $stdObj->listar = true;
            $stdObj->tabelafk = '';
            $stdObj->campoModulofkid = '';
            $stdObj->campoModulofktext = '';
            $stdObj->camposModuloFk = [];
            $stdObj->opcoesDoCampo = [];
            $stdObj->ordem = 1;
            $fieldsArray = [$stdObj];
        }

        foreach ($cmsFieldsObj as $field) {
            $camposModuloFkChoosed = [];
            if ($field->getTableFk() != '') {
                $moduloFk = $this->db->where('tabela', $field->getTableFk())
                    ->get('cms_modulo')
                    ->row();
                if (isset($moduloFk->id)) {
                    $camposModuloFk = $this
                        ->db
                        ->select('cms_field_administravel.field')
                        ->join('cms_field as cms_field_administravel', 'cms_field_administravel.id = cms_field_fk_text.cms_field_fk_text')
                        ->where('cms_field_fk_text.cms_field', $field->getId())
                        ->get('cms_field_fk_text')
                        ->result();
                    if (is_array($camposModuloFk)) {
                        foreach ($camposModuloFk as $campoModuloFk) {
                            $camposModuloFkChoosed[] = $campoModuloFk->field;
                        }
                    }
                }
            }
            $data['campo'] = (string)$field->getField();
            $data['ordem'] = (int)$field->getOrdem();
            $data['descricao'] = (string)$field->getDescricao();
            $data['tipo'] = (int)$field->getCmsTipoCamposId();
            $data['caracters'] = (int)$field->getTamanho();
            $data['obrigatorio'] = (bool)($field->getObrigatorio() ? true : false);
            $data['plugin'] = (int)$field->getCmsPluginsId();
            $data['listar'] = (bool)($field->getListar() ? true : false);
            $data['tabelafk'] = (string)$field->getTableFk();
            $data['campoModulofkid'] = (string)$field->getFkid();
            $data['campoModulofktext'] = $camposModuloFkChoosed;
            $data['opcoesDoCampo'] = (new Init())->getOpcoesDoCampo($field);

            $fieldsArray[] = $data;
        }

        Ajax::retorno(['level' => 0, 'cmsFields' => $fieldsArray, 'mensagem' => 'Campos para o modulo ' . $cmsModulo->getModulo() . '" encontrados']);
    }

    /**
     * @param $table
     * @return CmsModulo
     */
    private function getModuloIdByTable($table) {
        $cmsModulo = new CmsModulo();
        $cmsModulo = $cmsModulo->getRow($this->db->where('tabela', $table));

        return $cmsModulo;
    }

    /**
     * @return array
     */
    public function getModulos() {
        $modulos = new CmsModulo();
        $modulosObj = $modulos->getRows($this->db->order_by('modulo', 'ASC'));

        $modulosArray = [];
        foreach ($modulosObj as $modulo) {
            $moduloAtual['id'] = $modulo->getId();
            $moduloAtual['tabela'] = $modulo->getTabela();
            $moduloAtual['value'] = $modulo->getModulo();
            $modulosArray[] = $moduloAtual;
        }
        if (input_angular_http() || input_post()) {
            Ajax::retorno($modulosArray);
        }

        return $modulosArray;
    }

    /**
     * @des
     * @param null $tabela
     * @return array
     */
    public function getCamposByTabela($tabela = null) {
        if (input_angular_http()) {
            $tabela = input_angular_http('tabela_fk');
        }
        if (input_post()) {
            $tabela = input_post('tabela_fk');
        }

        $output = [];
        $result = $this->db->select('`column_name`')
            ->where('`table_schema`', LI_CONNECT_DATABASE)
            ->where('`table_name`', $tabela)
            ->get('`information_schema`.`columns`')
            ->result();

        foreach ($result as $row) {
            $campos['value'] = $row->column_name;
            $output[] = $campos;
        }

        if (input_angular_http() || input_post()) {
            Ajax::retorno($output);
        }

        return $output;
    }

    /**
     *
     */
    public function SalvarModuloAdministravel() {
        (new CriarModuloAdministravel())->Init();
    }


    public function GenerateVoucher() {
        $voucher = new Voucher();
        $voucher->generateVoucher();
    }

    public function SaveVoucher() {
        $voucher = new Voucher();
        if ($voucher->saveVoucher()) {
            Response::json(['status' => 'ok', 'message' => 'Voucher salvo com sucesso e pronto para uso']);
        } else {
            Response::json(['status' => 'error', 'message' => 'Voucher não foi salvo, motivos desconhecidos']);
        }
    }

    public function GetVoucher() {
        $voucher = new Voucher();
        $voucher->getVoucher();
    }


}

