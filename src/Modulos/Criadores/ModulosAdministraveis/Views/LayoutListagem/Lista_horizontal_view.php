<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 03/12/2016
 * Time: 15:23
 * @var $conteudosEntidadeModuloAdministravel array
 * @var $campos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField[]
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $CampoPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $extensoes \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsExtensoes[]
 * @var $paginacao (string)
 */

use Modulos\Criadores\ModulosAdministraveis\Models\Repositorios\Campos;

Aplicacao\Ferramentas\GetAssets::CSS('ListagemHorizontal', 'Criadores', 'ModulosAdministraveis');
?>
    <div class="panel panel-default" ng-controller="ListagemModuloAdministravel" style="padding: 0;">
        <div class="panel-heading">
            <i class="fa fa-cog"></i>
            Modulo <b><?php echo $moduloInfos->getModulo(); ?></b>
            <i class="fa fa-angle-right"></i>
            Administrar
            <small><b>administração de conteúdo: Adicionar, editar, deletar</b></small>
        </div>
        <div class="panel-body">
            <div class="col-md-12" style="padding: 0;">
                <div class="portlet-title" style="padding: 0;">
                    <div class="col-md-12 actions" style="padding: 0;">
                        <div class="text-left col-md-6">
                            <md-button class="md-raised md-warn" onclick="voltarPagina();">
                                <i class="fa fa-external-link" aria-hidden="true"></i>
                                VOLTAR
                            </md-button>
                        </div>
                        <div class="text-right col-md-6">
                            <a href="<?php echo Aplicacao\Url::ModuloAdmin('Criadores/ModulosAdministraveis/Index/AdministrarModulo?tipoAdministracaoId=1&tabela=' . $moduloInfos->getTabela()); ?>">
                                <md-button class="btn-success md-raised md-primary" ng-value="Primary">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                    ADICIONAR
                                </md-button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 ModuloAdministravel-table-campos">
                <table class="table table-striped" style="width:100%;overflow: auto;">
                    <thead>
                    <tr>
                        <?php
                        foreach ($campos as $campo) {
                            if (!$campo->getListar()) {
                                continue;
                            }
                            ?>
                            <th>
                                <?php
                                echo $campo->getDescricao();
                                ?>
                            </th>
                            <?php
                        }
                        ?>
                        <th colspan="2">
                            Ações
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($conteudosEntidadeModuloAdministravel as $conteudo) {
                        ?>
                        <tr valign="center">
                            <?php
                            foreach ($campos as $campo) {
                                if (!$campo->getListar()) {
                                    continue;
                                }
                                $campoPkGetterConversion = 'get' . \Aplicacao\Conversao::underscoreToCamelCase($CampoPk->getField());
                                ?>
                                <td class="" aria-label="<?php echo $campo->getField(); ?>"
                                    data-view="<?php echo $campo->getCmsTipoCampos()->getViewName(); ?>">
                                    <md-tooltip md-direction="top">
                                        Editar Campo <?php echo $campo->getDescricao(); ?> Linha
                                        (<?php echo $conteudo->{$campoPkGetterConversion}(); ?>)
                                    </md-tooltip>
                                    <?php echo Campos::getCampoListagem($conteudo, $campo, $moduloInfos, $conteudo); ?>
                                </td>
                                <?php
                            }
                            ?>
                            <td>
                                <md-button style="max-width: 45px !important;min-width: 45px !important;"
                                           aria-label="<?php echo $campo->getField() . 'editar'; ?>"
                                           class="md-raised md-primary md-button md-ink-ripple"
                                           href="<?php echo Aplicacao\Url::ModuloAdmin('Criadores/ModulosAdministraveis/Index/AdministrarModulo?tipoAdministracaoId=1&editar=' . $conteudo->{$campoPkGetterConversion}() . '&tabela=' . $moduloInfos->getTabela()); ?>"
                                           data-trigger="hover" data-placement="top" data-content="Editar"
                                           data-original-title="" title="">
                                    <md-tooltip md-direction="top">
                                        Editar Essa Linha (<?php echo $conteudo->{$campoPkGetterConversion}(); ?>)
                                    </md-tooltip>
                                    <i class="fa fa-edit"></i>
                                </md-button>
                            </td>
                            <td>
                                <md-button style="max-width: 45px !important;min-width: 45px !important;"
                                           class="md-raised md-warn md-button md-ink-ripple"
                                           aria-label="<?php echo $campo->getField() . 'excluir'; ?>"
                                           data-toggle="modal" data-trigger="hover" data-placement="top"
                                           data-content="Remover"
                                           ng-click="deletarConteudo($event, '<?php echo $conteudo->{$campoPkGetterConversion}(); ?>');"
                                           data-original-title="Deletar" title="Deletar">
                                    <md-tooltip md-direction="top">
                                        Excluir essa linha (<?php echo $conteudo->{$campoPkGetterConversion}(); ?>)
                                    </md-tooltip>
                                    <i class="fa fa-remove"></i>
                                </md-button>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
                <?php echo $paginacao; ?>
            </div>
        </div>
    </div>
    <script>
        var tabela = '<?php echo $moduloInfos->getTabela();?>';
    </script>
<?php
Aplicacao\Ferramentas\GetAssets::JS('ModuloAdministravel', 'Criadores', 'ModulosAdministraveis');
Aplicacao\Ferramentas\GetAssets::CSS('CriadorModulosAdministraveis', 'Criadores', 'ModulosAdministraveis');
?>