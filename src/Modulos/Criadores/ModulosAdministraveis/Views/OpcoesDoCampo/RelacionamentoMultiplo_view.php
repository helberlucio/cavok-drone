<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 30/01/2017
 * Time: 14:58
 */
?>
<style>
    .md-select-menu-container {
        z-index: 999999;
    }
</style>
<div class="row">
    <div class="col-md-12" style="padding:5px;margin-bottom:15px;">
        <label>
            <b>Modulo Para relacionamento multiplo (N P/ N):</b>
        </label>
        <md-select ng-change="pegarCamposDoModulo(configuracao[<?php echo $index; ?>], <?php echo $index; ?>)"
                   ng-model="configuracao[<?php echo $index; ?>].tabelafk"
                   style="height:32px;width: 100%;margin:13px 0 0 0 !important;">
            <md-optgroup label="Modulos de Relacionamento">
                <md-option ng-value="0">Selecionar Tabela</md-option>
                <md-option ng-repeat="modulo in modulos" ng-value="modulo.tabela">{{modulo.value}}
                </md-option>
            </md-optgroup>
        </md-select>
    </div>
    <div class="col-md-12" style="padding:5px;margin-bottom:15px;">
        <label>
            <b>Chave de ligação:</b>
        </label>
        <md-select ng-model="configuracao[<?php echo $index; ?>].campoModulofkid"
                   style="height:32px;width: 100%;margin:13px 0 0 0 !important;">
            <md-optgroup label="Campo chave de relacionamento">
                <md-option ng-value="0">Selecion Campo</md-option>
                <md-option ng-repeat="campo in configuracao[<?php echo $index; ?>].camposModuloFk"
                           ng-value="campo.value">
                    {{campo.value}}
                </md-option>
            </md-optgroup>
        </md-select>
    </div>
    <div class="col-md-12" style="padding:5px;margin-bottom:15px;">
        <label>
            <b>Pode selecionar registros pré existentes</b>
        </label>
        <md-switch ng-model="configuracao[<?php echo $index; ?>].opcoesDoCampo.pode_selecionar_registros_existentes"
                   aria-label="Obrigatório"
                   style="height:43px;margin:13px 0 0 0;width:100%;">

        </md-switch>
    </div>
</div>
<div class="desenvolvedor col-md-12">
    <code>
        {{configuracao[<?php echo $index; ?>].opcoesDoCampo}}
    </code>
</div>
