<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 28/01/2017
 * Time: 18:30
 * @var $indexDaConfiguracao
 * @var $plugins
 * @var $config
 * @var $nomeDaTabela
 */

?>
<style>
    .md-select-menu-container {
        z-index: 999999;
    }
</style>
<md-select ng-model="configuracao[<?php echo $indexDaConfiguracao;?>].plugin" style="height:32px;width: 100%;margin:13px 0 0 0 !important;">
    <md-optgroup label="Plugins">
        <md-option ng-value="0">-- Selecionar --</md-option>
        <?php foreach ($plugins as $plugin){ ;?>
        <md-option ng-value="<?php echo $plugin->id;?>"><?php echo $plugin->label;?></md-option>
        <?php } ?>
    </md-optgroup>
</md-select>

