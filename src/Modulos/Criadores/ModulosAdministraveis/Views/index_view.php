<?php
/**
 * @var $modulos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo[]
 * @var $menus \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsMenu[]
 * @var $tipos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModuloTipos[]
 * @var $layouts \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModuloLayout[]
 */
?>
<div class="container-fluid" style="padding: 0;">
    <div class="panel panel-default" ng-controller="formularioArea">
        <div class="panel-heading">
            <i class="fa fa-cog"></i>
            Criador
            <i class="fa fa-angle-right"></i>
            Módulos Administraveis
            <small>Criador de tabelas e paginas para administração do conteúdo</small>
        </div>
        <div class="panel-body" style="padding: 0;">
            <!-- ## Parte 1 ## -->
            <div class="col-md-12 CriadorModulosAdministraveis-tipo-diretorio" layout="column">
                <form action="<?php echo \Aplicacao\Url::ModuloAtual('Index/SalvarModuloAdministravel'); ?>">
                    <div class="col-md-6">
                        <md-input-container style="width: 100%;">
                            <label>Icone do módulo <i class="fa fa-cog"></i></label>
                            <input type="text" name="icone" ng-model="configuracao.icone">
                        </md-input-container>
                    </div>
                    <div class="col-md-6">
                        <md-input-container style="width: 100%;">
                            <label>Nome do Módulo</label>
                            <input class="form-control" type="text" ng-model="configuracao.modulo" name="modulo">
                        </md-input-container>
                    </div>
                    <div class="col-md-6">
                        <md-input-container style="width: 100%;">
                            <label>Tabela do Módulo</label>
                            <input class="form-control" type="text" ng-model="configuracao.tabela" name="tabela">
                        </md-input-container>
                    </div>
                    <div class="col-md-6">
                        <md-input-container style="width:100%;">
                            <label>Em qual menu seu Módulo fará parte</label>
                            <md-select ng-model="configuracao.menu">
                                <md-option ng-value="0">
                                    Selecione
                                </md-option>
                                <md-optgroup label="Menus">
                                    <?php
                                    foreach ($menus as $menu) {
                                        ?>
                                        <md-option ng-value="<?php echo $menu->getId(); ?>">
                                            <?php echo $menu->getMenu(); ?>
                                        </md-option>
                                        <?php
                                    }
                                    ?>
                                </md-optgroup>
                            </md-select>
                        </md-input-container>
                    </div>
                    <div class="col-md-6">
                        <md-input-container style="width:100%;">
                            <label>Tipo de administração</label>
                            <md-select ng-model="configuracao.tipo">
                                <md-optgroup label="Tipos">
                                    <?php
                                    foreach ($tipos as $tipo) {
                                        ?>
                                        <md-option ng-value="<?php echo $tipo->getId(); ?>">
                                            <?php echo $tipo->getTitulo(); ?>
                                        </md-option>
                                        <?php
                                    }
                                    ?>
                                </md-optgroup>
                            </md-select>
                        </md-input-container>
                    </div>
                    <div class="col-md-6">
                        <md-input-container style="width:100%;">
                            <label>Layout de visualização</label>
                            <md-select ng-model="configuracao.layout">
                                <md-optgroup label="Layout">
                                    <?php
                                    foreach ($layouts as $layout) {
                                        ?>
                                        <md-option ng-value="<?php echo $layout->getId(); ?>">
                                            <?php echo $layout->getTitulo(); ?>
                                        </md-option>
                                        <?php
                                    }
                                    ?>
                                </md-optgroup>
                            </md-select>
                        </md-input-container>
                    </div>
                    <div class="col-md-12">
                        <md-input-container style="width:100%;">
                            <label>Tipo de exclusão</label>
                            <md-select ng-model="configuracao.tipo_exclusao">
                                <md-optgroup label="Tipo de exclusão do conteúdo">
                                    <md-option ng-value="0">
                                        Exclusão física
                                    </md-option>
                                    <md-option ng-value="1">
                                        Exclusão Lógica
                                    </md-option>
                                </md-optgroup>
                            </md-select>
                        </md-input-container>
                    </div>
                    <div class="col-md-9 pull-right" style="padding: 0;">
                        <div class="col-md-2 pull-right text-center buttons-area" style="padding: 0 15px;">
                            <div class="col-md-12 success-button-area text-center" style="padding: 0;">
                                <md-button ng-click="submitForm()" class="md-raised md-success btn-success">Salvar
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </md-button>
                            </div>
                        </div>
                        <div class="col-md-2 pull-right text-center buttons-area" style="padding: 0 15px;">
                            <div class="col-md-12 warning-button-area" style="padding: 0;">
                                <md-button class="md-raised md-warn">Cancelar</md-button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12 desenvolvedor" style="padding: 0;">
                <code>
                    {{configuracao}}
                </code>
            </div>
        </div>
    </div>
</div>
<?php
Aplicacao\Ferramentas\GetAssets::JS('ModuloAdministravel', 'Criadores', 'ModulosAdministraveis', true);
Aplicacao\Ferramentas\GetAssets::CSS('CriadorModulosAdministraveis', 'Criadores', 'ModulosAdministraveis');
?>
