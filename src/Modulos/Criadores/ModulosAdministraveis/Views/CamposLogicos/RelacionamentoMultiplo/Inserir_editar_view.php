<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 03/12/2016
 * Time: 15:23
 * @var $conteudoEntidadeModuloAdministravel (object)
 * @var $campos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField[]
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $CampoPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $camposValoresJson (json)
 * @var $moduloInstancia
 * @var $editar int
 */

use Modulos\Criadores\ModulosAdministraveis\Models\Repositorios\Campos;

Aplicacao\Ferramentas\GetAssets::CSS('ListagemHorizontal', 'Criadores', 'ModulosAdministraveis');
?>
    <div class="panel panel-default" ng-controller="EditarInserirConteudo" style="padding: 0;">
        <div class="panel-heading">
            <i class="fa fa-cog"></i>
            Modulo <b><?php echo $moduloInfos->getModulo(); ?></b>
            <i class="fa fa-angle-right"></i>
            <?php
            if ($editar) {
                echo 'Editar';
            } else {
                echo 'Adicionar';
            }
            ?>
        </div>
        <div class="panel-body">
            <div class="col-md-12" style="padding: 0;">
                <div class="portlet-title" style="padding: 0;">
                    <div class="col-md-12 actions" style="padding: 0;">
                        <?php
                        if ((int)$moduloInfos->getCmsModuloTiposId() != 1) {
                            ?>
                            <div class="text-left col-md-6" style="padding: 0;">
                                    <md-button class="md-raised md-warn" onclick="voltarPagina();">
                                        <i class="fa fa-external-link" aria-hidden="true"></i>
                                        VOLTAR
                                    </md-button>
                            </div>
                            <?php
                        }
                        ?>
                        <div style="padding: 0;" class="<?php echo (int)$moduloInfos->getCmsModuloTiposId() != 1 ? 'text-right' : 'text-left';?> col-md-6">
                            <md-button class="btn-success md-raised md-primary" ng-value="Primary"
                                       ng-click="salvarConteudo();">
                                <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                                SALVAR
                            </md-button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 ModuloAdministravel-table-campos" style="overflow: hidden !important;">
                <?php
                foreach ($campos as $campo) {
                    if ($campo->getField() == $CampoPk->getField() && !input_get('editar')) {
                        continue;
                    }
                    echo Campos::getCampoInsertUpdate($conteudoEntidadeModuloAdministravel, $campo, $moduloInfos);
                }
                ?>
            </div>
            <div class="col-md-12" style="padding: 0;">
                <div class="portlet-title" style="padding: 0;">
                    <div class="col-md-12 actions" style="padding: 0;">
                        <?php
                        if ((int)$moduloInfos->getCmsModuloTiposId() != 1) {
                            ?>
                            <div style="padding: 0;" class="text-left col-md-6">
                                <a href="<?php echo Aplicacao\Url::ModuloAdmin('Criadores/ModulosAdministraveis/Index/AdministrarModulo?tabela=' . $moduloInfos->getTabela()); ?>">
                                    <md-button class="md-raised md-warn">
                                        <i class="fa fa-external-link" aria-hidden="true"></i>
                                        VOLTAR
                                    </md-button>
                                </a>
                            </div>
                            <?php
                        }
                        ?>
                        <div style="padding: 0;" class="<?php echo (int)$moduloInfos->getCmsModuloTiposId() != 1 ? 'text-right' : 'text-left';?> col-md-6">
                            <md-button class="btn-success md-raised md-primary" ng-value="Primary"
                                       ng-click="salvarConteudo();">
                                <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                                SALVAR
                            </md-button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 desenvolvedor">
                <code>{{conteudo}}</code>
            </div>
        </div>
    </div>
    <script>
        var tabela = '<?php echo $moduloInfos->getTabela();;?>';
        var camposValoresJson = <?php echo $camposValoresJson;?>;
        var moduloInstancia = '<?php echo $moduloInstancia;?>';
        var edicao = '<?php echo $editar;?>';
        var campoPk = '<?php echo $CampoPk->getField();?>';
        var anexo = '<?php echo input_get('anexo');?>';
        var anexoRegistro = '<?php echo input_get('anexoRegistro');?>';
        if(1 == 1){
            console.log('==============================================================================');
            console.log('Tabela: '+ tabela);
            console.log('CamposJson:');
            console.log(camposValoresJson);
            console.log('ModuloInstancia:');
            console.log(moduloInstancia);
            console.log('Edicao:'+edicao);
            console.log('Campo Fk:'+campoPk);
            console.log('==============================================================================');
        }


    </script>
<?php
Aplicacao\Ferramentas\GetAssets::JS('CamposLogicos/RelacionamentoMultiplo', 'Criadores', 'ModulosAdministraveis');
Aplicacao\Ferramentas\GetAssets::CSS('CriadorModulosAdministraveis', 'Criadores', 'ModulosAdministraveis');