<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 03/12/2016
 * Time: 15:23
 * @var $conteudosEntidadeModuloAdministravel array
 * @var $campos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField[]
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $CampoPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloExtensao \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $relacionamentoMultiploEntidade \Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Administracao\RelacionamentoMultiplo\Entidades\RelacionamentoMultiploEntidade
 */

use Modulos\Criadores\ModulosAdministraveis\Models\Repositorios\Campos;

Aplicacao\Ferramentas\GetAssets::CSS('ListagemHorizontal', 'Criadores', 'ModulosAdministraveis');
Aplicacao\Ferramentas\GetAssets::JS('CamposLogicos/RelacionamentoMultiplo', 'Criadores', 'ModulosAdministraveis');
?>
    <div class="panel panel-default" ng-controller="ListagemCtrl" style="padding: 0;">
        <div class="panel-heading">
            <i class="fa fa-cog"></i>
            Modulo <b><?php echo $moduloInfos->getModulo(); ?></b> para
            <b><?php echo $moduloExtensao->getModulo(); ?></b>
            <i class="fa fa-angle-right"></i>
            Administrar
            <small><b><?php echo $moduloInfos->getModulo(); ?></b> para
                <b><?php echo $moduloExtensao->getModulo(); ?></b></small>
        </div>
        <div class="panel-body">
            <div class="col-md-12" style="padding: 0;">
                <div class="portlet-title" style="padding: 0;">
                    <div class="col-md-12 actions" style="padding: 0;">
                        <div class="text-left col-md-5">
                            <md-button class="md-raised md-warn" onclick="voltarPagina();">
                                <i class="fa fa-external-link" aria-hidden="true"></i>
                                VOLTAR P/ <?php echo $moduloExtensao->getModulo(); ?>
                            </md-button>
                        </div>
                        <div class="text-right col-md-7">
                            <a href="<?php echo Aplicacao\Url::ModuloAdmin('Criadores/ModulosAdministraveis/Index/AdministrarModulo?tabela=' . $moduloInfos->getTabela() . '&anexo=' . $moduloExtensao->getTabela() . '&anexoRegistro=' . input_get('anexoRegistro') . '&extensao=RelacionamentoMultiplo&metodo=adicionar'); ?>">
                                <md-button class="btn-success md-raised md-primary" ng-value="Primary">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                    Adicionar
                                    <?php echo $moduloInfos->getModulo() . ' P/ ' . $moduloExtensao->getModulo(); ?>
                                </md-button>
                            </a>
                            <?php
                            if ($relacionamentoMultiploEntidade->getOpcoesDoCampo()->getPodeSelecionarRegistrosExistentes()) {
                                ?>
                                <a href="<?php echo Aplicacao\Url::ModuloAdmin('Criadores/ModulosAdministraveis/Index/AdministrarModulo?tabela=' . $moduloInfos->getTabela() . '&anexo=' . $moduloExtensao->getTabela() . '&anexoRegistro=' . input_get('anexoRegistro') . '&selecionar=true'); ?>">
                                    <md-button class="btn-primary md-raised md-primary" ng-value="Primary">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        Selecionar
                                        <?php echo $moduloInfos->getModulo() . ' P/ ' . $moduloExtensao->getModulo(); ?>
                                    </md-button>
                                </a>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 ModuloAdministravel-table-campos">
                <table class="table table-striped" style="width:100%;overflow: auto;">
                    <thead>
                    <tr>
                        <?php
                        foreach ($campos as $campo) {
                            if (!$campo->getListar()) {
                                continue;
                            }
                            ?>
                            <th>
                                <?php
                                echo $campo->getDescricao();
                                ?>
                            </th>
                            <?php
                        }
                        ?>
                        <th colspan="2">
                            Ações
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (!count($conteudosEntidadeModuloAdministravel)) {
                        ?>
                        <tr valign="center">
                            <td colspan="<?php echo(count($campos) + 1); ?>">
                                <b>Nenhum registro encontrado para <?php echo $moduloInfos->getModulo(); ?></b><br>
                                <a href="<?php echo Aplicacao\Url::ModuloAdmin('Criadores/ModulosAdministraveis/Index/AdministrarModulo?tabela=' . $moduloInfos->getTabela() . '&anexo=' . $moduloExtensao->getTabela() . '&anexoRegistro=' . input_get('anexoRegistro') . '&extensao=RelacionamentoMultiplo&metodo=adicionar'); ?>">
                                    <md-button class="btn-success md-raised md-primary" ng-value="Primary">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                        Adicionar
                                        <?php echo $moduloInfos->getModulo() . ' P/ ' . $moduloExtensao->getModulo(); ?>
                                    </md-button>
                                </a>
                                <?php
                                if ($relacionamentoMultiploEntidade->getOpcoesDoCampo()->getPodeSelecionarRegistrosExistentes()) {
                                    ?>
                                    <a href="<?php echo Aplicacao\Url::ModuloAdmin('Criadores/ModulosAdministraveis/Index/AdministrarModulo?tabela=' . $moduloInfos->getTabela() . '&anexo=' . $moduloExtensao->getTabela() . '&anexoRegistro=' . input_get('anexoRegistro') . '&selecionar=true'); ?>">
                                        <md-button class="btn-primary md-raised md-primary" ng-value="Primary">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                            Selecionar
                                            <?php echo $moduloInfos->getModulo() . ' P/ ' . $moduloExtensao->getModulo(); ?>
                                        </md-button>
                                    </a>
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    foreach ($conteudosEntidadeModuloAdministravel as $conteudo) {
                        ?>
                        <tr valign="center">
                            <?php
                            foreach ($campos as $campo) {
                                if (!$campo->getListar()) {
                                    continue;
                                }
                                $campoPkGetterConversion = 'get' . \Aplicacao\Conversao::underscoreToCamelCase($CampoPk->getField());
                                ?>
                                <td class="" aria-label="<?php echo $campo->getField(); ?>"
                                    data-view="<?php echo $campo->getCmsTipoCampos()->getViewName(); ?>">
                                    <md-tooltip md-direction="top">
                                        Editar Campo <?php echo $campo->getDescricao(); ?> Linha
                                        (<?php echo $conteudo->{$campoPkGetterConversion}(); ?>)
                                    </md-tooltip>
                                    <?php echo Campos::getCampoListagem($conteudo, $campo, $moduloInfos, $conteudo); ?>
                                </td>
                                <?php
                            }
                            ?>
                            <td>
                                <md-button
                                        style="max-width: 45px !important;min-width: 45px !important;background:#d60f0f !important;"
                                        class="md-raised md-warn md-button md-ink-ripple"
                                        aria-label="<?php echo $campo->getField() . 'excluir'; ?>"
                                        data-toggle="modal" data-trigger="hover" data-placement="top"
                                        data-content="Remover"
                                        ng-click="deletarConteudo($event, '<?php echo $conteudo->{$campoPkGetterConversion}(); ?>');"
                                        data-original-title="Deletar" title="Deletar">
                                    <md-tooltip md-direction="top">
                                        Retirar da lista (<?php echo $conteudo->{$campoPkGetterConversion}(); ?>)
                                    </md-tooltip>
                                    <i class="fa fa-ban" aria-hidden="true"></i>
                                </md-button>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        var tabela = '<?php echo $moduloInfos->getTabela();?>';
        var anexoRegistro = '<?php echo input_get('anexoRegistro');?>';
        var anexo = '<?php echo input_get('anexo');?>';
    </script>
<?php
Aplicacao\Ferramentas\GetAssets::CSS('CriadorModulosAdministraveis', 'Criadores', 'ModulosAdministraveis');
?>