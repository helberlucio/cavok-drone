<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 21/11/2016
 * Time: 20:48
 * @var $tipoCampos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsTipoCampos[]
 */
?>
<div class="panel panel-default panel-campos-configuracao" ng-controller="Campos" style="padding: 0;/*display: none;*/">
    <div class="panel-heading">
        <i class="fa fa-cog"></i>
        Criador
        <i class="fa fa-angle-right"></i>
        Módulos Físicos > Campos
        <small>Campos referente ao banco de dados (dados que deseja guardar)</small>
    </div>
    <div class="panel-body">
        <div class="col-md-12 ModuloAdministravel-table-campos">
            <table class="table table-striped" style="overflow: auto;">
                <thead>
                <tr>
                    <th>
                        Campo
                    </th>
                    <th>
                        Descrição
                    </th>
                    <th>
                        Tipo do campo
                    </th>
                    <th>
                        Tamhno de caracters
                    </th>
                    <th>
                        Obrigatorio
                    </th>

                    <th>
                        Listar
                    </th>
                    <th>
                        Ordem do campo ("ASC")
                    </th>
                    <th>
                        Deletar Campo
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="(index, campoModulo) in configuracao">
                    <td>
                        <md-input-container
                            style="height:43px;width: 100% !important;margin:13px 0 0 0 !important;">
                            <label>Seu Campo</label>
                            <input class="form-control" type="text" ng-model="campoModulo.campo" name="modulo">
                        </md-input-container>
                    </td>
                    <td>
                        <md-input-container style="height:43px;width: 100%;margin:13px 0 0 0;">
                            <label>Descrição</label>
                            <input class="form-control" type="text" ng-model="campoModulo.descricao" name="modulo">
                        </md-input-container>
                    </td>
                    <td>
                        <md-select ng-model="campoModulo.tipo"
                                   style="height:32px;width: 100%;margin:13px 0 0 0 !important;">
                            <?php
                            $grupoCampo = 0;
                            foreach ($tipoCampos as $campo) {
                            if ($campo->getCmsGrupoTipoCamposId() != $grupoCampo){
                                if($grupoCampo != 0){
                                    ?>
                                    </md-optgroup>
                                    <?php
                                }
                            $grupoCampo = $campo->getCmsGrupoTipoCamposId();
                            ?>
                            <md-optgroup label="<?php echo $campo->getCmsGrupoTipoCampos()->getLabel();?>">
                                <?php
                                }
                                ?>
                                <md-option ng-value="<?php echo $campo->getId();?>">
                                    <?php echo $campo->getLabel();?>
                                </md-option>
                                <?php } ?>
                        </md-select>
                        <md-button ng-click="openConfig(index)" class="md-raised md-primary btn-primary" style="margin:0;width:100%;line-height: 0px;min-height: 25px;">
                            <i class="fa fa-cog" aria-hidden="true"></i>
                        </md-button>
                    </td>
                    <td>
                        <md-input-container style="height:43px;width: 100%;margin:13px 0 0 0;">
                            <label>Caracters</label>
                            <input type="number" name="input" ng-model="campoModulo.caracters"
                                   min="0" max="1500" required>
                        </md-input-container>
                    </td>
                    <td style="width: 80px !important;">
                        <md-switch ng-model="campoModulo.obrigatorio" aria-label="Obrigatório"
                                   style="height:43px;margin:13px 0 0 0;width:100%;">

                        </md-switch>
                    </td>
                    <td>
                        <md-switch ng-model="campoModulo.listar" aria-label="Listar"
                                   style="height:43px;margin:13px 0 0 0;width:100%;">

                        </md-switch>
                    </td>

                    <td>
                        <md-input-container style="height:43px;width: 100%;margin:13px 0 0 0;">
                            <label>Ordem do campo</label>
                            <input type="number" name="input" ng-model="campoModulo.ordem"
                                   min="0" max="644" required>
                        </md-input-container>
                    </td>
                    <td>
                        <md-button ng-click="removerCampo(campoModulo);"
                                   class="md-raised md-warn"
                                   style="border-radius: 1000px !important;max-width: 42px;min-width: 36px;">
                            <i class="fa fa-window-close" aria-hidden="true"></i>
                        </md-button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-12 pull-right" style="padding: 0;">
            <div class="col-md-2 pull-right text-center buttons-area" style="padding: 0 15px;">
                <div class="col-md-12 primary-button-area text-center" style="padding: 0;">
                    <md-button ng-click="adicionarCampo()" class="md-raised md-success btn-success">
                        Adicionar <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    </md-button>
                </div>
            </div>
            <div class="col-md-2 pull-left text-center buttons-area" style="padding: 0 15px;">
                <div class="col-md-12 success-button-area text-center" style="padding: 0;">
                    <md-button ng-click="salvarCampos()" class="md-raised md-primary btn-primary">
                        Salvar <i class="fa fa-cloud" aria-hidden="true"></i>
                    </md-button>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div ng-repeat="erro in erros" class="alert alert-danger">
                {{erro}}
            </div>
            <div ng-repeat="sucessoCampos in sucesso" class="alert alert-success">
                {{sucessoCampos}}
            </div>
        </div>
        <div class="col-md-12 desenvolvedor">
            <code>{{configuracao}}</code>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <md-button type="button" class="md-raised md-default btn-default" data-dismiss="modal">
                    Fechar
                </md-button>
                <md-button type="button" class="md-raised md-success btn-success" data-dismiss="modal">
                    Salvar
                </md-button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<script>
    var tabela = '<?php echo $tabela;?>';
</script>
<?php
Aplicacao\Ferramentas\GetAssets::JS('ModuloAdministravel', 'Criadores', 'ModulosAdministraveis', true);
Aplicacao\Ferramentas\GetAssets::CSS('CriadorModulosAdministraveis', 'Criadores', 'ModulosAdministraveis');
Aplicacao\Ferramentas\GetAssets::CSS('CriadorModuloAdministravelCampos', 'Criadores', 'ModulosAdministraveis');
?>

