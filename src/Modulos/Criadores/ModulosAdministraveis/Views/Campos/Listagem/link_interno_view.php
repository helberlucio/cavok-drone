<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */
$pkOfModule = $this->db->where('cms_modulo_id', $moduloInfos->getId())->where('tipo', 'pk')->get('cms_field')->row();

if(isset($pkOfModule->field)){
    $pkField = $pkOfModule->field;
}else{
    $pkField = '';
}

if(isset($conteudo->{$pkField})){
    $valuePk = $conteudo->{$pkField};
}else{
    $valuePk = '';
}

?>
<a href="<?php echo Aplicacao\Url::Base($campo->getField().'/'.$valuePk.'/'.$pkField);?>">
    <button class="btn green btn-success md-button md-ink-ripple" type="button" ng-transclude=""
            aria-label="galeria(0)"><span class="ng-scope">
            <?php
            echo $campo->getDescricao();
            ?>
        </span>
        <div class="md-ripple-container"></div>
    </button>
</a>