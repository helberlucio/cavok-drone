<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */
if ($campo->getCmsPluginsId()) {
    $dataConfigPlugin['cmsFieldPk'] = $cmsFieldPk;
    $dataConfigPlugin['moduloInfos'] = $moduloInfos;
    $dataConfigPlugin['db'] = $db;
    $dataConfigPlugin['campo'] = $campo;
    $dataConfigPlugin['conteudo'] = $conteudo;
    (new \Core\Modelos\Carregamentos())->AdminView('Campos/Plugins/List/'.$campo->getCmsPlugin()->getCode(), $dataConfigPlugin, false, $emodulo);
} else{
    echo 'Plugin não encontrado.';
}