<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */

$id = $campo->getField();
if(!@is_file($conteudo)){
    if(@is_file(substr($conteudo,1))){
        $conteudo = substr($conteudo,1);
    }
}
?>
<style>
    .<?php echo $id;?> {
        transition: all 0.6s linear;
        border-radius: 5px !important;
        box-shadow: 3px 3px 13px #B7B7B7;
        max-width: 166px;
        max-height: 240px;
    }
    .<?php echo $id;?>:hover {
        transition: all 0.6s linear;
        transform: scale(1.2);
        box-shadow: 3px 3px 13px #000;
    }
</style>

<a class="example-image-link" href="<?php echo @is_file($conteudo) ? Aplicacao\Url::Base($conteudo) : \Aplicacao\Url::Base('src/Assets/Images/Default/imagem-nao-encontrada.jpg'); ?>" data-lightbox="example-set"
   data-title="Link da imagem">
    <img alt="" class="example-image imageList <?php echo $id; ?>" src="<?php echo @is_file($conteudo) ? Aplicacao\Url::Base($conteudo) : \Aplicacao\Url::Base('src/Assets/Images/Default/imagem-nao-encontrada.jpg'); ?>">
</a>
