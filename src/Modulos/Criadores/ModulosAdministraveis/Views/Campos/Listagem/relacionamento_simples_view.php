<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */


$objetoFk = $db->where($campo->getFkid(), $conteudo)->get($campo->getTableFk())->row(false);

$tableFkFieldsIds = $db->select('GROUP_CONCAT(cms_field_fk_text," ") as stringIds')
    ->where('cms_field', $campo->getId())
    ->get('cms_field_fk_text')
    ->row(false);
$fkTexts = array();
if (isset($tableFkFieldsIds->stringIds)) {
    $filtro = $db->select('field, descricao')
        ->where('`id` in (' . $tableFkFieldsIds->stringIds . ')', '', true);
    $fkTexts = (new \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField())->getRows($filtro);
}
if (is_object($objetoFk)) {
    foreach ($fkTexts as $key => $fkText) {
        echo $fkText->getDescricao() . ': ' .
            $objetoFk->{$fkText->getField()} . (count($fkTexts) > ($key + 1) ? '; ' : '.');
    }
} else {
    echo 'Não ecnontrado!';
}

