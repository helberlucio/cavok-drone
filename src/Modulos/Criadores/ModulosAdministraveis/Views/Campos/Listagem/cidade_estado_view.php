<?php
/**
 */

$cmsField = isset($data['campo']) ? $data['campo'] : (new \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField());
$ufGetterMethod = 'get'.\Aplicacao\Conversao::underscoreToCamelCase($cmsField->getField().'_uf');
$cityGetterMethod = 'get'.\Aplicacao\Conversao::underscoreToCamelCase($cmsField->getField().'_city');
$uf = method_exists($data['registro'], $ufGetterMethod) ? $data['registro']->{$ufGetterMethod}() : false;
$city = method_exists($data['registro'], $cityGetterMethod) ? $data['registro']->{$cityGetterMethod}() : false;
echo $city.' - '.$uf;