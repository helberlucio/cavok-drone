<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */
?>
<div class="form-group">
    <md-input-container style="width: 100%;">
        <label><b><?php echo $campo->getDescricao(); ?></b></label>
        <input onkeyup="replacePassword(this)" type="password" name="<?php echo $campo->getField();?>"
               ng-value="conteudo.<?php echo $campo->getField();?>">
    </md-input-container>
    <input type="hidden" name="<?php echo $campo->getField(); ?>"
           class="input-value-hidden-<?php echo $campo->getDescricao(); ?>"
           ng-model="conteudo.<?php echo $campo->getField();?>"
           id="hidden-<?php echo $campo->getField(); ?>">

</div>
<button style="opacity: 0;" class="buttonAtualizarTipo<?php echo $campo->getField(); ?>"
        ng-click="atualizarValorConteudoPorInput('input-value-hidden-<?php echo $campo->getDescricao(); ?>', '<?php echo $campo->getField(); ?>');"></button>

<script>
    function replacePassword($this) {
        var value = $($this).val();
        $('#hidden-<?php echo $campo->getField(); ?>').val(value);
        if (value != '') {
            $.post('<?php echo Aplicacao\Url::ModuloAdmin("Api/ApiEncrypt/Index/Sha1?value=");?>' + value, {}, function (data) {
                $('#hidden-<?php echo $campo->getField(); ?>').val(data.callback);
                $('.buttonAtualizarTipo<?php echo $campo->getField();?>').click();
            }, 'json');
        }
    }
</script>

