<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 
 */
?>
<div class="form-group">
    <label class="control-label"><b><?php echo $campo->getDescricao(); ?></b></label>
    <?php
    if ($campo->getCmsPluginsId()) {
        $dataConfigPlugin['cmsFieldPk'] = $cmsFieldPk;
        $dataConfigPlugin['moduloInfos'] = $moduloInfos;
        $dataConfigPlugin['db'] = $db;
        $dataConfigPlugin['campo'] = $campo;
        $dataConfigPlugin['conteudo'] = $conteudo;

        (new \Core\Modelos\Carregamentos())->AdminView('Campos/Plugins/InsertUpdate/'.$campo->getCmsPlugin()->getCode(), $dataConfigPlugin, false, $emodulo);
    } else {
        ?>
        <textarea ng-model="conteudo.<?php echo $campo->getField(); ?>" name="<?php echo $campo->getField(); ?>" class="form-control"
                  placeholder="<?php echo $campo->getDescricao(); ?>">{{conteudo.<?php echo $campo->getField(); ?>}}</textarea>
        <?php
    }
    ?>
</div>

