<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */
?>
<div class="form-group">
    <label><b><?php echo $campo->getDescricao(); ?></b></label>

    <div class="input-group date" id="datepicker<?php echo $campo->getField();?>" data-provide="datepicker">
        <input style="min-width: 135px;" type="text" name="<?php echo $campo->getField(); ?>"
               ng-model="conteudo.<?php echo $campo->getField(); ?>"
               class="form-control data-<?php echo $campo->getField();?>"
               onblur="atualizarCampo<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField());?>(this);">
        <div class="input-group-addon">
            <span class="glyphicon glyphicon-th"></span>
        </div>
    </div>
    <input type="hidden" ng-model="conteudo.<?php echo $campo->getField(); ?>" class="hidden-data<?php echo $campo->getField();?>">
</div>
<button style="opacity: 0;" class="buttonAtualizarTipo<?php echo $campo->getField();?>" ng-click="atualizarValorConteudoPorInput('hidden-data<?php echo $campo->getField();?>', '<?php echo $campo->getField();?>', 'disable', 15);">REFRESH</button>
<script>
    function atualizarCampo<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField());?>($this){
        var valueFromField = $($this).val();
        $('.hidden-data<?php echo $campo->getField();?>').val(valueFromField);
        if(valueFromField != ''){
            $('.buttonAtualizarTipo<?php echo $campo->getField();?>').click();
        }
    }
    $(function () {
        $('#datepicker<?php echo $campo->getField();?>').datepicker({
            format: 'yyyy-mm-dd'
        });
    });
</script>