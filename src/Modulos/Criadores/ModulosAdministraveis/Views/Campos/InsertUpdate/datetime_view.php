<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */

?>
<div class="row">
    <label><b><?php echo $campo->getDescricao();?></b></label>

    <div class="input-group date">
        <?php
        if($conteudo) {
            ?>
            <input style="min-width: 135px;" type="datetime-local" name="<?php echo $campo->getField(); ?>" value="<?php echo $conteudo;?>" class="form-control">
            <?php
        }else {
            ?>
            <input style="min-width: 135px;" type="datetime-local" value="2000-01-01T00:00:00+05:00" name="<?php echo $campo->getField(); ?>" class="form-control">
            <?php
        }
        ?>
        <div class="input-group-addon">
            <span class="glyphicon glyphicon-th"></span>
        </div>
    </div>
</div>
