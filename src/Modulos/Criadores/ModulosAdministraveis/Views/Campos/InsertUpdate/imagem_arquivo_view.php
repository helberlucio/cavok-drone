<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */
if(!@is_file($conteudo)){
    if(@is_file(substr($conteudo,1))){
        $conteudo = substr($conteudo,1);
    }
}
$noimage = \Aplicacao\Url::Base('src/Assets/Images/Default/imagem-nao-encontrada.jpg');
?>
<div style="float:left;width:100%;" class="form-group">
    <div class="col-md-3 fileinput fileinput-new" data-provides="fileinput" >
        <b><?php echo $campo->getDescricao() ?></b><br>
        <div class="fileinput-new thumbnail" style="overflow: hidden;padding: 0;background-color:#f3f3f3;width: 200px; height: 150px;">
            <img class="recebeImagem<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?>"
                 src="<?php echo @is_file($conteudo) ? Aplicacao\Url::Base($conteudo) : $noimage; ?>"
                 ng-model="conteudo.<?php echo $campo->getField(); ?>" alt="">
        </div>
        <div class="fileinput-preview fileinput-exists thumbnail" style="display:none;max-width: 200px; max-height: 150px;"></div>
        <div>
          <span class="btn default btn-file" style="width: 50%;">
              <span
                  class="fileinput-new fileinput-new<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?> fileinput-button<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?>"
                  data-name="<?php echo $campo->getField(); ?>"> Selecionar
              </span>
              <span
                  class="fileinput-exists fileinput-exists<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?> fileinput-button<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?>"
                  data-name="<?php echo $campo->getField(); ?>"> Modificar
              </span>
          </span>
            <div class="col-md-6" style="padding: 0px;">
               <span>
                    <button type="button" style="width: 100%;"
                            class="btn remove-item<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?> red fileinput-exists"
                            data-dismiss="fileinput"> Remover
                    </button>
               </span>
            </div>
        </div>
    </div>
</div>
<button style="opacity: 0;"
        class="buttonAtualizarTipo<?php echo $campo->getField();?>"
        ng-click="atualizarValorConteudoPorInput('<?php echo $campo->getField();?>', '<?php echo $campo->getField();?>', 'disable', 15);">
    REFRESH
</button>

<input type="hidden" name="<?php echo $campo->getField(); ?>"
       class="<?php echo $campo->getField(); ?>
       recebeCaminho<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?>"
       value="<?php echo $conteudo; ?>"
    ng-model="conteudo.<?php echo $campo->getField(); ?>">
<?php
if ($conteudo) {
    ?>
    <script>
        $(function () {
            $('.fileinput-new<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?>').fadeOut('fast');
            $('.fileinput-exists<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?>').fadeIn('fast');
            $('.remove-item<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?>').fadeIn('fast');
        })
    </script>
    <?php
}
?>
<script>
    var ENV = '<?php echo LI_BASE_PATH;?>';
  var caminhoArquivo = '';
    function capturarCaminho<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField());?>($caminho, $inputName, $urlCompleta) {
        if($inputName !== '<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?>'){
            console.log('diferents fields: '+$inputName+' is diferent of <?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?>');
            return false;
        }
       $('.load-gear').fadeIn();
        caminhoArquivo = $caminho.replace("../", "").replace("../", "").replace(ENV+'/', '');
        console.log(caminhoArquivo);
       if($urlCompleta === 'sendedAllUrl'){
            $.post(base_url('Modulo/Gerenciador/Elfinder1/Index/decryptHashFile'), {
              hash:caminhoArquivo
            }, function(data){
              if(!data.hash){
                alert('Erro! Nao foi encontrado o caminho do arquivo. Contate o suporte.');
                return false;
              }
              caminhoArquivo = '<?php echo LI_DIR_GERENCIADOR;?>'+data.hash;
              setTimeout(function(){
                $(".recebeCaminho" + $inputName.replace(' ', '')).val(caminhoArquivo);
                $('.buttonAtualizarTipo<?php echo $campo->getField();?>').click();
              }, 2500);

            }, 'json');

          }
      //Colcoar caminho do arquivo no input hidden  
      $(".recebeCaminho" + $inputName.replace(' ', '')).val(caminhoArquivo);
        //Retirar provisoriamente a imagem
       $(".recebeImagem" + $inputName.replace(' ', '')).attr('src', '<?= \Aplicacao\Url::Base('src/Assets/Images/Default/imagem-nao-encontrada.jpg') ?>');
       //Recebe imagem escolhida no gerenciador de arquivos
        setTimeout(function(){
            $(".recebeImagem" + $inputName.replace(' ', '')).attr('src', '<?php echo Aplicacao\Url::Base();?>' + caminhoArquivo);  
        },2500);
       
       //toca de botoes 
       $('.fileinput-new' + $inputName.replace(' ', '')).fadeOut('fast');
       $('.fileinput-exists' + $inputName.replace(' ', '')).fadeIn('fast');
       $('.remove-item' + $inputName.replace(' ', '')).fadeIn('fast');
       //Atualiza os Valores do angular
       $('.buttonAtualizarTipo<?php echo $campo->getField();?>').click();
       // desabilitar carregamento
       setTimeout(function(){
         $('.load-gear').fadeOut();
       }, 2500);
    }
    $(function () {
        $('.fileinput-button<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?>').on('click', function () {
            var nameTracker = $(this).attr("data-name");
            $(".putImagem" + nameTracker).slideUp("fast");
            window.open("<?php echo Aplicacao\Url::ModuloAdmin("Gerenciador/Elfinder1/Index/index/");?><?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField());?>/?campo=<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField());?>", "Gerenciador", "width=1300, height=400");
        });
        $('.remove-item<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?>').on('click', function () {
            $(this).fadeOut('fast');
            $('.fileinput-exists<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?>').fadeOut('fast');
            $('.fileinput-new<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?>').fadeIn('fast');
            $('.recebeCaminho<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?>').val(' ');
            $('.recebeImagem<?php echo \Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?>').attr('src', 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image');
        });
    });
</script>