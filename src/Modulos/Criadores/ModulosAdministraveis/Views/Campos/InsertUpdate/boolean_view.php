<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */
?>
<div class="form-group">
    <label class="control-label"><b><?php echo $campo->getDescricao(); ?></b></label>
    <md-select style="min-width: 135px;"
               id="single"
               class=""
               tabindex="-1"
               ng-model="conteudo.<?php echo $campo->getField(); ?>"
               aria-hidden="true">
        <md-option value="0">Não</md-option>
        <md-option value="1">Sim</md-option>
    </md-select>
</div>
