<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 07/11/2017
 * Time: 22:27
 */

?>
<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */
$uf = '';
$city = '';
if (isset($data['registro']) && is_object($data['registro'])) {
    $ufGetterMethod = 'get' . \Aplicacao\Conversao::underscoreToCamelCase($campo->getField() . '_uf');
    $cityGetterMethod = 'get' . \Aplicacao\Conversao::underscoreToCamelCase($campo->getField() . '_city');
    $uf = method_exists($data['registro'], $ufGetterMethod) ? $data['registro']->{$ufGetterMethod}() : false;
    $city = method_exists($data['registro'], $cityGetterMethod) ? $data['registro']->{$cityGetterMethod}() : false;
}
?>
<div class="form-group">
    <label><b><?php echo $campo->getDescricao(); ?></b></label>

    <div class="input-group">
     <span class="input-group-addon">
        <i class="fa fa-map-marker" aria-hidden="true"></i>
     </span>
        <!-- ################ PAIS ################# -->
        <select
                id="paises_<?php echo $campo->getField(); ?>"
                name="pais_<?php echo $campo->getField(); ?>"
                class="form-control">
            <option value="" disabled selected>Selecione o pais</option>
        </select>
    </div>

    <div class="input-group" style="margin-top:20px;">
     <span class="input-group-addon">
        <i class="fa fa-map-marker" aria-hidden="true"></i>
     </span>
        <!-- ################ ESTADO ################# -->
        <input
                style="min-width: 135px;"
                type="text"
                name="<?php echo $campo->getField(); ?>"
                class="form-control <?php echo $campo->getField() . '_uf_simple'; ?>"
                placeholder="Escolher estado"
                ng-model="conteudo.<?php echo $campo->getField() . '_uf'; ?>">

        <select name="<?php echo $campo->getField(); ?>"
                class="form-control <?php echo $campo->getField() . '_uf'; ?>" ng-model="conteudo.<?php echo $campo->getField() . '_uf'; ?>">

        </select>
    </div>
    <div class="input-group" style="margin-top:20px;">
     <span class="input-group-addon">
        <i class="fa fa-map-marker" aria-hidden="true"></i>
     </span>
        <!-- ################ CIDADE ################# -->

        <input
                style="min-width: 135px;"
                type="text"
                name="<?php echo $campo->getField(); ?>"
                class="form-control <?php echo $campo->getField() . '_city_simple'; ?>"
                placeholder="Escolher cidade"
                ng-model="conteudo.<?php echo $campo->getField() . '_city'; ?>">

        <select name="<?php echo $campo->getField(); ?>"
                class="form-control <?php echo $campo->getField() . '_city'; ?>" ng-model="conteudo.<?php echo $campo->getField() . '_city'; ?>">

        </select>
    </div>
</div>


<button style="opacity: 0;" class="atualizarPorInput-valor<?php echo $campo->getField() . '_uf'; ?>"
        ng-click="atualizarValorConteudo('<?php echo $campo->getField() . '_uf'; ?>', '<?php echo $uf; ?>');">
</button>
<button style="opacity: 0;" class="atualizarPorInput-valor<?php echo $campo->getField() . '_city'; ?>"
        ng-click="atualizarValorConteudo('<?php echo $campo->getField() . '_city'; ?>', '<?php echo $city; ?>');">
</button>
<script>

    function getAllCountry() {
        $.get('https://restcountries.eu/rest/v2/all', function (data) {
            $.each(data, function (i, item) {
                $('#paises_<?php echo $campo->getField();?>').append($('<option>', {
                    value: item.value + ' - ' + item.alpha2Code + ' - ' + item.subregion,
                    text: item.name
                }));
            });

        }, 'json')
    }

    $(function () {
        montaPais();
        setTimeout(function () {
            $('.atualizarPorInput-valor<?php echo $campo->getField() . '_uf'; ?>').click();
            $('.atualizarPorInput-valor<?php echo $campo->getField() . '_city'; ?>').click();
        });
    });


    function montaCidade(estado, pais) {
        $.ajax({
            type: 'GET',
            url: 'http://api.londrinaweb.com.br/PUC/Cidades/' + estado + '/' + pais + '/0/10000',
            contentType: "application/json; charset=utf-8",
            dataType: "jsonp",
            async: false
        }).done(function (response) {
            $(".<?php echo $campo->getField() . '_city'?>").empty();
            $.each(response, function (c, cidade) {
                $(".<?php echo $campo->getField() . '_city'?>").append($('<option>', {
                    value: cidade,
                    text: cidade
                }));

            });
        });
    }

    function montaUF(pais) {
        $.ajax({
            type: 'GET',
            url: 'http://api.londrinaweb.com.br/PUC/Estados/' + pais + '/0/10000',
            contentType: "application/json; charset=utf-8",
            dataType: "jsonp",
            async: false
        }).done(function (response) {
            $(".<?php echo $campo->getField() . '_uf'?>").empty();
            $.each(response, function (e, estado) {
                $(".<?php echo $campo->getField() . '_uf'?>").append($('<option>', {
                    value: estado.UF,
                    text: estado.Estado
                }));

            });

            // CHAMA A FUNÇÃO QUE PREENCHE AS CIDADES DE ACORDO COM O ESTADO
            montaCidade($(".<?php echo $campo->getField() . '_uf'?>").val(), pais);

            // VERIFICA A MUDANÇA NO VALOR DO CAMPO ESTADO E ATUALIZA AS CIDADES
            $(".<?php echo $campo->getField() . '_uf'?>").change(function () {
                montaCidade($(this).val(), pais);
            });

        });
    }

    function montaPais() {
        $.ajax({
            type: 'GET',
            url: 'http://api.londrinaweb.com.br/PUC/Paisesv2/0/1000',
            contentType: "application/json; charset=utf-8",
            dataType: "jsonp",
            async: false
        }).done(function (response) {

            paises = '';

            $.each(response, function (p, pais) {

                if (pais.Pais === 'Brasil') {
                    paises += '<option value="' + pais.Sigla + '" selected>' + pais.Pais + '</option>';
                } else {
                    paises += '<option value="' + pais.Sigla + '">' + pais.Pais + '</option>';
                }

            });

            // PREENCHE O SELECT DE PAÍSES
            $('#paises_<?php echo $campo->getField();?>').html(paises);

            // PREENCHE O SELECT DE ACORDO COM O VALOR DO PAÍS
            montaUF($('#paises_<?php echo $campo->getField();?>').val());

            // SE O VALOR FOR BR E CONFIRMA OS SELECTS
            $('.<?php echo $campo->getField().'_city_simple';?>').hide();
            $(".<?php echo $campo->getField().'_uf_simple';?>").hide();

            $('.<?php echo $campo->getField().'_city';?>').show();
            $(".<?php echo $campo->getField().'_uf';?>").show();

            // VERIFICA A MUDANÇA DO VALOR DO SELECT DE PAÍS
            $('#paises_<?php echo $campo->getField();?>').change(function () {
                if($('#paises_<?php echo $campo->getField();?>').val() === 'BR'){
                    // SE O VALOR FOR BR E CONFIRMA OS SELECTS
                    $('.<?php echo $campo->getField().'_city_simple';?>').hide();
                    $(".<?php echo $campo->getField().'_uf_simple';?>").hide();

                    $('.<?php echo $campo->getField().'_city';?>').show();
                    $(".<?php echo $campo->getField().'_uf';?>").show();

                    // CHAMA A FUNÇÃO QUE MONTA OS ESTADOS
                    montaUF($('#paises_<?php echo $campo->getField();?>').val());
                } else {
                    $('.<?php echo $campo->getField().'_city_simple';?>').show();
                    $(".<?php echo $campo->getField().'_uf_simple';?>").show();

                    $('.<?php echo $campo->getField().'_city';?>').hide();
                    $(".<?php echo $campo->getField().'_uf';?>").hide();
                }

            })

        });
    }
</script>