<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 * @var $registro
 */
if (!$registro) {
    return;
}
?>
<div class="form-group" style="margin-top:10px;">
  <label><b>Multipla escolha de <?php echo (new \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo())->getRow($db->where('tabela', $campo->getTableFk()))->getModulo(); ?>:</b></label>
    <a href="<?php echo \Aplicacao\Url::ModuloAdmin(
        'Criadores/ModulosAdministraveis/Index/AdministrarModulo?tabela=' .
        $campo->getTableFk() .
        '&anexo=' . $moduloInfos->getTabela() .
        '&anexoRegistro=' . $registro->{'get' . \Aplicacao\Conversao::underscoreToCamelCase($cmsFieldPk->getField())}().
        'extensao=RelacionamentoMultiplo'
    ); ?>" target="_blank">
        <md-button class="md-raised md-primary">
            <?php echo (new \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo())->getRow($db->where('tabela', $campo->getTableFk()))->getModulo(); ?>
            <i class="fa fa-sign-out" aria-hidden="true"></i>
        </md-button>
    </a>
</div>
