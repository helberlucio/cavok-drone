<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */
$tableFkResults = $this->db->get($campo->getTableFk())->result(false);
$tableFkFieldsIds = $db->select('GROUP_CONCAT(cms_field_fk_text," ") as stringIds')
    ->where('cms_field', $campo->getId())
    ->get('cms_field_fk_text')
    ->row(false);
$fkTexts = array();
if (isset($tableFkFieldsIds->stringIds)) {
    $filtro = $db->select('field, descricao')
        ->where('`id` in (' . $tableFkFieldsIds->stringIds . ')', '', true);
    $fkTexts = (new \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField())->getRows($filtro,false);
}
$dadosAutoComplete = [];
foreach ($tableFkResults as $offset => $value) {
    $dadosAutoComplete[$offset]['id'] = $value->{$campo->getFkid()};
    $valoresDeValue = '';
    foreach ($fkTexts as $key => $fkText) {
        $valoresDeValue .= $fkText->getDescricao() . ': ' . $value->{$fkText->getField()} . (count($fkTexts) > ($key + 1) ? '; ' : '.');
    }
    $dadosAutoComplete[$offset]['value'] = $valoresDeValue;
}
$dadosAutoComplete = json_encode($dadosAutoComplete);
?>
<script>
    var url<?php echo $campo->getField();?> = <?php echo $dadosAutoComplete;?>;

    $(function () {
        var id = '<?php echo $campo->getField() ? $campo->getField() : 0; ?>',
            autoComplete<?php echo ucfirst($campo->getField() ? $campo->getField() : 0); ?> =
                new AutoCompleteAPI(id, url<?php echo $campo->getField();?>, 'id', 'value', 'registros');
        $('#realinput-' + id).change(function () {
            $('.atualizarPorInput-valor<?php echo $campo->getField() ? $campo->getField() : 0; ?>').click();
        });
    });
</script>
<button style="opacity: 0;" class="atualizarPorInput-valor<?php echo $campo->getField() ? $campo->getField() : 0; ?>"
        ng-click="atualizarValorConteudoPorInput('realinput-<?php echo $campo->getField() ? $campo->getField() : 0; ?>', '<?php echo $campo->getField() ? $campo->getField() : 0; ?>','disable', 15);">
</button>
<div class="form-group">
    <div class="col-md-12" style="padding:0;">
        <label for="single" class="control-label"><b><?php echo $campo->getDescricao(); ?></b></label>
    </div>
    <div class="col-md-10" style="padding: 12px 0 0;">
        <input id="<?php echo $campo->getField() ? $campo->getField() : 0; ?>" type="text" class="form-control" placeholder="<?php echo $campo->getDescricao(); ?>" name="<?php echo $campo->getField() ? $campo->getField() : 0; ?>" data-id="<?php echo $conteudo; ?>" >
    </div>
    <div class="col-md-1" style="text-align:center;padding: 0;">
        <a href="<?php echo \Aplicacao\Url::ModuloAdmin('Criadores/ModulosAdministraveis/Index/AdministrarModulo?tipoAdministracaoId=1&editar={{conteudo.' . $campo->getField() . '}}&tabela=' . $campo->getTableFk()); ?>"
           target="_blank">
            <md-button class="md-raised md-primary" ng-value="Primary">
                Acessar
                <i class="fa fa-sign-out" aria-hidden="true"></i>
            </md-button>
        </a>
    </div>
</div>