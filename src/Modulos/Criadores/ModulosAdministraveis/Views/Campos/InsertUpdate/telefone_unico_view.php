<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */
?>
<div class="form-group">
    <label><b><?php echo $campo->getDescricao(); ?></b></label>
    <div class="input-group">
     <span class="input-group-addon">
        <i class="fa fa-phone"></i>
     </span>
        <input
            style="min-width: 135px;"
            type="text"
            name="<?php echo $campo->getField(); ?>"
            class="form-control <?php echo $campo->getField();?>"
            placeholder="Telefone"
            id="maskfone<?php echo $campo->getField(); ?>"
            ng-model="conteudo.<?php echo $campo->getField(); ?>"
            onchange="AtualizarValorTelefone<?php echo $campo->getField();?>()">
    </div>
</div>
<input hidden type="tex" class="telefoneHidden<?php echo $campo->getField(); ?>" value="<?php echo $conteudo;?>">
<button style="opacity: 0;" class="telefone<?php echo $campo->getField(); ?>" ng-click="atualizarValorConteudoPorInput('telefoneHidden<?php echo $campo->getField(); ?>', '<?php echo $campo->getField();?>', 'disable', 15);">
</button>
<script>

    function AtualizarValorTelefone<?php echo $campo->getField();?>(){
        var numeroSemTratamento = $('.<?php echo $campo->getField(); ?>').val();
        var numeroComTratamento = numeroSemTratamento.replace('_','');
        var sizeNumero = numeroComTratamento.length;
        var numeroCompleto = numeroComTratamento;
        if(sizeNumero == 15){
            var partOne = numeroComTratamento.substring(0, 5);
            var partTwo = numeroComTratamento.substring(5, 6);
            var partTre = numeroComTratamento.substring(6, 15);
            numeroCompleto = partOne+partTwo+' '+partTre;
            var splitTrace = numeroCompleto.replace('-','');
            var placeTracePartOne = splitTrace.substring(0, 11);
            var placeTracePartTwo = splitTrace.substring(11, 17);
            numeroCompleto = placeTracePartOne+'-'+placeTracePartTwo;
        }

        $('.telefoneHidden<?php echo $campo->getField(); ?>').val(numeroCompleto);
        $('.telefone<?php echo $campo->getField(); ?>').click();
    }

    $(function(){
        var idselect = document.getElementById("maskfone<?php echo $campo->getField(); ?>");
        var im = new Inputmask("(99) 9999-99999");
        im.mask(idselect);
    });
</script>