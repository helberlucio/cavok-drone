<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */
?>
<div class="form-group">
    <div class="col-md-6" style="padding-left: 0;">
        <div class="input-group">
    <span class="input-group-addon" id="basic-addon1">
        <i class="fa fa-long-arrow-up" aria-hidden="true"></i>
    </span>
            <input
                type="text"
                class="form-control map text-<?php echo $campo->getField(); ?>"
                aria-describedby="basic-addon1"
                id="<?php echo Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?>-latitude"
                placeholder="Latitude">
        </div>
    </div>

    <div class="col-md-6" style="padding-right: 0;">
        <div class="input-group">
    <span class="input-group-addon" id="basic-addon1">
       <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
    </span>
            <input
                type="text"
                class="form-control map text-<?php echo $campo->getField(); ?>"
                aria-describedby="basic-addon1"
                id="<?php echo Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?>-longitude"
                placeholder="longitude">
        </div>
    </div>
    <div class="col-md-12">
        <iframe style="display: none;" class="mapa-<?php echo $campo->getField();?>" src="" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</div>
<script>
    var latitude =  $('#<?php echo Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?>-latitude');
    var longitude =  $('#<?php echo Aplicacao\Conversao::underscoreToCamelCase($campo->getField()); ?>-longitude');

    function reloadMap(){
        var url = "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!2d"+longitude.val()+"!3d"+latitude.val()+"!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef47b6c2083a3%3A0x626a99b9d193501d!2sGoi%C3%A2nia+Shopping!5e0!3m2!1sen!2sbr!4v1487532507554";
        console.log(url);
        $('.mapa-<?php echo $campo->getField();?>').slideDown();
        $('.mapa-<?php echo $campo->getField();?>').attr('src', url);
    }

    $(function(){
       latitude.change(function(){
           reloadMap();
       });
        longitude.change(function(){
            reloadMap();
        });
    });

</script>
