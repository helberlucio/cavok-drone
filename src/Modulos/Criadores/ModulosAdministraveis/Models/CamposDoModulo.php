<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 22/11/2016
 * Time: 20:36
 */

namespace Modulos\Criadores\ModulosAdministraveis\Models;


use Aplicacao\Conversao;
use Aplicacao\Ferramentas\Ajax;
use Aplicacao\Ferramentas\Identificadores;
use Aplicacao\Url;
use Core\Modelos\ModelagemDb;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Modulos\Criadores\Entidades\Models\Gerador;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsExtensoes;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsFieldFkText;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsPlugins;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsTipoCampo;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsTipoCampos;
use Modulos\Criadores\ModulosAdministraveis\Models\OpcoesDoCampo\Init;

class CamposDoModulo
{
    /**
     * @var ModelagemDb
     */
    private $db;
    private $chavePrimaria = '';

    private $colunasDepois = array();
    private $colunasAntes = array();

    private $erros = array();
    /**
     * @var \Illuminate\Database\Schema\Builder
     */
    public $schema;
    public $tableName;

    public function __construct()
    {
        ini_set('max_execution_time', 0);
        $this->tableName = input_angular_http('tabeleName');
        $this->db = new ModelagemDb();
        $this->schema = $this->db->Capsule()->schema();
    }

    public function salvarCampos()
    {
        $this->validarCampos();
        if (!empty($this->erros)) {
            Ajax::retorno(array('level' => 1, 'erros' => $this->erros));
        }
        $this->migracaoTabelaCampos();
    }

    private function SalvarCmsField()
    {
        $moduloId = $this->getModuloId();
        if (!$moduloId) {
            return;
        }

        $cmsField = new CmsField();
        $camposParaExcluir = $cmsField->getRows($this->db->where('cms_modulo_id', $moduloId));

        foreach ($camposParaExcluir as $campoParaExcluir) {
            $this->db
                ->where('cms_field', (int)$campoParaExcluir->getId())
                ->delete('cms_field_fk_text');
            $campoParaExcluir->delete();
        }

        foreach (input_angular_http('config') as $campoConfiguracao) {
            $cmsField->setOrdem(isset($campoConfiguracao->ordem) ? 1 : 0)
                ->setField($campoConfiguracao->campo)
                ->setOrdem($campoConfiguracao->ordem)
                ->setCmsPluginsId((int)$campoConfiguracao->plugin)
                ->setCmsModuloId((int)$moduloId)
                ->setCmsTipoCamposId($campoConfiguracao->tipo)
                ->setDescricao($campoConfiguracao->descricao)
                ->setTamanho(isset($campoConfiguracao->caracters) ? $campoConfiguracao->caracters : 0)
                ->setListar($campoConfiguracao->listar)
                ->setTableFk($campoConfiguracao->tabelafk)
                ->setFkid($campoConfiguracao->campoModulofkid)
                ->setObrigatorio($campoConfiguracao->obrigatorio == 'true' ? 1 : 0);

            $cmsFieldInsertId = $cmsField->insert();

            if (isset($campoConfiguracao->campoModulofktext) && is_array($campoConfiguracao->campoModulofktext) &&
                !empty($campoConfiguracao->campoModulofktext)
            ) {
                $this->salvarFkTexto($cmsFieldInsertId, $campoConfiguracao->tabelafk, $campoConfiguracao->campoModulofktext, $campoConfiguracao);
            }
            if (isset($campoConfiguracao->opcoesDoCampo)) {
                $cmsCampo = (new CmsField())->getRow($this->db->where('id', $cmsFieldInsertId));
                (new Init())->setOpcoesDoCampo($cmsCampo, $campoConfiguracao);
            }
        }
    }

    private function salvarFkTexto($cmsFieldId, $tabelaFk, $fkTextos = array(), $campoConfiguracao)
    {
        $cmsFieldFkText = new CmsFieldFkText();
        $fkModulo = $this->db->where('tabela', $tabelaFk)->get('cms_modulo')->row();
        if (!isset($fkModulo->id)) {
            return;
        }
        $this->db->where('cms_field', $cmsFieldId)->delete($cmsFieldFkText::TABELA);
        $textosInseridosId = array();
        foreach ($fkTextos as $fkTexto) {
            $cmsFieldFk = $this->db
                ->select('id')
                ->where('cms_modulo_id', $fkModulo->id)
                ->where('field', $fkTexto)->get('cms_field')->row();
            $cmsFieldFkId = 0;
            if (isset($cmsFieldFk->id)) {
                $cmsFieldFkId = $cmsFieldFk->id;
            }
            $cmsFieldTextEntidade = $cmsFieldFkText
                ->setCmsField((int)$cmsFieldId)
                ->setCmsFieldFkText((int)$cmsFieldFkId);
            $textosInseridosId[] = $cmsFieldTextEntidade->insert();
        }
        //var_dump($textosInseridosId);
    }

    /**
     * @return Entidades\id
     */
    public function getModuloId()
    {
        $cmsModulo = new CmsModulo();
        return $cmsModulo->getRow($this->db->where('tabela', $this->tableName))->getId();
    }

    private function validarCampos()
    {
        if (!is_array(input_angular_http('config'))) {
            $this->erros[] = 'Os campos devem ser enviados como um array de objetos contendo suas informações necessárias';
        }
        $this->colunasAntes = $this->getColunasByTabela();
        foreach (input_angular_http('config') as $camposConfiguracao) {

            $this->colunasDepois[] = array('value' => $camposConfiguracao->campo);

            if ($camposConfiguracao->campo == '') {
                $this->erros[] = 'Todos os campos devem ser definidos antes de salva-los.';
            }

            if (!isset($camposConfiguracao->ordem) || $camposConfiguracao->ordem < 0) {
                $this->erros[] = 'Defina a ordem do seu campo ' . $camposConfiguracao->campo . ' antes de continuar ("último campo da linha"). ';
            }

            if ($camposConfiguracao->tipo == 0) {
                $this->erros[] = 'O campo ' . $camposConfiguracao->campo . ' Não possui um tipo especificado. Especifique o tipo antes de prosseguir.';
            }

            if ($camposConfiguracao->tipo == 1 && !$this->chavePrimaria == '' && $pkDefinido = true) {
                $this->erros[] = 'O Campo: ' . $camposConfiguracao->campo . ' Não pode ser chave Primaria, pois o cmapo: ' . $this->chavePrimaria . ' já foi definido como chave primaria.';
            }

            if ($camposConfiguracao->tipo == 1 && $this->chavePrimaria == '') {
                $this->chavePrimaria = $camposConfiguracao->campo;
            }
        }
        $this->deletarColunasNaoUtilizadas();

    }

    /**
     * @descriptiom CriarTabela e campos / Alterar seus campos
     */
    private function migracaoTabelaCampos()
    {
        $tableExist = $this->schema->hasTable($this->tableName);
        $tableFunction = $tableExist ? 'table' : 'create';
        $this->schema->{$tableFunction}($this->tableName, function (Blueprint $table) {
            foreach (input_angular_http('config') as $campoConfiguracao) {
                $cmsTipoCampo = (new CmsTipoCampos())->getRow($this->db->where('id', $campoConfiguracao->tipo));
                $columnExist = $this->schema->hasColumn($this->tableName, $campoConfiguracao->campo);
                if ($cmsTipoCampo->getCampoLogico()) {
                    $classe = '\\Modulos\\Criadores\\ModulosAdministraveis\\Models\\CamposLogicos\\' . Conversao::underscoreToCamelCase($cmsTipoCampo->getLaravelDatabaseMigrationCode());
                    if (!class_exists($classe)) {
                        continue;
                    }
                    $campoLogicoClasse = new $classe();
                    $tipo = 'create';
                    if ($columnExist) {
                        $tipo = 'update';
                    }
                    if (!method_exists($campoLogicoClasse, $tipo)) {
                        Ajax::retorno(array('level' => 1, 'erros' => array('Metodo: ' . $tipo . ' da Classe: ' . $classe . 'não foi encontrado')), true);
                        continue;
                    }
                    try {
                        $campoLogicoClasse->{$tipo}($table, $campoConfiguracao->campo, $this->tableName, $campoConfiguracao);
                        continue;
                    } catch (\Exception $e) {
                        Ajax::retorno(array('level' => 1, 'erros' => array($e->getMessage())));
                    }
                }

                $configuracaoTipoDoCampo = $this->getTipoCampoPorId($campoConfiguracao->tipo);
                $configuracaoCampoTamanho = isset($campoConfiguracao->caracters) ? $campoConfiguracao->caracters : 0;
                $tamanhoCampo = $configuracaoTipoDoCampo['tamanho_default'] ? $configuracaoTipoDoCampo['tamanho_default'] : $configuracaoCampoTamanho;

                if (
                    ($configuracaoTipoDoCampo['nullable'] || $campoConfiguracao->obrigatorio) &&
                    ($campoConfiguracao->campo != $this->chavePrimaria)
                ) {
                    if ($columnExist) {
                        if ($configuracaoTipoDoCampo["tipo"] == 'integer') {
                            $table->{$configuracaoTipoDoCampo["tipo"]}($campoConfiguracao->campo, false, true)->length(11)->unsigned()->change();
                        } else {
                            $table->{$configuracaoTipoDoCampo["tipo"]}($campoConfiguracao->campo, $tamanhoCampo)->nullable()->change();
                        }
                    } else {
                        if ($configuracaoTipoDoCampo["tipo"] == 'integer') {
                            $table->{$configuracaoTipoDoCampo["tipo"]}($campoConfiguracao->campo, false, true)->length(11)->unsigned();
                        } else {
                            $table->{$configuracaoTipoDoCampo["tipo"]}($campoConfiguracao->campo, $tamanhoCampo)->nullable();
                        }
                    }
                } else {
                    if ($columnExist) {
                        if ($configuracaoTipoDoCampo["tipo"] == 'integer') {
                            $table->{$configuracaoTipoDoCampo["tipo"]}($campoConfiguracao->campo, false, true)->length(11)->unsigned()->change();
                        } else {
                            $table->{$configuracaoTipoDoCampo["tipo"]}($campoConfiguracao->campo, $tamanhoCampo)->change();
                        }
                    } else {
                        if ($configuracaoTipoDoCampo["tipo"] == 'integer') {
                            $table->{$configuracaoTipoDoCampo["tipo"]}($campoConfiguracao->campo, false, true)->length(11)->unsigned();
                        } else {
                            $table->{$configuracaoTipoDoCampo["tipo"]}($campoConfiguracao->campo, $tamanhoCampo);
                        }
                    }
                }
            }
        });
        $tableExist = $this->schema->hasTable($this->tableName);
        if (!$tableExist) {
            Ajax::retorno(array('level' => 1, 'mensagem' => 'Tabela ' . $this->tableName . ' não foi criada corretamente'));
        }
        $this->SalvarCmsField();
        $this->alterTableCmsSettings();
        $gerador = new Gerador();
        $entidadeGerada = $gerador->gerarEntidade($this->tableName, 'Criadores', 'ModulosAdministraveis');

        if ($entidadeGerada['level'] == 1) {
            Ajax::retorno($entidadeGerada);
        }
        $sucessoMensagens[] = 'Modulo administravel Criado com sucesso!';
        $sucessoMensagens[] = $entidadeGerada['mensagem'];
        $sucessoMensagens[] = 'Estamos te redirecionando para sua página de administração';

        $urlRedirect = Url::ModuloAdmin('Criadores/ModulosAdministraveis/Index/AdministrarModulo?tabela=' . $this->tableName);
        Ajax::retorno(array('level' => 0, 'redirect' => $urlRedirect, 'sucesso' => $sucessoMensagens));

    }

    private $result = false;

    public function alterTableCmsSettings($tablename = false)
    {
        if ($tablename) {
            $this->tableName = $tablename;
        }

        //$col = Identificadores::tipoColuna($tablename, 'cms_usuario_inseriu');

        $this->schema->table($this->tableName, function (Blueprint $table) {
            $colunaCmsUsuarioInseriu = $this->schema->hasColumn($this->tableName, 'cms_usuario_inseriu');
            if (!$colunaCmsUsuarioInseriu) {
                $table->integer('cms_usuario_inseriu', false, true)->nullable();
            }
            $colunaCmsUsuarioEditou = $this->schema->hasColumn($this->tableName, 'cms_usuario_editou');
            if (!$colunaCmsUsuarioEditou) {
                $table->integer('cms_usuario_editou', false, true)->nullable();
            }
            $colunaDataInsert = $this->schema->hasColumn($this->tableName, 'insert_data');
            if (!$colunaDataInsert) {
                $table->timestamp('insert_data')->useCurrent()->nullable();
            }
        });
        $colunaCmsUsuarioInseriu = Identificadores::tipoColuna($this->tableName, 'cms_usuario_inseriu');
        $colunaCmsUsuarioEditou = Identificadores::tipoColuna($this->tableName, 'cms_usuario_editou');
        $colunaDataInsert = Identificadores::tipoColuna($this->tableName, 'insert_data');

        if ((!$colunaCmsUsuarioInseriu) || (!$colunaCmsUsuarioEditou) || (!$colunaDataInsert)) {
            $gerador = new Gerador();
            $classeCriada = $gerador->gerarEntidade($this->tableName, 'Criadores', 'ModulosAdministraveis');
           return $this->result = isset($classeCriada['level']) ? ($classeCriada['level'] == 0 ? true : false) : false;
        }
        return false;
    }

    public $colunaEmAnalize = '';

    /**
     * @description Drop Coluna caso ela não exista para a migração
     */
    private function deletarColunasNaoUtilizadas()
    {
        foreach ($this->colunasAntes as $colunaAntes) {
            $this->colunaEmAnalize = $colunaAntes['value'];
            if (!$this->colunaExisteAposMigracao()) {
                $columnExist = $this->db->Capsule()->schema()->hasColumn($this->tableName, $this->colunaEmAnalize);
                if ($columnExist) {
                    $this->db->query('UPDATE ' . $this->tableName . ' SET `' . $this->colunaEmAnalize . '` = "2017-01-01 10:10:10"');
                    $this->db->query('ALTER TABLE ' . $this->tableName . ' DROP COLUMN ' . $this->colunaEmAnalize);
                }
            }
        }
    }

    /**
     * @description Verifica se a coluna em analize existe para a migracao
     * @return bool
     */
    private function colunaExisteAposMigracao()
    {
        foreach ($this->colunasDepois as $colunaDepois) {
            if ($colunaDepois['value'] == $this->colunaEmAnalize) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return array
     */
    public function getColunasByTabela()
    {
        $output = array();
        $result = $this->db->select('`column_name`')
            ->where('`table_schema`', LI_CONNECT_DATABASE)
            ->where('`table_name`', $this->tableName)
            ->get('`information_schema`.`columns`')
            ->result();

        foreach ($result as $row) {
            $campos['value'] = $row->column_name;
            $output[] = $campos;
        }
        return $output;
    }

    private function getTipoCampoPorId($idTipoCampo)
    {
        $db = $this->db->where('id', $idTipoCampo);
        $tipoCampo = (new CmsTipoCampos())->getRow($db);
        if (!$tipoCampo->getId()) {
            $tipoCampo->setLaravelDatabaseMigrationCode('string');
            $tipoCampo->setLaravelDatabaseMigrationLength(255);
            $tipoCampo->setLaravelDatabaseMigrationNullable(1);
            $tipoCampo->setUnsigned(0);
        }
        return array(
            'tipo' => $tipoCampo->getLaravelDatabaseMigrationCode(),
            'tamanho_default' => $tipoCampo->getLaravelDatabaseMigrationLength(),
            'nullable' => $tipoCampo->getLaravelDatabaseMigrationNullable(),
            'unsigned' => $tipoCampo->getUnsigned()
        );
    }
}