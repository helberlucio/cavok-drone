<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class InstitucionalComunicacaoEntidade extends EntidadeHelp
{
    private $id;
    private $banner;
    private $altura_banner;
    private $banner_mobile;
    private $altura_banner_mobile;
    private $titulo;
    private $chamada;
    private $cms_usuario_inseriu;
    private $cms_usuario_editou;
    private $insert_data;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "institucional_comunicacao";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'InstitucionalComunicacaoEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return InstitucionalComunicacaoEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getBanner()
    {
        return (string)$this->banner;
    }

    /**
     * @param string $banner
     * @return InstitucionalComunicacaoEntidade
     */
    public function setBanner($banner)
    {
        $this->banner = $banner;
        return $this;
    }

    /**
     * @return int
     */
    public function getAlturaBanner()
    {
        return (int)$this->altura_banner;
    }

    /**
     * @param int $altura_banner
     * @return InstitucionalComunicacaoEntidade
     */
    public function setAlturaBanner($altura_banner)
    {
        $this->altura_banner = $altura_banner;
        return $this;
    }

    /**
     * @return string
     */
    public function getBannerMobile()
    {
        return (string)$this->banner_mobile;
    }

    /**
     * @param string $banner_mobile
     * @return InstitucionalComunicacaoEntidade
     */
    public function setBannerMobile($banner_mobile)
    {
        $this->banner_mobile = $banner_mobile;
        return $this;
    }

    /**
     * @return int
     */
    public function getAlturaBannerMobile()
    {
        return (int)$this->altura_banner_mobile;
    }

    /**
     * @param int $altura_banner_mobile
     * @return InstitucionalComunicacaoEntidade
     */
    public function setAlturaBannerMobile($altura_banner_mobile)
    {
        $this->altura_banner_mobile = $altura_banner_mobile;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return (string)$this->titulo;
    }

    /**
     * @param string $titulo
     * @return InstitucionalComunicacaoEntidade
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getChamada()
    {
        return (string)$this->chamada;
    }

    /**
     * @param string $chamada
     * @return InstitucionalComunicacaoEntidade
     */
    public function setChamada($chamada)
    {
        $this->chamada = $chamada;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioInseriu()
    {
        return (int)$this->cms_usuario_inseriu;
    }

    /**
     * @param int $cms_usuario_inseriu
     * @return InstitucionalComunicacaoEntidade
     */
    public function setCmsUsuarioInseriu($cms_usuario_inseriu)
    {
        $this->cms_usuario_inseriu = $cms_usuario_inseriu;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioEditou()
    {
        return (int)$this->cms_usuario_editou;
    }

    /**
     * @param int $cms_usuario_editou
     * @return InstitucionalComunicacaoEntidade
     */
    public function setCmsUsuarioEditou($cms_usuario_editou)
    {
        $this->cms_usuario_editou = $cms_usuario_editou;
        return $this;
    }

    /**
     * @return string
     */
    public function getInsertData()
    {
        return (string)$this->insert_data;
    }

    /**
     * @param string $insert_data
     * @return InstitucionalComunicacaoEntidade
     */
    public function setInsertData($insert_data)
    {
        $this->insert_data = $insert_data;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'banner' => 0,
            'altura_banner' => 0,
            'banner_mobile' => 0,
            'altura_banner_mobile' => 0,
            'titulo' => 0,
            'chamada' => 0,
            'cms_usuario_inseriu' => 0,
            'cms_usuario_editou' => 0,
            'insert_data' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'banner' => 'string',
            'altura_banner' => 'int',
            'banner_mobile' => 'string',
            'altura_banner_mobile' => 'int',
            'titulo' => 'string',
            'chamada' => 'string',
            'cms_usuario_inseriu' => 'int',
            'cms_usuario_editou' => 'int',
            'insert_data' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'institucional_comunicacao' => array(
                'id',
                'banner',
                'altura_banner',
                'banner_mobile',
                'altura_banner_mobile',
                'titulo',
                'chamada',
                'cms_usuario_inseriu',
                'cms_usuario_editou',
                'insert_data',
            ),
        );

    }

}

