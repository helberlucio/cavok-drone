<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class SystemAlinhamento extends EntidadeHelp
{
    private $id;
    private $lado;
    private $codigo;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "system_alinhamento";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'SystemAlinhamento';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return SystemAlinhamento
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getLado()
    {
        return (string)$this->lado;
    }

    /**
     * @param string $lado
     * @return SystemAlinhamento
     */
    public function setLado($lado)
    {
        $this->lado = $lado;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodigo()
    {
        return (string)$this->codigo;
    }

    /**
     * @param string $codigo
     * @return SystemAlinhamento
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'lado' => 0,
            'codigo' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'lado' => 'string',
            'codigo' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'system_alinhamento' => array(
                'id',
                'lado',
                'codigo',
            ),
        );

    }

}