<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class SysDesignAdmin extends EntidadeHelp
{
    private $id;
    private $logo;
    private $cor_topo;
    private $cor_background_menu_lateral;
    private $cor_fonte_menu_lateral;
    private $cor_menu_ativo;
    private $cor_menu_hover;
    private $cor_drop_down;
    private $cms_usuario_inseriu;
    private $cms_usuario_editou;
    private $insert_data;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "sys_design_admin";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'SysDesignAdmin';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return SysDesignAdmin
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return (string)$this->logo;
    }

    /**
     * @param string $logo
     * @return SysDesignAdmin
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
        return $this;
    }

    /**
     * @return string
     */
    public function getCorTopo()
    {
        return (string)$this->cor_topo;
    }

    /**
     * @param string $cor_topo
     * @return SysDesignAdmin
     */
    public function setCorTopo($cor_topo)
    {
        $this->cor_topo = $cor_topo;
        return $this;
    }

    /**
     * @return string
     */
    public function getCorBackgroundMenuLateral()
    {
        return (string)$this->cor_background_menu_lateral;
    }

    /**
     * @param string $cor_background_menu_lateral
     * @return SysDesignAdmin
     */
    public function setCorBackgroundMenuLateral($cor_background_menu_lateral)
    {
        $this->cor_background_menu_lateral = $cor_background_menu_lateral;
        return $this;
    }

    /**
     * @return string
     */
    public function getCorFonteMenuLateral()
    {
        return (string)$this->cor_fonte_menu_lateral;
    }

    /**
     * @param string $cor_fonte_menu_lateral
     * @return SysDesignAdmin
     */
    public function setCorFonteMenuLateral($cor_fonte_menu_lateral)
    {
        $this->cor_fonte_menu_lateral = $cor_fonte_menu_lateral;
        return $this;
    }

    /**
     * @return string
     */
    public function getCorMenuAtivo()
    {
        return (string)$this->cor_menu_ativo;
    }

    /**
     * @param string $cor_menu_ativo
     * @return SysDesignAdmin
     */
    public function setCorMenuAtivo($cor_menu_ativo)
    {
        $this->cor_menu_ativo = $cor_menu_ativo;
        return $this;
    }

    /**
     * @return string
     */
    public function getCorMenuHover()
    {
        return (string)$this->cor_menu_hover;
    }

    /**
     * @param string $cor_menu_hover
     * @return SysDesignAdmin
     */
    public function setCorMenuHover($cor_menu_hover)
    {
        $this->cor_menu_hover = $cor_menu_hover;
        return $this;
    }

    /**
     * @return string
     */
    public function getCorDropDown()
    {
        return (string)$this->cor_drop_down;
    }

    /**
     * @param string $cor_drop_down
     * @return SysDesignAdmin
     */
    public function setCorDropDown($cor_drop_down)
    {
        $this->cor_drop_down = $cor_drop_down;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioInseriu()
    {
        return (int)$this->cms_usuario_inseriu;
    }

    /**
     * @param int $cms_usuario_inseriu
     * @return SysDesignAdmin
     */
    public function setCmsUsuarioInseriu($cms_usuario_inseriu)
    {
        $this->cms_usuario_inseriu = $cms_usuario_inseriu;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioEditou()
    {
        return (int)$this->cms_usuario_editou;
    }

    /**
     * @param int $cms_usuario_editou
     * @return SysDesignAdmin
     */
    public function setCmsUsuarioEditou($cms_usuario_editou)
    {
        $this->cms_usuario_editou = $cms_usuario_editou;
        return $this;
    }

    /**
     * @return string
     */
    public function getInsertData()
    {
        return (string)$this->insert_data;
    }

    /**
     * @param string $insert_data
     * @return SysDesignAdmin
     */
    public function setInsertData($insert_data)
    {
        $this->insert_data = $insert_data;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'logo' => 0,
            'cor_topo' => 0,
            'cor_background_menu_lateral' => 0,
            'cor_fonte_menu_lateral' => 0,
            'cor_menu_ativo' => 0,
            'cor_menu_hover' => 0,
            'cor_drop_down' => 0,
            'cms_usuario_inseriu' => 0,
            'cms_usuario_editou' => 0,
            'insert_data' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'logo' => 'string',
            'cor_topo' => 'string',
            'cor_background_menu_lateral' => 'string',
            'cor_fonte_menu_lateral' => 'string',
            'cor_menu_ativo' => 'string',
            'cor_menu_hover' => 'string',
            'cor_drop_down' => 'string',
            'cms_usuario_inseriu' => 'int',
            'cms_usuario_editou' => 'int',
            'insert_data' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'sys_design_admin' => array(
                'id',
                'logo',
                'cor_topo',
                'cor_background_menu_lateral',
                'cor_fonte_menu_lateral',
                'cor_menu_ativo',
                'cor_menu_hover',
                'cor_drop_down',
                'cms_usuario_inseriu',
                'cms_usuario_editou',
                'insert_data',
            ),
        );

    }

}
