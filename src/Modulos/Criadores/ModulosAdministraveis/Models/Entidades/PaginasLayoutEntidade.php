<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class PaginasLayoutEntidade extends EntidadeHelp
{
    private $id;
    private $layout;
    private $titulo;
    private $layout_mobile;
    private $largura;
    private $ordem;
    private $cms_usuario_inseriu;
    private $cms_usuario_editou;
    private $insert_data;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "paginas_layout";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'PaginasLayoutEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return PaginasLayoutEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getLayout()
    {
        return (string)$this->layout;
    }

    /**
     * @param string $layout
     * @return PaginasLayoutEntidade
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return (string)$this->titulo;
    }

    /**
     * @param string $titulo
     * @return PaginasLayoutEntidade
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getLayoutMobile()
    {
        return (string)$this->layout_mobile;
    }

    /**
     * @param string $layout_mobile
     * @return PaginasLayoutEntidade
     */
    public function setLayoutMobile($layout_mobile)
    {
        $this->layout_mobile = $layout_mobile;
        return $this;
    }

    /**
     * @return int
     */
    public function getLargura()
    {
        return (int)$this->largura;
    }

    /**
     * @param int $largura
     * @return PaginasLayoutEntidade
     */
    public function setLargura($largura)
    {
        $this->largura = $largura;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrdem()
    {
        return (int)$this->ordem;
    }

    /**
     * @param int $ordem
     * @return PaginasLayoutEntidade
     */
    public function setOrdem($ordem)
    {
        $this->ordem = $ordem;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioInseriu()
    {
        return (int)$this->cms_usuario_inseriu;
    }

    /**
     * @param int $cms_usuario_inseriu
     * @return PaginasLayoutEntidade
     */
    public function setCmsUsuarioInseriu($cms_usuario_inseriu)
    {
        $this->cms_usuario_inseriu = $cms_usuario_inseriu;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioEditou()
    {
        return (int)$this->cms_usuario_editou;
    }

    /**
     * @param int $cms_usuario_editou
     * @return PaginasLayoutEntidade
     */
    public function setCmsUsuarioEditou($cms_usuario_editou)
    {
        $this->cms_usuario_editou = $cms_usuario_editou;
        return $this;
    }

    /**
     * @return string
     */
    public function getInsertData()
    {
        return (string)$this->insert_data;
    }

    /**
     * @param string $insert_data
     * @return PaginasLayoutEntidade
     */
    public function setInsertData($insert_data)
    {
        $this->insert_data = $insert_data;
        return $this;
    }

    /**
     * @return \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\RelationshipLayoutsPaginasLayout[]
     */
    public function getRelationshipLayoutsPaginasLayout()
    {
        $db = new ModelagemDb;
        $db->where("paginas_layout_id", $this->getId());
        $RelationshipLayoutsPaginasLayout = (new RelationshipLayoutsPaginasLayout())->getRows($db);
        return $RelationshipLayoutsPaginasLayout;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'layout' => 0,
            'titulo' => 0,
            'layout_mobile' => 0,
            'largura' => 0,
            'ordem' => 0,
            'cms_usuario_inseriu' => 0,
            'cms_usuario_editou' => 0,
            'insert_data' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'layout' => 'string',
            'titulo' => 'string',
            'layout_mobile' => 'string',
            'largura' => 'int',
            'ordem' => 'int',
            'cms_usuario_inseriu' => 'int',
            'cms_usuario_editou' => 'int',
            'insert_data' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'paginas_layout' => array(
                'id',
                'layout',
                'titulo',
                'layout_mobile',
                'largura',
                'ordem',
                'cms_usuario_inseriu',
                'cms_usuario_editou',
                'insert_data',
            ),
        );

    }

}

