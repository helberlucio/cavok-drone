<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class RelationshipAcegImoveisAcegImoveisImagensEntidade extends EntidadeHelp
{
    private $id;
    private $aceg_imoveis_id;
    private $aceg_imoveis_imagens_id;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "relationship_aceg_imoveis_aceg_imoveis_imagens";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'RelationshipAcegImoveisAcegImoveisImagensEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return RelationshipAcegImoveisAcegImoveisImagensEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getAcegImoveisId()
    {
        return (int)$this->aceg_imoveis_id;
    }

    /**
     * @param int $aceg_imoveis_id
     * @return RelationshipAcegImoveisAcegImoveisImagensEntidade
     */
    public function setAcegImoveisId($aceg_imoveis_id)
    {
        $this->aceg_imoveis_id = $aceg_imoveis_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getAcegImoveisImagensId()
    {
        return (int)$this->aceg_imoveis_imagens_id;
    }

    /**
     * @param int $aceg_imoveis_imagens_id
     * @return RelationshipAcegImoveisAcegImoveisImagensEntidade
     */
    public function setAcegImoveisImagensId($aceg_imoveis_imagens_id)
    {
        $this->aceg_imoveis_imagens_id = $aceg_imoveis_imagens_id;
        return $this;
    }

    /**
     * @return AcegImoveis
     */
    public function getAcegImoveis()
    {
        $db = new ModelagemDb();
        $db->where("aceg_imoveis_id", $this->aceg_imoveis_id);
        $AcegImoveis = ( new AcegImoveis())->getRow($db);
        return $AcegImoveis;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'aceg_imoveis_id' => 0,
            'aceg_imoveis_imagens_id' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'aceg_imoveis_id' => 'int',
            'aceg_imoveis_imagens_id' => 'int',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'relationship_aceg_imoveis_aceg_imoveis_imagens' => array(
                'id',
                'aceg_imoveis_id',
                'aceg_imoveis_imagens_id',
            ),
        );

    }

}

