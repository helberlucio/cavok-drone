<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class SiteQuestoesEntidade extends EntidadeHelp
{
    private $id;
    private $ordem;
    private $pergunta;
    private $resposta_a;
    private $resposta_b;
    private $resposta_c;
    private $resposta_d;
    private $resposta_correta;
    private $curso;
    private $cms_usuario_inseriu;
    private $cms_usuario_editou;
    private $insert_data;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "site_questoes";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'SiteQuestoesEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return SiteQuestoesEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrdem()
    {
        return (int)$this->ordem;
    }

    /**
     * @param int $ordem
     * @return SiteQuestoesEntidade
     */
    public function setOrdem($ordem)
    {
        $this->ordem = $ordem;
        return $this;
    }

    /**
     * @return string
     */
    public function getPergunta()
    {
        return (string)$this->pergunta;
    }

    /**
     * @param string $pergunta
     * @return SiteQuestoesEntidade
     */
    public function setPergunta($pergunta)
    {
        $this->pergunta = $pergunta;
        return $this;
    }

    /**
     * @return string
     */
    public function getRespostaA()
    {
        return (string)$this->resposta_a;
    }

    /**
     * @param string $resposta_a
     * @return SiteQuestoesEntidade
     */
    public function setRespostaA($resposta_a)
    {
        $this->resposta_a = $resposta_a;
        return $this;
    }

    /**
     * @return string
     */
    public function getRespostaB()
    {
        return (string)$this->resposta_b;
    }

    /**
     * @param string $resposta_b
     * @return SiteQuestoesEntidade
     */
    public function setRespostaB($resposta_b)
    {
        $this->resposta_b = $resposta_b;
        return $this;
    }

    /**
     * @return string
     */
    public function getRespostaC()
    {
        return (string)$this->resposta_c;
    }

    /**
     * @param string $resposta_c
     * @return SiteQuestoesEntidade
     */
    public function setRespostaC($resposta_c)
    {
        $this->resposta_c = $resposta_c;
        return $this;
    }

    /**
     * @return string
     */
    public function getRespostaD()
    {
        return (string)$this->resposta_d;
    }

    /**
     * @param string $resposta_d
     * @return SiteQuestoesEntidade
     */
    public function setRespostaD($resposta_d)
    {
        $this->resposta_d = $resposta_d;
        return $this;
    }

    /**
     * @return string
     */
    public function getRespostaCorreta()
    {
        return (string)$this->resposta_correta;
    }

    /**
     * @param string $resposta_correta
     * @return SiteQuestoesEntidade
     */
    public function setRespostaCorreta($resposta_correta)
    {
        $this->resposta_correta = $resposta_correta;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurso()
    {
        return (string)$this->curso;
    }

    /**
     * @param string $curso
     * @return SiteQuestoesEntidade
     */
    public function setCurso($curso)
    {
        $this->curso = $curso;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioInseriu()
    {
        return (int)$this->cms_usuario_inseriu;
    }

    /**
     * @param int $cms_usuario_inseriu
     * @return SiteQuestoesEntidade
     */
    public function setCmsUsuarioInseriu($cms_usuario_inseriu)
    {
        $this->cms_usuario_inseriu = $cms_usuario_inseriu;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioEditou()
    {
        return (int)$this->cms_usuario_editou;
    }

    /**
     * @param int $cms_usuario_editou
     * @return SiteQuestoesEntidade
     */
    public function setCmsUsuarioEditou($cms_usuario_editou)
    {
        $this->cms_usuario_editou = $cms_usuario_editou;
        return $this;
    }

    /**
     * @return string
     */
    public function getInsertData()
    {
        return (string)$this->insert_data;
    }

    /**
     * @param string $insert_data
     * @return SiteQuestoesEntidade
     */
    public function setInsertData($insert_data)
    {
        $this->insert_data = $insert_data;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'ordem' => 0,
            'pergunta' => 0,
            'resposta_a' => 0,
            'resposta_b' => 0,
            'resposta_c' => 0,
            'resposta_d' => 0,
            'resposta_correta' => 0,
            'curso' => 0,
            'cms_usuario_inseriu' => 0,
            'cms_usuario_editou' => 0,
            'insert_data' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'ordem' => 'int',
            'pergunta' => 'string',
            'resposta_a' => 'string',
            'resposta_b' => 'string',
            'resposta_c' => 'string',
            'resposta_d' => 'string',
            'resposta_correta' => 'string',
            'curso' => 'string',
            'cms_usuario_inseriu' => 'int',
            'cms_usuario_editou' => 'int',
            'insert_data' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'site_questoes' => array(
                'id',
                'ordem',
                'pergunta',
                'resposta_a',
                'resposta_b',
                'resposta_c',
                'resposta_d',
                'resposta_correta',
                'curso',
                'cms_usuario_inseriu',
                'cms_usuario_editou',
                'insert_data',
            ),
        );

    }

}

