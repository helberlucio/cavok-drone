<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class SiteHomeInternetParaEntidade extends EntidadeHelp
{
    private $id;
    private $titulo;
    private $subtitulo;
    private $empresa;
    private $residencia;
    private $alta_velocidade;
    private $cms_usuario_inseriu;
    private $cms_usuario_editou;
    private $insert_data;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "site_home_internet_para";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'SiteHomeInternetParaEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return SiteHomeInternetParaEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return (string)$this->titulo;
    }

    /**
     * @param string $titulo
     * @return SiteHomeInternetParaEntidade
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubtitulo()
    {
        return (string)$this->subtitulo;
    }

    /**
     * @param string $subtitulo
     * @return SiteHomeInternetParaEntidade
     */
    public function setSubtitulo($subtitulo)
    {
        $this->subtitulo = $subtitulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmpresa()
    {
        return (string)$this->empresa;
    }

    /**
     * @param string $empresa
     * @return SiteHomeInternetParaEntidade
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }

    /**
     * @return string
     */
    public function getResidencia()
    {
        return (string)$this->residencia;
    }

    /**
     * @param string $residencia
     * @return SiteHomeInternetParaEntidade
     */
    public function setResidencia($residencia)
    {
        $this->residencia = $residencia;
        return $this;
    }

    /**
     * @return string
     */
    public function getAltaVelocidade()
    {
        return (string)$this->alta_velocidade;
    }

    /**
     * @param string $alta_velocidade
     * @return SiteHomeInternetParaEntidade
     */
    public function setAltaVelocidade($alta_velocidade)
    {
        $this->alta_velocidade = $alta_velocidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioInseriu()
    {
        return (int)$this->cms_usuario_inseriu;
    }

    /**
     * @param int $cms_usuario_inseriu
     * @return SiteHomeInternetParaEntidade
     */
    public function setCmsUsuarioInseriu($cms_usuario_inseriu)
    {
        $this->cms_usuario_inseriu = $cms_usuario_inseriu;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioEditou()
    {
        return (int)$this->cms_usuario_editou;
    }

    /**
     * @param int $cms_usuario_editou
     * @return SiteHomeInternetParaEntidade
     */
    public function setCmsUsuarioEditou($cms_usuario_editou)
    {
        $this->cms_usuario_editou = $cms_usuario_editou;
        return $this;
    }

    /**
     * @return string
     */
    public function getInsertData()
    {
        return (string)$this->insert_data;
    }

    /**
     * @param string $insert_data
     * @return SiteHomeInternetParaEntidade
     */
    public function setInsertData($insert_data)
    {
        $this->insert_data = $insert_data;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'titulo' => 0,
            'subtitulo' => 0,
            'empresa' => 0,
            'residencia' => 0,
            'alta_velocidade' => 0,
            'cms_usuario_inseriu' => 0,
            'cms_usuario_editou' => 0,
            'insert_data' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'titulo' => 'string',
            'subtitulo' => 'string',
            'empresa' => 'string',
            'residencia' => 'string',
            'alta_velocidade' => 'string',
            'cms_usuario_inseriu' => 'int',
            'cms_usuario_editou' => 'int',
            'insert_data' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'site_home_internet_para' => array(
                'id',
                'titulo',
                'subtitulo',
                'empresa',
                'residencia',
                'alta_velocidade',
                'cms_usuario_inseriu',
                'cms_usuario_editou',
                'insert_data',
            ),
        );

    }

}

