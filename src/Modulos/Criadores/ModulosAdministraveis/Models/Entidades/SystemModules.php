<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
class SystemModules
{
    const TABELA = 'system_modules';
    const PREFIXO = '';
    const NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
    const FILE_NAME = 'SystemModules';

    private $sm_id;
    private $system_group_module_id;
    private $sm_titulo;

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getSmId()
    {
        return (int)$this->sm_id;
    }

    /**
     * @param int $sm_id
     * @return SystemModules
     */
    public function setSmId($sm_id)
    {
        $this->sm_id = $sm_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getSystemGroupModuleId()
    {
        return (int)$this->system_group_module_id;
    }

    /**
     * @param int $system_group_module_id
     * @return SystemModules
     */
    public function setSystemGroupModuleId($system_group_module_id)
    {
        $this->system_group_module_id = $system_group_module_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getSmTitulo()
    {
        return (string)$this->sm_titulo;
    }

    /**
     * @param string $sm_titulo
     * @return SystemModules
     */
    public function setSmTitulo($sm_titulo)
    {
        $this->sm_titulo = $sm_titulo;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'sm_id' => 0,
            'system_group_module_id' => 0,
            'sm_titulo' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'sm_id' => 'int',
            'system_group_module_id' => 'int',
            'sm_titulo' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'system_modules' => array(
                'sm_id',
                'system_group_module_id',
                'sm_titulo',
            ),
        );

    }


    /**
     * @param ModelagemDb $objectFilter
     * @return $this
     */
    public function getRow(ModelagemDb $objectFilter)
    {
        $obj = $objectFilter->get(self::TABELA)->row();
        $thisClass = "\\". self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $classe = Conversao::objetoParaClasse($obj, new $thisClass);
        return $classe;
    }

    /**
     * @param ModelagemDb $objectFilter
     * @return $this[]
     */
    public function getRows(ModelagemDb $objectFilter)
    {
        $classe = array();
        $objects = $objectFilter->get(self::TABELA)->result();
        $thisClass = "\\". self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        foreach ($objects as $row)
        {
            $classe[] = Conversao::objetoParaClasse($row, new $thisClass);
        }
        return $classe;
    }

    public function update()
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $data = GenericRepository::createSaveData($thisClass, $this);
        $db->update(self::TABELA, $data);
    }

    public function delete()
    {
        $db = new ModelagemDb();
        $data["sm_id"] = $this->getSmId();
        $db->delete(self::TABELA, $data);
    }

    /**
     * @return int
     */
    public function insert()
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $data = GenericRepository::createSaveData($thisClass, $this);
        return $db->insert(self::TABELA, $data);
    }

}