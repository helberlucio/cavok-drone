<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class SiteHomeBemVindoEntidade extends EntidadeHelp
{
    private $id;
    private $img_missao;
    private $titulo_missao;
    private $texto_missao;
    private $url_missao;
    private $img_cursos;
    private $titulo_cursos;
    private $texto_cursos;
    private $url_cursos;
    private $cms_usuario_inseriu;
    private $cms_usuario_editou;
    private $insert_data;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "site_home_bem_vindo";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'SiteHomeBemVindoEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return SiteHomeBemVindoEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getImgMissao()
    {
        return (string)$this->img_missao;
    }

    /**
     * @param string $img_missao
     * @return SiteHomeBemVindoEntidade
     */
    public function setImgMissao($img_missao)
    {
        $this->img_missao = $img_missao;
        return $this;
    }

    /**
     * @return string
     */
    public function getTituloMissao()
    {
        return (string)$this->titulo_missao;
    }

    /**
     * @param string $titulo_missao
     * @return SiteHomeBemVindoEntidade
     */
    public function setTituloMissao($titulo_missao)
    {
        $this->titulo_missao = $titulo_missao;
        return $this;
    }

    /**
     * @return string
     */
    public function getTextoMissao()
    {
        return (string)$this->texto_missao;
    }

    /**
     * @param string $texto_missao
     * @return SiteHomeBemVindoEntidade
     */
    public function setTextoMissao($texto_missao)
    {
        $this->texto_missao = $texto_missao;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrlMissao()
    {
        return (string)$this->url_missao;
    }

    /**
     * @param string $url_missao
     * @return SiteHomeBemVindoEntidade
     */
    public function setUrlMissao($url_missao)
    {
        $this->url_missao = $url_missao;
        return $this;
    }

    /**
     * @return string
     */
    public function getImgCursos()
    {
        return (string)$this->img_cursos;
    }

    /**
     * @param string $img_cursos
     * @return SiteHomeBemVindoEntidade
     */
    public function setImgCursos($img_cursos)
    {
        $this->img_cursos = $img_cursos;
        return $this;
    }

    /**
     * @return string
     */
    public function getTituloCursos()
    {
        return (string)$this->titulo_cursos;
    }

    /**
     * @param string $titulo_cursos
     * @return SiteHomeBemVindoEntidade
     */
    public function setTituloCursos($titulo_cursos)
    {
        $this->titulo_cursos = $titulo_cursos;
        return $this;
    }

    /**
     * @return string
     */
    public function getTextoCursos()
    {
        return (string)$this->texto_cursos;
    }

    /**
     * @param string $texto_cursos
     * @return SiteHomeBemVindoEntidade
     */
    public function setTextoCursos($texto_cursos)
    {
        $this->texto_cursos = $texto_cursos;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrlCursos()
    {
        return (string)$this->url_cursos;
    }

    /**
     * @param string $url_cursos
     * @return SiteHomeBemVindoEntidade
     */
    public function setUrlCursos($url_cursos)
    {
        $this->url_cursos = $url_cursos;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioInseriu()
    {
        return (int)$this->cms_usuario_inseriu;
    }

    /**
     * @param int $cms_usuario_inseriu
     * @return SiteHomeBemVindoEntidade
     */
    public function setCmsUsuarioInseriu($cms_usuario_inseriu)
    {
        $this->cms_usuario_inseriu = $cms_usuario_inseriu;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioEditou()
    {
        return (int)$this->cms_usuario_editou;
    }

    /**
     * @param int $cms_usuario_editou
     * @return SiteHomeBemVindoEntidade
     */
    public function setCmsUsuarioEditou($cms_usuario_editou)
    {
        $this->cms_usuario_editou = $cms_usuario_editou;
        return $this;
    }

    /**
     * @return string
     */
    public function getInsertData()
    {
        return (string)$this->insert_data;
    }

    /**
     * @param string $insert_data
     * @return SiteHomeBemVindoEntidade
     */
    public function setInsertData($insert_data)
    {
        $this->insert_data = $insert_data;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'img_missao' => 0,
            'titulo_missao' => 0,
            'texto_missao' => 0,
            'url_missao' => 0,
            'img_cursos' => 0,
            'titulo_cursos' => 0,
            'texto_cursos' => 0,
            'url_cursos' => 0,
            'cms_usuario_inseriu' => 0,
            'cms_usuario_editou' => 0,
            'insert_data' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'img_missao' => 'string',
            'titulo_missao' => 'string',
            'texto_missao' => 'string',
            'url_missao' => 'string',
            'img_cursos' => 'string',
            'titulo_cursos' => 'string',
            'texto_cursos' => 'string',
            'url_cursos' => 'string',
            'cms_usuario_inseriu' => 'int',
            'cms_usuario_editou' => 'int',
            'insert_data' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'site_home_bem_vindo' => array(
                'id',
                'img_missao',
                'titulo_missao',
                'texto_missao',
                'url_missao',
                'img_cursos',
                'titulo_cursos',
                'texto_cursos',
                'url_cursos',
                'cms_usuario_inseriu',
                'cms_usuario_editou',
                'insert_data',
            ),
        );

    }

}

