<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class SysPagesEntidade extends EntidadeHelp
{
    private $id;
    private $banner;
    private $titulo;
    private $descricao;
    private $palavras_chaves;
    private $pagina;
    private $largura_banner;
    private $altura_banner;
    private $cms_usuario_inseriu;
    private $cms_usuario_editou;
    private $insert_data;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "sys_pages";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'SysPagesEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return SysPagesEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getBanner()
    {
        return (string)$this->banner;
    }

    /**
     * @param string $banner
     * @return SysPagesEntidade
     */
    public function setBanner($banner)
    {
        $this->banner = $banner;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return (string)$this->titulo;
    }

    /**
     * @param string $titulo
     * @return SysPagesEntidade
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return (string)$this->descricao;
    }

    /**
     * @param string $descricao
     * @return SysPagesEntidade
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * @return string
     */
    public function getPalavrasChaves()
    {
        return (string)$this->palavras_chaves;
    }

    /**
     * @param string $palavras_chaves
     * @return SysPagesEntidade
     */
    public function setPalavrasChaves($palavras_chaves)
    {
        $this->palavras_chaves = $palavras_chaves;
        return $this;
    }

    /**
     * @return string
     */
    public function getPagina()
    {
        return (string)$this->pagina;
    }

    /**
     * @param string $pagina
     * @return SysPagesEntidade
     */
    public function setPagina($pagina)
    {
        $this->pagina = $pagina;
        return $this;
    }

    /**
     * @return int
     */
    public function getLarguraBanner()
    {
        return (int)$this->largura_banner;
    }

    /**
     * @param int $largura_banner
     * @return SysPagesEntidade
     */
    public function setLarguraBanner($largura_banner)
    {
        $this->largura_banner = $largura_banner;
        return $this;
    }

    /**
     * @return int
     */
    public function getAlturaBanner()
    {
        return (int)$this->altura_banner;
    }

    /**
     * @param int $altura_banner
     * @return SysPagesEntidade
     */
    public function setAlturaBanner($altura_banner)
    {
        $this->altura_banner = $altura_banner;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioInseriu()
    {
        return (int)$this->cms_usuario_inseriu;
    }

    /**
     * @param int $cms_usuario_inseriu
     * @return SysPagesEntidade
     */
    public function setCmsUsuarioInseriu($cms_usuario_inseriu)
    {
        $this->cms_usuario_inseriu = $cms_usuario_inseriu;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioEditou()
    {
        return (int)$this->cms_usuario_editou;
    }

    /**
     * @param int $cms_usuario_editou
     * @return SysPagesEntidade
     */
    public function setCmsUsuarioEditou($cms_usuario_editou)
    {
        $this->cms_usuario_editou = $cms_usuario_editou;
        return $this;
    }

    /**
     * @return string
     */
    public function getInsertData()
    {
        return (string)$this->insert_data;
    }

    /**
     * @param string $insert_data
     * @return SysPagesEntidade
     */
    public function setInsertData($insert_data)
    {
        $this->insert_data = $insert_data;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'banner' => 0,
            'titulo' => 0,
            'descricao' => 0,
            'palavras_chaves' => 0,
            'pagina' => 0,
            'largura_banner' => 0,
            'altura_banner' => 0,
            'cms_usuario_inseriu' => 0,
            'cms_usuario_editou' => 0,
            'insert_data' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'banner' => 'string',
            'titulo' => 'string',
            'descricao' => 'string',
            'palavras_chaves' => 'string',
            'pagina' => 'string',
            'largura_banner' => 'int',
            'altura_banner' => 'int',
            'cms_usuario_inseriu' => 'int',
            'cms_usuario_editou' => 'int',
            'insert_data' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'sys_pages' => array(
                'id',
                'banner',
                'titulo',
                'descricao',
                'palavras_chaves',
                'pagina',
                'largura_banner',
                'altura_banner',
                'cms_usuario_inseriu',
                'cms_usuario_editou',
                'insert_data',
            ),
        );

    }

}

