<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class SiteCursosEntidade extends EntidadeHelp
{
    private $id;
    private $nome;
    private $descricao;
    private $public_alvo;
    private $objetivo;
    private $certificado;
    private $metodologia;
    private $valor_individual;
    private $valor_turma;
    private $duracao_individual;
    private $duracao_turma;
    private $imagem;
    private $ordem;
    private $duracao_individual_dias;
    private $duracao_turma_dias;
    private $link_pagamento;
    private $cms_usuario_inseriu;
    private $cms_usuario_editou;
    private $insert_data;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "site_cursos";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'SiteCursosEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return SiteCursosEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return (string)$this->nome;
    }

    /**
     * @param string $nome
     * @return SiteCursosEntidade
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return (string)$this->descricao;
    }

    /**
     * @param string $descricao
     * @return SiteCursosEntidade
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * @return string
     */
    public function getPublicAlvo()
    {
        return (string)$this->public_alvo;
    }

    /**
     * @param string $public_alvo
     * @return SiteCursosEntidade
     */
    public function setPublicAlvo($public_alvo)
    {
        $this->public_alvo = $public_alvo;
        return $this;
    }

    /**
     * @return string
     */
    public function getObjetivo()
    {
        return (string)$this->objetivo;
    }

    /**
     * @param string $objetivo
     * @return SiteCursosEntidade
     */
    public function setObjetivo($objetivo)
    {
        $this->objetivo = $objetivo;
        return $this;
    }

    /**
     * @return string
     */
    public function getCertificado()
    {
        return (string)$this->certificado;
    }

    /**
     * @param string $certificado
     * @return SiteCursosEntidade
     */
    public function setCertificado($certificado)
    {
        $this->certificado = $certificado;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetodologia()
    {
        return (string)$this->metodologia;
    }

    /**
     * @param string $metodologia
     * @return SiteCursosEntidade
     */
    public function setMetodologia($metodologia)
    {
        $this->metodologia = $metodologia;
        return $this;
    }

    /**
     * @return double
     */
    public function getValorIndividual()
    {
        return (double)$this->valor_individual;
    }

    /**
     * @param double $valor_individual
     * @return SiteCursosEntidade
     */
    public function setValorIndividual($valor_individual)
    {
        $this->valor_individual = $valor_individual;
        return $this;
    }

    /**
     * @return double
     */
    public function getValorTurma()
    {
        return (double)$this->valor_turma;
    }

    /**
     * @param double $valor_turma
     * @return SiteCursosEntidade
     */
    public function setValorTurma($valor_turma)
    {
        $this->valor_turma = $valor_turma;
        return $this;
    }

    /**
     * @return int
     */
    public function getDuracaoIndividual()
    {
        return (int)$this->duracao_individual;
    }

    /**
     * @param int $duracao_individual
     * @return SiteCursosEntidade
     */
    public function setDuracaoIndividual($duracao_individual)
    {
        $this->duracao_individual = $duracao_individual;
        return $this;
    }

    /**
     * @return int
     */
    public function getDuracaoTurma()
    {
        return (int)$this->duracao_turma;
    }

    /**
     * @param int $duracao_turma
     * @return SiteCursosEntidade
     */
    public function setDuracaoTurma($duracao_turma)
    {
        $this->duracao_turma = $duracao_turma;
        return $this;
    }

    /**
     * @return string
     */
    public function getImagem()
    {
        return (string)$this->imagem;
    }

    /**
     * @param string $imagem
     * @return SiteCursosEntidade
     */
    public function setImagem($imagem)
    {
        $this->imagem = $imagem;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrdem()
    {
        return (int)$this->ordem;
    }

    /**
     * @param int $ordem
     * @return SiteCursosEntidade
     */
    public function setOrdem($ordem)
    {
        $this->ordem = $ordem;
        return $this;
    }

    /**
     * @return int
     */
    public function getDuracaoIndividualDias()
    {
        return (int)$this->duracao_individual_dias;
    }

    /**
     * @param int $duracao_individual_dias
     * @return SiteCursosEntidade
     */
    public function setDuracaoIndividualDias($duracao_individual_dias)
    {
        $this->duracao_individual_dias = $duracao_individual_dias;
        return $this;
    }

    /**
     * @return int
     */
    public function getDuracaoTurmaDias()
    {
        return (int)$this->duracao_turma_dias;
    }

    /**
     * @param int $duracao_turma_dias
     * @return SiteCursosEntidade
     */
    public function setDuracaoTurmaDias($duracao_turma_dias)
    {
        $this->duracao_turma_dias = $duracao_turma_dias;
        return $this;
    }

    /**
     * @return string
     */
    public function getLinkPagamento()
    {
        return (string)$this->link_pagamento;
    }

    /**
     * @param string $link_pagamento
     * @return SiteCursosEntidade
     */
    public function setLinkPagamento($link_pagamento)
    {
        $this->link_pagamento = $link_pagamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioInseriu()
    {
        return (int)$this->cms_usuario_inseriu;
    }

    /**
     * @param int $cms_usuario_inseriu
     * @return SiteCursosEntidade
     */
    public function setCmsUsuarioInseriu($cms_usuario_inseriu)
    {
        $this->cms_usuario_inseriu = $cms_usuario_inseriu;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioEditou()
    {
        return (int)$this->cms_usuario_editou;
    }

    /**
     * @param int $cms_usuario_editou
     * @return SiteCursosEntidade
     */
    public function setCmsUsuarioEditou($cms_usuario_editou)
    {
        $this->cms_usuario_editou = $cms_usuario_editou;
        return $this;
    }

    /**
     * @return string
     */
    public function getInsertData()
    {
        return (string)$this->insert_data;
    }

    /**
     * @param string $insert_data
     * @return SiteCursosEntidade
     */
    public function setInsertData($insert_data)
    {
        $this->insert_data = $insert_data;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'nome' => 0,
            'descricao' => 0,
            'public_alvo' => 0,
            'objetivo' => 0,
            'certificado' => 0,
            'metodologia' => 0,
            'valor_individual' => 0,
            'valor_turma' => 0,
            'duracao_individual' => 0,
            'duracao_turma' => 0,
            'imagem' => 0,
            'ordem' => 0,
            'duracao_individual_dias' => 0,
            'duracao_turma_dias' => 0,
            'link_pagamento' => 0,
            'cms_usuario_inseriu' => 0,
            'cms_usuario_editou' => 0,
            'insert_data' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'nome' => 'string',
            'descricao' => 'string',
            'public_alvo' => 'string',
            'objetivo' => 'string',
            'certificado' => 'string',
            'metodologia' => 'string',
            'valor_individual' => 'double',
            'valor_turma' => 'double',
            'duracao_individual' => 'int',
            'duracao_turma' => 'int',
            'imagem' => 'string',
            'ordem' => 'int',
            'duracao_individual_dias' => 'int',
            'duracao_turma_dias' => 'int',
            'link_pagamento' => 'string',
            'cms_usuario_inseriu' => 'int',
            'cms_usuario_editou' => 'int',
            'insert_data' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'site_cursos' => array(
                'id',
                'nome',
                'descricao',
                'public_alvo',
                'objetivo',
                'certificado',
                'metodologia',
                'valor_individual',
                'valor_turma',
                'duracao_individual',
                'duracao_turma',
                'imagem',
                'ordem',
                'duracao_individual_dias',
                'duracao_turma_dias',
                'link_pagamento',
                'cms_usuario_inseriu',
                'cms_usuario_editou',
                'insert_data',
            ),
        );

    }

}

