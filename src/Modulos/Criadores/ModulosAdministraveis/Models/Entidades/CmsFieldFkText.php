<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
class CmsFieldFkText
{
    const TABELA = 'cms_field_fk_text';
    const PREFIXO = 'cms_';
    const NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
    const FILE_NAME = 'CmsFieldFkText';

    private $cms_field;
    private $cms_field_fk_text;

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getCmsField()
    {
        return (int)$this->cms_field;
    }

    /**
     * @param int $cms_field
     * @return CmsFieldFkText
     */
    public function setCmsField($cms_field)
    {
        $this->cms_field = $cms_field;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsFieldFkText()
    {
        return (int)$this->cms_field_fk_text;
    }

    /**
     * @param int $cms_field_fk_text
     * @return CmsFieldFkText
     */
    public function setCmsFieldFkText($cms_field_fk_text)
    {
        $this->cms_field_fk_text = $cms_field_fk_text;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'cms_field' => 0,
            'cms_field_fk_text' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'cms_field' => 'int',
            'cms_field_fk_text' => 'int',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'cms_field_fk_text' => array(
                'cms_field',
                'cms_field_fk_text',
            ),
        );

    }


    /**
     * @param ModelagemDb $objectFilter
     * @return $this
     */
    public function getRow(ModelagemDb $objectFilter)
    {
        $obj = $objectFilter->get(self::TABELA)->row();
        $thisClass = "\\". self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $classe = Conversao::objetoParaClasse($obj, new $thisClass);
        return $classe;
    }

    /**
     * @param ModelagemDb $objectFilter
     * @return $this[]
     */
    public function getRows(ModelagemDb $objectFilter)
    {
        $classe = array();
        $objects = $objectFilter->get(self::TABELA)->result();
        $thisClass = "\\". self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        foreach ($objects as $row)
        {
            $classe[] = Conversao::objetoParaClasse($row, new $thisClass);
        }
        return $classe;
    }

    public function update()
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $data = GenericRepository::createSaveData($thisClass, $this);
        $db->update(self::TABELA, $data);
    }

    public function delete()
    {
        $db = new ModelagemDb();
        $data[""] = $this->get();
        $db->where("", $this->get())->delete(self::TABELA, $data);
    }

    /**
     * @return int
     */
    public function insert()
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $data = GenericRepository::createSaveData($thisClass, $this);
        return $db->insert(self::TABELA, $data);
    }

}