<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
class CmsFieldOpcoesDoCampo
{
    const TABELA = 'cms_field_opcoes_do_campo';
    const PREFIXO = '';
    const NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
    const FILE_NAME = 'CmsFieldOpcoesDoCampo';

    private $id;
    private $cms_field_id;
    private $cms_modulo_id;
    private $campo;
    private $valor;

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return CmsFieldOpcoesDoCampo
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsFieldId()
    {
        return (int)$this->cms_field_id;
    }

    /**
     * @param int $cms_field_id
     * @return CmsFieldOpcoesDoCampo
     */
    public function setCmsFieldId($cms_field_id)
    {
        $this->cms_field_id = $cms_field_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsModuloId()
    {
        return (int)$this->cms_modulo_id;
    }

    /**
     * @param int $cms_modulo_id
     * @return CmsFieldOpcoesDoCampo
     */
    public function setCmsModuloId($cms_modulo_id)
    {
        $this->cms_modulo_id = $cms_modulo_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCampo()
    {
        return (string)$this->campo;
    }

    /**
     * @param string $campo
     * @return CmsFieldOpcoesDoCampo
     */
    public function setCampo($campo)
    {
        $this->campo = $campo;
        return $this;
    }

    /**
     * @return string
     */
    public function getValor()
    {
        return (string)$this->valor;
    }

    /**
     * @param string $valor
     * @return CmsFieldOpcoesDoCampo
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'cms_field_id' => 0,
            'cms_modulo_id' => 0,
            'campo' => 0,
            'valor' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'cms_field_id' => 'int',
            'cms_modulo_id' => 'int',
            'campo' => 'string',
            'valor' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'cms_field_opcoes_do_campo' => array(
                'id',
                'cms_field_id',
                'cms_modulo_id',
                'campo',
                'valor',
            ),
        );

    }


    /**
     * @param ModelagemDb $objectFilter
     * @return $this
     */
    public function getRow(ModelagemDb $objectFilter)
    {
        $obj = $objectFilter->get(self::TABELA)->row();
        $thisClass = "\\". self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $classe = Conversao::objetoParaClasse($obj, new $thisClass);
        return $classe;
    }

    /**
     * @param ModelagemDb $objectFilter
     * @return $this[]
     */
    public function getRows(ModelagemDb $objectFilter)
    {
        $classe = array();
        $objects = $objectFilter->get(self::TABELA)->result();
        $thisClass = "\\". self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        foreach ($objects as $row)
        {
            $classe[] = Conversao::objetoParaClasse($row, new $thisClass);
        }
        return $classe;
    }

    public function update()
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $data = GenericRepository::createSaveData($thisClass, $this);
        $db->update(self::TABELA, $data);
    }

    public function delete()
    {
        $db = new ModelagemDb();
        $data["id"] = $this->getId();
        $db->where("id", $this->getId())->delete(self::TABELA, $data);
    }

    /**
     * @return int
     */
    public function insert()
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $data = GenericRepository::createSaveData($thisClass, $this);
        return $db->insert(self::TABELA, $data);
    }

}
