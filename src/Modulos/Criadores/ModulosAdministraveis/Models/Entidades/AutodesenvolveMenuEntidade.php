<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class AutodesenvolveMenuEntidade extends EntidadeHelp
{
    private $id;
    private $icone;
    private $nome;
    private $link;
    private $submenu;
    private $ativo;
    private $ordem;
    private $url_amigavel;
    private $escritorio;
    private $target;
    private $cms_usuario_inseriu;
    private $cms_usuario_editou;
    private $insert_data;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "autodesenvolve_menu";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'AutodesenvolveMenuEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return AutodesenvolveMenuEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getIcone()
    {
        return (string)$this->icone;
    }

    /**
     * @param string $icone
     * @return AutodesenvolveMenuEntidade
     */
    public function setIcone($icone)
    {
        $this->icone = $icone;
        return $this;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return (string)$this->nome;
    }

    /**
     * @param string $nome
     * @return AutodesenvolveMenuEntidade
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return (string)$this->link;
    }

    /**
     * @param string $link
     * @return AutodesenvolveMenuEntidade
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubmenu()
    {
        return (string)$this->submenu;
    }

    /**
     * @param string $submenu
     * @return AutodesenvolveMenuEntidade
     */
    public function setSubmenu($submenu)
    {
        $this->submenu = $submenu;
        return $this;
    }

    /**
     * @return int
     */
    public function getAtivo()
    {
        return (int)$this->ativo;
    }

    /**
     * @param int $ativo
     * @return AutodesenvolveMenuEntidade
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrdem()
    {
        return (int)$this->ordem;
    }

    /**
     * @param int $ordem
     * @return AutodesenvolveMenuEntidade
     */
    public function setOrdem($ordem)
    {
        $this->ordem = $ordem;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrlAmigavel()
    {
        return (string)$this->url_amigavel;
    }

    /**
     * @param string $url_amigavel
     * @return AutodesenvolveMenuEntidade
     */
    public function setUrlAmigavel($url_amigavel)
    {
        $this->url_amigavel = $url_amigavel;
        return $this;
    }

    /**
     * @return string
     */
    public function getEscritorio()
    {
        return (string)$this->escritorio;
    }

    /**
     * @param string $escritorio
     * @return AutodesenvolveMenuEntidade
     */
    public function setEscritorio($escritorio)
    {
        $this->escritorio = $escritorio;
        return $this;
    }

    /**
     * @return string
     */
    public function getTarget()
    {
        return (string)$this->target;
    }

    /**
     * @param string $target
     * @return AutodesenvolveMenuEntidade
     */
    public function setTarget($target)
    {
        $this->target = $target;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioInseriu()
    {
        return (int)$this->cms_usuario_inseriu;
    }

    /**
     * @param int $cms_usuario_inseriu
     * @return AutodesenvolveMenuEntidade
     */
    public function setCmsUsuarioInseriu($cms_usuario_inseriu)
    {
        $this->cms_usuario_inseriu = $cms_usuario_inseriu;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioEditou()
    {
        return (int)$this->cms_usuario_editou;
    }

    /**
     * @param int $cms_usuario_editou
     * @return AutodesenvolveMenuEntidade
     */
    public function setCmsUsuarioEditou($cms_usuario_editou)
    {
        $this->cms_usuario_editou = $cms_usuario_editou;
        return $this;
    }

    /**
     * @return string
     */
    public function getInsertData()
    {
        return (string)$this->insert_data;
    }

    /**
     * @param string $insert_data
     * @return AutodesenvolveMenuEntidade
     */
    public function setInsertData($insert_data)
    {
        $this->insert_data = $insert_data;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'icone' => 0,
            'nome' => 0,
            'link' => 0,
            'submenu' => 0,
            'ativo' => 0,
            'ordem' => 0,
            'url_amigavel' => 0,
            'escritorio' => 0,
            'target' => 0,
            'cms_usuario_inseriu' => 0,
            'cms_usuario_editou' => 0,
            'insert_data' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'icone' => 'string',
            'nome' => 'string',
            'link' => 'string',
            'submenu' => 'string',
            'ativo' => 'int',
            'ordem' => 'int',
            'url_amigavel' => 'string',
            'escritorio' => 'string',
            'target' => 'string',
            'cms_usuario_inseriu' => 'int',
            'cms_usuario_editou' => 'int',
            'insert_data' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'autodesenvolve_menu' => array(
                'id',
                'icone',
                'nome',
                'link',
                'submenu',
                'ativo',
                'ordem',
                'url_amigavel',
                'escritorio',
                'target',
                'cms_usuario_inseriu',
                'cms_usuario_editou',
                'insert_data',
            ),
        );

    }

}

