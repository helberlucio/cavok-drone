<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class ComunicacaoBlogEntidade extends EntidadeHelp
{
    private $id;
    private $banner;
    private $banner_mobile;
    private $titulo;
    private $sub_titulo;
    private $altura_banner_mobile;
    private $altura_banner;
    private $cms_usuario_inseriu;
    private $cms_usuario_editou;
    private $insert_data;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "comunicacao_blog";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'ComunicacaoBlogEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return ComunicacaoBlogEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getBanner()
    {
        return (string)$this->banner;
    }

    /**
     * @param string $banner
     * @return ComunicacaoBlogEntidade
     */
    public function setBanner($banner)
    {
        $this->banner = $banner;
        return $this;
    }

    /**
     * @return string
     */
    public function getBannerMobile()
    {
        return (string)$this->banner_mobile;
    }

    /**
     * @param string $banner_mobile
     * @return ComunicacaoBlogEntidade
     */
    public function setBannerMobile($banner_mobile)
    {
        $this->banner_mobile = $banner_mobile;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return (string)$this->titulo;
    }

    /**
     * @param string $titulo
     * @return ComunicacaoBlogEntidade
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubTitulo()
    {
        return (string)$this->sub_titulo;
    }

    /**
     * @param string $sub_titulo
     * @return ComunicacaoBlogEntidade
     */
    public function setSubTitulo($sub_titulo)
    {
        $this->sub_titulo = $sub_titulo;
        return $this;
    }

    /**
     * @return int
     */
    public function getAlturaBannerMobile()
    {
        return (int)$this->altura_banner_mobile;
    }

    /**
     * @param int $altura_banner_mobile
     * @return ComunicacaoBlogEntidade
     */
    public function setAlturaBannerMobile($altura_banner_mobile)
    {
        $this->altura_banner_mobile = $altura_banner_mobile;
        return $this;
    }

    /**
     * @return int
     */
    public function getAlturaBanner()
    {
        return (int)$this->altura_banner;
    }

    /**
     * @param int $altura_banner
     * @return ComunicacaoBlogEntidade
     */
    public function setAlturaBanner($altura_banner)
    {
        $this->altura_banner = $altura_banner;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioInseriu()
    {
        return (int)$this->cms_usuario_inseriu;
    }

    /**
     * @param int $cms_usuario_inseriu
     * @return ComunicacaoBlogEntidade
     */
    public function setCmsUsuarioInseriu($cms_usuario_inseriu)
    {
        $this->cms_usuario_inseriu = $cms_usuario_inseriu;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioEditou()
    {
        return (int)$this->cms_usuario_editou;
    }

    /**
     * @param int $cms_usuario_editou
     * @return ComunicacaoBlogEntidade
     */
    public function setCmsUsuarioEditou($cms_usuario_editou)
    {
        $this->cms_usuario_editou = $cms_usuario_editou;
        return $this;
    }

    /**
     * @return string
     */
    public function getInsertData()
    {
        return (string)$this->insert_data;
    }

    /**
     * @param string $insert_data
     * @return ComunicacaoBlogEntidade
     */
    public function setInsertData($insert_data)
    {
        $this->insert_data = $insert_data;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'banner' => 0,
            'banner_mobile' => 0,
            'titulo' => 0,
            'sub_titulo' => 0,
            'altura_banner_mobile' => 0,
            'altura_banner' => 0,
            'cms_usuario_inseriu' => 0,
            'cms_usuario_editou' => 0,
            'insert_data' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'banner' => 'string',
            'banner_mobile' => 'string',
            'titulo' => 'string',
            'sub_titulo' => 'string',
            'altura_banner_mobile' => 'int',
            'altura_banner' => 'int',
            'cms_usuario_inseriu' => 'int',
            'cms_usuario_editou' => 'int',
            'insert_data' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'comunicacao_blog' => array(
                'id',
                'banner',
                'banner_mobile',
                'titulo',
                'sub_titulo',
                'altura_banner_mobile',
                'altura_banner',
                'cms_usuario_inseriu',
                'cms_usuario_editou',
                'insert_data',
            ),
        );

    }

}

