<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
class SystemVoucher
{
    const TABELA = 'system_voucher';
    const PREFIXO = '';
    const NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
    const FILE_NAME = 'SystemVoucher';

    private $id;
    private $tipo;
    private $chave;
    private $maximo_utilizacoes;
    private $valor;
    private $date_time;

    public function __construct()
    {
    }

    /**
     * @return id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return SystemVoucher
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return tipo
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param string $tipo
     * @return SystemVoucher
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
        return $this;
    }

    /**
     * @return chave
     */
    public function getChave()
    {
        return $this->chave;
    }

    /**
     * @param string $chave
     * @return SystemVoucher
     */
    public function setChave($chave)
    {
        $this->chave = $chave;
        return $this;
    }

    /**
     * @return maximo_utilizacoes
     */
    public function getMaximoUtilizacoes()
    {
        return $this->maximo_utilizacoes;
    }

    /**
     * @param string $maximo_utilizacoes
     * @return SystemVoucher
     */
    public function setMaximoUtilizacoes($maximo_utilizacoes)
    {
        $this->maximo_utilizacoes = $maximo_utilizacoes;
        return $this;
    }

    /**
     * @return valor
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param string $valor
     * @return SystemVoucher
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
        return $this;
    }

    /**
     * @return date_time
     */
    public function getDateTime()
    {
        return $this->date_time;
    }

    /**
     * @param string $date_time
     * @return SystemVoucher
     */
    public function setDateTime($date_time)
    {
        $this->date_time = $date_time;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'tipo' => 0,
            'chave' => 0,
            'maximo_utilizacoes' => 0,
            'valor' => 0,
            'date_time' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'string',
            'tipo' => 'string',
            'chave' => 'string',
            'maximo_utilizacoes' => 'string',
            'valor' => 'string',
            'date_time' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'system_voucher' => array(
                'id',
                'tipo',
                'chave',
                'maximo_utilizacoes',
                'valor',
                'date_time',
            ),
        );

    }


    /**
     * @param ModelagemDb $objectFilter
     * @return $this
     */
    public function getRow(ModelagemDb $objectFilter)
    {
        $obj = $objectFilter->get(self::TABELA)->row();
        $thisClass = "\\". self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $classe = Conversao::objetoParaClasse($obj, new $thisClass);
        return $classe;
    }

    /**
     * @param ModelagemDb $objectFilter
     * @return $this[]
     */
    public function getRows(ModelagemDb $objectFilter)
    {
        $classe = array();
        $objects = $objectFilter->get(self::TABELA)->result();
        $thisClass = "\\". self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        foreach ($objects as $row)
        {
            $classe[] = Conversao::objetoParaClasse($row, new $thisClass);
        }
        return $classe;
    }

    public function update()
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $data = GenericRepository::createSaveData($thisClass, $this);
        $db->update(self::TABELA, $data);
    }

    public function delete()
    {
        $db = new ModelagemDb();
        $data["id"] = $this->getId();
        $db->delete(self::TABELA, $data);
    }

    /**
     * @return int
     */
    public function insert()
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $data = GenericRepository::createSaveData($thisClass, $this);
        return $db->insert(self::TABELA, $data);
    }

}