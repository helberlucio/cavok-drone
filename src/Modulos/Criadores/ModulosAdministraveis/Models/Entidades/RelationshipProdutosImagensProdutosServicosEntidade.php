<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class RelationshipProdutosImagensProdutosServicosEntidade extends EntidadeHelp
{
    private $id;
    private $produtos_id;
    private $imagens_produtos_servicos_id;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "relationship_produtos_imagens_produtos_servicos";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'RelationshipProdutosImagensProdutosServicosEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return RelationshipProdutosImagensProdutosServicosEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getProdutosId()
    {
        return (int)$this->produtos_id;
    }

    /**
     * @param int $produtos_id
     * @return RelationshipProdutosImagensProdutosServicosEntidade
     */
    public function setProdutosId($produtos_id)
    {
        $this->produtos_id = $produtos_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getImagensProdutosServicosId()
    {
        return (int)$this->imagens_produtos_servicos_id;
    }

    /**
     * @param int $imagens_produtos_servicos_id
     * @return RelationshipProdutosImagensProdutosServicosEntidade
     */
    public function setImagensProdutosServicosId($imagens_produtos_servicos_id)
    {
        $this->imagens_produtos_servicos_id = $imagens_produtos_servicos_id;
        return $this;
    }

    /**
     * @return Produtos
     */
    public function getProdutos()
    {
        $db = new ModelagemDb();
        $db->where("produtos_id", $this->produtos_id);
        $Produtos = ( new Produtos())->getRow($db);
        return $Produtos;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'produtos_id' => 0,
            'imagens_produtos_servicos_id' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'produtos_id' => 'int',
            'imagens_produtos_servicos_id' => 'int',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'relationship_produtos_imagens_produtos_servicos' => array(
                'id',
                'produtos_id',
                'imagens_produtos_servicos_id',
            ),
        );

    }

}

