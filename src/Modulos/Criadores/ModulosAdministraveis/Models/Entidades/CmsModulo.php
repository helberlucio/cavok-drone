<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
class CmsModulo
{
    const TABELA = 'cms_modulo';
    const PREFIXO = '';
    const NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
    const FILE_NAME = 'CmsModulo';

    private $id;
    private $modulo;
    private $tabela;
    private $anexo;
    private $menu;
    private $icon;
    private $ativo;
    private $cms_modulo_tipos_id;
    private $cms_modulo_layout_id;

    public function __construct()
    {
    }

    /**
     * @return (int)
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return CmsModulo
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getModulo()
    {
        return $this->modulo;
    }

    /**
     * @param string $modulo
     * @return CmsModulo
     */
    public function setModulo($modulo)
    {
        $this->modulo = $modulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getTabela()
    {
        return $this->tabela;
    }

    /**
     * @param string $tabela
     * @return CmsModulo
     */
    public function setTabela($tabela)
    {
        $this->tabela = $tabela;
        return $this;
    }

    /**
     * @return anexo
     */
    public function getAnexo()
    {
        return $this->anexo;
    }

    /**
     * @param string $anexo
     * @return CmsModulo
     */
    public function setAnexo($anexo)
    {
        $this->anexo = $anexo;
        return $this;
    }

    /**
     * @return int
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * @param string $menu
     * @return CmsModulo
     */
    public function setMenu($menu)
    {
        $this->menu = $menu;
        return $this;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     * @return CmsModulo
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return ativo
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * @param string $ativo
     * @return CmsModulo
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
        return $this;
    }

    /**
     * @return cms_modulo_tipos_id
     */
    public function getCmsModuloTiposId()
    {
        return $this->cms_modulo_tipos_id;
    }

    /**
     * @param string $cms_modulo_tipos_id
     * @return CmsModulo
     */
    public function setCmsModuloTiposId($cms_modulo_tipos_id)
    {
        $this->cms_modulo_tipos_id = $cms_modulo_tipos_id;
        return $this;
    }

    /**
     * @return cms_modulo_layout_id
     */
    public function getCmsModuloLayoutId()
    {
        return $this->cms_modulo_layout_id;
    }

    /**
     * @param string $cms_modulo_layout_id
     * @return CmsModulo
     */
    public function setCmsModuloLayoutId($cms_modulo_layout_id)
    {
        $this->cms_modulo_layout_id = $cms_modulo_layout_id;
        return $this;
    }

    /**
     * @return \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsExtensoes[]
     */
    public function getCmsExtensoes()
    {
        $db = new ModelagemDb;
        $db->where("cms_modulo_id", $this->getId());
        $CmsExtensoes = (new CmsExtensoes())->getRows($db);
        return $CmsExtensoes;
    }

    /**
     * @return \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField[]
     */
    public function getCmsField()
    {
        $db = new ModelagemDb;
        $db->where("cms_modulo_id", $this->getId());
        $CmsField = (new CmsField())->getRows($db);
        return $CmsField;
    }

    /**
     * @return CmsModuloTipos
     */
    public function getCmsModuloTipos()
    {
        $db = new ModelagemDb();
        $db->where("cms_modulo_tipos_id", $this->cms_modulo_tipos_id);
        $CmsModuloTipos = ( new CmsModuloTipos())->getRow($db);
        return $CmsModuloTipos;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'modulo' => 0,
            'tabela' => 0,
            'anexo' => 0,
            'menu' => 0,
            'icon' => 0,
            'ativo' => 0,
            'cms_modulo_tipos_id' => 0,
            'cms_modulo_layout_id' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'string',
            'modulo' => 'string',
            'tabela' => 'string',
            'anexo' => 'string',
            'menu' => 'string',
            'icon' => 'string',
            'ativo' => 'string',
            'cms_modulo_tipos_id' => 'string',
            'cms_modulo_layout_id' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'cms_modulo' => array(
                'id',
                'modulo',
                'tabela',
                'anexo',
                'menu',
                'icon',
                'ativo',
                'cms_modulo_tipos_id',
                'cms_modulo_layout_id',
            ),
        );

    }


    /**
     * @param ModelagemDb $objectFilter
     * @return $this
     */
    public function getRow(ModelagemDb $objectFilter)
    {
        $obj = $objectFilter->get(self::TABELA)->row();
        $thisClass = "\\". self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $classe = Conversao::objetoParaClasse($obj, new $thisClass);
        return $classe;
    }

    /**
     * @param ModelagemDb $objectFilter
     * @return $this[]
     */
    public function getRows(ModelagemDb $objectFilter)
    {
        $classe = array();
        $objects = $objectFilter->get(self::TABELA)->result();
        $thisClass = "\\". self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        foreach ($objects as $row)
        {
            $classe[] = Conversao::objetoParaClasse($row, new $thisClass);
        }
        return $classe;
    }

    public function update()
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $data = GenericRepository::createSaveData($thisClass, $this);
        $db->update(self::TABELA, $data);
    }

    public function delete()
    {
        $db = new ModelagemDb();
        $data["id"] = $this->getId();
        $db->delete(self::TABELA, $data);
    }

    /**
     * @return int
     */
    public function insert()
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $data = GenericRepository::createSaveData($thisClass, $this);
        return $db->insert(self::TABELA, $data);
    }

}
