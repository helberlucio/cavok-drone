<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
class CmsTipoCampo
{
    const TABELA = 'cms_tipo_campo';
    const PREFIXO = '';
    const NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
    const FILE_NAME = 'CmsTipoCampo';

    private $id;
    private $label;
    private $code;
    private $cms_grupo_tipo_campos_id;

    public function __construct()
    {
    }

    /**
     * @return id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return CmsTipoCampo
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return label
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return CmsTipoCampo
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return code
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return CmsTipoCampo
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return cms_grupo_tipo_campos_id
     */
    public function getCmsGrupoTipoCamposId()
    {
        return $this->cms_grupo_tipo_campos_id;
    }

    /**
     * @param string $cms_grupo_tipo_campos_id
     * @return CmsTipoCampo
     */
    public function setCmsGrupoTipoCamposId($cms_grupo_tipo_campos_id)
    {
        $this->cms_grupo_tipo_campos_id = $cms_grupo_tipo_campos_id;
        return $this;
    }

    /**
     * @return \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField[]
     */
    public function getCmsField()
    {
        $db = new ModelagemDb;
        $db->where("cms_tipo_campo_id", $this->getId());
        $CmsField = (new CmsField())->getRows($db);
        return $CmsField;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'label' => 0,
            'code' => 0,
            'cms_grupo_tipo_campos_id' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'string',
            'label' => 'string',
            'code' => 'string',
            'cms_grupo_tipo_campos_id' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'cms_tipo_campo' => array(
                'id',
                'label',
                'code',
                'cms_grupo_tipo_campos_id',
            ),
        );

    }


    /**
     * @param ModelagemDb $objectFilter
     * @return $this
     */
    public function getRow(ModelagemDb $objectFilter)
    {
        $obj = $objectFilter->get(self::TABELA)->row();
        $thisClass = "\\". self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $classe = Conversao::objetoParaClasse($obj, new $thisClass);
        return $classe;
    }

    /**
     * @param ModelagemDb $objectFilter
     * @return $this[]
     */
    public function getRows(ModelagemDb $objectFilter)
    {
        $classe = array();
        $objects = $objectFilter->get(self::TABELA)->result();
        $thisClass = "\\". self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        foreach ($objects as $row)
        {
            $classe[] = Conversao::objetoParaClasse($row, new $thisClass);
        }
        return $classe;
    }

    public function update()
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $data = GenericRepository::createSaveData($thisClass, $this);
        $db->update(self::TABELA, $data);
    }

    public function delete()
    {
        $db = new ModelagemDb();
        $data["id"] = $this->getId();
        $db->delete(self::TABELA, $data);
    }

    /**
     * @return int
     */
    public function insert()
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $data = GenericRepository::createSaveData($thisClass, $this);
        return $db->insert(self::TABELA, $data);
    }

}
