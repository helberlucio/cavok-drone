<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class RelationshipCmsUsuariosSysSelecaoEscritorioEntidade extends EntidadeHelp
{
    private $id;
    private $cms_usuarios_id;
    private $sys_selecao_escritorio_id;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "relationship_cms_usuarios_sys_selecao_escritorio";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'RelationshipCmsUsuariosSysSelecaoEscritorioEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return RelationshipCmsUsuariosSysSelecaoEscritorioEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuariosId()
    {
        return (int)$this->cms_usuarios_id;
    }

    /**
     * @param int $cms_usuarios_id
     * @return RelationshipCmsUsuariosSysSelecaoEscritorioEntidade
     */
    public function setCmsUsuariosId($cms_usuarios_id)
    {
        $this->cms_usuarios_id = $cms_usuarios_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getSysSelecaoEscritorioId()
    {
        return (int)$this->sys_selecao_escritorio_id;
    }

    /**
     * @param int $sys_selecao_escritorio_id
     * @return RelationshipCmsUsuariosSysSelecaoEscritorioEntidade
     */
    public function setSysSelecaoEscritorioId($sys_selecao_escritorio_id)
    {
        $this->sys_selecao_escritorio_id = $sys_selecao_escritorio_id;
        return $this;
    }

    /**
     * @return SysSelecaoEscritorio
     */
    public function getSysSelecaoEscritorio()
    {
        $db = new ModelagemDb();
        $db->where("sys_selecao_escritorio_id", $this->sys_selecao_escritorio_id);
        $SysSelecaoEscritorio = ( new SysSelecaoEscritorio())->getRow($db);
        return $SysSelecaoEscritorio;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'cms_usuarios_id' => 0,
            'sys_selecao_escritorio_id' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'cms_usuarios_id' => 'int',
            'sys_selecao_escritorio_id' => 'int',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'relationship_cms_usuarios_sys_selecao_escritorio' => array(
                'id',
                'cms_usuarios_id',
                'sys_selecao_escritorio_id',
            ),
        );

    }

}

