<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class SiteCursosOnlineEntidade extends EntidadeHelp
{
    private $id;
    private $imagem;
    private $nome;
    private $descricao;
    private $public_alvo;
    private $objetivo;
    private $certificado;
    private $metodologia;
    private $valor;
    private $ordem;
    private $link_pagamento;
    private $valor_presencial;
    private $link_pagamento_presencial;
    private $certificado_pdf_frente;
    private $certificado_pdf_fundo;
    private $cms_usuario_inseriu;
    private $cms_usuario_editou;
    private $insert_data;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "site_cursos_online";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'SiteCursosOnlineEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return SiteCursosOnlineEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getImagem()
    {
        return (string)$this->imagem;
    }

    /**
     * @param string $imagem
     * @return SiteCursosOnlineEntidade
     */
    public function setImagem($imagem)
    {
        $this->imagem = $imagem;
        return $this;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return (string)$this->nome;
    }

    /**
     * @param string $nome
     * @return SiteCursosOnlineEntidade
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return (string)$this->descricao;
    }

    /**
     * @param string $descricao
     * @return SiteCursosOnlineEntidade
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * @return string
     */
    public function getPublicAlvo()
    {
        return (string)$this->public_alvo;
    }

    /**
     * @param string $public_alvo
     * @return SiteCursosOnlineEntidade
     */
    public function setPublicAlvo($public_alvo)
    {
        $this->public_alvo = $public_alvo;
        return $this;
    }

    /**
     * @return string
     */
    public function getObjetivo()
    {
        return (string)$this->objetivo;
    }

    /**
     * @param string $objetivo
     * @return SiteCursosOnlineEntidade
     */
    public function setObjetivo($objetivo)
    {
        $this->objetivo = $objetivo;
        return $this;
    }

    /**
     * @return string
     */
    public function getCertificado()
    {
        return (string)$this->certificado;
    }

    /**
     * @param string $certificado
     * @return SiteCursosOnlineEntidade
     */
    public function setCertificado($certificado)
    {
        $this->certificado = $certificado;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetodologia()
    {
        return (string)$this->metodologia;
    }

    /**
     * @param string $metodologia
     * @return SiteCursosOnlineEntidade
     */
    public function setMetodologia($metodologia)
    {
        $this->metodologia = $metodologia;
        return $this;
    }

    /**
     * @return double
     */
    public function getValor()
    {
        return (double)$this->valor;
    }

    /**
     * @param double $valor
     * @return SiteCursosOnlineEntidade
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrdem()
    {
        return (int)$this->ordem;
    }

    /**
     * @param int $ordem
     * @return SiteCursosOnlineEntidade
     */
    public function setOrdem($ordem)
    {
        $this->ordem = $ordem;
        return $this;
    }

    /**
     * @return string
     */
    public function getLinkPagamento()
    {
        return (string)$this->link_pagamento;
    }

    /**
     * @param string $link_pagamento
     * @return SiteCursosOnlineEntidade
     */
    public function setLinkPagamento($link_pagamento)
    {
        $this->link_pagamento = $link_pagamento;
        return $this;
    }

    /**
     * @return double
     */
    public function getValorPresencial()
    {
        return (double)$this->valor_presencial;
    }

    /**
     * @param double $valor_presencial
     * @return SiteCursosOnlineEntidade
     */
    public function setValorPresencial($valor_presencial)
    {
        $this->valor_presencial = $valor_presencial;
        return $this;
    }

    /**
     * @return string
     */
    public function getLinkPagamentoPresencial()
    {
        return (string)$this->link_pagamento_presencial;
    }

    /**
     * @param string $link_pagamento_presencial
     * @return SiteCursosOnlineEntidade
     */
    public function setLinkPagamentoPresencial($link_pagamento_presencial)
    {
        $this->link_pagamento_presencial = $link_pagamento_presencial;
        return $this;
    }

    /**
     * @return string
     */
    public function getCertificadoPdfFrente()
    {
        return (string)$this->certificado_pdf_frente;
    }

    /**
     * @param string $certificado_pdf_frente
     * @return SiteCursosOnlineEntidade
     */
    public function setCertificadoPdfFrente($certificado_pdf_frente)
    {
        $this->certificado_pdf_frente = $certificado_pdf_frente;
        return $this;
    }

    /**
     * @return string
     */
    public function getCertificadoPdfFundo()
    {
        return (string)$this->certificado_pdf_fundo;
    }

    /**
     * @param string $certificado_pdf_fundo
     * @return SiteCursosOnlineEntidade
     */
    public function setCertificadoPdfFundo($certificado_pdf_fundo)
    {
        $this->certificado_pdf_fundo = $certificado_pdf_fundo;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioInseriu()
    {
        return (int)$this->cms_usuario_inseriu;
    }

    /**
     * @param int $cms_usuario_inseriu
     * @return SiteCursosOnlineEntidade
     */
    public function setCmsUsuarioInseriu($cms_usuario_inseriu)
    {
        $this->cms_usuario_inseriu = $cms_usuario_inseriu;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioEditou()
    {
        return (int)$this->cms_usuario_editou;
    }

    /**
     * @param int $cms_usuario_editou
     * @return SiteCursosOnlineEntidade
     */
    public function setCmsUsuarioEditou($cms_usuario_editou)
    {
        $this->cms_usuario_editou = $cms_usuario_editou;
        return $this;
    }

    /**
     * @return string
     */
    public function getInsertData()
    {
        return (string)$this->insert_data;
    }

    /**
     * @param string $insert_data
     * @return SiteCursosOnlineEntidade
     */
    public function setInsertData($insert_data)
    {
        $this->insert_data = $insert_data;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'imagem' => 0,
            'nome' => 0,
            'descricao' => 0,
            'public_alvo' => 0,
            'objetivo' => 0,
            'certificado' => 0,
            'metodologia' => 0,
            'valor' => 0,
            'ordem' => 0,
            'link_pagamento' => 0,
            'valor_presencial' => 0,
            'link_pagamento_presencial' => 0,
            'certificado_pdf_frente' => 0,
            'certificado_pdf_fundo' => 0,
            'cms_usuario_inseriu' => 0,
            'cms_usuario_editou' => 0,
            'insert_data' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'imagem' => 'string',
            'nome' => 'string',
            'descricao' => 'string',
            'public_alvo' => 'string',
            'objetivo' => 'string',
            'certificado' => 'string',
            'metodologia' => 'string',
            'valor' => 'double',
            'ordem' => 'int',
            'link_pagamento' => 'string',
            'valor_presencial' => 'double',
            'link_pagamento_presencial' => 'string',
            'certificado_pdf_frente' => 'string',
            'certificado_pdf_fundo' => 'string',
            'cms_usuario_inseriu' => 'int',
            'cms_usuario_editou' => 'int',
            'insert_data' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'site_cursos_online' => array(
                'id',
                'imagem',
                'nome',
                'descricao',
                'public_alvo',
                'objetivo',
                'certificado',
                'metodologia',
                'valor',
                'ordem',
                'link_pagamento',
                'valor_presencial',
                'link_pagamento_presencial',
                'certificado_pdf_frente',
                'certificado_pdf_fundo',
                'cms_usuario_inseriu',
                'cms_usuario_editou',
                'insert_data',
            ),
        );

    }

}

