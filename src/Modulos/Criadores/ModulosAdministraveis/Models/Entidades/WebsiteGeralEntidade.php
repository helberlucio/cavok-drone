<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class WebsiteGeralEntidade extends EntidadeHelp
{
    private $id;
    private $logo;
    private $favicon;
    private $empresa;
    private $contato_telefone;
    private $contato_email;
    private $endereco;
    private $sobre;
    private $palavras_chaves;
    private $whatsapp;
    private $latitude_mapa;
    private $longitude_mapa;
    private $horario;
    private $facebook;
    private $instagram;
    private $youtube;
    private $num_ultimo_certificado;
    private $cms_usuario_inseriu;
    private $cms_usuario_editou;
    private $insert_data;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "website_geral";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'WebsiteGeralEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return WebsiteGeralEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getLogo()
    {
        return (string)$this->logo;
    }

    /**
     * @param string $logo
     * @return WebsiteGeralEntidade
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
        return $this;
    }

    /**
     * @return string
     */
    public function getFavicon()
    {
        return (string)$this->favicon;
    }

    /**
     * @param string $favicon
     * @return WebsiteGeralEntidade
     */
    public function setFavicon($favicon)
    {
        $this->favicon = $favicon;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmpresa()
    {
        return (string)$this->empresa;
    }

    /**
     * @param string $empresa
     * @return WebsiteGeralEntidade
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
        return $this;
    }

    /**
     * @return string
     */
    public function getContatoTelefone()
    {
        return (string)$this->contato_telefone;
    }

    /**
     * @param string $contato_telefone
     * @return WebsiteGeralEntidade
     */
    public function setContatoTelefone($contato_telefone)
    {
        $this->contato_telefone = $contato_telefone;
        return $this;
    }

    /**
     * @return string
     */
    public function getContatoEmail()
    {
        return (string)$this->contato_email;
    }

    /**
     * @param string $contato_email
     * @return WebsiteGeralEntidade
     */
    public function setContatoEmail($contato_email)
    {
        $this->contato_email = $contato_email;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndereco()
    {
        return (string)$this->endereco;
    }

    /**
     * @param string $endereco
     * @return WebsiteGeralEntidade
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
        return $this;
    }

    /**
     * @return string
     */
    public function getSobre()
    {
        return (string)$this->sobre;
    }

    /**
     * @param string $sobre
     * @return WebsiteGeralEntidade
     */
    public function setSobre($sobre)
    {
        $this->sobre = $sobre;
        return $this;
    }

    /**
     * @return string
     */
    public function getPalavrasChaves()
    {
        return (string)$this->palavras_chaves;
    }

    /**
     * @param string $palavras_chaves
     * @return WebsiteGeralEntidade
     */
    public function setPalavrasChaves($palavras_chaves)
    {
        $this->palavras_chaves = $palavras_chaves;
        return $this;
    }

    /**
     * @return string
     */
    public function getWhatsapp()
    {
        return (string)$this->whatsapp;
    }

    /**
     * @param string $whatsapp
     * @return WebsiteGeralEntidade
     */
    public function setWhatsapp($whatsapp)
    {
        $this->whatsapp = $whatsapp;
        return $this;
    }

    /**
     * @return string
     */
    public function getLatitudeMapa()
    {
        return (string)$this->latitude_mapa;
    }

    /**
     * @param string $latitude_mapa
     * @return WebsiteGeralEntidade
     */
    public function setLatitudeMapa($latitude_mapa)
    {
        $this->latitude_mapa = $latitude_mapa;
        return $this;
    }

    /**
     * @return string
     */
    public function getLongitudeMapa()
    {
        return (string)$this->longitude_mapa;
    }

    /**
     * @param string $longitude_mapa
     * @return WebsiteGeralEntidade
     */
    public function setLongitudeMapa($longitude_mapa)
    {
        $this->longitude_mapa = $longitude_mapa;
        return $this;
    }

    /**
     * @return string
     */
    public function getHorario()
    {
        return (string)$this->horario;
    }

    /**
     * @param string $horario
     * @return WebsiteGeralEntidade
     */
    public function setHorario($horario)
    {
        $this->horario = $horario;
        return $this;
    }

    /**
     * @return string
     */
    public function getFacebook()
    {
        return (string)$this->facebook;
    }

    /**
     * @param string $facebook
     * @return WebsiteGeralEntidade
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
        return $this;
    }

    /**
     * @return string
     */
    public function getInstagram()
    {
        return (string)$this->instagram;
    }

    /**
     * @param string $instagram
     * @return WebsiteGeralEntidade
     */
    public function setInstagram($instagram)
    {
        $this->instagram = $instagram;
        return $this;
    }

    /**
     * @return string
     */
    public function getYoutube()
    {
        return (string)$this->youtube;
    }

    /**
     * @param string $youtube
     * @return WebsiteGeralEntidade
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumUltimoCertificado()
    {
        return (int)$this->num_ultimo_certificado;
    }

    /**
     * @param int $num_ultimo_certificado
     * @return WebsiteGeralEntidade
     */
    public function setNumUltimoCertificado($num_ultimo_certificado)
    {
        $this->num_ultimo_certificado = $num_ultimo_certificado;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioInseriu()
    {
        return (int)$this->cms_usuario_inseriu;
    }

    /**
     * @param int $cms_usuario_inseriu
     * @return WebsiteGeralEntidade
     */
    public function setCmsUsuarioInseriu($cms_usuario_inseriu)
    {
        $this->cms_usuario_inseriu = $cms_usuario_inseriu;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioEditou()
    {
        return (int)$this->cms_usuario_editou;
    }

    /**
     * @param int $cms_usuario_editou
     * @return WebsiteGeralEntidade
     */
    public function setCmsUsuarioEditou($cms_usuario_editou)
    {
        $this->cms_usuario_editou = $cms_usuario_editou;
        return $this;
    }

    /**
     * @return string
     */
    public function getInsertData()
    {
        return (string)$this->insert_data;
    }

    /**
     * @param string $insert_data
     * @return WebsiteGeralEntidade
     */
    public function setInsertData($insert_data)
    {
        $this->insert_data = $insert_data;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'logo' => 0,
            'favicon' => 0,
            'empresa' => 0,
            'contato_telefone' => 0,
            'contato_email' => 0,
            'endereco' => 0,
            'sobre' => 0,
            'palavras_chaves' => 0,
            'whatsapp' => 0,
            'latitude_mapa' => 0,
            'longitude_mapa' => 0,
            'horario' => 0,
            'facebook' => 0,
            'instagram' => 0,
            'youtube' => 0,
            'num_ultimo_certificado' => 0,
            'cms_usuario_inseriu' => 0,
            'cms_usuario_editou' => 0,
            'insert_data' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'logo' => 'string',
            'favicon' => 'string',
            'empresa' => 'string',
            'contato_telefone' => 'string',
            'contato_email' => 'string',
            'endereco' => 'string',
            'sobre' => 'string',
            'palavras_chaves' => 'string',
            'whatsapp' => 'string',
            'latitude_mapa' => 'string',
            'longitude_mapa' => 'string',
            'horario' => 'string',
            'facebook' => 'string',
            'instagram' => 'string',
            'youtube' => 'string',
            'num_ultimo_certificado' => 'int',
            'cms_usuario_inseriu' => 'int',
            'cms_usuario_editou' => 'int',
            'insert_data' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'website_geral' => array(
                'id',
                'logo',
                'favicon',
                'empresa',
                'contato_telefone',
                'contato_email',
                'endereco',
                'sobre',
                'palavras_chaves',
                'whatsapp',
                'latitude_mapa',
                'longitude_mapa',
                'horario',
                'facebook',
                'instagram',
                'youtube',
                'num_ultimo_certificado',
                'cms_usuario_inseriu',
                'cms_usuario_editou',
                'insert_data',
            ),
        );

    }

}

