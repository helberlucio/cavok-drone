<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class RelationshipLayoutsPaginasLayoutEntidade extends EntidadeHelp
{
    private $id;
    private $layouts_id;
    private $paginas_layout_id;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "relationship_layouts_paginas_layout";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'RelationshipLayoutsPaginasLayoutEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return RelationshipLayoutsPaginasLayoutEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getLayoutsId()
    {
        return (int)$this->layouts_id;
    }

    /**
     * @param int $layouts_id
     * @return RelationshipLayoutsPaginasLayoutEntidade
     */
    public function setLayoutsId($layouts_id)
    {
        $this->layouts_id = $layouts_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getPaginasLayoutId()
    {
        return (int)$this->paginas_layout_id;
    }

    /**
     * @param int $paginas_layout_id
     * @return RelationshipLayoutsPaginasLayoutEntidade
     */
    public function setPaginasLayoutId($paginas_layout_id)
    {
        $this->paginas_layout_id = $paginas_layout_id;
        return $this;
    }

    /**
     * @return Layouts
     */
    public function getLayouts()
    {
        $db = new ModelagemDb();
        $db->where("layouts_id", $this->layouts_id);
        $Layouts = ( new Layouts())->getRow($db);
        return $Layouts;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'layouts_id' => 0,
            'paginas_layout_id' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'layouts_id' => 'int',
            'paginas_layout_id' => 'int',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'relationship_layouts_paginas_layout' => array(
                'id',
                'layouts_id',
                'paginas_layout_id',
            ),
        );

    }

}

