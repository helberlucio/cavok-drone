<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
class CmsField
{
    const TABELA = 'cms_field';
    const PREFIXO = '';
    const NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
    const FILE_NAME = 'CmsField';

    private $id;
    private $field;
    private $ordem;
    private $cms_modulo_id;
    private $cms_tipo_campos_id;
    private $tamanho;
    private $listar;
    private $cms_plugins_id;
    private $obrigatorio;
    private $table_fk;
    private $fkid;
    private $descricao;

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return CmsField
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return (string)$this->field;
    }

    /**
     * @param string $field
     * @return CmsField
     */
    public function setField($field)
    {
        $this->field = $field;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrdem()
    {
        return (int)$this->ordem;
    }

    /**
     * @param int $ordem
     * @return CmsField
     */
    public function setOrdem($ordem)
    {
        $this->ordem = $ordem;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsModuloId()
    {
        return (int)$this->cms_modulo_id;
    }

    /**
     * @param int $cms_modulo_id
     * @return CmsField
     */
    public function setCmsModuloId($cms_modulo_id)
    {
        $this->cms_modulo_id = $cms_modulo_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsTipoCamposId()
    {
        return (int)$this->cms_tipo_campos_id;
    }

    /**
     * @param int $cms_tipo_campos_id
     * @return CmsField
     */
    public function setCmsTipoCamposId($cms_tipo_campos_id)
    {
        $this->cms_tipo_campos_id = $cms_tipo_campos_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getTamanho()
    {
        return (int)$this->tamanho;
    }

    /**
     * @param int $tamanho
     * @return CmsField
     */
    public function setTamanho($tamanho)
    {
        $this->tamanho = $tamanho;
        return $this;
    }

    /**
     * @return string
     */
    public function getListar()
    {
        return (string)$this->listar;
    }

    /**
     * @param string $listar
     * @return CmsField
     */
    public function setListar($listar)
    {
        $this->listar = $listar;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsPluginsId()
    {
        return (int)$this->cms_plugins_id;
    }

    /**
     * @return CmsPlugins
     */
    public function getCmsPlugin()
    {
        $db = new ModelagemDb();
        return (new CmsPlugins())->getRow($db->where('id', $this->getCmsPluginsId()));
    }

    /**
     * @param int $cms_plugins_id
     * @return CmsField
     */
    public function setCmsPluginsId($cms_plugins_id)
    {
        $this->cms_plugins_id = $cms_plugins_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getObrigatorio()
    {
        return (int)$this->obrigatorio;
    }

    /**
     * @param int $obrigatorio
     * @return CmsField
     */
    public function setObrigatorio($obrigatorio)
    {
        $this->obrigatorio = $obrigatorio;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableFk()
    {
        return (string)$this->table_fk;
    }

    /**
     * @param string $table_fk
     * @return CmsField
     */
    public function setTableFk($table_fk)
    {
        $this->table_fk = $table_fk;
        return $this;
    }

    /**
     * @return string
     */
    public function getFkid()
    {
        return (string)$this->fkid;
    }

    /**
     * @param string $fkid
     * @return CmsField
     */
    public function setFkid($fkid)
    {
        $this->fkid = $fkid;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return (string)$this->descricao;
    }

    /**
     * @param string $descricao
     * @return CmsField
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * @return CmsTipoCampos
     */
    public function getCmsTipoCampos()
    {
        $db = new ModelagemDb();
        $db->where("id", $this->getCmsTipoCamposId());
        $CmsTipoCampos = ( new CmsTipoCampos())->getRow($db);
        return $CmsTipoCampos;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'field' => 0,
            'ordem' => 0,
            'cms_modulo_id' => 0,
            'cms_tipo_campos_id' => 0,
            'tamanho' => 0,
            'listar' => 0,
            'cms_plugins_id' => 0,
            'obrigatorio' => 0,
            'table_fk' => 0,
            'fkid' => 0,
            'fk_text' => 0,
            'descricao' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'field' => 'string',
            'ordem' => 'int',
            'cms_modulo_id' => 'int',
            'cms_tipo_campos_id' => 'int',
            'tamanho' => 'int',
            'listar' => 'string',
            'cms_plugins_id' => 'int',
            'obrigatorio' => 'int',
            'table_fk' => 'string',
            'fkid' => 'string',
            'fk_text' => 'string',
            'descricao' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'cms_field' => array(
                'id',
                'field',
                'ordem',
                'cms_modulo_id',
                'cms_tipo_campos_id',
                'tamanho',
                'listar',
                'cms_plugins_id',
                'obrigatorio',
                'table_fk',
                'fkid',
                'fk_text',
                'descricao',
            ),
        );

    }


    /**
     * @param ModelagemDb $objectFilter
     * @return $this
     */
    public function getRow(ModelagemDb $objectFilter)
    {
        $obj = $objectFilter->get(self::TABELA)->row();
        $thisClass = "\\". self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $classe = Conversao::objetoParaClasse($obj, new $thisClass);
        return $classe;
    }

    /**
     * @param ModelagemDb $objectFilter
     * @param bool $degub
     * @return array
     */
    public function getRows(ModelagemDb $objectFilter, $degub = false)
    {
        $classe = array();
        $objects = $objectFilter->get(self::TABELA)->result($degub,false);
        $thisClass = "\\". self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        foreach ($objects as $row)
        {
            $classe[] = Conversao::objetoParaClasse($row, new $thisClass);
        }
        return $classe;
    }

    public function update()
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $data = GenericRepository::createSaveData($thisClass, $this);
        $db->update(self::TABELA, $data);
    }

    public function delete()
    {
        $db = new ModelagemDb();
        $data["id"] = $this->getId();
        $db->where("id", $this->getId())->delete(self::TABELA, $data);
    }

    /**
     * @return int
     */
    public function insert()
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $data = GenericRepository::createSaveData($thisClass, $this);
        return $db->insert(self::TABELA, $data);
    }

}