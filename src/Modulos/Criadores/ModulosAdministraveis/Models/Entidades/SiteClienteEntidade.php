<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class SiteClienteEntidade extends EntidadeHelp
{
    private $id;
    private $nome;
    private $rg;
    private $cpf;
    private $telefone;
    private $celular;
    private $email;
    private $cidade;
    private $bairro;
    private $cep;
    private $numero;
    private $plano;
    private $forma_pagamento;
    private $data_vencimento;
    private $senha;
    private $inativo;
    private $cms_usuario_inseriu;
    private $cms_usuario_editou;
    private $insert_data;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "site_cliente";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'SiteClienteEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return SiteClienteEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return (string)$this->nome;
    }

    /**
     * @param string $nome
     * @return SiteClienteEntidade
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return string
     */
    public function getRg()
    {
        return (string)$this->rg;
    }

    /**
     * @param string $rg
     * @return SiteClienteEntidade
     */
    public function setRg($rg)
    {
        $this->rg = $rg;
        return $this;
    }

    /**
     * @return string
     */
    public function getCpf()
    {
        return (string)$this->cpf;
    }

    /**
     * @param string $cpf
     * @return SiteClienteEntidade
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
        return $this;
    }

    /**
     * @return string
     */
    public function getTelefone()
    {
        return (string)$this->telefone;
    }

    /**
     * @param string $telefone
     * @return SiteClienteEntidade
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
        return $this;
    }

    /**
     * @return string
     */
    public function getCelular()
    {
        return (string)$this->celular;
    }

    /**
     * @param string $celular
     * @return SiteClienteEntidade
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return (string)$this->email;
    }

    /**
     * @param string $email
     * @return SiteClienteEntidade
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getCidade()
    {
        return (string)$this->cidade;
    }

    /**
     * @param string $cidade
     * @return SiteClienteEntidade
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
        return $this;
    }

    /**
     * @return string
     */
    public function getBairro()
    {
        return (string)$this->bairro;
    }

    /**
     * @param string $bairro
     * @return SiteClienteEntidade
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
        return $this;
    }

    /**
     * @return string
     */
    public function getCep()
    {
        return (string)$this->cep;
    }

    /**
     * @param string $cep
     * @return SiteClienteEntidade
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumero()
    {
        return (string)$this->numero;
    }

    /**
     * @param string $numero
     * @return SiteClienteEntidade
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlano()
    {
        return (string)$this->plano;
    }

    /**
     * @param string $plano
     * @return SiteClienteEntidade
     */
    public function setPlano($plano)
    {
        $this->plano = $plano;
        return $this;
    }

    /**
     * @return string
     */
    public function getFormaPagamento()
    {
        return (string)$this->forma_pagamento;
    }

    /**
     * @param string $forma_pagamento
     * @return SiteClienteEntidade
     */
    public function setFormaPagamento($forma_pagamento)
    {
        $this->forma_pagamento = $forma_pagamento;
        return $this;
    }

    /**
     * @return string
     */
    public function getDataVencimento()
    {
        return (string)$this->data_vencimento;
    }

    /**
     * @param string $data_vencimento
     * @return SiteClienteEntidade
     */
    public function setDataVencimento($data_vencimento)
    {
        $this->data_vencimento = $data_vencimento;
        return $this;
    }

    /**
     * @return string
     */
    public function getSenha()
    {
        return (string)$this->senha;
    }

    /**
     * @param string $senha
     * @return SiteClienteEntidade
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
        return $this;
    }

    /**
     * @return int
     */
    public function getInativo()
    {
        return (int)$this->inativo;
    }

    /**
     * @param int $inativo
     * @return SiteClienteEntidade
     */
    public function setInativo($inativo)
    {
        $this->inativo = $inativo;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioInseriu()
    {
        return (int)$this->cms_usuario_inseriu;
    }

    /**
     * @param int $cms_usuario_inseriu
     * @return SiteClienteEntidade
     */
    public function setCmsUsuarioInseriu($cms_usuario_inseriu)
    {
        $this->cms_usuario_inseriu = $cms_usuario_inseriu;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioEditou()
    {
        return (int)$this->cms_usuario_editou;
    }

    /**
     * @param int $cms_usuario_editou
     * @return SiteClienteEntidade
     */
    public function setCmsUsuarioEditou($cms_usuario_editou)
    {
        $this->cms_usuario_editou = $cms_usuario_editou;
        return $this;
    }

    /**
     * @return string
     */
    public function getInsertData()
    {
        return (string)$this->insert_data;
    }

    /**
     * @param string $insert_data
     * @return SiteClienteEntidade
     */
    public function setInsertData($insert_data)
    {
        $this->insert_data = $insert_data;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'nome' => 0,
            'rg' => 0,
            'cpf' => 0,
            'telefone' => 0,
            'celular' => 0,
            'email' => 0,
            'cidade' => 0,
            'bairro' => 0,
            'cep' => 0,
            'numero' => 0,
            'plano' => 0,
            'forma_pagamento' => 0,
            'data_vencimento' => 0,
            'senha' => 0,
            'inativo' => 0,
            'cms_usuario_inseriu' => 0,
            'cms_usuario_editou' => 0,
            'insert_data' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'nome' => 'string',
            'rg' => 'string',
            'cpf' => 'string',
            'telefone' => 'string',
            'celular' => 'string',
            'email' => 'string',
            'cidade' => 'string',
            'bairro' => 'string',
            'cep' => 'string',
            'numero' => 'string',
            'plano' => 'string',
            'forma_pagamento' => 'string',
            'data_vencimento' => 'string',
            'senha' => 'string',
            'inativo' => 'int',
            'cms_usuario_inseriu' => 'int',
            'cms_usuario_editou' => 'int',
            'insert_data' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'site_cliente' => array(
                'id',
                'nome',
                'rg',
                'cpf',
                'telefone',
                'celular',
                'email',
                'cidade',
                'bairro',
                'cep',
                'numero',
                'plano',
                'forma_pagamento',
                'data_vencimento',
                'senha',
                'inativo',
                'cms_usuario_inseriu',
                'cms_usuario_editou',
                'insert_data',
            ),
        );

    }

}

