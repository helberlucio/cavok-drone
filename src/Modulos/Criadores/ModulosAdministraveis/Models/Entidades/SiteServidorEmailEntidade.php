<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class SiteServidorEmailEntidade extends EntidadeHelp
{
    private $id;
    private $seguranca;
    private $host;
    private $porta;
    private $username;
    private $password;
    private $from;
    private $from_name;
    private $reply;
    private $email_destinatario;
    private $cms_usuario_inseriu;
    private $cms_usuario_editou;
    private $insert_data;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "site_servidor_email";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'SiteServidorEmailEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return SiteServidorEmailEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getSeguranca()
    {
        return (string)$this->seguranca;
    }

    /**
     * @param string $seguranca
     * @return SiteServidorEmailEntidade
     */
    public function setSeguranca($seguranca)
    {
        $this->seguranca = $seguranca;
        return $this;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return (string)$this->host;
    }

    /**
     * @param string $host
     * @return SiteServidorEmailEntidade
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return string
     */
    public function getPorta()
    {
        return (string)$this->porta;
    }

    /**
     * @param string $porta
     * @return SiteServidorEmailEntidade
     */
    public function setPorta($porta)
    {
        $this->porta = $porta;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return (string)$this->username;
    }

    /**
     * @param string $username
     * @return SiteServidorEmailEntidade
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return (string)$this->password;
    }

    /**
     * @param string $password
     * @return SiteServidorEmailEntidade
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getFrom()
    {
        return (string)$this->from;
    }

    /**
     * @param string $from
     * @return SiteServidorEmailEntidade
     */
    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return string
     */
    public function getFromName()
    {
        return (string)$this->from_name;
    }

    /**
     * @param string $from_name
     * @return SiteServidorEmailEntidade
     */
    public function setFromName($from_name)
    {
        $this->from_name = $from_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getReply()
    {
        return (string)$this->reply;
    }

    /**
     * @param string $reply
     * @return SiteServidorEmailEntidade
     */
    public function setReply($reply)
    {
        $this->reply = $reply;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmailDestinatario()
    {
        return (string)$this->email_destinatario;
    }

    /**
     * @param string $email_destinatario
     * @return SiteServidorEmailEntidade
     */
    public function setEmailDestinatario($email_destinatario)
    {
        $this->email_destinatario = $email_destinatario;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioInseriu()
    {
        return (int)$this->cms_usuario_inseriu;
    }

    /**
     * @param int $cms_usuario_inseriu
     * @return SiteServidorEmailEntidade
     */
    public function setCmsUsuarioInseriu($cms_usuario_inseriu)
    {
        $this->cms_usuario_inseriu = $cms_usuario_inseriu;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioEditou()
    {
        return (int)$this->cms_usuario_editou;
    }

    /**
     * @param int $cms_usuario_editou
     * @return SiteServidorEmailEntidade
     */
    public function setCmsUsuarioEditou($cms_usuario_editou)
    {
        $this->cms_usuario_editou = $cms_usuario_editou;
        return $this;
    }

    /**
     * @return string
     */
    public function getInsertData()
    {
        return (string)$this->insert_data;
    }

    /**
     * @param string $insert_data
     * @return SiteServidorEmailEntidade
     */
    public function setInsertData($insert_data)
    {
        $this->insert_data = $insert_data;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'seguranca' => 0,
            'host' => 0,
            'porta' => 0,
            'username' => 0,
            'password' => 0,
            'from' => 0,
            'from_name' => 0,
            'reply' => 0,
            'email_destinatario' => 0,
            'cms_usuario_inseriu' => 0,
            'cms_usuario_editou' => 0,
            'insert_data' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'seguranca' => 'string',
            'host' => 'string',
            'porta' => 'string',
            'username' => 'string',
            'password' => 'string',
            'from' => 'string',
            'from_name' => 'string',
            'reply' => 'string',
            'email_destinatario' => 'string',
            'cms_usuario_inseriu' => 'int',
            'cms_usuario_editou' => 'int',
            'insert_data' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'site_servidor_email' => array(
                'id',
                'seguranca',
                'host',
                'porta',
                'username',
                'password',
                'from',
                'from_name',
                'reply',
                'email_destinatario',
                'cms_usuario_inseriu',
                'cms_usuario_editou',
                'insert_data',
            ),
        );

    }

}

