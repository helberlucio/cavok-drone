<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class CmsUsuariosEntidade extends EntidadeHelp
{
    private $id;
    private $imagem;
    private $nome;
    private $email;
    private $senha;
    private $token;
    private $nivel_usuario;
    private $cms_usuario_inseriu;
    private $cms_usuario_editou;
    private $insert_data;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "cms_usuarios";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'CmsUsuariosEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return CmsUsuariosEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getImagem()
    {
        return (string)$this->imagem;
    }

    /**
     * @param string $imagem
     * @return CmsUsuariosEntidade
     */
    public function setImagem($imagem)
    {
        $this->imagem = $imagem;
        return $this;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return (string)$this->nome;
    }

    /**
     * @param string $nome
     * @return CmsUsuariosEntidade
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return (string)$this->email;
    }

    /**
     * @param string $email
     * @return CmsUsuariosEntidade
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getSenha()
    {
        return (string)$this->senha;
    }

    /**
     * @param string $senha
     * @return CmsUsuariosEntidade
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return (string)$this->token;
    }

    /**
     * @param string $token
     * @return CmsUsuariosEntidade
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return string
     */
    public function getNivelUsuario()
    {
        return (string)$this->nivel_usuario;
    }

    /**
     * @param string $nivel_usuario
     * @return CmsUsuariosEntidade
     */
    public function setNivelUsuario($nivel_usuario)
    {
        $this->nivel_usuario = $nivel_usuario;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioInseriu()
    {
        return (int)$this->cms_usuario_inseriu;
    }

    /**
     * @param int $cms_usuario_inseriu
     * @return CmsUsuariosEntidade
     */
    public function setCmsUsuarioInseriu($cms_usuario_inseriu)
    {
        $this->cms_usuario_inseriu = $cms_usuario_inseriu;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioEditou()
    {
        return (int)$this->cms_usuario_editou;
    }

    /**
     * @param int $cms_usuario_editou
     * @return CmsUsuariosEntidade
     */
    public function setCmsUsuarioEditou($cms_usuario_editou)
    {
        $this->cms_usuario_editou = $cms_usuario_editou;
        return $this;
    }

    /**
     * @return string
     */
    public function getInsertData()
    {
        return (string)$this->insert_data;
    }

    /**
     * @param string $insert_data
     * @return CmsUsuariosEntidade
     */
    public function setInsertData($insert_data)
    {
        $this->insert_data = $insert_data;
        return $this;
    }

    /**
     * @return \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\RelationshipCmsUsuariosSysSelecaoEscritorio[]
     */
    public function getRelationshipCmsUsuariosSysSelecaoEscritorio()
    {
        $db = new ModelagemDb;
        $db->where("cms_usuarios_id", $this->getId());
        $RelationshipCmsUsuariosSysSelecaoEscritorio = (new RelationshipCmsUsuariosSysSelecaoEscritorio())->getRows($db);
        return $RelationshipCmsUsuariosSysSelecaoEscritorio;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'imagem' => 0,
            'nome' => 0,
            'email' => 0,
            'senha' => 0,
            'token' => 0,
            'nivel_usuario' => 0,
            'cms_usuario_inseriu' => 0,
            'cms_usuario_editou' => 0,
            'insert_data' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'imagem' => 'string',
            'nome' => 'string',
            'email' => 'string',
            'senha' => 'string',
            'token' => 'string',
            'nivel_usuario' => 'string',
            'cms_usuario_inseriu' => 'int',
            'cms_usuario_editou' => 'int',
            'insert_data' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'cms_usuarios' => array(
                'id',
                'imagem',
                'nome',
                'email',
                'senha',
                'token',
                'nivel_usuario',
                'cms_usuario_inseriu',
                'cms_usuario_editou',
                'insert_data',
            ),
        );

    }

}

