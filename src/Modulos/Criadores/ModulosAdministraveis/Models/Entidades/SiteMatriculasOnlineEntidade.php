<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class SiteMatriculasOnlineEntidade extends EntidadeHelp
{
    private $id;
    private $aluno;
    private $curso;
    private $data;
    private $confirmado;
    private $pago;
    private $situacao_pagamento;
    private $presencial;
    private $cms_usuario_inseriu;
    private $cms_usuario_editou;
    private $insert_data;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "site_matriculas_online";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'SiteMatriculasOnlineEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return SiteMatriculasOnlineEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAluno()
    {
        return (string)$this->aluno;
    }

    /**
     * @param string $aluno
     * @return SiteMatriculasOnlineEntidade
     */
    public function setAluno($aluno)
    {
        $this->aluno = $aluno;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurso()
    {
        return (string)$this->curso;
    }

    /**
     * @param string $curso
     * @return SiteMatriculasOnlineEntidade
     */
    public function setCurso($curso)
    {
        $this->curso = $curso;
        return $this;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return (string)$this->data;
    }

    /**
     * @param string $data
     * @return SiteMatriculasOnlineEntidade
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return int
     */
    public function getConfirmado()
    {
        return (int)$this->confirmado;
    }

    /**
     * @param int $confirmado
     * @return SiteMatriculasOnlineEntidade
     */
    public function setConfirmado($confirmado)
    {
        $this->confirmado = $confirmado;
        return $this;
    }

    /**
     * @return int
     */
    public function getPago()
    {
        return (int)$this->pago;
    }

    /**
     * @param int $pago
     * @return SiteMatriculasOnlineEntidade
     */
    public function setPago($pago)
    {
        $this->pago = $pago;
        return $this;
    }

    /**
     * @return string
     */
    public function getSituacaoPagamento()
    {
        return (string)$this->situacao_pagamento;
    }

    /**
     * @param string $situacao_pagamento
     * @return SiteMatriculasOnlineEntidade
     */
    public function setSituacaoPagamento($situacao_pagamento)
    {
        $this->situacao_pagamento = $situacao_pagamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getPresencial()
    {
        return (int)$this->presencial;
    }

    /**
     * @param int $presencial
     * @return SiteMatriculasOnlineEntidade
     */
    public function setPresencial($presencial)
    {
        $this->presencial = $presencial;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioInseriu()
    {
        return (int)$this->cms_usuario_inseriu;
    }

    /**
     * @param int $cms_usuario_inseriu
     * @return SiteMatriculasOnlineEntidade
     */
    public function setCmsUsuarioInseriu($cms_usuario_inseriu)
    {
        $this->cms_usuario_inseriu = $cms_usuario_inseriu;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioEditou()
    {
        return (int)$this->cms_usuario_editou;
    }

    /**
     * @param int $cms_usuario_editou
     * @return SiteMatriculasOnlineEntidade
     */
    public function setCmsUsuarioEditou($cms_usuario_editou)
    {
        $this->cms_usuario_editou = $cms_usuario_editou;
        return $this;
    }

    /**
     * @return string
     */
    public function getInsertData()
    {
        return (string)$this->insert_data;
    }

    /**
     * @param string $insert_data
     * @return SiteMatriculasOnlineEntidade
     */
    public function setInsertData($insert_data)
    {
        $this->insert_data = $insert_data;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'aluno' => 0,
            'curso' => 0,
            'data' => 0,
            'confirmado' => 0,
            'pago' => 0,
            'situacao_pagamento' => 0,
            'presencial' => 0,
            'cms_usuario_inseriu' => 0,
            'cms_usuario_editou' => 0,
            'insert_data' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'aluno' => 'string',
            'curso' => 'string',
            'data' => 'string',
            'confirmado' => 'int',
            'pago' => 'int',
            'situacao_pagamento' => 'string',
            'presencial' => 'int',
            'cms_usuario_inseriu' => 'int',
            'cms_usuario_editou' => 'int',
            'insert_data' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'site_matriculas_online' => array(
                'id',
                'aluno',
                'curso',
                'data',
                'confirmado',
                'pago',
                'situacao_pagamento',
                'presencial',
                'cms_usuario_inseriu',
                'cms_usuario_editou',
                'insert_data',
            ),
        );

    }

}

