<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class BlogPostagensEntidade extends EntidadeHelp
{
    private $id;
    private $thumb;
    private $banner;
    private $banner_mobile;
    private $altura_banner;
    private $altura_banner_mobile;
    private $titulo;
    private $chamada;
    private $descricao;
    private $destaque;
    private $visualizacoes;
    private $ordem;
    private $categoria;
    private $data_inicio_destaque;
    private $data_fim_destaque;
    private $palavras_chaves;
    private $cms_usuario_inseriu;
    private $cms_usuario_editou;
    private $insert_data;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "blog_postagens";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'BlogPostagensEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return BlogPostagensEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getThumb()
    {
        return (string)$this->thumb;
    }

    /**
     * @param string $thumb
     * @return BlogPostagensEntidade
     */
    public function setThumb($thumb)
    {
        $this->thumb = $thumb;
        return $this;
    }

    /**
     * @return string
     */
    public function getBanner()
    {
        return (string)$this->banner;
    }

    /**
     * @param string $banner
     * @return BlogPostagensEntidade
     */
    public function setBanner($banner)
    {
        $this->banner = $banner;
        return $this;
    }

    /**
     * @return string
     */
    public function getBannerMobile()
    {
        return (string)$this->banner_mobile;
    }

    /**
     * @param string $banner_mobile
     * @return BlogPostagensEntidade
     */
    public function setBannerMobile($banner_mobile)
    {
        $this->banner_mobile = $banner_mobile;
        return $this;
    }

    /**
     * @return int
     */
    public function getAlturaBanner()
    {
        return (int)$this->altura_banner;
    }

    /**
     * @param int $altura_banner
     * @return BlogPostagensEntidade
     */
    public function setAlturaBanner($altura_banner)
    {
        $this->altura_banner = $altura_banner;
        return $this;
    }

    /**
     * @return int
     */
    public function getAlturaBannerMobile()
    {
        return (int)$this->altura_banner_mobile;
    }

    /**
     * @param int $altura_banner_mobile
     * @return BlogPostagensEntidade
     */
    public function setAlturaBannerMobile($altura_banner_mobile)
    {
        $this->altura_banner_mobile = $altura_banner_mobile;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return (string)$this->titulo;
    }

    /**
     * @param string $titulo
     * @return BlogPostagensEntidade
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getChamada()
    {
        return (string)$this->chamada;
    }

    /**
     * @param string $chamada
     * @return BlogPostagensEntidade
     */
    public function setChamada($chamada)
    {
        $this->chamada = $chamada;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return (string)$this->descricao;
    }

    /**
     * @param string $descricao
     * @return BlogPostagensEntidade
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * @return int
     */
    public function getDestaque()
    {
        return (int)$this->destaque;
    }

    /**
     * @param int $destaque
     * @return BlogPostagensEntidade
     */
    public function setDestaque($destaque)
    {
        $this->destaque = $destaque;
        return $this;
    }

    /**
     * @return int
     */
    public function getVisualizacoes()
    {
        return (int)$this->visualizacoes;
    }

    /**
     * @param int $visualizacoes
     * @return BlogPostagensEntidade
     */
    public function setVisualizacoes($visualizacoes)
    {
        $this->visualizacoes = $visualizacoes;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrdem()
    {
        return (int)$this->ordem;
    }

    /**
     * @param int $ordem
     * @return BlogPostagensEntidade
     */
    public function setOrdem($ordem)
    {
        $this->ordem = $ordem;
        return $this;
    }

    /**
     * @return string
     */
    public function getCategoria()
    {
        return (string)$this->categoria;
    }

    /**
     * @param string $categoria
     * @return BlogPostagensEntidade
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
        return $this;
    }

    /**
     * @return string
     */
    public function getDataInicioDestaque()
    {
        return (string)$this->data_inicio_destaque;
    }

    /**
     * @param string $data_inicio_destaque
     * @return BlogPostagensEntidade
     */
    public function setDataInicioDestaque($data_inicio_destaque)
    {
        $this->data_inicio_destaque = $data_inicio_destaque;
        return $this;
    }

    /**
     * @return string
     */
    public function getDataFimDestaque()
    {
        return (string)$this->data_fim_destaque;
    }

    /**
     * @param string $data_fim_destaque
     * @return BlogPostagensEntidade
     */
    public function setDataFimDestaque($data_fim_destaque)
    {
        $this->data_fim_destaque = $data_fim_destaque;
        return $this;
    }

    /**
     * @return string
     */
    public function getPalavrasChaves()
    {
        return (string)$this->palavras_chaves;
    }

    /**
     * @param string $palavras_chaves
     * @return BlogPostagensEntidade
     */
    public function setPalavrasChaves($palavras_chaves)
    {
        $this->palavras_chaves = $palavras_chaves;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioInseriu()
    {
        return (int)$this->cms_usuario_inseriu;
    }

    /**
     * @param int $cms_usuario_inseriu
     * @return BlogPostagensEntidade
     */
    public function setCmsUsuarioInseriu($cms_usuario_inseriu)
    {
        $this->cms_usuario_inseriu = $cms_usuario_inseriu;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioEditou()
    {
        return (int)$this->cms_usuario_editou;
    }

    /**
     * @param int $cms_usuario_editou
     * @return BlogPostagensEntidade
     */
    public function setCmsUsuarioEditou($cms_usuario_editou)
    {
        $this->cms_usuario_editou = $cms_usuario_editou;
        return $this;
    }

    /**
     * @return string
     */
    public function getInsertData()
    {
        return (string)$this->insert_data;
    }

    /**
     * @param string $insert_data
     * @return BlogPostagensEntidade
     */
    public function setInsertData($insert_data)
    {
        $this->insert_data = $insert_data;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'thumb' => 0,
            'banner' => 0,
            'banner_mobile' => 0,
            'altura_banner' => 0,
            'altura_banner_mobile' => 0,
            'titulo' => 0,
            'chamada' => 0,
            'descricao' => 0,
            'destaque' => 0,
            'visualizacoes' => 0,
            'ordem' => 0,
            'categoria' => 0,
            'data_inicio_destaque' => 0,
            'data_fim_destaque' => 0,
            'palavras_chaves' => 0,
            'cms_usuario_inseriu' => 0,
            'cms_usuario_editou' => 0,
            'insert_data' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'thumb' => 'string',
            'banner' => 'string',
            'banner_mobile' => 'string',
            'altura_banner' => 'int',
            'altura_banner_mobile' => 'int',
            'titulo' => 'string',
            'chamada' => 'string',
            'descricao' => 'string',
            'destaque' => 'int',
            'visualizacoes' => 'int',
            'ordem' => 'int',
            'categoria' => 'string',
            'data_inicio_destaque' => 'string',
            'data_fim_destaque' => 'string',
            'palavras_chaves' => 'string',
            'cms_usuario_inseriu' => 'int',
            'cms_usuario_editou' => 'int',
            'insert_data' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'blog_postagens' => array(
                'id',
                'thumb',
                'banner',
                'banner_mobile',
                'altura_banner',
                'altura_banner_mobile',
                'titulo',
                'chamada',
                'descricao',
                'destaque',
                'visualizacoes',
                'ordem',
                'categoria',
                'data_inicio_destaque',
                'data_fim_destaque',
                'palavras_chaves',
                'cms_usuario_inseriu',
                'cms_usuario_editou',
                'insert_data',
            ),
        );

    }

}

