<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;
class ProdutosEntidade extends EntidadeHelp
{
    private $id;
    private $imagem;
    private $titulo;
    private $descricao;
    private $whatsapp;
    private $tipo_do_item;
    private $palavra_chave;
    private $cms_usuario_inseriu;
    private $cms_usuario_editou;
    private $insert_data;

    public function __construct()
    {
        parent::__construct();
        self::$TABELA = "produtos";
        self::$PREFIXO = "";
        self::$NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
        self::$FILE_NAME = 'ProdutosEntidade';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return ProdutosEntidade
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getImagem()
    {
        return (string)$this->imagem;
    }

    /**
     * @param string $imagem
     * @return ProdutosEntidade
     */
    public function setImagem($imagem)
    {
        $this->imagem = $imagem;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return (string)$this->titulo;
    }

    /**
     * @param string $titulo
     * @return ProdutosEntidade
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return (string)$this->descricao;
    }

    /**
     * @param string $descricao
     * @return ProdutosEntidade
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    /**
     * @return string
     */
    public function getWhatsapp()
    {
        return (string)$this->whatsapp;
    }

    /**
     * @param string $whatsapp
     * @return ProdutosEntidade
     */
    public function setWhatsapp($whatsapp)
    {
        $this->whatsapp = $whatsapp;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipoDoItem()
    {
        return (string)$this->tipo_do_item;
    }

    /**
     * @param string $tipo_do_item
     * @return ProdutosEntidade
     */
    public function setTipoDoItem($tipo_do_item)
    {
        $this->tipo_do_item = $tipo_do_item;
        return $this;
    }

    /**
     * @return string
     */
    public function getPalavraChave()
    {
        return (string)$this->palavra_chave;
    }

    /**
     * @param string $palavra_chave
     * @return ProdutosEntidade
     */
    public function setPalavraChave($palavra_chave)
    {
        $this->palavra_chave = $palavra_chave;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioInseriu()
    {
        return (int)$this->cms_usuario_inseriu;
    }

    /**
     * @param int $cms_usuario_inseriu
     * @return ProdutosEntidade
     */
    public function setCmsUsuarioInseriu($cms_usuario_inseriu)
    {
        $this->cms_usuario_inseriu = $cms_usuario_inseriu;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsUsuarioEditou()
    {
        return (int)$this->cms_usuario_editou;
    }

    /**
     * @param int $cms_usuario_editou
     * @return ProdutosEntidade
     */
    public function setCmsUsuarioEditou($cms_usuario_editou)
    {
        $this->cms_usuario_editou = $cms_usuario_editou;
        return $this;
    }

    /**
     * @return string
     */
    public function getInsertData()
    {
        return (string)$this->insert_data;
    }

    /**
     * @param string $insert_data
     * @return ProdutosEntidade
     */
    public function setInsertData($insert_data)
    {
        $this->insert_data = $insert_data;
        return $this;
    }

    /**
     * @return \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\RelationshipProdutosImagensProdutosServicos[]
     */
    public function getRelationshipProdutosImagensProdutosServicos()
    {
        $db = new ModelagemDb;
        $db->where("produtos_id", $this->getId());
        $RelationshipProdutosImagensProdutosServicos = (new RelationshipProdutosImagensProdutosServicos())->getRows($db);
        return $RelationshipProdutosImagensProdutosServicos;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'imagem' => 0,
            'titulo' => 0,
            'descricao' => 0,
            'whatsapp' => 0,
            'tipo_do_item' => 0,
            'palavra_chave' => 0,
            'cms_usuario_inseriu' => 0,
            'cms_usuario_editou' => 0,
            'insert_data' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'imagem' => 'string',
            'titulo' => 'string',
            'descricao' => 'string',
            'whatsapp' => 'string',
            'tipo_do_item' => 'string',
            'palavra_chave' => 'string',
            'cms_usuario_inseriu' => 'int',
            'cms_usuario_editou' => 'int',
            'insert_data' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'produtos' => array(
                'id',
                'imagem',
                'titulo',
                'descricao',
                'whatsapp',
                'tipo_do_item',
                'palavra_chave',
                'cms_usuario_inseriu',
                'cms_usuario_editou',
                'insert_data',
            ),
        );

    }

}

