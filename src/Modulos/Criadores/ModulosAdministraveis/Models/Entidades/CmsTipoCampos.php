<?php

namespace Modulos\Criadores\ModulosAdministraveis\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;

class CmsTipoCampos
{
    const TABELA = 'cms_tipo_campos';
    const PREFIXO = '';
    const NAMESPACE_FILE = 'Modulos\Criadores\ModulosAdministraveis\Models\Entidades';
    const FILE_NAME = 'CmsTipoCampos';

    private $id;
    private $label;
    private $view_name;
    private $cms_grupo_tipo_campos_id;
    private $laravel_database_migration_code;
    private $laravel_database_migration_length;
    private $laravel_database_migration_nullable;
    private $campo_logico;
    private $unsigned;

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @param int $id
     * @return CmsTipoCampos
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return (string)$this->label;
    }

    /**
     * @param string $label
     * @return CmsTipoCampos
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getViewName()
    {
        return (string)$this->view_name;
    }

    /**
     * @param string $view_name
     * @return CmsTipoCampos
     */
    public function setViewName($view_name)
    {
        $this->view_name = $view_name;
        return $this;
    }

    /**
     * @return int
     */
    public function getCmsGrupoTipoCamposId()
    {
        return (int)$this->cms_grupo_tipo_campos_id;
    }

    /**
     * @return CmsGrupoTipoCampos
     */
    public function getCmsGrupoTipoCampos()
    {
        $db = new ModelagemDb();
        $cmsGrupoTipoCampos = new CmsGrupoTipoCampos();
        return $cmsGrupoTipoCampos->getRow($db->where('id', $this->getCmsGrupoTipoCamposId()));
    }

    /**
     * @param int $cms_grupo_tipo_campos_id
     * @return CmsTipoCampos
     */
    public function setCmsGrupoTipoCamposId($cms_grupo_tipo_campos_id)
    {
        $this->cms_grupo_tipo_campos_id = $cms_grupo_tipo_campos_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getLaravelDatabaseMigrationCode()
    {
        return (string)$this->laravel_database_migration_code;
    }

    /**
     * @param string $laravel_database_migration_code
     * @return CmsTipoCampos
     */
    public function setLaravelDatabaseMigrationCode($laravel_database_migration_code)
    {
        $this->laravel_database_migration_code = $laravel_database_migration_code;
        return $this;
    }

    /**
     * @return int
     */
    public function getLaravelDatabaseMigrationLength()
    {
        return (int)$this->laravel_database_migration_length;
    }

    /**
     * @param int $laravel_database_migration_length
     * @return CmsTipoCampos
     */
    public function setLaravelDatabaseMigrationLength($laravel_database_migration_length)
    {
        $this->laravel_database_migration_length = $laravel_database_migration_length;
        return $this;
    }

    /**
     * @return int
     */
    public function getLaravelDatabaseMigrationNullable()
    {
        return (int)$this->laravel_database_migration_nullable;
    }

    /**
     * @param int $laravel_database_migration_nullable
     * @return CmsTipoCampos
     */
    public function setLaravelDatabaseMigrationNullable($laravel_database_migration_nullable)
    {
        $this->laravel_database_migration_nullable = $laravel_database_migration_nullable;
        return $this;
    }

    /**
     * @return int
     */
    public function getCampoLogico()
    {
        return (int)$this->campo_logico;
    }

    /**
     * @param int $campo_logico
     * @return CmsTipoCampos
     */
    public function setCampoLogico($campo_logico)
    {
        $this->campo_logico = $campo_logico;
        return $this;
    }

    /**
     * @return int
     */
    public function getUnsigned()
    {
        return (int)$this->unsigned;
    }

    /**
     * @param int $unsigned
     * @return CmsTipoCampos
     */
    public function setUnsigned($unsigned)
    {
        $this->unsigned = $unsigned;
        return $this;
    }

    /**
     * @return \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField[]
     */
    public function getCmsField()
    {
        $db = new ModelagemDb;
        $db->where("cms_tipo_campos_id", $this->getId());
        $CmsField = (new CmsField())->getRows($db);
        return $CmsField;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'label' => 0,
            'view_name' => 0,
            'cms_grupo_tipo_campos_id' => 0,
            'laravel_database_migration_code' => 0,
            'laravel_database_migration_length' => 0,
            'laravel_database_migration_nullable' => 0,
            'campo_logico' => 0,
            'unsigned' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'int',
            'label' => 'string',
            'view_name' => 'string',
            'cms_grupo_tipo_campos_id' => 'int',
            'laravel_database_migration_code' => 'string',
            'laravel_database_migration_length' => 'int',
            'laravel_database_migration_nullable' => 'int',
            'campo_logico' => 'int',
            'unsigned' => 'int',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'cms_tipo_campos' => array(
                'id',
                'label',
                'view_name',
                'cms_grupo_tipo_campos_id',
                'laravel_database_migration_code',
                'laravel_database_migration_length',
                'laravel_database_migration_nullable',
                'campo_logico',
                'unsigned',
            ),
        );

    }


    /**
     * @param ModelagemDb $objectFilter
     * @return $this
     */
    public function getRow(ModelagemDb $objectFilter)
    {
        $obj = $objectFilter->get(self::TABELA)->row();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $classe = Conversao::objetoParaClasse($obj, new $thisClass);
        return $classe;
    }

    /**
     * @param ModelagemDb $objectFilter
     * @return $this[]
     */
    public function getRows(ModelagemDb $objectFilter)
    {
        $classe = array();
        $objects = $objectFilter->get(self::TABELA)->result();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        foreach ($objects as $row) {
            $classe[] = Conversao::objetoParaClasse($row, new $thisClass);
        }
        return $classe;
    }

    public function update()
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $data = GenericRepository::createSaveData($thisClass, $this);
        $db->update(self::TABELA, $data);
    }

    public function delete()
    {
        $db = new ModelagemDb();
        $data["id"] = $this->getId();
        $db->where("id", $this->getId())->delete(self::TABELA, $data);
    }

    /**
     * @return int
     */
    public function insert()
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $data = GenericRepository::createSaveData($thisClass, $this);
        return $db->insert(self::TABELA, $data);
    }

}
