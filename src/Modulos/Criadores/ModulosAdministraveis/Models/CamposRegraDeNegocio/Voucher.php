<?php

/**
 * Created by PhpStorm.
 * User: Adriana Gata
 * Date: 26/06/2016
 * Time: 23:10
 */

namespace Modulos\Criadores\ModulosAdministraveis\Models\CamposRegraDeNegocio;

use Aplicacao\Ferramentas\Ajax;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SystemVoucher;

class Voucher
{

    private $db;

    public function __construct()
    {
        $this->db = new \Core\Modelos\ModelagemDb();
    }

    public function generateVoucher()
    {
        $key = strtoupper(substr(sha1(time()), 0, 12)) . strtoupper(substr(sha1(time() . 'part2'), 0, 12));
        $part1 = substr($key, 0, 8);
        $part2 = substr($key, 9, 8);
        $part3 = substr($key, 10, 8);
        $data['voucher'] = $part1 . '-' . $part2 . '-' . $part3;
        echo json_encode($data);
    }

    public function saveVoucher()
    {
        $data['tipo'] = input('tipo');
        $data['chave'] = input('chave');
        $data['valor'] = input('valor');
        $data['maximo_utilizacoes'] = input('maximo_utilizacoes');

        $voucher = $this->db->where('chave', input_post('chave'))->get('system_voucher')->row();
        if (isset($voucher->id) && $voucher->id) {
            $data['id'] = $voucher->id;
            $idVoucher = $this->db->where('id', $voucher->id)->update('system_voucher', $data);
        } else {
            $idVoucher =$this->db->insert('system_voucher', $data);
        }
        return $idVoucher;
    }


    public function getVoucher()
    {
        $voucher = $this->db
            ->join('voucher_type', 'voucher_type.id = system_voucher.tipo')
            ->where('chave', input_post('voucher'))
            ->get('system_voucher')
            ->row(false);
        if(!isset($voucher->chave)){
            Ajax::retorno(array('chave'=>123, 'maximo_utilizacoes'=>10, 'tipo'=>1, 'valor'=>(double)0.00));
        }
        $data['chave'] = $voucher->chave;
        $data['maximo_utilizacoes'] = (int)$voucher->maximo_utilizacoes;
        $data['tipo'] = (int)$voucher->tipo;
        $data['valor'] = (double)$voucher->valor;

        echo json_encode($data);
    }

}