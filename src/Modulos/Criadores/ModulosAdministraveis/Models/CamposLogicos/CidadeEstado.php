<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 07/01/2017
 * Time: 18:08
 */

namespace Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos;


use Aplicacao\Ferramentas\Ajax;
use Core\Modelos\ModelagemDb;
use Illuminate\Database\Schema\Blueprint;
use Modulos\Criadores\Entidades\Models\Gerador;
use Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Abstracts\CamposLogicosAbstracao;
use Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Administracao\RelacionamentoMultiplo\Entidades\RelacionamentoMultiploEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\CriarTabelaRelacionamento;
use Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Administracao\RelacionamentoMultiplo\Administracao;

class CidadeEstado extends CamposLogicosAbstracao
{

    private $tipoCampoExtensao = 'cidade_estado';
    private $db;

    public function __construct()
    {
        $this->db = new ModelagemDb();
    }

    /**
     * @param $tableCallBack
     * @param $campo
     * @param $table
     * @param $campoConfiguracao
     */
    public function create($tableCallBack, $campo, $table, $campoConfiguracao)
    {
        /**
         * @var $tableCallBack Blueprint
         */
        $columnUfExist = (new ModelagemDb())->Capsule()->schema()->hasColumn($table, $campo . '_uf');
        $columnCityExist = (new ModelagemDb())->Capsule()->schema()->hasColumn($table, $campo . '_city');
        if (!$columnUfExist) {
            $tableCallBack->string($campo . '_uf', 64)->nullable();
        }
        if (!$columnCityExist) {
            $tableCallBack->string($campo . '_city', 64)->nullable();
        }
    }

    /**
     * @param $tableCallBack
     * @param $campo
     * @param $table
     * @param $campoConfiguracao
     */
    public function update($tableCallBack, $campo, $table, $campoConfiguracao)
    {
        /**
         * @var $tableCallBack Blueprint
         */
        $columnUfExist = (new ModelagemDb())->Capsule()->schema()->hasColumn($table, $campo . '_uf');
        $columnCityExist = (new ModelagemDb())->Capsule()->schema()->hasColumn($table, $campo . '_city');
        if (!$columnUfExist) {
            $tableCallBack->string($campo . '_uf', 64)->nullable()->change();
        }
        if (!$columnCityExist) {
            $tableCallBack->string($campo . '_city', 64)->nullable()->change();
        }
    }

    /**
     * @param $moduloAdministravel
     * @param $camposModuloAdministravel
     * @param bool $entidadeConteudo
     * @return array|string
     */
    public function getAdministracao($moduloAdministravel, $camposModuloAdministravel, $entidadeConteudo = false)
    {
        $relacionamentoMultiploEntidade = new RelacionamentoMultiploEntidade();
        $administracao = new Administracao($relacionamentoMultiploEntidade, $moduloAdministravel, $camposModuloAdministravel);

        if ((Ajax::getRequestTipo() == Ajax::PUT || Ajax::getRequestTipo() == Ajax::POST) && $entidadeConteudo) {
            return $administracao->salvarConteudoModuloAdministravel($entidadeConteudo);
        }
        if (Ajax::getRequestTipo() == Ajax::DELETE && $entidadeConteudo) {
            return $administracao->deleteAdministracaoConteudo($entidadeConteudo);
        }
        if (Ajax::getRequestTipo() == Ajax::PUT || Ajax::getRequestTipo() == Ajax::POST) {
            return $administracao->getAdministracaoInsercaoView();
        }
        return $administracao->getAdministracaoListagemView();
    }
}