<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 07/01/2017
 * Time: 18:08
 */

namespace Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos;


use Aplicacao\Ferramentas\Ajax;
use Core\Modelos\ModelagemDb;
use Modulos\Criadores\Entidades\Models\Gerador;
use Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Abstracts\CamposLogicosAbstracao;
use Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Administracao\RelacionamentoMultiplo\Entidades\RelacionamentoMultiploEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\CriarTabelaRelacionamento;
use Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Administracao\RelacionamentoMultiplo\Administracao;

class RelacionamentoMultiplo extends CamposLogicosAbstracao
{

    private $tipoCampoExtensao = 'relacionamento_multiplo';
    private $db;

    public function __construct()
    {
        $this->db = new ModelagemDb();
    }

    public function create($tableCallBack, $campo, $table, $campoConfiguracao)
    {
        $this->gerarRelacionamentoMultiplo($table, $campoConfiguracao->tabelafk);
    }

    public function update($tableCallBack, $campo, $table, $campoConfiguracao)
    {
        $this->create($tableCallBack, $campo, $table, $campoConfiguracao);
    }

    /**
     * @param $tabela
     * @param $tableFkCampo
     */
    private function gerarRelacionamentoMultiplo($tabela, $tableFkCampo)
    {
        $gerador = new Gerador();
        $tabelaRelacionamento = new CriarTabelaRelacionamento();
        $nomeTabelaRelacionamento = $tabelaRelacionamento->CriarTabelaRelacionamento($tabela, $tableFkCampo);
        $gerador->gerarEntidade($nomeTabelaRelacionamento, 'Criadores', 'ModulosAdministraveis');
    }

    /**
     * @param $moduloAdministravel
     * @param $camposModuloAdministravel
     * @param bool $entidadeConteudo
     * @return array|string
     */
    public function getAdministracao($moduloAdministravel, $camposModuloAdministravel, $entidadeConteudo = false)
    {
        $relacionamentoMultiploEntidade = new RelacionamentoMultiploEntidade();
        $administracao = new Administracao($relacionamentoMultiploEntidade, $moduloAdministravel, $camposModuloAdministravel);

        if ((Ajax::getRequestTipo() == Ajax::PUT || Ajax::getRequestTipo() == Ajax::POST) && $entidadeConteudo) {
            return $administracao->salvarConteudoModuloAdministravel($entidadeConteudo);
        }
        if (Ajax::getRequestTipo() == Ajax::DELETE && $entidadeConteudo) {
            return $administracao->deleteAdministracaoConteudo($entidadeConteudo);
        }
        if (Ajax::getRequestTipo() == Ajax::PUT || Ajax::getRequestTipo() == Ajax::POST) {
            return $administracao->getAdministracaoInsercaoView();
        }
        return $administracao->getAdministracaoListagemView();
    }
}