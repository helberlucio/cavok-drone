<?php

/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 04/02/2017
 * Time: 15:18
 */

namespace Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Administracao\RelacionamentoMultiplo\IAdministracao;
use Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Administracao\RelacionamentoMultiplo\Entidades\Abstracts\RelacionamentoMultiploAbstract as RelacionamentoMultiploEntidade;

interface IAdministracao
{
    public function __construct(RelacionamentoMultiploEntidade $relacionamentoMultiploEntidade, $moduloAdministravel, $camposModuloAdministravel);

    public function getAdministracaoListagemView();

    public function getAdministracaoInsercaoView();

    public function salvarConteudoModuloAdministravel($entidadeConteudo);

    public function deleteAdministracaoConteudo($entidadeConteudo);

}