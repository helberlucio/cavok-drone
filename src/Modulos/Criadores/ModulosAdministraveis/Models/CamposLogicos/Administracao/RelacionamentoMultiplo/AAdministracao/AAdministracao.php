<?php

/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 04/02/2017
 * Time: 15:22
 */
namespace Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Administracao\RelacionamentoMultiplo\AAdministracao;

use Aplicacao\Conversao;
use Aplicacao\Ferramentas\Ajax;
use Aplicacao\Ferramentas\Identificadores;
use Core\Modelos\Carregamentos;
use Core\Modelos\ModelagemDb;
use Modulos\Criadores\Entidades\Models\Gerador;
use Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Administracao\RelacionamentoMultiplo\IAdministracao\IAdministracao;
use Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Administracao\RelacionamentoMultiplo\Entidades\Abstracts\RelacionamentoMultiploAbstract as RelacionamentoMultiploEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\CriarTabelaRelacionamento;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField;
use Modulos\Criadores\ModulosAdministraveis\Models\Repositorios\Campos;

abstract class AAdministracao implements IAdministracao
{
    private $load;

    private $relacionamentoMultiploEntidade;
    private $extensao;
    private $modulo;
    private $moduloAdministravel;
    private $camposModuloAdministravel;

    static $NAMESCPACE = '\\Modulos\\Criadores\\ModulosAdministraveis\\Models\\Entidades\\';

    public function __construct(RelacionamentoMultiploEntidade $relacionamentoMultiploEntidade, $moduloAdministravel, $camposModuloAdministravel)
    {
        $this->load = new Carregamentos();
        $this->relacionamentoMultiploEntidade = $relacionamentoMultiploEntidade;
        $this->extensao = $this->relacionamentoMultiploEntidade->getExtensao();
        $this->modulo = $this->relacionamentoMultiploEntidade->getModulo();
        $this->moduloAdministravel = $moduloAdministravel;
        $this->camposModuloAdministravel = $camposModuloAdministravel;
    }

    public function getAdministracaoListagemView()
    {
        $conteudos = $this->getConteudos();
        $data['relacionamentoMultiploEntidade'] = $this->relacionamentoMultiploEntidade;
        $data['CampoPk'] = Campos::getPkByModuloId($this->extensao->getId());
        $data['moduloInfos'] = $this->extensao;
        $data['conteudosEntidadeModuloAdministravel'] = $conteudos;
        $data['moduloExtensao'] = $this->modulo;
        $data['campos'] = $this->camposModuloAdministravel;
        $data['pagina'] = 'CamposLogicos/RelacionamentoMultiplo/Lista_horizontal';
        $view = $this->load->AdminView('Home/index', $data, true);
        return $view;
    }

    /**
     * @return mixed
     */
    private function getConteudos()
    {
        $db = new ModelagemDb();
        $filtro = $db
            ->select(input_get('tabela') . '.*')
            ->join('relationship_' . input_get('anexo') . '_' . input_get('tabela') . ' rlp',
                'rlp' . '.' . input_get('tabela') . '_id = ' .
                input_get('tabela') . '.' . Campos::getPkByModuloId($this->extensao->getId())->getField() . ' AND ' .
                'rlp.' . input_get('anexo') . '_id = ' . input_get('anexoRegistro'));
        return $this->moduloAdministravel->getRows($filtro);
    }


    public function getAdministracaoInsercaoView()
    {
        $pkFieldObj = Campos::getPkByModuloId($this->extensao->getId());
        $conteudo = false;
        $editar = 0;
        if (input_get('editar')) {
            $modelagemDb = $this->load->db->where($pkFieldObj->getField(), input_get('editar'));
            $conteudo = $this->moduloAdministravel->getRow($modelagemDb);
            if ($conteudo->{'get' . Conversao::underscoreToCamelCase($pkFieldObj->getField())}()) {
                $editar = $conteudo->{'get' . Conversao::underscoreToCamelCase($pkFieldObj->getField())}();
            }
        }
        if ((int)$this->extensao->getCmsModuloTiposId() == 1) {
            $modelagemDb = $this->load->db->limit(1);
            $conteudo = $this->extensao->getRow($modelagemDb);
            if ($conteudo->{'get' . Conversao::underscoreToCamelCase($pkFieldObj->getField())}()) {
                $editar = $conteudo->{'get' . Conversao::underscoreToCamelCase($pkFieldObj->getField())}();
            }
        }
        $nomeModuloInstancia = str_replace('\\', '#', self::$NAMESCPACE . Conversao::underscoreToCamelCase($this->extensao->getTabela())).'Entidade';
        $data['editar'] = $editar;
        $data['moduloInstancia'] = $nomeModuloInstancia;
        $data['camposValoresJson'] = $this->getCamposValoresJson($conteudo);
        $data['campos'] = $this->camposModuloAdministravel;
        $data['CampoPk'] = $pkFieldObj;
        $data['moduloInfos'] = $this->extensao;
        $data['conteudoEntidadeModuloAdministravel'] = $conteudo;
        $data['pagina'] = 'CamposLogicos/RelacionamentoMultiplo/Inserir_editar';
        $view = $this->load->AdminView('Home/index', $data, true);
        return $view;
    }

    private function getCamposValoresJson($conteudo)
    {
        $camposValores = array();
        foreach ($this->camposModuloAdministravel as $campo) {
            $metodo = 'get' . Conversao::underscoreToCamelCase($campo->getField());
            $conteudoField = $conteudo && method_exists($conteudo, $metodo) ? $conteudo->{$metodo}() : null;
            $camposValores[$campo->getField()] = $conteudoField;
        }
        return json_encode($camposValores);
    }

    private $testarTabelaEntidade = 0;
    private $testarCamposEntidade = 0;

    public function salvarConteudoModuloAdministravel($entidadeConteudo, $conteudoExtensaoId = 0)
    {
        if (!$conteudoExtensaoId) {
            $conteudoExtensaoId = $entidadeConteudo->insert();
        }

        if ($conteudoExtensaoId) {
            $entidadeConteudo->update();
        }

        $chavePrimariaModulo = Identificadores::chavePrimaria(input_get('anexo'));
        $conteudoModulo = $this->relacionamentoMultiploEntidade->getRegistroAtualModulo();
        if (!isset($conteudoModulo->{$chavePrimariaModulo})) {
            return false;
        }
        $conteudoModuloId = $conteudoModulo->{$chavePrimariaModulo};

        $tabelaLigacao = 'relationship_' . input_get('anexo') . '_' . input_get('tabela');
        $classeNome = self::$NAMESCPACE . Conversao::underscoreToCamelCase($tabelaLigacao).'Entidade';
        $tabelaExiste = $this->load->db->Capsule()->schema()->hasTable($tabelaLigacao);

        if (!class_exists($classeNome) || !$tabelaExiste) {
            if ($this->testarTabelaEntidade == 0) {
                $this->regerarTabelaEntidadeLigacao($entidadeConteudo, $conteudoExtensaoId, 'testarTabelaEntidade');
            }
            return array('level' => 1, 'mensagem' => 'Entidade de ligação não foi encontrada: ' . $classeNome);
        }
        $metodos = array(
            'set' . Conversao::underscoreToCamelCase(input_get('anexo')) . 'Id' => $conteudoModuloId,
            'set' . Conversao::underscoreToCamelCase(input_get('tabela')) . 'Id' => $conteudoExtensaoId
        );

        $classe = new $classeNome();
        foreach ($metodos as $metodo => $conteudoId) {
            if (!method_exists($classe, $metodo)) {
                if ($this->testarCamposEntidade == 0) {
                    $this->load->db->Capsule()->schema()->drop('relationship_' . input_get('anexo') . '_' . input_get('tabela'));
                    $this->regerarTabelaEntidadeLigacao($entidadeConteudo, $conteudoExtensaoId, 'testarCamposEntidade');
                }
                return array('level' => 1, 'mensagem' => 'Metodo ' . $metodo . ' não foi encontrado na entidade de ligação ' . $classeNome);
            }
            $classe->{$metodo}($conteudoId);
        }
        if (!$classe->insert()) {
            return array('level' => 1, 'mensagem' => 'Erro ao tentar inserir os registros na entidade de ligação ' . $classeNome);
        }
        return array('level' => 0, 'mensagem' => 'Conteúdo salvo com sucesso!');
    }

    private function regerarTabelaEntidadeLigacao($entidadeConteudo, $conteudoExtensaoId, $testador)
    {
        $this->{$testador} = 1;
        $tabelaLigacaoNome = (new CriarTabelaRelacionamento())->CriarTabelaRelacionamento(input_get('anexo'), input_get('tabela'));
        (new Gerador())->gerarEntidade(input_get('anexo'), 'Criadores', 'ModulosAdministraveis');
        (new Gerador())->gerarEntidade(input_get('tabela'), 'Criadores', 'ModulosAdministraveis');
        (new Gerador())->gerarEntidade($tabelaLigacaoNome, 'Criadores', 'ModulosAdministraveis');
        $this->salvarConteudoModuloAdministravel($entidadeConteudo, $conteudoExtensaoId);
        exit;
    }

    public function deleteAdministracaoConteudo($entidadeConteudo)
    {
        $registroConteudoId = input_angular_http('conteudoId');
        $moduloRegistroId = input_angular_http('anexoRegistro');

        $delete = $this->load->db->where(input_get('anexo') . '_id', $moduloRegistroId)->where(input_get('tabela') . '_id', $registroConteudoId)
            ->delete('relationship_' . input_get('anexo') . '_' . input_get('tabela'));
        if(!$delete){
            return array('level' => 1, 'mensagem' => 'Conteúdo não foi deletado! Tente novamente. Registro: ' . $registroConteudoId);
        }
        return array('level'=>0, 'mensagem'=>'Registro ('.$registroConteudoId.') foi excluido da sua lista de relacionamento com sucesso!');
    }
}