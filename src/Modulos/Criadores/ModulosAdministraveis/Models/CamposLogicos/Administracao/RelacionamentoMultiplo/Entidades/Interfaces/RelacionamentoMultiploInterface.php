<?php

/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 04/02/2017
 * Time: 15:36
 */

namespace Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Administracao\RelacionamentoMultiplo\Entidades\Interfaces;

interface RelacionamentoMultiploInterface
{
    public function getModulo();

    public function getExtensao();

    public function getRegistroAtualModulo();

}