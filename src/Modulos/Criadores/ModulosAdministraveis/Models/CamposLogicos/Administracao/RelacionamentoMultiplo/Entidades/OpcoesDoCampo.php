<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 04/02/2017
 * Time: 18:00
 */

namespace Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Administracao\RelacionamentoMultiplo\Entidades;


use Core\Modelos\ModelagemDb;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsFieldOpcoesDoCampo;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo;

class OpcoesDoCampo
{

    private $opcoesDoModuloRegistro;
    private $opcoesDoModuloValores;
    private $campo;
    private $db;

    protected $nomeOpcoesDoCampos;
    protected $moduloRelacionado;
    protected $chaveDeLigacaoDoModuloRelacionado;
    protected $podeSelecionarRegistrosExistentes;

    /**
     * OpcoesDoCampo constructor.
     * @param CmsFieldOpcoesDoCampo $opcoesDoCampoRegistro
     */
    public function __construct(CmsFieldOpcoesDoCampo $opcoesDoCampoRegistro)
    {
        $this->db = new ModelagemDb();
        $this->opcoesDoModuloRegistro =  $opcoesDoCampoRegistro;
        $this->opcoesDoModuloValores = (int)$opcoesDoCampoRegistro->getValor();
        $this->campo = (new CmsField())->getRow($this->db->where('id', $opcoesDoCampoRegistro->getCmsFieldId()));
        $this->nomeOpcoesDoCampos = $this->getNomeOpcoesDoCampos();
        $this->moduloRelacionado = $this->getModuloRelacionado();
        $this->chaveDeLigacaoDoModuloRelacionado = $this->getChaveDeLigacaoDoModuloRelacionado();
        $this->podeSelecionarRegistrosExistentes = $this->getPodeSelecionarRegistrosExistentes();
    }

    /**
     * @return string
     */
    public function getNomeOpcoesDoCampos()
    {
        return $this->opcoesDoModuloRegistro->getCampo();
    }

    /**
     * @return CmsModulo
     */
    public function getModuloRelacionado()
    {
        if($this->campo->getTableFk()){
            $moduloRelacionamento = (new CmsModulo())->getRow($this->db->where('tabela', $this->campo->getTableFk()));
            return $moduloRelacionamento;
        }
        return (new CmsModulo());
    }

    /**
     * @return bool|string
     */
    public function getChaveDeLigacaoDoModuloRelacionado()
    {
        if($this->campo->getFkid()){
            return $this->campo->getFkid();
        }
        return false;
    }

    /**
     * @return int
     */
    public function getPodeSelecionarRegistrosExistentes()
    {
        return $this->opcoesDoModuloValores;
    }

}