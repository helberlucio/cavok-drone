<?php

/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 04/02/2017
 * Time: 15:39
 */

namespace Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Administracao\RelacionamentoMultiplo\Entidades\Abstracts;

use Aplicacao\Ferramentas\Identificadores;
use Core\Modelos\ModelagemDb;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsFieldOpcoesDoCampo;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo;
use Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Administracao\RelacionamentoMultiplo\Entidades\Interfaces\RelacionamentoMultiploInterface;
use Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Administracao\RelacionamentoMultiplo\Entidades\OpcoesDoCampo;
class RelacionamentoMultiploAbstract implements RelacionamentoMultiploInterface
{

    /**
     * @var ModelagemDb
     */
    private $db;

    public function __construct()
    {
        $db = new ModelagemDb();
        $this->db = $db;
    }

    /**
     * @return CmsModulo
     */
    public function getModulo()
    {
        return (new CmsModulo())->getRow($this->db->where('tabela', input_get('anexo')));
    }

    /**
     * @return CmsModulo
     */
    public function getExtensao()
    {
        return (new CmsModulo())->getRow($this->db->where('tabela', input_get('tabela')));
    }

    public function getRegistroAtualExtensao()
    {
        $chavePrimaria = Identificadores::chavePrimaria(input_get('tabela'));
        $registroAtual = $this->db->where($chavePrimaria, input_get('moduloRegistroAtual'))->get(input_get('tabela'))->row();
        if(!is_object($registroAtual)){
            return false;
        }
        $registroAtual->pk = $chavePrimaria;
        $registroAtual->valor_pk = $registroAtual->{$chavePrimaria};
        return $registroAtual;
    }

    public function getRegistroAtualModulo()
    {
        $chavePrimaria = Identificadores::chavePrimaria(input_get('anexo'));
        $registroAtual = $this->db->where($chavePrimaria, input_get('anexoRegistro'))->get(input_get('anexo'))->row();
        if(!is_object($registroAtual)){
            return false;
        }
        $registroAtual->pk = $chavePrimaria;
        $registroAtual->valor_pk = $registroAtual->{$chavePrimaria};
        return $registroAtual;
    }

    /**
     * @return OpcoesDoCampo
     */
    public function getOpcoesDoCampo()
    {
        $opcoesDoCampoRelacionamentoMultiplo = (new CmsFieldOpcoesDoCampo())
            ->getRow($this->db->where('cms_modulo_id', $this->getModulo()->getId()));
        return (new OpcoesDoCampo($opcoesDoCampoRelacionamentoMultiplo));
    }

}