<?php

/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 04/02/2017
 * Time: 16:04
 */
namespace Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Abstracts;

use Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Interfaces\CamposLogicosInterfaces;

abstract class CamposLogicosAbstracao implements CamposLogicosInterfaces
{
    /**
     * @param $tableCallBack
     * @param $campo
     * @param $table
     * @param $campoConfiguracao
     * @return mixed
     */
    public function create($tableCallBack, $campo, $table, $campoConfiguracao)
    {
        return $tableCallBack->integer($campo, false, true)->length(11)->unsigned();
    }

    /**
     * @param $tableCallBack
     * @param $campo
     * @param $table
     * @param $campoConfiguracao
     * @return mixed
     */
    public function update($tableCallBack, $campo, $table, $campoConfiguracao)
    {
        return $tableCallBack->integer($campo, false, true)->length(11)->unsigned()->change();
    }

    /**
     * @param $moduloAdministravel
     * @param $camposModuloAdministravel
     */
    public function getAdministracao($moduloAdministravel, $camposModuloAdministravel){}

}