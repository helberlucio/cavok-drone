<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 12/12/2016
 * Time: 13:23
 */

namespace Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos;


use Core\Modelos\Carregamentos;
use Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Abstracts\CamposLogicosAbstracao;

class RelacionamentoSimples extends CamposLogicosAbstracao
{

    /**
     * @param $tableCallBack
     * @param $campo
     * @param $table
     * @param $campoConfiguracao
     * @return mixed
     */
    public function create($tableCallBack, $campo, $table, $campoConfiguracao)
    {
        return $tableCallBack->string($campo, 255)->length(255);
    }

    /**
     * @param $tableCallBack
     * @param $campo
     * @param $table
     * @param $campoConfiguracao
     * @return mixed
     */
    public function update($tableCallBack, $campo, $table, $campoConfiguracao)
    {
        return $tableCallBack->string($campo, 255)->length(255)->change();
    }

}