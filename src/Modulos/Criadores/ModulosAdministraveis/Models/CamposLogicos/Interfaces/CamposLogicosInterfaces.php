<?php

/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 04/02/2017
 * Time: 16:00
 */

namespace Modulos\Criadores\ModulosAdministraveis\Models\CamposLogicos\Interfaces;
interface CamposLogicosInterfaces
{

    public function create($tableCallBack, $campo, $table, $campoConfiguracao);

    public function update($tableCallBack, $campo, $table, $campoConfiguracao);

    public function getAdministracao($moduloAdministravel, $camposModuloAdministravel);

}