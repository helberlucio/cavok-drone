<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 07/01/2017
 * Time: 17:07
 */

namespace Modulos\Criadores\ModulosAdministraveis\Models;


use Core\Modelos\ModelagemDb;

class CriarTabelaRelacionamento
{


    private $tableName;
    private $fields = array();

    private $db;
    private $schema;

    public function __construct()
    {
        $this->db = new ModelagemDb();
        $this->schema = $this->db->Capsule()->schema();
    }

    public function CriarTabelaRelacionamento($primaryTable = '', $otherTable = '')
    {
        if ($primaryTable == '' || $otherTable == '') {
            return false;
        }
        $this->CreateTableName($primaryTable, $otherTable);
        if(!$this->schema->hasTable($this->tableName)){
            $this->CreateFields($primaryTable, $otherTable);
        }
        return $this->tableName;
    }

    private function CreateTableName($primaryTable, $otherTable)
    {
        $this->tableName = 'relationship_' . $primaryTable . '_' . $otherTable;
    }

    private function CreateFields($primaryTable, $otherTable)
    {
        $fields = array();

        $fields[] = 'id';
        $fields[] = $primaryTable.'_id';
        $fields[] = $otherTable.'_id';

        $this->fields = $fields;

        $schema = $this->schema;
        $schema->create($this->tableName, function($table){
            foreach ($this->fields as $key => $field){
                if($key != 0){
                    $table->integer($field, false, true)->length(11);
                    continue;
                }
                $table->increments($field, false, true)->length(11)->unsigned();
            }
        });
    }

}