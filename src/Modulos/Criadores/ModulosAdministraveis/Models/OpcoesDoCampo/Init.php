<?php
/**
 * @description
 * @author Gabriel Ariza Gomes de Catro
 */
namespace Modulos\Criadores\ModulosAdministraveis\Models\OpcoesDoCampo;

use Aplicacao\Conversao;
use Core\Modelos\ModelagemDb;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField;

class Init
{
    const CAMPOS_NAMESPACE = '\\Modulos\\Criadores\\ModulosAdministraveis\\Models\\OpcoesDoCampo\\Campos\\';
    private $erro = array();
    /**
     * @var ModelagemDb
     */
    private $db;

    public function __construct()
    {
        $this->db = new ModelagemDb();
    }

    /**
     * @param $configuracao
     * @param $indexDaConfiguracao
     * @param $nomeDaTabela
     * @return array
     */
    public function getOpcoesDoCampoView($configuracao, $indexDaConfiguracao, $nomeDaTabela)
    {

        $tipoDoCampo = $this->db
            ->where('id', $configuracao->tipo)->limit(1)->get('cms_tipo_campos')->row();

        if (!isset($tipoDoCampo->code)) {
            $this->erro[] = 'Não foi encontrado o tipo especificado do campo.';
            return array();
        }

        $conversaoNomeDoTipoDeCampoParaNomeDaClasse = Conversao::underscoreToCamelCase($tipoDoCampo->code);
        $classe = self::CAMPOS_NAMESPACE . $conversaoNomeDoTipoDeCampoParaNomeDaClasse;

        if (!class_exists($classe)) {
            $this->erro[] = 'Classe: ' . $classe . ' não foi encontrada.';
            return array();
        }

        if (!method_exists($classe, 'getOpcoesDoCampoView')) {
            $this->erro[] = 'Metodo: getCampos não foi encontrado na classe: ' . $classe . '.';
            return array();
        }

        $instanciaClasseResponsavelPelosCampos = new $classe();
        $campos = $instanciaClasseResponsavelPelosCampos->getOpcoesDoCampoView($configuracao, $indexDaConfiguracao, $nomeDaTabela);
        return $campos;
    }

    /**
     * @param CmsField $campo
     * @return array
     */
    public function getOpcoesDoCampo(CmsField $campo)
    {
        $tipoDoCampo = current($this->db->Capsule()->table('cms_tipo_campos')
            ->where('id', '=', $campo->getCmsTipoCamposId())->limit(1)->get()->all());
        if (!isset($tipoDoCampo->code)) {
            $this->erro[] = 'Não foi encontrado o tipo especificado do campo.';
            return array();
        }

        $conversaoNomeDoTipoDeCampoParaNomeDaClasse = Conversao::underscoreToCamelCase($tipoDoCampo->code);
        $classe = self::CAMPOS_NAMESPACE . $conversaoNomeDoTipoDeCampoParaNomeDaClasse;

        if (!class_exists($classe)) {
            $this->erro[] = 'Classe: ' . $classe . ' não foi encontrada.';
            return array();
        }

        if (!method_exists($classe, 'getOpcoesDoCampo')) {
            $this->erro[] = 'Metodo: getCampos não foi encontrado na classe: ' . $classe . '.';
            return array();
        }

        $instanciaClasseResponsavelPelosCampos = new $classe();
        $campos = $instanciaClasseResponsavelPelosCampos->getOpcoesDoCampo($campo);
        return $campos;
    }

    /**
     * @param CmsField $cmsCampo
     * @param $configuracao
     * @return array|bool
     */
    public function setOpcoesDoCampo(CmsField $cmsCampo, $configuracao)
    {
        $tipoDoCampo = current($this->db->Capsule()->table('cms_tipo_campos')
            ->where('id', '=', $configuracao->tipo)->limit(1)->get()->all());

        if (!isset($tipoDoCampo->code)) {
            $this->erro[] = 'Não foi encontrado o tipo especificado do campo.';
            return array();
        }

        $conversaoNomeDoTipoDeCampoParaNomeDaClasse = Conversao::underscoreToCamelCase($tipoDoCampo->code);
        $classe = self::CAMPOS_NAMESPACE . $conversaoNomeDoTipoDeCampoParaNomeDaClasse;

        if (!class_exists($classe)) {
            $this->erro[] = 'Classe: ' . $classe . ' não foi encontrada.';
            return array();
        }

        if (!method_exists($classe, 'setOpcoesDoCampo')) {
            $this->erro[] = 'Metodo: setCampos não foi encontrado na classe: ' . $classe . '.';
            return array();
        }

        if (!isset($configuracao->opcoesDoCampo)) {
            $this->erro[] = 'Não foram encontradas opcoes de campo para o campo: ' . $configuracao->campo;
            return false;
        }

        $instanciaClasseResponsavelPelosCampos = new $classe();
        $campos = $instanciaClasseResponsavelPelosCampos->setOpcoesDoCampo($cmsCampo, $configuracao->opcoesDoCampo);
        return $campos;
    }

    /**
     * @return array
     */
    public function getErros()
    {
        return $this->erro;
    }


}