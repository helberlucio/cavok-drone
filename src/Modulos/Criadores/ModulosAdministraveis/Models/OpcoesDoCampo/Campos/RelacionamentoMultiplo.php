<?php
/**
 * @description
 * @author Gabriel Ariza Gomes de Catro
 */
namespace Modulos\Criadores\ModulosAdministraveis\Models\OpcoesDoCampo\Campos;

use Core\Modelos\Carregamentos;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsFieldOpcoesDoCampo;

class RelacionamentoMultiplo implements ACampos\APadraoClasseMetodosOpcoesDosCampos
{
    private $carregamento;
    private $erros = array();

    public function __construct()
    {
        $this->carregamento = new Carregamentos();
    }

    public function getOpcoesDoCampoView($configuracao, $indexDaConfiguracao, $nomeDaTabela)
    {
        $data['config'] = $configuracao;
        $data['index'] = $indexDaConfiguracao;
        $data['nomeDaTabela'] = $nomeDaTabela;
        return $this->carregamento->AdminView('OpcoesDoCampo/RelacionamentoMultiplo', $data, true);
    }

    public function getOpcoesDoCampo(CmsField $campo)
    {
        $opcoesDoModulo = $this->carregamento->db->where('cms_modulo_id', $campo->getCmsModuloId())->where('cms_field_id', $campo->getId())->get('cms_field_opcoes_do_campo')->row();
        if(!isset($opcoesDoModulo->campo)){
            $opcoesDoCampoConfiguracao = new \stdClass();
            $opcoesDoCampoConfiguracao->podeSelecionarRegistrosPreExistentes = 0;
            return $opcoesDoCampoConfiguracao;
        }
        $opcoesDoCampoConfiguracao = new \stdClass();
        $opcoesDoCampoConfiguracao->pode_selecionar_registros_existentes = ($opcoesDoModulo->valor ? true : false);
        return $opcoesDoCampoConfiguracao;
    }

    /**
     * @param CmsField $cmsCampo
     * @param $opcoesDoCampo
     * @return bool
     */
    public function setOpcoesDoCampo(CmsField $cmsCampo, $opcoesDoCampo)
    {
        $this->carregamento->db->where('cms_modulo_id', $cmsCampo->getCmsModuloId())->delete('cms_field_opcoes_do_campo');
        if (!isset($opcoesDoCampo->pode_selecionar_registros_existentes)) {
            (new CmsFieldOpcoesDoCampo())
                ->setCmsModuloId($cmsCampo->getCmsModuloId())
                ->setCmsFieldId($cmsCampo->getId())
                ->setCampo('pode_selecionar_registros_existentes')
                ->setValor('0')->insert();
            return true;
        }
        (new CmsFieldOpcoesDoCampo())
            ->setCmsModuloId($cmsCampo->getCmsModuloId())
            ->setCmsFieldId($cmsCampo->getId())
            ->setCampo('pode_selecionar_registros_existentes')
            ->setValor($opcoesDoCampo->pode_selecionar_registros_existentes ? 1 : 0)
            ->insert();
        return true;
    }

    public function getTratamentos()
    {

    }

}