<?php
/**
 * @description
 * @author Gabriel Ariza Gomes de Catro
 */
namespace Modulos\Criadores\ModulosAdministraveis\Models\OpcoesDoCampo\Campos;

use Core\Modelos\Carregamentos;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField;

class RelacionamentoSimples implements ACampos\APadraoClasseMetodosOpcoesDosCampos
{
    private $carregamento;

    public function __construct()
    {
        $this->carregamento = new Carregamentos();
    }

    public function getOpcoesDoCampoView($configuracao, $indexDaConfiguracao, $nomeDaTabela)
    {
        $data['config'] = $configuracao;
        $data['index'] = $indexDaConfiguracao;
        $data['nomeDaTabela'] = $nomeDaTabela;
        return $this->carregamento->AdminView('OpcoesDoCampo/RelacionamentoSimples', $data, true);
    }

    public function getOpcoesDoCampo(CmsField $campo)
    {
        // TODO: Implement getOpcoesDoCampo() method.
    }

    public function setOpcoesDoCampo(CmsField $cmsCampo, $opcoesDoCampo)
    {
        // TODO: Implement setOpcoesDoCampo() method.
    }

    public function getTratamentos()
    {
        // TODO: Implement getTratamentos() method.
    }

}