<?php
/**
 * @description
 * @author Gabriel Ariza Gomes de Catro
 */
namespace Modulos\Criadores\ModulosAdministraveis\Models\OpcoesDoCampo\Campos\ACampos;

use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField;

interface APadraoClasseMetodosOpcoesDosCampos
{

    public function getOpcoesDoCampoView($configuracao, $indexDaConfiguracao, $nomeDaTabela);

    public function getOpcoesDoCampo(CmsField $campo);

    public function setOpcoesDoCampo(CmsField $cmsCampo, $opcoesDoCampo);

    public function getTratamentos();

}