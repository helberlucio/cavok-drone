<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 27/12/2016
 * Time: 14:01
 */

namespace Modulos\Criadores\ModulosAdministraveis\Models;


use Aplicacao\Ferramentas\Ajax;
use Core\Modelos\ModelagemDb;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsExtensoes;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo;

class CriarModuloAdministravel
{

    /**
     * @var ModelagemDb
     */
    private $db;

    /**
     * CriarModuloAdministravel constructor.
     */
    public function __construct()
    {
        $this->db = new ModelagemDb();
    }

    /**
     * @description Iniciar a construcao do modulo administravel
     */
    public function Init()
    {
        $this->validarCmsModulo();
        $modulosCMS = new CmsModulo();
        $modulosCMS
            ->setIcon(input_angular_http('icone'))
            ->setModulo(input_angular_http('modulo'))
            ->setTabela(strtolower(input_angular_http('tabela')))
            ->setMenu((int)input_angular_http('menu'))
            ->setCmsModuloTiposId(input_angular_http('tipo'))
            ->setCmsModuloLayoutId(input_angular_http('layout'))
            ->setAtivo(1);
        $lastModuloInsertId = $modulosCMS->insert();
        if(!$lastModuloInsertId){
            Ajax::retorno(array('level'=>1, 'mensagem'=>'Erro ao gravar módulo! Por favor tente novamente.'));
        }
        Ajax::retorno(array('level' => 0, 'mensagem' => 'Módulo criado! Estamos te redirecionando para criar os campos, aguarde...'));
    }

    /**
     * @description Validar o modulo antes de inserir
     */
    private function validarCmsModulo()
    {
        $moduloExistente = (int)$this->moduloJaExiste();
        if ($moduloExistente) {
            Ajax::retorno(array('level' => 1, 'mensagem' => 'Módulo já existe. <span onclick="ativarModulo(' . $moduloExistente . ');"> Clique aqui para ativa-lo </span>'));
        }
        if (!input_angular_http('tabela')) {
            Ajax::retorno(array('level' => 1, 'mensagem' => 'Preencha o campo da tabela'));
        }
        if (!input_angular_http('modulo')) {
            Ajax::retorno(array('level' => 1, 'mensagem' => 'Preencha o campo Nome do Módulo'));
        }
        if (!input_angular_http('tipo')) {
            Ajax::retorno(array('level' => 1, 'mensagem' => 'Selecione o tipo de registro antes de continuar.'));
        }
        if (!input_angular_http('layout')) {
            Ajax::retorno(array('level' => 1, 'mensagem' => 'Indique o modelo de layout para a administração desse Módulo'));
        }
    }

    /**
     * @return Entidades\int
     */
    private function moduloJaExiste()
    {
        $modulosCMS = (new CmsModulo())
            ->getRow($this->db->where('tabela', input_angular_http('tabela')));
        return $modulosCMS->getId();
    }

}