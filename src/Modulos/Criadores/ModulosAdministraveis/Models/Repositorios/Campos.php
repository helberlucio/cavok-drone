<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 10/12/2016
 * Time: 16:22
 */

namespace Modulos\Criadores\ModulosAdministraveis\Models\Repositorios;


use Core\Modelos\Carregamentos;
use Core\Modelos\ModelagemDb;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsExtensoes;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo;

class Campos
{

    /**
     * @param $conteudo
     * @param CmsField $campo
     * @param CmsModulo $moduloInfos
     * @return string|void
     */
    public static function getCampoListagem($conteudo, CmsField $campo, CmsModulo $moduloInfos, $registro)
    {
        $campoCamelCase = \Aplicacao\Conversao::underscoreToCamelCase($campo->getField());
        $getCampo = 'get' . $campoCamelCase;

        $load = new Carregamentos();
        $data['db'] = new ModelagemDb();
        $data['registro'] = $registro;
        $data['conteudo'] = method_exists($conteudo, $getCampo) ? $conteudo->{$getCampo}() : false;
        $data['campo'] = $campo;
        $data['moduloInfos'] = $moduloInfos;
        $data['cmsFieldPk'] = self::getPkByModuloId($moduloInfos->getId());
        return $load->AdminView('Campos/Listagem/' . $campo->getCmsTipoCampos()->getViewName(), $data, true);
    }

    public static function getCampoInsertUpdate($conteudo, CmsField $campo, CmsModulo $moduloInfos)
    {
        $campoCamelCase = \Aplicacao\Conversao::underscoreToCamelCase($campo->getField());
        $getCampo = 'get' . $campoCamelCase;

        $load = new Carregamentos();
        $data['db'] = new ModelagemDb();
        $data['conteudo'] = method_exists($conteudo, $getCampo) ? $conteudo->{$getCampo}() : '';
        $data['registro'] = $conteudo ? $conteudo : false;
        $data['campo'] = $campo;
        $data['moduloInfos'] = $moduloInfos;
        $data['cmsFieldPk'] = self::getPkByModuloId($moduloInfos->getId());
        $viewName = $campo->getCmsTipoCampos()->getViewName();
        if(!$viewName){
            return '';
        }
        return $load->AdminView('Campos/InsertUpdate/' . $viewName, $data, true, false);
    }

    /**
     * @param $moduloId
     * @return CmsField
     */
    public static function getPkByModuloId($moduloId)
    {
        $db = new ModelagemDb();
        $filtro = $db->where('cms_modulo_id', $moduloId);
        return (new CmsField())->getRow($filtro);
    }

    /**
     * @param $tabela
     * @return CmsModulo
     */
    public static function getModuloByTable($tabela)
    {
        $db = new ModelagemDb();
        $filtro = $db->where('tabela', $tabela);
        $cmsModulo = (new CmsModulo())->getRow($filtro);
        return $cmsModulo;
    }

}