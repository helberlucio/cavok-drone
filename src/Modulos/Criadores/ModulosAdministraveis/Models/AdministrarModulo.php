<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 25/11/2016
 * Time: 21:10
 */

namespace Modulos\Criadores\ModulosAdministraveis\Models;

use Aplicacao\Conversao;
use Aplicacao\Ferramentas\Ajax;
use Aplicacao\Ferramentas\Identificadores;
use Aplicacao\Ferramentas\Paginacao;
use Aplicacao\Url;
use Core\Modelos\Carregamentos;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp;
use Modulos\Criadores\Entidades\Models\Gerador;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsUsuarios;
use Modulos\Criadores\ModulosAdministraveis\Models\Repositorios\Campos;

class AdministrarModulo
{

	private $db;
	private $load;
	private $tabela;

	static $NAMESCPACE = '\\Modulos\\Criadores\\ModulosAdministraveis\\Models\\Entidades\\';

	/**
	 * @var EntidadePadraoAbsHelp
	 */
	private $moduloAdministravel;
	/**
	 * @var CmsField[]
	 */
	private $camposModuloAdministravel;
	/**
	 * @var CmsModulo
	 */
	private $moduloCMS;

	private $tipoAdministracaoTitulo;
	private $tipoAdministracaoId;
	private $moduloAdministravelParaInstancia;
	private $extensao = 0;

	public function __construct()
	{
		$this->setRequestType();
		$this->db = new ModelagemDb();
		$this->load = new Carregamentos();
		$this->tabela = input_get('tabela');

		$filtroModuloAdministravel = $this->db->where('tabela', input_get('tabela'));
		$this->moduloCMS = (new CmsModulo())->getRow($filtroModuloAdministravel);
		$this->verificarConfiguracao();
		$moduloName = Conversao::underscoreToCamelCase($this->moduloCMS->getTabela()) . 'Entidade';
		$this->moduloAdministravelParaInstancia = self::$NAMESCPACE . $moduloName;

		(new CamposDoModulo())->alterTableCmsSettings($this->tabela);
		if (!class_exists($this->moduloAdministravelParaInstancia))
		{
			if ($this->criarEntidadeAdministracao())
			{
				header('Location:' . Url::Current());
			}
		}
		$this->moduloAdministravel = new $this->moduloAdministravelParaInstancia();
		$this->camposModuloAdministravel = (new CmsField())->getRows($this->db->where('cms_modulo_id', $this->moduloCMS->getId())->order_by('ordem', 'asc'));

		$this->tipoAdministracaoTitulo = $this->moduloCMS->getCmsModuloTipos()->getTitulo();
		$this->tipoAdministracaoId = $this->moduloCMS->getCmsModuloTiposId();
		$this->ReconfigurarModulo();
		$this->verificarConfiguracao();
	}

	private function criarEntidadeAdministracao()
	{
		$gerador = new Gerador();
		$classeCriada = $gerador->gerarEntidade($this->tabela, 'Criadores', 'ModulosAdministraveis');

		return isset($classeCriada['level']) ? ($classeCriada['level'] == 0 ? true : false) : false;
	}

	private function setRequestType()
	{
		if (input_get('metodo') == 'adicionar')
		{
			Ajax::setRequestTipo(Ajax::POST);
		}
		if (input_get('metodo') == 'editar')
		{
			Ajax::setRequestTipo(Ajax::PUT);
		}
		if (input_get('metodo') == 'delete')
		{
			Ajax::setRequestTipo(Ajax::DELETE);
		}
		if (input_get('metodo') == 'selecionar' || input_get('metodo') == 'selecionar')
		{
			Ajax::setRequestTipo(Ajax::GET);
		}
		if (input_get('editar'))
		{
			Ajax::setRequestTipo(Ajax::PUT);
		}
	}

	private function page404($mensagem = null)
	{
		$data['mensagem'] = $mensagem;
		$data['pagina'] = '404';
		$this->load->AdminView('Home/index', $data);
	}

	private function verificarConfiguracao()
	{
		if (!$this->moduloCMS->getTabela())
		{
			$this->page404('Modulo Não Encontrado!');
			exit;
		}
	}

	/**
	 * @description Reconfigurar o modulo de acordo com as acoes dadas
	 */
	private function ReconfigurarModulo()
	{
		if (input_get('tipoAdministracaoId'))
		{
			$this->tipoAdministracaoId = input_get('tipoAdministracaoId');
		}
		$this->verificarEExtensao();
	}

	/**
	 * @return bool
	 */
	private function verificarEExtensao()
	{
		$this->extensao = 0;
		if (input_get('extensao'))
		{
			$this->extensao = 1;
		}
	}

	/**
	 * @param bool $entidadeConteudo
	 * @return bool
	 */
	private function getExtensaoAdministracao($entidadeConteudo = false)
	{
		if ($this->extensao)
		{
			$namespace = '\\Modulos\\Criadores\\ModulosAdministraveis\\Models\\CamposLogicos\\';
			$classeName = $namespace . input_get('extensao');
			if (!class_exists($classeName))
			{
				return false;
			}
			$classe = new $classeName();
			$metodo = 'getAdministracao';
			if (!method_exists($classe, $metodo))
			{
				return false;
			}

			return $classe->{$metodo}($this->moduloAdministravel, $this->camposModuloAdministravel, $entidadeConteudo);
		}
	}

	/**
	 *
	 */
	public function getPaginaAdministrarModulo()
	{
		$validacoes = $this->validarModulo();
		if (!empty($validacoes))
		{
			Ajax::retorno(['level' => 1, 'mensagens' => $validacoes]);
		}

		return $this->identificarAdministracaoDoModulo();
	}

	/**
	 * @return string
	 */
	private function identificarAdministracaoDoModulo()
	{
		if ($this->extensao)
		{
			return $this->getExtensaoAdministracao();
		}
		if ($this->tipoAdministracaoId == 1)
		{
			return $this->administracaoUnicoRegistro();
		}
		if ($this->moduloCMS->getCmsModuloLayoutId() == 2)
		{
			return $this->getLayoutEmGrid();
		}

		return $this->getLayoutEmLista();
	}

	/**
	 * @return string
	 */
	private function getLayoutEmLista()
	{
		$db = clone $this->db;

		$configPagination['base_url'] = 'Admin/Modulo/Criadores/ModulosAdministraveis/Index/AdministrarModulo';
		$configPagination['total_rows'] = isset($this->db->select('count(1) as total')->get($this->tabela)->row()->total) ? $this->db->select('count(1) as total')->get($this->tabela)->row()->total : 0;
		$configPagination['per_page'] = $configPagination['total_rows'] > 500 ? ($configPagination['total_rows'] > 1000 ? 100 : 50) : ($configPagination['total_rows'] <= 50 ? 10 : 20);

		$paginacao = new Paginacao($configPagination);
		$offsetPaginacao = $paginacao->getOffset();
		$linksPaginacao = $paginacao->create_links();
		$filtro = $db->order_by($this->moduloAdministravel->getPkField(), 'DESC')->limit($offsetPaginacao['start'], $offsetPaginacao['end']);
		$conteudos = $this->moduloAdministravel->getRows($filtro);

		$data['paginacao'] = $linksPaginacao;
		$data['CampoPk'] = $pkFieldObj = Campos::getPkByModuloId($this->moduloCMS->getId());
		$data['moduloInfos'] = $this->moduloCMS;
		$data['conteudosEntidadeModuloAdministravel'] = $conteudos;
		$data['moduloExtensao'] = $this->getModuloExtensao();
		$data['campos'] = $this->camposModuloAdministravel;
		$data['pagina'] = 'LayoutListagem/Lista_horizontal';

		$view = $this->load->AdminView('Home/index', $data, true);

		return $view;
	}

	/**
	 * @return CmsModulo|bool
	 */
	private function getModuloExtensao()
	{
		if ($this->extensao)
		{
			$cmsModuloExtensao = new CmsModulo();

			return $cmsModuloExtensao->getRow($this->db->where('tabela', input_get('anexo')));
		}

		return false;
	}

	/**
	 * @return string|void
	 */
	private function getLayoutEmGrid()
	{
		$data['pagina'] = 'LayoutListagem/Grid';
		$view = $this->load->AdminView('Home/index', $data, true);

		return $view;
	}

	/**
	 * @return string
	 */
	private function administracaoUnicoRegistro()
	{
		$pkFieldObj = \Modulos\Criadores\ModulosAdministraveis\Models\Repositorios\Campos::getPkByModuloId($this->moduloCMS->getId());
		$conteudo = false;
		$editar = 0;
		if (input_get('editar'))
		{
			$modelagemDb = $this->db->where($pkFieldObj->getField(), input_get('editar'));
			$conteudo = $this->moduloAdministravel->getRow($modelagemDb);
			if ($conteudo->{'get' . Conversao::underscoreToCamelCase($pkFieldObj->getField())}())
			{
				$editar = $conteudo->{'get' . Conversao::underscoreToCamelCase($pkFieldObj->getField())}();
			}
		}
		if ((int)$this->moduloCMS->getCmsModuloTiposId() == 1)
		{
			$modelagemDb = $this->db->limit(1);
			$conteudo = $this->moduloAdministravel->getRow($modelagemDb);
			if ($conteudo->{'get' . Conversao::underscoreToCamelCase($pkFieldObj->getField())}())
			{
				$editar = $conteudo->{'get' . Conversao::underscoreToCamelCase($pkFieldObj->getField())}();
			}
		}
		$data['editar'] = $editar;
		$data['moduloInstancia'] = str_replace('\\', '#', $this->moduloAdministravelParaInstancia);
		$data['camposValoresJson'] = $this->getCamposValoresJson($conteudo);
		$data['CampoPk'] = $pkFieldObj;
		$data['moduloInfos'] = $this->moduloCMS;
		$data['conteudoEntidadeModuloAdministravel'] = $conteudo;
		$data['campos'] = $this->camposModuloAdministravel;
		$data['pagina'] = 'LayoutGravacao/inserir_editar_default';
		$view = $this->load->AdminView('Home/index', $data, true, false, true, false);

		return $view;
	}

	private function getCamposValoresJson($conteudo)
	{
		$camposValores = [];
		foreach ($this->camposModuloAdministravel as $campo)
		{
			$metodo = 'get' . Conversao::underscoreToCamelCase($campo->getField());
			$conteudoField = $conteudo && method_exists($conteudo, $metodo) ? $conteudo->{$metodo}() : null;
			$camposValores[ $campo->getField() ] = $conteudoField;
		}

		return json_encode($camposValores);
	}

	/**
	 * @return array
	 */
	private function validarModulo()
	{
		return [];
	}

	public function DeletarConteudoModuloAdministravel()
	{
		if (!input_angular_http('conteudoId'))
		{
			Ajax::retorno(['level' => 1, 'mensagem' => 'Identificador do conteúdo não foi encontrado!']);
		}
		$chavePrimaria = Identificadores::chavePrimaria(input_get('tabela'));
		$conteudoModuloAdministravelEntidade = $this->moduloAdministravel->getRow($this->db->where($chavePrimaria, input_angular_http('conteudoId')));
		if ($this->extensao)
		{
			Ajax::setRequestTipo(Ajax::DELETE);
			$salvarPelaExtensao = $this->getExtensaoAdministracao($conteudoModuloAdministravelEntidade);
			Ajax::retorno($salvarPelaExtensao);
		}
		$conteudoModuloAdministravelEntidade->delete();
		Ajax::retorno(['level' => 0, 'mensagem' => 'Conteúdo deletado com sucesso!']);
	}

	public function salvarConteudoModuloAdministravel()
	{
		$conteudo = input_angular_http('conteudo');
		$config = input_angular_http('config');
		$moduloAdministravel = str_replace('#', '\\', $config->moduloInstancia);
		/** @var  $moduloAdministravelInstancia EntidadePadraoAbsHelp */
		$moduloAdministravelInstancia = new $moduloAdministravel();

		foreach (get_object_vars($conteudo) as $field => $value)
		{
			$setter = 'set' . Conversao::underscoreToCamelCase($field);
			if (!method_exists($moduloAdministravelInstancia, $setter))
			{
				// Ajax::retorno(array('level' => 3, 'mensagem' => 'Campo ' . $field . ' não encontrado como função ' . $setter . ' na classe ' . $moduloAdministravel), false);
				continue;
			}
			$moduloAdministravelInstancia->{$setter}($value);
		}

		$admin = isset($_SESSION['admin']) ? $_SESSION['admin'] : (new CmsUsuarios());

		if (method_exists($moduloAdministravelInstancia, 'setCmsUsuarioInseriu'))
		{
			$usuarioInseriu = $admin->getId();
			if (method_exists($moduloAdministravelInstancia, 'getCmsUsuarioInseriu'))
			{
				if ($moduloAdministravelInstancia->getCmsUsuarioInseriu())
				{
					$usuarioInseriu = $moduloAdministravelInstancia->getCmsUsuarioInseriu();
				}
			}
			$moduloAdministravelInstancia->setCmsUsuarioInseriu($usuarioInseriu);
		}
		if (method_exists($moduloAdministravelInstancia, 'setCmsUsuarioEditou'))
		{
			$moduloAdministravelInstancia->setCmsUsuarioEditou($admin->getId());
		}
		if (method_exists($moduloAdministravelInstancia, 'setInsertData'))
		{
			$moduloAdministravelInstancia->setInsertData(date('Y-m-d H:i:s '));
		}

		$palavraPasse = 'salvo';
		if ($config->edicao)
		{
			Ajax::setRequestTipo(Ajax::PUT);
			$palavraPasse = 'editado';
		}

		$mensagem = 'Conteúdo ' . $palavraPasse . ' com sucesso!';
		if (ENV == 'DESENVOLVIMENTO')
		{
			$namespaces = explode('\\', $moduloAdministravel);
			$ntityName = $namespaces[6];
			$mensagem = 'Entidade "' . $ntityName . '" salvou os dados com sucesso';
		}

		if ($this->extensao)
		{
			$salvarPelaExtensao = $this->getExtensaoAdministracao($moduloAdministravelInstancia);
			Ajax::retorno($salvarPelaExtensao);
		}
		if ($config->edicao)
		{
			if (!method_exists($moduloAdministravelInstancia, 'cms_usuario'))
				if ($moduloAdministravelInstancia->update()->error)
				{
					Ajax::retorno(['level' => 1, 'mensagem' => 'Erro ao tentar atualizar: ' . $moduloAdministravelInstancia->update()->error]);
				}
			Ajax::retorno(['level' => 0, 'mensagem' => $mensagem]);
		}
		if (!$moduloAdministravelInstancia->insert())
		{
			Ajax::retorno(['level' => 1, 'mensagem' => 'Erro ao tentar salvar novo registro em ' . $ntityName]);
		}

		$fileUltimaAtualizacaoGeral = str_replace('\\', '/', realpath('./')) . '/ultima_atualizacao_geral.html';
		file_put_contents($fileUltimaAtualizacaoGeral, '#i' . date('Y-m-d H:i:s') . '#f', LOCK_EX);
		Ajax::retorno(['level' => 0, 'mensagem' => $mensagem]);
	}

}