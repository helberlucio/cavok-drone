<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */

$cmsModuloFk =  \Modulos\Criadores\ModulosAdministraveis\Models\Repositorios\Campos::getPkByModuloId($campo->getTableFk());
$campoTabelaFk = \Modulos\Criadores\ModulosAdministraveis\Models\Repositorios\Campos::getPkByModuloId($cmsModuloFk->getId());
$pk = \Aplicacao\Ferramentas\Identificadores::chavePrimaria($campo->getTableFk());
$objetoFk = $db->where($pk, $conteudo)->get($campo->getTableFk())->row(false);

if(is_object($objetoFk)){
    $itemFk = $objetoFk->{$campo->getFkText()};
    echo $itemFk;
}else{
    echo 'Não ecnontrado!';
}

