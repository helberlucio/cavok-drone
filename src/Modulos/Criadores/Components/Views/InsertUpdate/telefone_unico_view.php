<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */
?>
<div class="form-group">
    <label><b><?php echo $campo->getDescricao(); ?></b></label>
    <div class="input-group">
     <span class="input-group-addon">
        <i class="fa fa-phone"></i>
     </span>
        <input
            style="min-width: 135px;"
            type="text"
            name="<?php echo $campo->getField(); ?>"
            class="form-control <?php echo $campo->getField();?>"
            placeholder="Telefone"
            id="maskfone<?php echo $campo->getField(); ?>"
            ng-model="conteudo.<?php echo $campo->getField(); ?>"
            onchange="AtualizarValorTelefone<?php echo $campo->getField();?>()">
    </div>
</div>
<button style="opacity: 0;" class="telefone<?php echo $campo->getField(); ?>" ng-click="atualizarValorConteudoPorInput('<?php echo $campo->getField(); ?>', '<?php echo $campo->getField();?>', 'disable', 15);">
</button>
<script>
  
  function AtualizarValorTelefone<?php echo $campo->getField();?>(){
    $('.telefone<?php echo $campo->getField(); ?>').click();
   // console.log($('.<?php //echo $campo->getField();?>').val());
  }
  
    $(function(){
        var idselect = document.getElementById("maskfone<?php echo $campo->getField(); ?>");
        var im = new Inputmask("(99)9999-99999");
        im.mask(idselect);
    });
</script>