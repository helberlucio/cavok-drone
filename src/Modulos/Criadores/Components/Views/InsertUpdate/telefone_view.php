<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */
?>
<div class="form-group">
    <label class="control-label"><b><?php echo $campo->getDescricao(); ?></b></label>
    <?php
    if ($conteudo) {
        ?>
        <input id="maskfone<?php echo $campo->getField(); ?>" type="text" name="<?php echo $campo->getField(); ?>" class="form-control"
               placeholder="<?php echo $campo->getDescricao(); ?> (00) 0000-0000" value="<?php echo $conteudo;?>">
        <?php
    } else {
        ?>
        <input id="maskfone<?php echo $campo->getField(); ?>" type="text" name="<?php echo $campo->getField(); ?>" class="form-control"
               placeholder="<?php echo $campo->getField(); ?> (00) 0000-0000">
        <?php
    }
    ?>
</div>

<script>
    $(document).ready(function(){
        var idselect = document.getElementById("maskfone<?php echo $campo->getField(); ?>");
        var im = new Inputmask("(99)9999-99999");
        im.mask(idselect);
    });
</script>