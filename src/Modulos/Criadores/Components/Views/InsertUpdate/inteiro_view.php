<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */
?>
<div class="form-group">
    <label><b><?php echo $campo->getDescricao();?></b></label>

        <input ng-model="conteudo.<?php echo $campo->getField();?>" style="min-width: 155px; max-width: none !important;" type="number" name="<?php echo $campo->getField(); ?>" placeholder="<?php echo $campo->getDescricao();?>" class="form-control bfh-number">
 </div>