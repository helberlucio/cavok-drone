<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */
?>
<div class="form-group">
    <label><b><?php echo $campo->getDescricao(); ?></b></label>
    <div class="input-group">
     <span class="input-group-addon">
        <i class="fa fa-envelope"></i>
     </span>
        <input
            style="min-width: 135px;"
            type="text"
            name="<?php echo $campo->getField(); ?>"
            class="form-control"
            placeholder="Email Address"
            ng-model="conteudo.<?php echo $campo->getField(); ?>">
    </div>
</div>