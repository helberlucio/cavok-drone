<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */
?>
<div class="form-group">
    <md-input-container style="width: 100%;">
        <label><b><?php echo $campo->getDescricao(); ?></b></label>
        <input style="min-width: 135px;" id="<?php echo $campo->getField(); ?>" type="text" name="<?php echo $campo->getField(); ?>" ng-model="conteudo.<?php echo $campo->getField(); ?>">
    </md-input-container>
</div>