<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */
?>
<div class="form-group">
    <div class="col-md-12" style="padding: 0;">
        <label class="control-label"><b><?php echo $campo->getDescricao(); ?></b></label>
    </div>
    <div class="col-md-12" style="padding: 0;">
        <div class="col-md-3" style="padding: 0;">
            <md-input-container style="width: 100%;">
                <label><b>Tipo</b></label>
                <md-select ng-model="tipoVoucher<?php echo Aplicacao\Conversao::underscoreToCamelCase($campo->getField());?>">
                    <md-option value="0"><b>TIPO</b></md-option>
                    <?php
                    $typeVouchers = $this->db->get('voucher_type')->result();
                    foreach ($typeVouchers as $type) {
                        ?>
                        <md-option value="<?php echo $type->id; ?>"><?php echo $type->titulo; ?></md-option>
                        <?php
                    }
                    ?>
                </md-select>
            </md-input-container>
            <input type="text" hidden ng-model="tipoVoucher<?php echo Aplicacao\Conversao::underscoreToCamelCase($campo->getField());?>"
                   class="voucher-tipo-<?php echo $campo->getField();?>">
        </div>
        <div class="col-md-6" style="padding: 0;">
            <md-input-container style="width: 50%;" class="keyAreaNG">
                <label class="control-label"><b>Key</b></label>
                <input type="text" class="chave form-control voucherKey<?php echo $campo->getField(); ?>"
                       ng-model="conteudo.<?php echo $campo->getField(); ?>">
            </md-input-container>
            <md-input-container style="width: 24%;">
                <label class="control-label"><b>Qauntidade</b></label>
                <input type="text" ng-model="maximo_utilizacoes_model<?php echo $campo->getField();?>" class="chave form-control maximo_utilizacoes<?php echo $campo->getField(); ?>">
            </md-input-container>
            <md-input-container style="width: 24%;">
                <label class="control-label"><b>Valor</b></label>
                <input type="text" id="valor_voucher"
                       class="chave form-control voucherValor-<?php echo $campo->getField(); ?>" ng-model="valo_voucher_campo<?php echo $campo->getField();?>">
            </md-input-container>
        </div>
        <div class="col-md-3" style="padding: 15px 0 0 0; margin-top:-8px;">
            <div class="col-md-6" style="padding: 0;">
                <md-button style="width: 95%;" class="btn green" onclick="GenerateVoucher();"
                           ng-click="atualizarValorConteudoPorInput('voucher-input-hidden-key-<?php echo $campo->getField(); ?>', '<?php echo $campo->getField(); ?>');">
                    Gerar Key
                </md-button>
            </div>
            <div class="col-md-6" style="padding: 0;">
                <md-button style="width: 95%;" class="btn green confirm-key" onclick="SaveKey();">
                    Confirmar
                </md-button>
            </div>
        </div>
    </div>
</div>
<input type="text" hidden class="voucher-input-hidden-key-<?php echo $campo->getField(); ?>" ng-model="conteudo.<?php echo $campo->getField(); ?>" >
<input type="text" hidden class="maximo_utilizacoes_campo<?php echo $campo->getField();?>" ng-model="maximo_utilizacoes_model<?php echo $campo->getField();?>">
<input type="text" hidden class="valo_voucher_campo<?php echo $campo->getField();?>" ng-model="valo_voucher_campo<?php echo $campo->getField();?>">
<button style="opacity: 0;" class="buttonAtualizarTipo<?php echo $campo->getField(); ?>" ng-click="atualizarValorConteudoPorInput('voucher-tipo-<?php echo $campo->getField(); ?>', 'tipovoucher<?php echo $campo->getField(); ?>');"></button>
<button style="opacity: 0;" class="atualizarPorInput-tipo<?php echo $campo->getField(); ?>" ng-click="atualizarPorInput('voucher-tipo-<?php echo $campo->getField();?>', 'tipoVoucher<?php echo Aplicacao\Conversao::underscoreToCamelCase($campo->getField());?>');"></button>
<button style="opacity: 0;" class="atualizarPorInput-utilizacoes<?php echo $campo->getField(); ?>" ng-click="atualizarPorInput('maximo_utilizacoes_campo<?php echo $campo->getField();?>', 'maximo_utilizacoes_model<?php echo $campo->getField();?>');"></button>
<button style="opacity: 0;" class="atualizarPorInput-valor<?php echo $campo->getField(); ?>" ng-click="atualizarPorInput('valo_voucher_campo<?php echo $campo->getField();?>', 'valo_voucher_campo<?php echo $campo->getField();?>');"></button>
<script>
var timestoverify = 3;
    function editVoucher() {
        $('.load-gear').fadeIn();
        var key = $('.voucherKey<?php echo $campo->getField();?>').val();
        if (key == '') {
            setTimeout(function () {
              timestoverify = (timestoverify - 1);
              if(timestoverify >= 0){
                editVoucher();
              }else{
                $('.load-gear').fadeOut();  
              }
               console.log("verify voucher:"+timestoverify);
            }, 2000);
            return false;
        }

        if (key.length >= 26) {
            $.post('<?php echo Aplicacao\Url::ModuloAdmin("Criadores/ModulosAdministraveis/Index/GetVoucher");?>', {voucher: key}, function (data) {
                $('.voucher-tipo-<?php echo $campo->getField();?>').val(data.tipo);
                $('.maximo_utilizacoes_campo<?php echo $campo->getField();?>').val(data.maximo_utilizacoes);
                $('.valo_voucher_campo<?php echo $campo->getField();?>').val(data.valor);
                $('.atualizarPorInput-tipo<?php echo $campo->getField(); ?>').click();
                $('.atualizarPorInput-utilizacoes<?php echo $campo->getField(); ?>').click();
                $('.atualizarPorInput-valor<?php echo $campo->getField(); ?>').click();
                $('.valor').val(data.valor);
                $('.load-gear').fadeOut();
            }, 'json');
        }
    }


    function GenerateVoucher() {
        $.post('<?php echo Aplicacao\Url::ModuloAdmin("Criadores/ModulosAdministraveis/Index/GenerateVoucher");?>', {}, function (data) {
            $('.voucher-input-hidden-key-<?php echo $campo->getField();?>').val(data.voucher);
        }, 'json');
    }

    function SaveKey() {
        var tipo = $('.voucher-tipo-<?php echo $campo->getField();?>').val();
        var chave = $('.voucher-input-hidden-key-<?php echo $campo->getField();?>').val();
        var valor = $('.voucherValor-<?php echo $campo->getField();?>').val();
        var maximo_utilizacoes = $('.maximo_utilizacoes<?php echo $campo->getField();?>').val();

        $('.load-gear').fadeIn('fast');
        $.post('<?php echo Aplicacao\Url::ModuloAdmin("Criadores/ModulosAdministraveis/Index/SaveVoucher");?>',
            {tipo: tipo, chave: chave, maximo_utilizacoes: maximo_utilizacoes, valor: valor},
            function (data) {
                //var key = $('.generatekeyValue').val(data.voucher);
                console.debug(data);
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                $('.load-gear').fadeOut('slow');
                toastr.success('Voucher salvo com sucesso e pronto para uso');
            }, 'json');
    }

    $(function () {
        editVoucher();
    });
</script>
