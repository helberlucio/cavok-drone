<?php
/**
 * @var $cmsFieldPk \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db \Core\Modelos\ModelagemDb
 * @var $campo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo (string)
 */
$tableFkResults = $this->db->get($campo->getTableFk())->result(false);
?>
<div class="form-group">
    <div class="col-md-12" style="padding:0;">
        <label for="single" class="control-label"><b><?php echo $campo->getDescricao(); ?></b></label>
    </div>
    <div class="col-md-10" style="padding: 12px 0 0;">
        <md-select ng-model="conteudo.<?php echo $campo->getField() ? $campo->getField() : 0;?>" style="min-width: 145px;" id="single" name="<?php echo $campo->getField(); ?>" >
            <md-option value="0">Selecionar</md-option>
            <?php
            foreach ($tableFkResults as $value) {
                ?>
                <md-option value="<?php echo $value->{$campo->getFkid()}; ?>"><?php echo $value->{$campo->getFkText()}; ?></md-option>
                <?php
            }
            ?>
        </md-select>
    </div>
    <div class="col-md-2" style="text-align:center;padding: 0;">
        <a href="<?php echo \Aplicacao\Url::ModuloAdmin('Criadores/ModulosAdministraveis/Index/AdministrarModulo?tipoAdministracaoId=1&editar={{conteudo.'.$campo->getField().'}}&tabela='.$campo->getTableFk());?>" target="_blank">
            <md-button class="md-raised md-primary" ng-value="Primary">
                Acessar
                <i class="fa fa-sign-out" aria-hidden="true"></i>
            </md-button>
        </a>

    </div>
</div>
