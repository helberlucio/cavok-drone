<?php
/**
 * @var $cmsFieldPk  \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $moduloInfos \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsModulo
 * @var $db          \Core\Modelos\ModelagemDb
 * @var $campo       \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsField
 * @var $conteudo    (string)
 */

$campoEditor = $campo->getField();
$campoEditorMin = Aplicacao\Conversao::underscoreToCamelCase($campoEditor);

use Aplicacao\Conversao; ?>
<div id="editor<?= $campoEditorMin ?>"><?= $conteudo ?></div>


<textarea hidden
          class="realText<?= $campoEditorMin ?>"></textarea>
<button style="opacity: 0;" class="buttonAtualizarTipo<?= $campoEditor ?>"
        ng-click="atualizarValorConteudoPorInput('realText<?= $campoEditorMin ?>', '<?= $campoEditor ?>', 'disable', 15);"></button>

<script>
    $(function () {
        ClassicEditor
            .create(document.querySelector('#editor<?= $campoEditorMin ?>'), {

                toolbar: {
                    items: [
                        'heading',
                        '|',
                        'undo',
                        'redo',
                        '|',
                        'fontColor',
                        'fontSize',
                        'fontFamily',
                        '|',
                        'bold',
                        'italic',
                        'link',
                        'bulletedList',
                        'numberedList',
                        '|',
                        'alignment:left',
                        'alignment:center',
                        'alignment:right',
                        '|',
                        'indent',
                        'outdent',
                        '|',
                        'imageUpload',
                        'mediaEmbed'
                    ]
                },
                language: 'pt-br',
                image: {
                    toolbar: [
                        'imageTextAlternative',
                        'imageStyle:full',
                        'imageStyle:side'
                    ]
                },
                table: {
                    contentToolbar: [
                        'tableColumn',
                        'tableRow',
                        'mergeTableCells'
                    ]
                },
                licenseKey: '',
            })
            .then(editor => {
                editor.model.document.on('change:data', function (event) {
                    $('.realText<?= $campoEditorMin ?>').val(editor.getData());
                    $('.buttonAtualizarTipo<?= $campoEditor ?>').click();
                })
            })
            .catch(error => {
                console.error(error);
            });
    });
</script>