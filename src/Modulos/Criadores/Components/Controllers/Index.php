<?php
namespace Modulos\Criadores\Components\Controllers;

use Core\Controlador\Controlador;

class Index extends Controlador{

        public function index(){

            $data["pagina"] = "index";

            $this->load->view("Home/index", $data);

        }

}

