<?php

namespace Modulos\Criadores\SiteMap\Models\RegrasDeNegocio;

use Api\ApiExeption;
use Aplicacao\Ferramentas\Identificadores;
use Aplicacao\Url;
use ModuloHelp\PadraoCriacao\ModelRegraDeNegocio\Abstracts\ModelRegraDeNegocioPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\PalavraschavesEmpaginasUrlsEntidade;

class CriadoresSiteMapRN extends ModelRegraDeNegocioPadraoAbsHelp {

    public function gerar($output = true) {
        $urlApelido = file_get_contents('url_apelido.json');
        if (!Identificadores::eJson($urlApelido)) {
            throw new ApiExeption('Url Apelido não é json');
        }
        $urlApelido = json_decode($urlApelido);

        $baseUrl = Url::Base();

        $xml = '';
        $xml .= '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
        $xml .= '<url><loc>' . $baseUrl . '</loc><lastmod>' . date("Y-m-d") . 'T' . date("H:i:s+00:00") . '</lastmod>';
        $xml .= '<priority>1.00</priority></url>';
//        $this->db->query('truncate palavraschaves_empaginas_urls');
//        $palavrasChavesEmPaginaUrlsEntidade = (new PalavraschavesEmpaginasUrlsEntidade());
//        $palavrasChavesEmPaginaUrlsEntidade->setUrlReal('IndexControlador/')->setUrlApelido('Home')->insert();

        foreach ($urlApelido as $url) {
//            $palavrasChavesEmPaginaUrlsEntidade->setUrlReal($url->url_real)->setUrlApelido($url->url_apelido)->insert();
            $urlAtual = $baseUrl . $url->url_apelido;
            $xml .= '<url><loc>' . $urlAtual . '</loc><lastmod>' . date("Y-m-d") . 'T' . date("H:i:s+00:00") . '</lastmod><priority>0.80</priority></url>';
        }
        $xml .= '</urlset>';

        file_put_contents('sitemap.xml', $xml);
        if ($output) {
            Header("Content-type: text/xml");
            echo $xml;
        }

        $robots = 'User-agent: Googlebot' . PHP_EOL
            . 'User-agent: Slurp' . PHP_EOL
            . 'User-agent: MSNBot' . PHP_EOL
            . 'User-agent: Googlebot-Image' . PHP_EOL
            . 'User-agent: yahoo-mmcrawler' . PHP_EOL
            . 'User-agent: psbot' . PHP_EOL
            . 'User-agent: *' . PHP_EOL
            . "Sitemap: {$baseUrl}sitemap.xml";
        file_put_contents('robots.txt', $robots);
    }
}
