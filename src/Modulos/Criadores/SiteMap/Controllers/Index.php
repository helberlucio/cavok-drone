<?php

namespace Modulos\Criadores\SiteMap\Controllers;

use Api\ApiExeption;
use Modulos\Criadores\SiteMap\Models\RegrasDeNegocio\CriadoresSiteMapRN;

class Index
{

    public $admin = false;

    public function index()
    {
        try {
            (new CriadoresSiteMapRN())->gerar();
        } catch (ApiExeption $e) {
            show_array($e);
        }
    }
}