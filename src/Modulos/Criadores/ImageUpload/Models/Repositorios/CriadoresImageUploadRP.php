<?php

/**
 * Created by PhpStorm.
 * User: Gabriel PHP
 * Date: 11/06/2017
 * Time: 21:04
 */

namespace Modulos\Criadores\ImageUpload\Models\Repositorios;

use \ModuloHelp\PadraoCriacao\RepositorioPadrao\Abstracts\RepositorioPadraoAbsHelp;
use Modulos\Criadores\ImageUpload\Models\RegrasDeNegocio\CriadoresImageUploadRN;

class CriadoresImageUploadRP extends RepositorioPadraoAbsHelp
{

    public static function getIndexPage($view = 'index', $data = array(), $admin = true)
    {
        if(!isset($data['CallbackFunction'])){
            $data['CallbackFunction'] = 'ImageUpload';
        }
        if(!isset($data['microtime'])){
            $data['microtime'] = microtime();
        }
        $repositorio = new CriadoresImageUploadRN();
        return $repositorio->getModulo($view, $data, $admin);
    }

    public function getModulo($view = 'index', $data = array(), $admin = false)
    {
        return parent::getModulo($view, $data, $admin); // TODO: Change the autogenerated stub
    }
}