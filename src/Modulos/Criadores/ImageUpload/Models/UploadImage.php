<?php

namespace Modulos\Criadores\ImageUpload\Models;

use Aplicacao\Ferramentas\Ajax;
use Aplicacao\Url;

class UploadImage
{

    private $dirUpload = '';
    private $error = array();

    public function __construct()
    {
        $this->dirUpload = LI_DIR_GERENCIADOR . date('Y') . '/Uploads';
        if (isset($_SESSION['CriadoresUploadImage_dir_to_upload'])) {
            $this->dirUpload = $_SESSION['CriadoresUploadImage_dir_to_upload'];
        }
    }

    public function init()
    {
        $FileUploaded = false;
        $directory = $this->dirUpload;
        if (!isset($_POST['resumableFilename']) || !isset($_POST['resumableChunkNumber'])) {
            Ajax::retorno(array('level' => 1, 'mensagem' => 'resumableFilename Not founded! 404 Error'));
        }
        $filename = $_POST['resumableFilename'];
        $resumableChunkNumber = $_POST['resumableChunkNumber'];
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }
        // loop through files and move the chunks to a temporarily created directory
        if (!empty($_FILES)) {
            foreach ($_FILES as $key => $file) {
                if ($file['error'] != 0) {
                    echo json_encode(array('level' => 1, 'mensagem' => 'Erro ao enviar Arquivo ' . $filename));
                    continue;
                }
                $temp_dir = '';
                if (isset($_POST['resumableIdentifier']) && trim($_POST['resumableIdentifier']) != '') {
                    $temp_dir = $directory . '/' . $_POST['resumableIdentifier'];
                }

                $dest_file = $temp_dir . '/' . $filename . '.part' . $resumableChunkNumber;
                if (!is_dir($temp_dir)) {
                    mkdir($temp_dir, 0777, true);
                }
                // move the temporary file
                if (!move_uploaded_file($file['tmp_name'], $dest_file)) {
                    echo json_encode(array('level' => 1, 'mensagem' => 'Error saving (move_uploaded_file) chunk ' . $_POST['resumableChunkNumber'] . ' for file ' . $_POST['resumableFilename']));
                } else {
                    // check if all the parts present, and create the final destination file
                    $_SESSION['filename'] = $filename;
                    $FileUploaded = $this->createFileFromChunks($temp_dir, $filename, $_POST['resumableChunkSize'], $_POST['resumableTotalSize'], $_POST['resumableTotalChunks']);
                }
            }
        }
        if($FileUploaded){
            Ajax::retorno(array('dir'=>$this->dirUpload, 'file_upload'=>$FileUploaded));
        }
    }


    /**
     * @param $temp_dir
     * @param $fileName
     * @param $chunkSize
     * @param $totalSize
     * @param $total_files
     * @return bool
     */
    function createFileFromChunks($temp_dir, $fileName, $chunkSize, $totalSize, $total_files)
    {
        $total_files_on_server_size = 0;
        $temp_total = 0;
        foreach (scandir($temp_dir) as $file) {
            $temp_total = $total_files_on_server_size;
            $tempfilesize = filesize($temp_dir . '/' . $file);
            $total_files_on_server_size = $temp_total + $tempfilesize;
        }

        if ($total_files_on_server_size >= $totalSize) {
            if (($fp = fopen($temp_dir . $fileName, 'w')) !== false) {
                for ($i = 1; $i <= $total_files; $i++) {
                    fwrite($fp, file_get_contents($temp_dir . '/' . $fileName . '.part' . $i));
                }
                fclose($fp);
            } else {
                die('cannot create the destination file');
            }
            if (rename($temp_dir, $temp_dir . '_UNUSED')) {
               $newFile = $this->rrmdir($temp_dir . '_UNUSED');
            } else {
                $newFile = $this->rrmdir($temp_dir);
            }
            return $newFile;
        }
        return false;
    }

    /**
     * @param $dir
     * @return string
     * @description Delete a directory RECURSIVELY
     */
    function rrmdir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);

            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir") {
                        $this->rrmdir($dir . "/" . $object);
                    } else {
                        unlink($dir . "/" . $object);
                    }
                }
            }
            $identifie = input_post('resumableIdentifier') . input_post('resumableFilename');
            $newIdentfie = strtoupper(Url::Title(substr(strtoupper(sha1($identifie . time())), 0, 6) . input_post('resumableFilename')));
            $targetFile = $this->dirUpload . '/' . $identifie;
            $newTargetFile = $this->dirUpload . '/' . $newIdentfie;
            $imageFileType = pathinfo($targetFile, PATHINFO_EXTENSION);
            rename($targetFile, $newTargetFile . '.' . $imageFileType);
            reset($objects);
            rmdir($dir);
            return $newIdentfie.'.'.$imageFileType;
        }
    }
}
