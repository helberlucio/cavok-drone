<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 23/12/2016
 * Time: 09:11
 * @var $CallbackFunction
 * @var $microtime
 */
$microtimeHash = substr(sha1($microtime), 5, 9);
Aplicacao\Ferramentas\GetAssets::CSS('resumable', 'Criadores', 'ImageUpload');
?>
<script>
    var urlDragAndDropImage = '<?php echo \Aplicacao\Url::Base() . \Aplicacao\Ferramentas\GetAssets::IMAGE('DragAndDrop.jpg', 'Criadores', 'ImageUpload'); ?>';
</script>
<?php
Aplicacao\Ferramentas\GetAssets::JS('aplicar_no_botao', 'Criadores', 'ImageUpload');
Aplicacao\Ferramentas\GetAssets::JS('resumable', 'Criadores', 'ImageUpload');
?>
<div class="col-md-12 areaToolsImageUpload areaToolsImageUpload-<?php echo $microtimeHash; ?>">
    <div id="progress-upload">
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                 style="width: 0%;">
                <span class="sr-only" style="position: relative;">0% Complete</span>
            </div>
        </div>
    </div>
    <div id="buttonUpload" class="imagePreview"
         style="
                 background-position: center !important;
                 background-size: auto 100% !important;
                 background-repeat: no-repeat !important;
                 background-image: url(<?php echo \Aplicacao\Url::Base() . \Aplicacao\Ferramentas\GetAssets::IMAGE('DragAndDrop.jpg', 'Criadores', 'ImageUpload'); ?>);">
    </div>
    <button class="finishedUploadImage"><i class="fa fa-check-circle" aria-hidden="true"></i></button>
    <div class="col-md-12 buttons-actionArea">
        <div class="col-md-6 divisorias">
            <button class="btn btn-danger" onclick="cancelImageUpload();" type="button">
               Remover
            </button>
        </div>
        <div class="col-md-6 divisorias">
            <button class="btn btn-success startUpload" type="button">
                Confirmar <i class="fa fa-check-circle-o" aria-hidden="true"></i>
            </button>
        </div>
    </div>
</div>

<script>
    var fileToCancel;
    var fileObj;

function cancelImageUpload() {
    $('.imagePreview').css({'background-image': 'url(' + urlDragAndDropImage + ')'});
    $('.finishedUploadImage').fadeOut(90);
    $('.startUpload').fadeOut(90);
    $('.buttons-actionArea').fadeOut(90);
    fileToCancel.cancel();
}

$(function () {
    var d = new Date();
    var microtime = d.getMilliseconds();
    var microtimeHash = '<?php echo $microtimeHash;?>';
    var maxFiles = 1;
    var viewFile = '';

    var obj = {
        url: base_url('Modulo/Criadores/ImageUpload/Index/initUpload?up=1'),
        chunkSize: ((1 * 1024) * 1024),
        simultaneousUploads: 2,
        testChunks: false,
        throttleProgressCallbacks: 1,
        maxFiles: maxFiles
    };

    window['a_' + microtime] = new Resumable({
        target: obj.url,
        chunkSize: obj.chunkSize,
        simultaneousUploads: obj.simultaneousUploads,
        testChunks: obj.testChunks,
        throttleProgressCallbacks: obj.throttleProgressCallbacks,
        maxFilesErrorCallback: function (files, errorCount) {
            $('.callback-' + microtimeHash).empty().append('Selecione no maximo '+maxFiles+' arqivo'+(maxFiles.length > 1 ? 's' : ''));
        },
        maxFiles: obj.maxFiles
    });

    window['a_' + microtime].assignBrowse($('.areaToolsImageUpload-' + microtimeHash + ' #buttonUpload'));
    window['a_' + microtime].assignDrop($('.areaToolsImageUpload-' + microtimeHash + ' #buttonUpload'));

    window['a_' + microtime].on('fileSuccess', function (file, res) {
        $('.finishedUploadImage').fadeIn(1000);
        setTimeout(function () {
            $('.finishedUploadImage').fadeOut(600);
        }, 2500);
        var ObjResponse = $.parseJSON(res);
        window['<?php echo $CallbackFunction;?>'](ObjResponse.dir + '/' + ObjResponse.file_upload, viewFile, $('.areaToolsImageUpload-' + microtimeHash + ' .imagePreview'), fileObj);
    });
    window['a_' + microtime].on('fileAdded', function (file, event) {
        $('.startUpload').fadeIn();
        $('.buttons-actionArea').fadeIn();

        var fileToRender = {};
        $.each(window['a_' + microtime].files, function (key, val) {
            fileToRender = window['a_' + microtime].files[key].file;
        });
        var showImage = false;
        var imageThumb = event.target.result;
        if (fileToRender.type === 'image/png' ||
            fileToRender.type === 'image/jpg' ||
            fileToRender.type === 'image/jpeg' ||
            fileToRender.type === 'image/gif' ||
            fileToRender.type === 'image/bmp') {
            showImage = true;
        }

        if (!showImage) {
            identifieFile(fileToRender.name);
        }

        if (showImage) {
            var fileReaderObj = new FileReader();
            fileReaderObj.readAsDataURL(fileToRender);
            fileReaderObj.onload = function (event) {
                viewFile = '';
                $('.areaToolsImageUpload-' + microtimeHash + ' .imagePreview').css({'background-image': 'url(' + event.target.result + ')'});
            };
        }
        fileObj = fileToRender;
        fileToCancel = file;
    });

    window['a_' + microtime].on('uploadStart', function () {
        //$('.areaToolsImageUpload-' + microtimeHash + ' .buttons-actionArea').fadeOut();
        $('.areaToolsImageUpload-' + microtimeHash + ' #progress-upload').fadeIn(90);
        $('.areaToolsImageUpload-' + microtimeHash + ' .startUpload').fadeOut();
        if (obj.progress === '1') {
            $.fn.progress.start();
        }
    });
    window['a_' + microtime].on('complete', function (arg1, arg2) {
        $('.areaToolsImageUpload-' + microtimeHash + ' #progress-upload').fadeOut(300);
        window['a_' + microtime].cancel();
        if (obj.progress === '1') {
            $.fn.progress.stop();
        }
    });
    window['a_' + microtime].on('fileError', function (file, message) {
        console.log(message);
        console.log(file);
    });
    window['a_' + microtime].on('progress', function () {
        var porcentagem = parseInt(window['a_' + microtime].progress() * 100);
        $('.areaToolsImageUpload-' + microtimeHash + ' #progress-upload').css('display', 'block');
        $('.areaToolsImageUpload-' + microtimeHash + ' #progress-upload .progress-bar .sr-only').html(porcentagem + '%');
        $('.areaToolsImageUpload-' + microtimeHash + ' #progress-upload .progress-bar').attr('aria-valuenow', porcentagem)
            .css({width: porcentagem + '%'});
        if (porcentagem === 100) {
            $('.areaToolsImageUpload-' + microtimeHash + ' #progress-upload .progress-bar .sr-only').html('Completo!');
        }
    });

    $('.startUpload').click(function () {
        window['a_' + microtime].upload();
    });
    function identifieFile(arquivo) {
        $.post(base_url('Admin/Modulo/Criadores/ImageUpload/IdentificarArquivo'), {arquivo: arquivo}, function (data) {
            viewFile = data.file;
            $('.areaToolsImageUpload-' + microtimeHash + ' .imagePreview').css({'background-image': 'url(' + base_url(viewFile) + ')'});
        }, 'json');
    }
});
</script>
