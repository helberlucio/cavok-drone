<?php
/**
 * Created by PhpStorm.
 * User: Gabriel PHP
 * Date: 26/06/2017
 * Time: 18:25
 */

namespace Modulos\Criadores\ImageUpload\Controllers;


use Aplicacao\Ferramentas\Ajax;
use Aplicacao\Ferramentas\Identificadores;
use Aplicacao\Url;
use Core\Controlador\Controlador;

class IdentificarArquivo extends Controlador
{

    public function index()
    {
        $partesArquivo = explode('.', input_post('arquivo'));
        $extensao = end($partesArquivo);
        $imagemReferentAExtensao = 'src/Assets/Images/Default/icons/files/' . $extensao . '.png';
        if (!is_file($imagemReferentAExtensao)) {
            $imagemReferentAExtensao = 'src/Assets/Images/Default/icons/files/default.png';
        }
        Ajax::retorno(array('file' => $imagemReferentAExtensao.'?realFile='.$extensao.'.png'));
    }

}