<?php
namespace Modulos\Criadores\ImageUpload\Controllers;

use Core\Controlador\Controlador;
use Modulos\Criadores\ImageUpload\Models\UploadImage;

class Index extends Controlador
{

    public function index()
    {
        $data["pagina"] = "index";
        $this->load->view("Home/index", $data);
    }

    public function initUpload()
    {
        $uploadImage = new UploadImage();
        $uploadImage->init();
    }

}

