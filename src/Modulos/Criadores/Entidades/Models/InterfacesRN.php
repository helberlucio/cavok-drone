<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 28/08/2016
 * Time: 16:26
 */

namespace system\Libraries\Modulos\GeradorCodigo\EntidadeRepositorio\Models;

class InterfacesRN
{
    private $modulo;
    private $interface;
    private $metodos;
    private $codigoGerado;

    /**
     * Interfaces constructor.
     * @param $modulo
     * @param $interface
     * @param $metodos
     */
    public function __construct($modulo = null, $interface = null, $metodos = null)
    {
        if (null !== $modulo) {
            $this->setModulo($modulo);
        }
        if (null !== $interface) {
            $this->setInterface($interface);
        }
        if (null !== $metodos) {
            $this->setMetodos($metodos);
        }
    }


    /**
     * @param string $modulo
     * @return InterfacesRN
     */
    public function setModulo($modulo)
    {
        $this->modulo = $modulo;
        return $this;
    }

    /**
     * @param string $interface
     * @return InterfacesRN
     */
    public function setInterface($interface)
    {
        $this->interface = underscoreToCamelCase($interface);
        return $this;
    }

    /**
     * @param array $metodos
     * @return InterfacesRN
     */
    public function setMetodos($metodos)
    {
        $this->metodos = $metodos;
        return $this;
    }


    /**
     * @return InterfacesRN
     */
    public function gerar()
    {
        if (!isset($this->interface) || !isset($this->modulo) || !isset($this->metodos)) {
            return $this;
        }

        if (!defined('TAB')) {
            define('TAB', '    ');
        }

        $this->codigoGerado = '<?php' . PHP_EOL . PHP_EOL;
        $this->codigoGerado .= 'namespace library\\Modulos\\' . str_replace('/', '\\',$this->modulo) . '\\Interfaces;' . PHP_EOL . PHP_EOL;
        $this->codigoGerado .= Ferramentas::linha("use library\Modulos\\".str_replace('/', '\\',$this->modulo)."\\Entity\\{$this->interface} as {$this->interface}Entidade;");
        $this->codigoGerado .= Ferramentas::linha();
        $this->codigoGerado .= 'interface ' . $this->interface . PHP_EOL;
        $this->codigoGerado .= '{' . PHP_EOL;
        foreach ($this->metodos as $metodo) {
            switch ($metodo) {
                case 'buscar_por_id':
                    $this->codigoGerado .= TAB . '/**' . PHP_EOL;
                    $this->codigoGerado .= TAB . ' * @param int $id' . PHP_EOL;
                    $this->codigoGerado .= TAB . ' */' . PHP_EOL;
                    $this->codigoGerado .= TAB . 'public function ' . underscoreToPascalCase($metodo) . '($id);' . PHP_EOL . PHP_EOL;
                    break;
                case 'salvar':
                    $this->codigoGerado .= TAB . '/**' . PHP_EOL;
                    $this->codigoGerado .= TAB . " * @param {$this->interface}Entidade \$registro" . PHP_EOL;
                    $this->codigoGerado .= TAB . ' */' . PHP_EOL;
                    $this->codigoGerado .= TAB . 'public function ' . underscoreToPascalCase($metodo) . "({$this->interface}Entidade \$registro);" . PHP_EOL . PHP_EOL;
                    break;
                case 'buscar':
                    $this->codigoGerado .= TAB . '/**' . PHP_EOL;
                    $this->codigoGerado .= TAB . " * @param string \$campo <p>Campo do banco de dados (aceita 'campo >', 'campo <=')</p>" . PHP_EOL;
                    $this->codigoGerado .= TAB . ' * @param string $valor <p>Valor do campo do banco de dados</p>' . PHP_EOL;
                    $this->codigoGerado .= TAB . ' * @param boolean $like <p>Se a pesquisa usara like \'%valor%\'</p>' . PHP_EOL;
                    $this->codigoGerado .= TAB . ' */' . PHP_EOL;
                    $this->codigoGerado .= TAB . 'public function ' . underscoreToPascalCase($metodo) . '($campo, $valor, $like = false);' . PHP_EOL . PHP_EOL;
                    break;
                case 'pesquisar':
                    $this->codigoGerado .= TAB . '/**' . PHP_EOL;
                    $this->codigoGerado .= TAB . " * @param array \$parametros Ex.: array( array('campo','valor'), array('campo >','valor','OR'), array('campo','valor','LIKE') )" . PHP_EOL;
                    $this->codigoGerado .= TAB . ' */' . PHP_EOL;
                    $this->codigoGerado .= TAB . 'public function ' . underscoreToPascalCase($metodo) . '($parametros);' . PHP_EOL . PHP_EOL;
                    break;
                case 'adicionar':
                    $this->codigoGerado .= TAB . '/**' . PHP_EOL;
                    $this->codigoGerado .= TAB . " * @param {$this->interface}Entidade \$registro" . PHP_EOL;
                    $this->codigoGerado .= TAB . ' */' . PHP_EOL;
                    $this->codigoGerado .= TAB . 'public function ' . underscoreToPascalCase($metodo) . "({$this->interface}Entidade \$registro);" . PHP_EOL . PHP_EOL;
                    break;
                case 'remover':
                    $this->codigoGerado .= TAB . '/**' . PHP_EOL;
                    $this->codigoGerado .= TAB . " * @param {$this->interface}Entidade \$registro" . PHP_EOL;
                    $this->codigoGerado .= TAB . ' */' . PHP_EOL;
                    $this->codigoGerado .= TAB . 'public function ' . underscoreToPascalCase($metodo) . "({$this->interface}Entidade \$registro);" . PHP_EOL . PHP_EOL;
                    break;
                case 'atualizar':
                    $this->codigoGerado .= TAB . '/**' . PHP_EOL;
                    $this->codigoGerado .= TAB . " * @param {$this->interface}Entidade \$registro" . PHP_EOL;
                    $this->codigoGerado .= TAB . ' */' . PHP_EOL;
                    $this->codigoGerado .= TAB . 'public function ' . underscoreToPascalCase($metodo) . "({$this->interface}Entidade \$registro);" . PHP_EOL . PHP_EOL;
                    break;
                default:
                    $this->codigoGerado .= TAB . 'public function ' . underscoreToPascalCase($metodo) . '();' . PHP_EOL . PHP_EOL;
            }
        }

        $this->codigoGerado .= TAB . '/**' . PHP_EOL;
        $this->codigoGerado .= TAB . ' * @return array Chave(s) Primaria(s) da Tabela' . PHP_EOL;
        $this->codigoGerado .= TAB . ' */' . PHP_EOL;
        $this->codigoGerado .= TAB . 'public function chavePrimaria();' . PHP_EOL;

        $this->codigoGerado .= '}' . PHP_EOL . PHP_EOL;

        return $this;

    }

    /**
     * @return InterfacesRN
     */
    public function salvar()
    {
        if (empty($this->codigoGerado)) {
            return $this;
        }
        $pasta = LI_DIR_MODULOS
            . $this->modulo . DIRECTORY_SEPARATOR
            . 'Interfaces';

        if (!is_dir($pasta)) {
            @mkdir($pasta);
            @chmod($pasta, 0777);
        }

        //src/Modulos/NomeDoModulo/Interfaces/Arquivo.php
        $arquivo = $pasta . DIRECTORY_SEPARATOR
            . $this->interface . EXT;

        file_put_contents($arquivo, $this->codigoGerado);
        @chmod($arquivo, 0777);

        return $this;
    }
}