<?php

/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 23/11/2016
 * Time: 21:37
 */

namespace Modulos\Criadores\Entidades\Models;

use Core\Modelos\ModelagemDb;

class Gerador
{
    private $entidades_compos_relacionados = array();

    public function gerarEntidade($tabela = null, $grupoModulo = null, $modulo = null, $raiz = null)
    {
        if($grupoModulo && !$modulo){
            return array('level' => 1, 'mensagem' => 'Indique o modulo para o grupo de módulo: '.$grupoModulo);
        }

        if($modulo && !$grupoModulo){
            return array('level' => 1, 'mensagem' => 'Indique o grupo de módulo para o módulo: '.$modulo);
        }

        if (!$raiz && (!$grupoModulo && !$modulo)) {
            return array('level' => 1, 'mensagem' => 'Antes de gerar sua entidade, selecione o diretório onde será gerada.');
        }

        $diretorio = '';
        if (($grupoModulo && $modulo) && !$raiz) {
            $diretorio = 'Modulos/'.$grupoModulo . '/' . $modulo.'/Models/Entidades/';
        }
        if ($raiz) {
            $diretorio = $raiz;
        }

        if (!$tabela) {
            return array('level' => 1, 'mensagem' => 'Antes de gerar sua entidade, selecione a tabela.');
        }

        $this->setTabelasLigacao($tabela);
        $Entidades = new EntidadesRN();

        foreach ($this->entidades_compos_relacionados as $tabelaEntidade => $entidadesRelacionadas) {
            $campos = $this->getCamposTabela($tabelaEntidade);
            $Entidades
                ->setModulo($diretorio)
                ->setTabela($tabelaEntidade)
                ->setRelacionamentos($entidadesRelacionadas)
                ->setTodosRelacionamentosPossiveis($this->entidades_compos_relacionados)
                ->setCampos($campos)
                ->gerar()
                ->salvar();
        }
        return array('level' => 0, 'mensagem' => 'Entidades no padrão ORM Criadas com sucesso em: <b>'.$diretorio.'</b>');

    }

    /**
     *@description Setar tabelas de ligacao
     * */
    private function setTabelasLigacao($tabela)
    {
        $db = new ModelagemDb();
        $colunas = $db->select('COLUMN_NAME, TABLE_NAME')
            ->where('TABLE_NAME !=', $tabela)
            ->where('`table_schema`', LI_CONNECT_DATABASE)
            ->where('COLUMN_NAME', $tabela . '_id')
            ->get('`information_schema`.`COLUMNS`')
            ->result(false);

        $ligacoes = array();
        foreach ($colunas as $coluna) {
            $ligacoes[] = array('tabela' => $coluna->TABLE_NAME, 'coluna' => $coluna->COLUMN_NAME);
            $this->setTabelasLigacao($coluna->TABLE_NAME);
        }
        $this->entidades_compos_relacionados[$tabela] = $ligacoes;
    }

    /**
     * @param $tabela
     * @return array
     */
    private static function getCamposTabela($tabela)
    {
        $db = new ModelagemDb();
        $output = array();
        $result = $db->select('`column_name`')
            ->where('`table_schema`', LI_CONNECT_DATABASE)
            ->where('`table_name`', $tabela)
            ->get('`information_schema`.`columns`')
            ->result();
        foreach ($result as $row) {
            $output[] = $row->column_name;
        }
        return $output;
    }

}