<?php

namespace Modulos\Criadores\Entidades\Models;

use Aplicacao\Conversao;
use \Aplicacao\Ferramentas\Criacao;
use Aplicacao\Ferramentas\Identificadores;
use Core\Modelos\ModelagemDb;

/**
 * Created by PhpStorm.
 * User: Gabriel Ariza
 * Date: 28/08/16
 * Time: 16:39
 */
class EntidadesRN
{

    private $modulo;
    private $entidade;
    private $relacionamentos;
    private $todosRelacionamentos;
    private $tabela;
    private $campos;
    private $tabelaLinguagem;
    private $camposLinguagem;
    private $codigoGerado;

    /**
     * Entidades constructor.
     * @param $modulo
     * @param $tabela
     * @param $campos
     * @param $tabelaLinguagem
     * @param $camposLinguagem
     */
    public function __construct(
        $modulo = null,
        $tabela = null,
        $campos = null,
        $tabelaLinguagem = null,
        $camposLinguagem = null
    )
    {
        if (null !== $modulo) {
            $this->setModulo($modulo);
        }
        if (null !== $tabela) {
            $this->setTabela($tabela);
        }
        if (null !== $tabelaLinguagem) {
            $this->setTabelaLinguagem($tabelaLinguagem);
        }
        if (null !== $campos) {
            $this->setCampos($campos);
        }
        if (null !== $camposLinguagem) {
            $this->setCamposLinguagem($camposLinguagem);
        }
    }

    /**
     * @param mixed $tabelaLinguagem
     * @return EntidadesRN
     */
    public function setTabelaLinguagem($tabelaLinguagem)
    {
        $this->tabelaLinguagem = $tabelaLinguagem;
        return $this;
    }

    /**
     * @param $campos
     * @return EntidadesRN
     */
    public function setCamposLinguagem($campos)
    {
        $prefixo = '';
        if (sizeof($campos) && strpos($campos[0], '_') !== false) {
            $prefixo = current(explode('_', $campos[0])) . '_';
            $prefixo = strpos_array($campos, $prefixo) !== false
                ? $prefixo
                : '';
        }

        $this->camposLinguagem = array();
        foreach ($campos as $campo) {
            $this->camposLinguagem[$campo] = array(
                'nome' => $campo,
                'get' => Conversao::campoParaGet($campo, $prefixo),
                'set' => Conversao::campoParaSet($campo, $prefixo),
                'padrao' => 'null',
                'tipo' => Conversao::tipoDoBancoParaPHP(Identificadores::tipoColuna($this->tabela, $campo)),
                'prefixo' => $prefixo,
                'tamanho' => 0,
            );
        }
        $this->buscaValoresPadrao($this->camposLinguagem, $this->tabelaLinguagem);
        return $this;
    }


    /**
     * @param mixed $tabela
     * @return EntidadesRN
     */
    public function setTabela($tabela)
    {
        $this->tabela = $tabela;
        $this->setEntidade($tabela);
        return $this;
    }

    /**
     * @param array $relacionamentos
     * @return $this
     */
    public function setRelacionamentos($relacionamentos = array())
    {
        $this->relacionamentos = $relacionamentos;
        return $this;
    }

    public function setTodosRelacionamentosPossiveis($relacionamentos = array())
    {
        $this->todosRelacionamentos = $relacionamentos;
        return $this;
    }

    /**
     * @param string $modulo
     * @return EntidadesRN
     */
    public function setModulo($modulo)
    {
        $this->modulo = $modulo;
        return $this;
    }

    /**
     * @param string $entidade
     * @return EntidadesRN
     */
    public function setEntidade($entidade)
    {
        $this->entidade = Conversao::underscoreToCamelCase($entidade);
        return $this;
    }

    /**
     * @param array $campos
     * @return EntidadesRN
     */
    public function setCampos($campos)
    {
        $prefixo = '';
        if (sizeof($campos) && strpos($campos[0], '_') !== false) {
            $prefixo = current(explode('_', $campos[0])) . '_';
            $prefixo = strpos_array($campos, $prefixo) !== false
                ? $prefixo
                : '';
        }

        $this->campos = array();
        foreach ($campos as $campo) {
            $this->campos[$campo] = array(
                'nome' => $campo,
                'get' => Conversao::campoParaGet($campo, $prefixo),
                'set' => Conversao::campoParaSet($campo, $prefixo),
                'padrao' => 'null',
                'tipo' => Conversao::tipoDoBancoParaPHP(Identificadores::tipoColuna($this->tabela, $campo)),
                'prefixo' => $prefixo,
                'tamanho' => 0,
            );
        }
        $this->buscaValoresPadrao($this->campos, $this->tabela);
        return $this;
    }


    /**
     * @return EntidadesRN
     */
    public function gerar()
    {
        if (!isset($this->entidade) || !isset($this->modulo) || !isset($this->campos) || !isset($this->tabela)) {
            return $this;
        }

        if (!defined('TAB')) {
            define('TAB', '    ');
        }

        $prefixo = current($this->campos)['prefixo'];

        $namespace = substr(str_replace('/', '\\', $pasta = $this->modulo . DIRECTORY_SEPARATOR), 0, -2);
        $this->codigoGerado = Criacao::linha('<?php', 0, 2);
        $this->codigoGerado .= Criacao::linha('namespace ' . $namespace . ';', 0, 2);
        $this->codigoGerado .= Criacao::linha('use Aplicacao\Conversao;');
        $this->codigoGerado .= Criacao::linha('use Aplicacao\GenericRepository;');
        $this->codigoGerado .= Criacao::linha('use Core\Modelos\ModelagemDb;');
        $this->codigoGerado .= Criacao::linha('use ModuloHelp\PadraoCriacao\EntidadePadrao\Abstracts\EntidadePadraoAbsHelp as EntidadeHelp;', 0, 1);
        $this->codigoGerado .= Criacao::linha('class ' . $this->entidade . 'Entidade extends EntidadeHelp');
        $this->codigoGerado .= Criacao::linha('{');


        foreach ($this->campos as $key => $value) {
            $this->codigoGerado .= Criacao::linha("private \${$key};", 1);
        }
        if (null !== $this->camposLinguagem) {
            foreach ($this->camposLinguagem as $key => $value) {
                $this->codigoGerado .= Criacao::linha("private \${$key};", 1);
            }
        }

        $this->codigoGerado .= Criacao::linha();

        /* Construct com valores padrao */
        $this->codigoGerado .= Criacao::linha("public function __construct()", 1);
        $this->codigoGerado .= Criacao::linha("{", 1);
        $this->codigoGerado .= Criacao::linha("parent::__construct();", 2, 1);
        $this->codigoGerado .= Criacao::linha('self::$TABELA = "'.$this->tabela.'";', 2, 1);
        $this->codigoGerado .= Criacao::linha('self::$PREFIXO = "'.$prefixo.'";', 2, 1);
        $this->codigoGerado .= Criacao::linha('self::$NAMESPACE_FILE = ' ."'". $namespace . "'".';', 2, 1);
        $this->codigoGerado .= Criacao::linha('self::$FILE_NAME = '."'". $this->entidade ."Entidade'".';', 2, 1);

        foreach ($this->campos as $key => $value) {
            $valor = "'" . $value['padrao'] . "'";
            if (is_numeric($value['padrao'])) {
                $valor = $value['padrao'];
            }
            if ($value['padrao'] == 'CURRENT_TIMESTAMP') {
                $valor = "date('Y-m-d H:i:s')";
            }
            if ($valor != "''") {
                $this->codigoGerado .= Criacao::linha("\$this->{$key} = {$valor};", 2);
            }
        }
        if (null !== $this->camposLinguagem) {
            foreach ($this->camposLinguagem as $key => $value) {
                $valor = "'" . $value['padrao'] . "'";
                if (is_numeric($value['padrao'])) {
                    $valor = $value['padrao'];
                }
                if ($value['padrao'] == 'CURRENT_TIMESTAMP') {
                    $valor = "date('Y-m-d H:i:s')";
                }
                if ($valor != "''") {
                    $this->codigoGerado .= Criacao::linha("\$this->{$key} = {$valor};", 2);
                }
            }
        }
        $this->codigoGerado .= Criacao::linha('}', 1, 2);

        /* GET / SET */
        foreach ($this->campos as $key => $value) {

            $campoNome = $value['nome'];
            $funcaoNome = Conversao::underscoreToCamelCase($campoNome);
            $this->codigoGerado .= Criacao::linha('/**', 1);

            $this->codigoGerado .= Criacao::linha(" * @return {$value['tipo']}", 1);
            $this->codigoGerado .= Criacao::linha(' */', 1);
            $this->codigoGerado .= Criacao::linha("public function get" . $funcaoNome . "()", 1);
            $this->codigoGerado .= Criacao::linha('{', 1);

            $this->codigoGerado .= Criacao::linha("return ({$value['tipo']})\$this->{$key};", 2);
            $this->codigoGerado .= Criacao::linha('}', 1, 2);

            $this->codigoGerado .= Criacao::linha('/**', 1);
            $this->codigoGerado .= Criacao::linha(" * @param {$value['tipo']} \${$key}", 1);
            $this->codigoGerado .= Criacao::linha(" * @return {$this->entidade}Entidade", 1);
            $this->codigoGerado .= Criacao::linha(' */', 1);
            $this->codigoGerado .= Criacao::linha("public function set" . $funcaoNome . "(\${$key})", 1);
            $this->codigoGerado .= Criacao::linha('{', 1);
            $this->codigoGerado .= Criacao::linha("\$this->{$key} = \${$key};", 2);
            $this->codigoGerado .= Criacao::linha('return $this;', 2);
            $this->codigoGerado .= Criacao::linha('}', 1, 2);
        }


        // Cria os relacionamentos da tabela 1 p N
        foreach ($this->relacionamentos as $relacionamento) {

            $tabelaRelacionamento = Conversao::underscoreToCamelCase($relacionamento['tabela']);
            $this->codigoGerado .= Criacao::linha('/**', 1);
            $this->codigoGerado .= Criacao::linha(' * @return \\' . $namespace . '\\' . $tabelaRelacionamento . '[]', 1);
            $this->codigoGerado .= Criacao::linha(' */', 1);
            $this->codigoGerado .= Criacao::linha("public function get{$tabelaRelacionamento}()", 1);
            $this->codigoGerado .= Criacao::linha('{', 1);
            $this->codigoGerado .= Criacao::linha('$db = new ModelagemDb;', 2);
            $this->codigoGerado .= Criacao::linha('$db->where("' . $relacionamento['coluna'] . '", $this->getId());', 2);
            $this->codigoGerado .= Criacao::linha("$" . $tabelaRelacionamento . " = (new {$tabelaRelacionamento}())->getRows(" . '$db' . ");", 2);
            $this->codigoGerado .= Criacao::linha("return $" . $tabelaRelacionamento . ";", 2);
            $this->codigoGerado .= Criacao::linha('}', 1, 2);
        }

        // Criar relacionamento 1 p 1
        foreach ($this->campos as $key => $value) {

            if (!strpos($value['nome'], '_id')) {
                continue;
            }

            $campoNome = str_replace('_id', '', $value['nome']);

            $relacionamentoInverso = false;
            foreach ($this->todosRelacionamentos as $tabela => $relacionamentosParaInversao) {
                if($tabela == $campoNome){
                    foreach ($relacionamentosParaInversao as $inversao){
                        if(!isset($inversao['coluna'])){
                            continue;
                        }
                        if($inversao['coluna'] == $campoNome.'_id'){
                            $relacionamentoInverso = true;
                        }
                    }
                }
            }

            if(!$relacionamentoInverso){
                continue;
            }

            $campoNome = str_replace('_id', '', $value['nome']);
            $funcaoNome = Conversao::underscoreToCamelCase($campoNome);
            $this->codigoGerado .= Criacao::linha('/**', 1);
            $this->codigoGerado .= Criacao::linha(" * @return {$funcaoNome}", 1);
            $this->codigoGerado .= Criacao::linha(' */', 1);
            $this->codigoGerado .= Criacao::linha("public function get" . $funcaoNome . "()", 1);
            $this->codigoGerado .= Criacao::linha('{', 1);

            $this->codigoGerado .= Criacao::linha('$db = new ModelagemDb();', 2);
            $this->codigoGerado .= Criacao::linha('$db->where("' . $value['nome'] . '", $this->' . $value['nome'] . ');', 2);
            $this->codigoGerado .= Criacao::linha('$' . $funcaoNome . ' = ( new ' . $funcaoNome . '())->getRow($db);', 2);
            $this->codigoGerado .= Criacao::linha("return $" . $funcaoNome . ';', 2);
            $this->codigoGerado .= Criacao::linha('}', 1, 2);
        }

        /* MAXLENGHT */
        $this->codigoGerado .= Criacao::linha('/**', 1);
        $this->codigoGerado .= Criacao::linha(' * @param string <p>Nome da coluna do banco de dados</p>', 1);
        $this->codigoGerado .= Criacao::linha(' * @return int <p>Quantidade maxima de caracteres permitidos</p>', 1);
        $this->codigoGerado .= Criacao::linha(' */', 1);
        $this->codigoGerado .= Criacao::linha('public function tamanhoMaximo($coluna)', 1);
        $this->codigoGerado .= Criacao::linha('{', 1);
        $this->codigoGerado .= Criacao::linha('$tamanhos = array(', 2);

        foreach ($this->campos as $key => $value) {
            $this->codigoGerado .= Criacao::linha("'{$key}' => {$value['tamanho']},", 3);
        }
        if (null !== $this->camposLinguagem) {
            foreach ($this->camposLinguagem as $key => $value) {
                $this->codigoGerado .= Criacao::linha("'{$key}' => {$value['tamanho']},", 3);
            }
        }
        $this->codigoGerado .= Criacao::linha(');', 2, 2);
        $this->codigoGerado .= Criacao::linha('return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;', 2);
        $this->codigoGerado .= Criacao::linha('}', 1, 2);

        /* Tipos */
        $this->codigoGerado .= Criacao::linha('/**', 1);
        $this->codigoGerado .= Criacao::linha(' * @param string <p>Nome da coluna do banco de dados</p>', 1);
        $this->codigoGerado .= Criacao::linha(' * @return string <p>Tipo da coluna no banco de dados</p>', 1);
        $this->codigoGerado .= Criacao::linha(' */', 1);
        $this->codigoGerado .= Criacao::linha('public function tipos($coluna)', 1);
        $this->codigoGerado .= Criacao::linha('{', 1);
        $this->codigoGerado .= Criacao::linha('$tipos = array(', 2);
        foreach ($this->campos as $key => $value) {
            $this->codigoGerado .= Criacao::linha("'{$key}' => '{$value['tipo']}',", 3);
        }


        $this->codigoGerado .= Criacao::linha(');', 2);
        $this->codigoGerado .= Criacao::linha('return isset($tipos[$coluna]) ? $tipos[$coluna] : null;', 2);
        $this->codigoGerado .= Criacao::linha('}', 1, 2);

        /* Nomes das colunas */
        $this->codigoGerado .= Criacao::linha('/**', 1);
        $this->codigoGerado .= Criacao::linha(' * @return array <p>Lista de colunas do banco de dados</p>', 1);
        $this->codigoGerado .= Criacao::linha(' */', 1);
        $this->codigoGerado .= Criacao::linha('public function colunas()', 1);
        $this->codigoGerado .= Criacao::linha('{', 1);
        $this->codigoGerado .= Criacao::linha('return array(', 2);

        $this->codigoGerado .= Criacao::linha("'{$this->tabela}' => array(", 3);
        foreach ($this->campos as $key => $value) {
            $this->codigoGerado .= Criacao::linha("'{$key}',", 4);
        }
        $this->codigoGerado .= Criacao::linha('),', 3);


        $this->codigoGerado .= Criacao::linha(');', 2, 2);
        $this->codigoGerado .= Criacao::linha('}', 1, 2);

//        // Buscar no banco apenas uma linha
//        $this->codigoGerado .= Criacao::linha('', 1, 1);
//        $this->codigoGerado .= Criacao::linha('/**', 1, 1);
//        $this->codigoGerado .= Criacao::linha('* @param ModelagemDb $objectFilter', 1, 1);
//        $this->codigoGerado .= Criacao::linha('* @return $this', 1, 1);
//        $this->codigoGerado .= Criacao::linha('*/', 1, 1);
//        $this->codigoGerado .= Criacao::linha('public function getRow(ModelagemDb $objectFilter)', 1, 1);
//        $this->codigoGerado .= Criacao::linha('{', 1, 1);
//
//        $this->codigoGerado .= Criacao::linha('$obj = $objectFilter->get(self::TABELA)->row();', 2, 1);
//        $this->codigoGerado .= Criacao::linha('$thisClass = "\\\\". self::NAMESPACE_FILE . "\\\\" . self::FILE_NAME;', 2, 1);
//        $this->codigoGerado .= Criacao::linha('$classe = Conversao::objetoParaClasse($obj, new $thisClass);', 2, 1);
//        $this->codigoGerado .= Criacao::linha('return $classe;', 2, 1);
//
//        $this->codigoGerado .= Criacao::linha('}', 1, 2);
//
//        // Buscar no banco de dados varias linhas
//        $this->codigoGerado .= Criacao::linha('/**', 1, 1);
//        $this->codigoGerado .= Criacao::linha('* @param ModelagemDb $objectFilter', 1, 1);
//        $this->codigoGerado .= Criacao::linha('* @return $this[]', 1, 1);
//        $this->codigoGerado .= Criacao::linha('*/', 1, 1);
//        $this->codigoGerado .= Criacao::linha('public function getRows(ModelagemDb $objectFilter)', 1, 1);
//        $this->codigoGerado .= Criacao::linha('{', 1, 1);
//
//        $this->codigoGerado .= Criacao::linha('$classe = array();', 2, 1);
//        $this->codigoGerado .= Criacao::linha('$objects = $objectFilter->get(self::TABELA)->result();', 2, 1);
//        $this->codigoGerado .= Criacao::linha('$thisClass = "\\\\". self::NAMESPACE_FILE . "\\\\" . self::FILE_NAME;', 2, 1);
//        $this->codigoGerado .= Criacao::linha('foreach ($objects as $row)', 2, 1);
//        $this->codigoGerado .= Criacao::linha('{', 2, 1);
//        $this->codigoGerado .= Criacao::linha('$classe[] = Conversao::objetoParaClasse($row, new $thisClass);', 3, 1);
//        $this->codigoGerado .= Criacao::linha('}', 2, 1);
//        $this->codigoGerado .= Criacao::linha('return $classe;', 2, 1);
//
//        $this->codigoGerado .= Criacao::linha('}', 1, 2);
//
//        $this->codigoGerado .= Criacao::linha('public function update()', 1, 1);
//        $this->codigoGerado .= Criacao::linha('{', 1, 1);
//        $this->codigoGerado .= Criacao::linha('$db = new ModelagemDb();', 2, 1);
//        $this->codigoGerado .= Criacao::linha('$thisClass = "\\\\" . self::NAMESPACE_FILE . "\\\\" . self::FILE_NAME;', 2, 1);
//        $this->codigoGerado .= Criacao::linha('$data = GenericRepository::createSaveData($thisClass, $this);', 2, 1);
//        $this->codigoGerado .= Criacao::linha('$db->update(self::TABELA, $data);', 2, 1);
//        $this->codigoGerado .= Criacao::linha('}', 1, 2);
//
//        $this->codigoGerado .= Criacao::linha('public function delete()', 1, 1);
//        $this->codigoGerado .= Criacao::linha('{', 1, 1);
//        $this->codigoGerado .= Criacao::linha(' $db = new ModelagemDb();', 2, 1);
//        $this->codigoGerado .= Criacao::linha('$data["'.Identificadores::chavePrimaria($this->tabela).'"] = $this->get'.Conversao::underscoreToCamelCase(Identificadores::chavePrimaria($this->tabela)).'();', 2, 1);
//        $this->codigoGerado .= Criacao::linha('$db->where("'.Identificadores::chavePrimaria($this->tabela).'", $this->get'.Conversao::underscoreToCamelCase(Identificadores::chavePrimaria($this->tabela)).'())->delete(self::TABELA, $data);', 2, 1);
//        $this->codigoGerado .= Criacao::linha('}', 1, 2);
//
//        $this->codigoGerado .= Criacao::linha('/**', 1, 1);
//        $this->codigoGerado .= Criacao::linha('* @return int', 1, 1);
//        $this->codigoGerado .= Criacao::linha('*/', 1, 1);
//        $this->codigoGerado .= Criacao::linha('public function insert()', 1, 1);
//        $this->codigoGerado .= Criacao::linha('{', 1, 1);
//        $this->codigoGerado .= Criacao::linha('$db = new ModelagemDb();', 2, 1);
//        $this->codigoGerado .= Criacao::linha('$thisClass = "\\\\" . self::NAMESPACE_FILE . "\\\\" . self::FILE_NAME;', 2, 1);
//        $this->codigoGerado .= Criacao::linha('$data = GenericRepository::createSaveData($thisClass, $this);', 2, 1);
//        $this->codigoGerado .= Criacao::linha('return $db->insert(self::TABELA, $data);', 2, 1);
//        $this->codigoGerado .= Criacao::linha('}', 1, 2);

        $this->codigoGerado .= Criacao::linha('}', 0, 2);

        return $this;

    }

    /**
     * @return EntidadesRN
     */
    public function salvar()
    {
        if (empty($this->codigoGerado)) {
            return $this;
        }

        $pasta = 'src/' . $this->modulo;
        if (!is_dir($pasta)) {
            mkdir($pasta, 0777, true);
            @chmod($pasta, 0777);
        }
        //src/GrupoDoModulo/NomeDoModulo/Entidades/Arquivo.php
        $arquivo = $pasta . DIRECTORY_SEPARATOR
            . $this->entidade.'Entidade'. EXT;

        file_put_contents($arquivo, $this->codigoGerado);
        @chmod($arquivo, 0777);

        return $this;
    }

    private function buscaValoresPadrao(&$campos, $tabela)
    {
        $db = new ModelagemDb();
        $colunas = "'" . implode("','", array_keys($campos)) . "'";
        try {
            $result = $db
                ->select('`column_name`, `column_default`, `data_type`,
                       `column_type`, `character_maximum_length`')
                ->where('`table_schema`', LI_CONNECT_DATABASE)
                ->where('`table_name`', $tabela)
                ->where('`column_name` IN ', '(' . $colunas . ')')
                ->get('`information_schema`.`columns`')
                ->result();
        } catch (\SQLiteException $sex) {
            $result = array();
        }

        foreach (array_keys($campos) as $campo) {
            $campos[$campo]['padrao'] = null;
            foreach ($result as $row) {
                if ($row->column_name == $campo) {
                    if (isset($row->column_default)) {
                        $campos[$campo]['padrao'] = $row->column_default;
                    }
                    if (isset($row->data_type)) {
                        $campos[$campo]['tipo'] = Conversao::tipoDoBancoParaPHP($row->data_type);
                    }
                    $campos[$campo]['tamanho'] = Conversao::tamanhoMaximoBanco($row);
                    break;
                }
            }
        }
    }

}