<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 28/08/2016
 * Time: 16:32
 */

namespace system\Libraries\Modulos\GeradorCodigo\EntidadeRepositorio\Models;

use system\Core\LI_Model\LI_DB;

class RepositoriosRN
{

    private $modulo;
    private $repositorio;
    private $metodos;
    private $tabela;
    private $campos;
    private $tabelaLinguagem;
    private $camposLinguagem;
    private $prefixo;
    private $codigoGerado;
    private $chavesPrimarias;

    /**
     * Repositorios constructor.
     * @param $modulo
     * @param $metodos
     * @param $tabela
     * @param $campos
     * @param $tabelaLinguagem
     * @param $camposLinguagem
     */
    public function __construct(
        $modulo = null, $metodos = null, $tabela = null, $campos = null, $tabelaLinguagem = null, $camposLinguagem = null
    )
    {
        if (null !== $modulo) {
            $this->setModulo($modulo);
        }
        if (null !== $tabela) {
            $this->setTabela($tabela);
        }
        if (null !== $metodos) {
            $this->setMetodos($metodos);
        }
        if (null !== $campos) {
            $this->setCampos($campos);
        }
        if (null !== $camposLinguagem) {
            $this->setCamposLinguagem($camposLinguagem);
        }
        if (null !== $tabelaLinguagem) {
            $this->setTabelaLinguagem($tabelaLinguagem);
        }

        $this->codigoGerado = array(
            'Abstracts' => '',
            'extended' => '',
        );

        $this->prefixo = '';
        $this->chavesPrimarias = array();
    }

    /**
     * @param mixed $tabelaLinguagem
     * @return RepositoriosRN
     */
    public function setTabelaLinguagem($tabelaLinguagem)
    {
        $this->tabelaLinguagem = $tabelaLinguagem;
        return $this;
    }

    /**
     * @param $campos
     * @return RepositoriosRN
     */
    public function setCamposLinguagem($campos)
    {
        $this->camposLinguagem = $campos;
        $this->setChavesPrimarias($this->tabelaLinguagem);

        return $this;
    }

    /**
     * @param mixed $campos
     * @return RepositoriosRN
     */
    public function setCampos($campos)
    {
        if (sizeof($campos) && strpos($campos[0], '_') !== false) {
            $prefixo = current(explode('_', $campos[0])) . '_';
            $this->prefixo = Ferramentas::strpos_array($campos, $prefixo) !== false ? $prefixo : '';
        }

        $this->campos = $campos;
        $this->setChavesPrimarias($this->tabela);
        return $this;
    }

    /**
     * @param mixed $tabela
     * @return RepositoriosRN
     */
    public function setTabela($tabela)
    {
        $this->tabela = $tabela;
        $this->setRepositorio($tabela);
        return $this;
    }

    /**
     * @param string $modulo
     * @return RepositoriosRN
     */
    public function setModulo($modulo)
    {
        $this->modulo = $modulo;
        return $this;
    }

    /**
     * @param string $repositorio
     * @return RepositoriosRN
     */
    public function setRepositorio($repositorio)
    {
        $this->repositorio = underscoreToCamelCase($repositorio);
        return $this;
    }

    /**
     * @param array $metodos
     * @return RepositoriosRN
     */
    public function setMetodos($metodos)
    {
        $this->metodos = $metodos;
        return $this;
    }

    /**
     * @param $tabela
     * @return RepositoriosRN
     */
    public function setChavesPrimarias($tabela)
    {
        $db = new LI_DB();
        try {
            $result = $db->where('`table_schema`', LI_CONNECT_DATABASE)->where('`column_key`', 'PRI')->where('`table_name`', $tabela)
                ->order_by('`extra`', 'DESC')->get('`information_schema`.`columns`')->result();
        } catch (\SQLiteException $sex) {
            $result = array();
        }

        if (count($result)) {
            foreach ($result as $row) {
                $this->chavesPrimarias[$tabela][] = isset($row->column_name) ? $row->column_name : '';
            }
        } else {
            foreach ($this->campos as $row) {
                $this->chavesPrimarias[$tabela][] = $row;
            }
        }

        return $this;
    }

    /**
     * @return RepositoriosRN
     */
    public function gerar()
    {
        if (!isset($this->repositorio) || !isset($this->modulo) || !isset($this->metodos) || !isset($this->tabela) || !isset($this->campos)
        ) {
            return $this;
        }

        if (!defined('TAB')) {
            define('TAB', '    ');
        }

        $codigoAbstract = &$this->codigoGerado['Abstracts'];
        $codigoExtended = &$this->codigoGerado['extended'];

        $codigoAbstract = Ferramentas::linha('<?php', 0, 2);

        $codigoAbstract .= Ferramentas::linha("namespace library\\Modulos\\".str_replace('/', '\\',$this->modulo)."\\Repository\\Abstracts;", 0, 2);

        $codigoAbstract .= Ferramentas::linha('use library\\Modulos\\'.str_replace('/', '\\',$this->modulo).'\\Interfaces\\'.$this->repositorio.' as '.$this->repositorio.'Interface;');
        $codigoAbstract .= Ferramentas::linha('use library\\Modulos\\'.str_replace('/', '\\',$this->modulo).'\\Entity\\'.$this->repositorio.' as '.$this->repositorio.'Entidade;');
        $codigoAbstract .= Ferramentas::linha('use system\Core\LI_Model\LI_DB;');
        $codigoAbstract .= Ferramentas::linha();

        $codigoAbstract .= Ferramentas::linha('/**', 0);
        $codigoAbstract .= Ferramentas::linha(' * Classe gerada automaticamente em ' . date('d/m/Y H:i:s'), 0);
        $codigoAbstract .= Ferramentas::linha(" * Class {$this->repositorio}", 0);
        $codigoAbstract .= Ferramentas::linha(" * @package library\\Modulos\\".str_replace('/', '\\',$this->modulo)."\\Repository\\Abstracts", 0);
        $codigoAbstract .= Ferramentas::linha(' */', 0);
        $codigoAbstract .= Ferramentas::linha("abstract class {$this->repositorio} implements {$this->repositorio}Interface");
        $codigoAbstract .= Ferramentas::linha('{');

        $codigoAbstract .= Ferramentas::linha('protected $db;', 1, 2);


        $codigoAbstract .= Ferramentas::linha('/**', 0);
        $codigoAbstract .= Ferramentas::linha('* ', 1);
        $codigoAbstract .= Ferramentas::linha('* @return LI_DB', 1);
        $codigoAbstract .= Ferramentas::linha('*/', 1);
        $codigoAbstract .= Ferramentas::linha('public function db()', 1);
        $codigoAbstract .= Ferramentas::linha('{', 1);
        $codigoAbstract .= Ferramentas::linha('return $this->db;', 2);
        $codigoAbstract .= Ferramentas::linha('}', 1);

        $codigoAbstract .= Ferramentas::linha('public function __construct()', 1);
        $codigoAbstract .= Ferramentas::linha('{', 1);
        $codigoAbstract .= Ferramentas::linha('$this->db = new LI_DB();', 2);
        $codigoAbstract .= Ferramentas::linha('}', 1, 2);

        foreach ($this->metodos as $metodo) {
            switch ($metodo) {
                case 'buscar_por_id':
                    $codigoAbstract .= $this->gerarBuscarPorId();
                    break;
                case 'buscar':
                    $codigoAbstract .= $this->gerarBuscar();
                    break;
                case 'salvar':
                    $codigoAbstract .= $this->gerarSalvar();
                    break;
                case 'adicionar':
                    $codigoAbstract .= $this->gerarAdicionar();
                    $codigoAbstract .= $this->gerarAdicionarPost();
                    break;
                case 'remover':
                    $codigoAbstract .= $this->gerarRemover();
                    break;
                case 'atualizar':
                    $codigoAbstract .= $this->gerarAtualizar();
                    $codigoAbstract .= $this->gerarAtualizarPost();
                    break;
            }
        }

        $codigoAbstract .= $this->gerarChavePrimaria();

        $codigoAbstract .= Ferramentas::linha('}', 0, 2);

        $codigoExtended = Ferramentas::linha('<?php', 0, 2);
        $codigoExtended .= Ferramentas::linha("namespace library\\Modulos\\".str_replace('/', '\\',$this->modulo)."\\Repository;", 0, 2);
        $codigoExtended .= Ferramentas::linha('/**', 0);
        $codigoExtended .= Ferramentas::linha(' * Classe gerada automaticamente em ' . date('d/m/Y H:i:s'), 0);
        $codigoExtended .= Ferramentas::linha(" * Class {$this->repositorio}", 0);
        $codigoExtended .= Ferramentas::linha(" * @package library\\Modulos\\".str_replace('/', '\\',$this->modulo)."\\Repository", 0);
        $codigoExtended .= Ferramentas::linha(' */', 0);
        $codigoExtended .= Ferramentas::linha("class {$this->repositorio} extends Abstracts\\{$this->repositorio}");
        $codigoExtended .= Ferramentas::linha('{');
        $codigoExtended .= Ferramentas::linha('/* TODO: implementar metodos extra ou modificar os existes se necessario */', 1);
        $codigoExtended .= Ferramentas::linha('}', 0, 2);

        return $this;
    }

    /**
     * @return RepositoriosRN
     */
    public function salvar()
    {
        if (empty($this->codigoGerado['extended']) || empty($this->codigoGerado['Abstracts'])) {
            return $this;
        }

        /* /library/Modulos/{NomeDoModulo}/Repositorios/Abstrato/{Arquivo}.php */
        $pasta = LI_DIR_MODULOS
            . $this->modulo . DIRECTORY_SEPARATOR
            . 'Repository' . DIRECTORY_SEPARATOR
            . 'Abstracts';

        if (!is_dir($pasta)) {
            @mkdir($pasta);
            @chmod($pasta, 0777);
        }

        $arquivo = $pasta . DIRECTORY_SEPARATOR . $this->repositorio . EXT;
        file_put_contents($arquivo, $this->codigoGerado['Abstracts']);
        @chmod($arquivo, 0777);


        /* /src/Modulos/{NomeDoModulo}/Repositorios/{Arquivo}.php */
        $arquivo = LI_DIR_MODULOS
            . $this->modulo . DIRECTORY_SEPARATOR
            . 'Repository' . DIRECTORY_SEPARATOR
            . $this->repositorio . EXT;
        if (!file_exists($arquivo)) {
            file_put_contents($arquivo, $this->codigoGerado['extended']);
            @chmod($arquivo, 0777);
        }

        return $this;
    }

    private function gerarSalvar()
    {
        $codigo = Ferramentas::linha('/**', 1);
        $codigo .= Ferramentas::linha(' * Metodo gerado automaticamente em ' . date('d/m/Y H:i:s'), 1);
        $codigo .= Ferramentas::linha(" * @param {$this->repositorio}Entidade \$entidade", 1);
        $codigo .= Ferramentas::linha(" * @return {$this->repositorio}Entidade", 1);
        $codigo .= Ferramentas::linha(' * @throws SqlException', 1);
        $codigo .= Ferramentas::linha(' * @throws Exception', 1);
        $codigo .= Ferramentas::linha(' */', 1);
        $codigo .= Ferramentas::linha("public function salvar({$this->repositorio}Entidade \$entidade)", 1);
        $codigo .= Ferramentas::linha('{', 1);
        $codigo .= Ferramentas::linha('$valores = Conversao::entidadeParaArray($entidade);', 2);
        $codigo .= Ferramentas::linha("\$chavePrimariaTabela = \$this->chavePrimaria()[{$this->repositorio}Entidade::TABELA];", 2);
        $codigo .= Ferramentas::linha('foreach ($chavePrimariaTabela as $chave) {', 2);
        $codigo .= Ferramentas::linha('$this->db->where($chave, $valores[$entidade::TABELA][$chave]);', 3);
        $codigo .= Ferramentas::linha('}', 2);
        $codigo .= Ferramentas::linha('try {', 2);
        $codigo .= Ferramentas::linha('if ($this->db->count_all_results($entidade::TABELA)) {', 3);
        $codigo .= Ferramentas::linha('try {', 4);
        $codigo .= Ferramentas::linha('return $this->atualizar($entidade);', 5);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 4);
        $codigo .= Ferramentas::linha('throw $sex;', 5);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 4);
        $codigo .= Ferramentas::linha('throw $ex;', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('} else {', 3);
        $codigo .= Ferramentas::linha('try {', 4);
        $codigo .= Ferramentas::linha('$id = $this->adicionar($entidade);', 5);
        $codigo .= Ferramentas::linha('try {', 5);
        $codigo .= Ferramentas::linha('return $this->buscarPorId($id);', 6);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 5);
        $codigo .= Ferramentas::linha('throw $sex;', 6);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 5);
        $codigo .= Ferramentas::linha('throw $ex;', 6);
        $codigo .= Ferramentas::linha('}', 5);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 4);
        $codigo .= Ferramentas::linha('throw $sex;', 5);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 4);
        $codigo .= Ferramentas::linha('throw $ex;', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 2);
        $codigo .= Ferramentas::linha('throw $sex;', 3);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 2);
        $codigo .= Ferramentas::linha('throw $ex;', 3);
        $codigo .= Ferramentas::linha('}', 2);
        $codigo .= Ferramentas::linha('}', 1, 2);
        return $codigo;
    }

    private function gerarBuscarPorId()
    {
        $codigo = Ferramentas::linha("/**", 1);
        $codigo .= Ferramentas::linha(' * Metodo gerado automaticamente em ' . date('d/m/Y H:i:s'), 1);
        $codigo .= Ferramentas::linha('* @param int|string $id', 1);
        $codigo .= Ferramentas::linha("* @return {$this->repositorio}Entidade", 1);
        $codigo .= Ferramentas::linha("*/", 1);
        $codigo .= Ferramentas::linha("public function buscarPorId(\$id)", 1);
        $codigo .= Ferramentas::linha("{", 1);
        $codigo .= Ferramentas::linha("\$chavePrimaria = current(\$this->chavePrimaria()[{$this->repositorio}Entidade::TABELA]);", 2);
        $codigo .= Ferramentas::linha("if (empty({$this->repositorio}Entidade::TABELA_LINGUAGEM)) {", 2);
        $codigo .= Ferramentas::linha("try {", 3);
        $codigo .= Ferramentas::linha('$row = $this->db', 4);
        $codigo .= Ferramentas::linha('->where($chavePrimaria, $id)', 5);
        $codigo .= Ferramentas::linha("->get({$this->repositorio}Entidade::TABELA)", 5);
        $codigo .= Ferramentas::linha("->row();", 5);
        $codigo .= Ferramentas::linha("return Conversao::objetoParaEntidade(\$row, new {$this->repositorio}Entidade());", 4);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 3);
        $codigo .= Ferramentas::linha("return new {$this->repositorio}Entidade();", 4);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 3);
        $codigo .= Ferramentas::linha("return new {$this->repositorio}Entidade();", 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('} else {', 2);
        $codigo .= Ferramentas::linha('try {', 3);
        $codigo .= Ferramentas::linha("\$chaves = \$this->chavePrimaria()[{$this->repositorio}Entidade::TABELA_LINGUAGEM];", 4);
        $codigo .= Ferramentas::linha("foreach (\$chaves as \$value) {", 4);
        $codigo .= Ferramentas::linha("if (strpos(\$value, {$this->repositorio}Entidade::TABELA)) {", 5);
        $codigo .= Ferramentas::linha("\$chaveJoinLinguagem = \$value;", 6);
        $codigo .= Ferramentas::linha("break;", 6);
        $codigo .= Ferramentas::linha("}", 5);
        $codigo .= Ferramentas::linha("}", 4);
        $codigo .= Ferramentas::linha("foreach (\$chaves as \$value) {", 4);
        $codigo .= Ferramentas::linha("if (strpos(\$value, \"linguagem\")) {", 5);
        $codigo .= Ferramentas::linha("\$chaveIdLinguagem = \$value;", 6);
        $codigo .= Ferramentas::linha("break;", 6);
        $codigo .= Ferramentas::linha("}", 5);
        $codigo .= Ferramentas::linha("}", 4);
        $codigo .= Ferramentas::linha("if (isset(\$chaveJoinLinguagem) && isset(\$chaveIdLinguagem)) {", 4);
        $codigo .= Ferramentas::linha("\$idLinguagemAtual = Linguagem::getIdLinguagemAtual();", 5);
        $codigo .= Ferramentas::linha("\$row = \$this->db", 5);
        $codigo .= Ferramentas::linha("->from({$this->repositorio}Entidade::TABELA)", 6);
        $codigo .= Ferramentas::linha("->join({$this->repositorio}Entidade::TABELA_LINGUAGEM, \"{\$chaveJoinLinguagem} = {\$chavePrimaria} AND {\$chaveIdLinguagem} = {\$idLinguagemAtual}\",\"left\",false)", 6);
        $codigo .= Ferramentas::linha('->where($chavePrimaria, $id)', 6);
        $codigo .= Ferramentas::linha('->get()', 6);
        $codigo .= Ferramentas::linha('->row();', 6);
        $codigo .= Ferramentas::linha('} else {', 4);
        $codigo .= Ferramentas::linha('$row = $this->db', 5);
        $codigo .= Ferramentas::linha('->where($chavePrimaria, $id)', 6);
        $codigo .= Ferramentas::linha("->get({$this->repositorio}Entidade::TABELA)", 6);
        $codigo .= Ferramentas::linha("->row();", 6);
        $codigo .= Ferramentas::linha("}", 4);
        $codigo .= Ferramentas::linha("return Conversao::objetoParaEntidade(\$row, new {$this->repositorio}Entidade());", 4);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 3);
        $codigo .= Ferramentas::linha("return new {$this->repositorio}Entidade();", 4);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 3);
        $codigo .= Ferramentas::linha("return new {$this->repositorio}Entidade();", 4);
        $codigo .= Ferramentas::linha("}", 3);
        $codigo .= Ferramentas::linha("}", 2);
        $codigo .= Ferramentas::linha("}", 1);

        return $codigo;
    }

    private function gerarRemover()
    {
        $codigo = Ferramentas::linha('/**', 1);
        $codigo .= Ferramentas::linha(' * Metodo gerado automaticamente em ' . date('d/m/Y H:i:s'), 1);
        $codigo .= Ferramentas::linha(" * @param {$this->repositorio}Entidade \$entidade", 1);
        $codigo .= Ferramentas::linha(' * @return bool', 1);
        $codigo .= Ferramentas::linha(' * @throws Exception', 1);
        $codigo .= Ferramentas::linha(' * @throws SqlException', 1);
        $codigo .= Ferramentas::linha(' */', 1);
        $codigo .= Ferramentas::linha("public function remover({$this->repositorio}Entidade \$entidade)", 1);
        $codigo .= Ferramentas::linha('{', 1);
        $codigo .= Ferramentas::linha('if (empty($entidade::TABELA_LINGUAGEM)) {', 2);
        $codigo .= Ferramentas::linha('foreach ($this->chavePrimaria()[$entidade::TABELA] as $chave) {', 3);
        $codigo .= Ferramentas::linha('$get = Conversao::campoParaGet($chave, $entidade::PREFIXO);', 4);
        $codigo .= Ferramentas::linha('$this->db->where($chave, $entidade->{$get}());', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('try {', 3);
        $codigo .= Ferramentas::linha('return (bool)$this->db->delete($entidade::TABELA);', 4);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 3);
        $codigo .= Ferramentas::linha('throw $sex;', 4);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 3);
        $codigo .= Ferramentas::linha('throw $ex;', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('} else {', 2);
        $codigo .= Ferramentas::linha('foreach ($this->chavePrimaria()[$entidade::TABELA] as $chave) {', 3);
        $codigo .= Ferramentas::linha('$get = Conversao::campoParaGet($chave, $entidade::PREFIXO);', 4);
        $codigo .= Ferramentas::linha('$this->db->where($chave, $entidade->{$get}());', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('try {', 3);
        $codigo .= Ferramentas::linha('$resultadoRemover = (bool)$this->db->delete($entidade::TABELA);', 4);
        $codigo .= Ferramentas::linha('foreach ($this->chavePrimaria()[$entidade::TABELA_LINGUAGEM] as $chave) {', 4);
        $codigo .= Ferramentas::linha('$get = Conversao::campoParaGet($chave, $entidade::PREFIXO);', 5);
        $codigo .= Ferramentas::linha('$this->db->where($chave, $entidade->{$get}());', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('try {', 4);
        $codigo .= Ferramentas::linha('if (!$this->db->delete($entidade::TABELA_LINGUAGEM)) {', 5);
        $codigo .= Ferramentas::linha('return false;', 6);
        $codigo .= Ferramentas::linha('}', 5);
        $codigo .= Ferramentas::linha('return $resultadoRemover;', 5);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 4);
        $codigo .= Ferramentas::linha('throw $sex;', 5);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 4);
        $codigo .= Ferramentas::linha('throw $ex;', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 3);
        $codigo .= Ferramentas::linha('throw $sex;', 4);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 3);
        $codigo .= Ferramentas::linha('throw $ex;', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('}', 2);
        $codigo .= Ferramentas::linha('}', 1, 2);

        return $codigo;
    }

    private function gerarAdicionar()
    {
        $codigo = Ferramentas::linha('/**', 1);
        $codigo .= Ferramentas::linha(' * Metodo gerado automaticamente em ' . date('d/m/Y H:i:s'), 1);
        $codigo .= Ferramentas::linha(" * @param {$this->repositorio}Entidade \$entidade", 1);
        $codigo .= Ferramentas::linha(' * @return int', 1);
        $codigo .= Ferramentas::linha(' * @throws Exception', 1);
        $codigo .= Ferramentas::linha(' * @throws SqlException', 1);
        $codigo .= Ferramentas::linha(' */', 1);
        $codigo .= Ferramentas::linha("public function adicionar({$this->repositorio}Entidade \$entidade)", 1);
        $codigo .= Ferramentas::linha('{', 1);
        $codigo .= Ferramentas::linha('$valores = Conversao::entidadeParaArray($entidade);', 2);
        $codigo .= Ferramentas::linha('if (empty($entidade::TABELA_LINGUAGEM)) {', 2);
        $codigo .= Ferramentas::linha('$valores = isset($valores[$entidade::TABELA]) ? $valores[$entidade::TABELA] : $valores;', 3);
        $codigo .= Ferramentas::linha('try {', 3);
        $codigo .= Ferramentas::linha('$this->db->insert($entidade::TABELA, $valores);', 4);
        $codigo .= Ferramentas::linha('return $this->db->insert_id();', 4);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 3);
        $codigo .= Ferramentas::linha('throw $sex;', 4);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 3);
        $codigo .= Ferramentas::linha('throw $ex;', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('} else {', 2);
        $codigo .= Ferramentas::linha('$chaves = $this->chavePrimaria()[$entidade::TABELA_LINGUAGEM];', 3);
        $codigo .= Ferramentas::linha('foreach ($chaves as $c) {', 3);
        $codigo .= Ferramentas::linha('if (strpos($c, $entidade::TABELA)) {', 4);
        $codigo .= Ferramentas::linha('$chave = $c;', 5);
        $codigo .= Ferramentas::linha('break;', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('if (isset($chave)) {', 3);
        $codigo .= Ferramentas::linha('if (empty($valores[$entidade::TABELA_LINGUAGEM][$chave])) {', 4);
        $codigo .= Ferramentas::linha('try {', 5);
        $codigo .= Ferramentas::linha('$this->db->insert($entidade::TABELA, $valores[$entidade::TABELA]);', 6);
        $codigo .= Ferramentas::linha('$valores[$entidade::TABELA_LINGUAGEM][$chave] = $this->db->insert_id();', 6);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 5);
        $codigo .= Ferramentas::linha('throw $sex;', 6);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 5);
        $codigo .= Ferramentas::linha('throw $ex;', 6);
        $codigo .= Ferramentas::linha('}', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('try {', 4);
        $codigo .= Ferramentas::linha('$this->db->insert($entidade::TABELA_LINGUAGEM, $valores[$entidade::TABELA_LINGUAGEM]);', 5);
        $codigo .= Ferramentas::linha('return $valores[$entidade::TABELA_LINGUAGEM][$chave];', 5);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 4);
        $codigo .= Ferramentas::linha('throw $sex;', 5);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 4);
        $codigo .= Ferramentas::linha('throw $ex;', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('} else {', 3);
        $codigo .= Ferramentas::linha('try {', 4);
        $codigo .= Ferramentas::linha('$this->db->insert($entidade::TABELA, $valores[$entidade::TABELA]);', 5);
        $codigo .= Ferramentas::linha('return $this->db->insert_id();', 5);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 4);
        $codigo .= Ferramentas::linha('throw $sex;', 5);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 4);
        $codigo .= Ferramentas::linha('throw $ex;', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('}', 2);
        $codigo .= Ferramentas::linha('}', 1, 2);

        return $codigo;
    }

    private function gerarAdicionarPost()
    {
        $codigo = Ferramentas::linha('/**', 1);
        $codigo .= Ferramentas::linha(' * Metodo gerado automaticamente em ' . date('d/m/Y H:i:s'), 1);
        $codigo .= Ferramentas::linha(' * @param array $post', 1);
        $codigo .= Ferramentas::linha(' * @return int', 1);
        $codigo .= Ferramentas::linha(' * @throws Exception', 1);
        $codigo .= Ferramentas::linha(' * @throws SqlException', 1);
        $codigo .= Ferramentas::linha(' */', 1);
        $codigo .= Ferramentas::linha('public function adicionarPost($post)', 1);
        $codigo .= Ferramentas::linha('{', 1);
        $codigo .= Ferramentas::linha('if (isset($post[\'linguagens\'])) {', 2);
        $codigo .= Ferramentas::linha('$idInserido = null;', 3);
        $codigo .= Ferramentas::linha('foreach ($post[\'linguagens\'] as $linguagem) {', 3);
        $codigo .= Ferramentas::linha("\$entidade = new {$this->repositorio}Entidade();", 4);
        $codigo .= Ferramentas::linha('foreach ($entidade->colunas()[$entidade::TABELA] as $coluna) {', 4);
        $codigo .= Ferramentas::linha('$set = Conversao::campoParaSet($coluna, $entidade::PREFIXO);', 5);
        $codigo .= Ferramentas::linha('if (isset($post[$coluna])) {', 5);
        $codigo .= Ferramentas::linha('$entidade->{$set}($post[$coluna]);', 6);
        $codigo .= Ferramentas::linha('}', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('if (null !== $idInserido) {', 4);
        $codigo .= Ferramentas::linha('$chaves = $this->chavePrimaria()[$entidade::TABELA_LINGUAGEM];', 5);
        $codigo .= Ferramentas::linha('foreach ($chaves as $c) {', 5);
        $codigo .= Ferramentas::linha('if (strpos($c, $entidade::TABELA)) {', 6);
        $codigo .= Ferramentas::linha('$chave = $c;', 7);
        $codigo .= Ferramentas::linha('break;', 7);
        $codigo .= Ferramentas::linha('}', 6);
        $codigo .= Ferramentas::linha('}', 5);
        $codigo .= Ferramentas::linha('if (isset($chave)) {', 5);
        $codigo .= Ferramentas::linha('$linguagem[$chave] = $idInserido;', 6);
        $codigo .= Ferramentas::linha('}', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('foreach ($entidade->colunas()[$entidade::TABELA_LINGUAGEM] as $coluna) {', 4);
        $codigo .= Ferramentas::linha('if (isset($linguagem[$coluna])) {', 5);
        $codigo .= Ferramentas::linha('$set = Conversao::campoParaSet($coluna, $entidade::PREFIXO);', 6);
        $codigo .= Ferramentas::linha('$entidade->{$set}($linguagem[$coluna]);', 6);
        $codigo .= Ferramentas::linha('}', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('try {', 4);
        $codigo .= Ferramentas::linha('$idInserido = $this->adicionar($entidade);', 5);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 4);
        $codigo .= Ferramentas::linha('throw $sex;', 5);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 4);
        $codigo .= Ferramentas::linha('throw $ex;', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('return $idInserido;', 3);
        $codigo .= Ferramentas::linha('} else {', 2);
        $codigo .= Ferramentas::linha("\$entidade = new {$this->repositorio}Entidade();", 3);
        $codigo .= Ferramentas::linha('foreach ($entidade->colunas()[$entidade::TABELA] as $coluna) {', 3);
        $codigo .= Ferramentas::linha('if (isset($post[$coluna])) {', 4);
        $codigo .= Ferramentas::linha('$set = Conversao::campoParaSet($coluna, $entidade::PREFIXO);', 5);
        $codigo .= Ferramentas::linha('$entidade->{$set}($post[$coluna]);', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('try {', 3);
        $codigo .= Ferramentas::linha('return $this->adicionar($entidade);', 4);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 3);
        $codigo .= Ferramentas::linha('throw $sex;', 4);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 3);
        $codigo .= Ferramentas::linha('throw $ex;', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('}', 2);
        $codigo .= Ferramentas::linha('}', 1, 2);

        return $codigo;
    }

    private function gerarAtualizar()
    {
        $codigo = Ferramentas::linha('/**', 1);
        $codigo .= Ferramentas::linha(' * Metodo gerado automaticamente em ' . date('d/m/Y H:i:s'), 1);
        $codigo .= Ferramentas::linha(" * @param {$this->repositorio}Entidade \$entidade", 1);
        $codigo .= Ferramentas::linha(" * @return {$this->repositorio}Entidade", 1);
        $codigo .= Ferramentas::linha(' * @throws Exception', 1);
        $codigo .= Ferramentas::linha(' * @throws SqlException', 1);
        $codigo .= Ferramentas::linha(' */', 1);
        $codigo .= Ferramentas::linha("public function atualizar({$this->repositorio}Entidade \$entidade)", 1);
        $codigo .= Ferramentas::linha('{', 1);
        $codigo .= Ferramentas::linha("\$chavePrimariaTabela = \$this->chavePrimaria()[{$this->repositorio}Entidade::TABELA];", 2);
        $codigo .= Ferramentas::linha('$valores = Conversao::entidadeParaArray($entidade);', 2);
        $codigo .= Ferramentas::linha('/* Nao possui tabela de linguagem, entao, apenas atualiza a tabela principal */', 2);
        $codigo .= Ferramentas::linha("if (!defined('Modulos\\{$this->modulo}\\Entidades\\{$this->repositorio}::TABELA_LINGUAGEM') || empty(\$entidade::TABELA_LINGUAGEM)) {", 2);
        $codigo .= Ferramentas::linha('$valores = isset($valores[$entidade::TABELA]) ? $valores[$entidade::TABELA] : $valores;', 3);
        $codigo .= Ferramentas::linha('try {', 3);
        $codigo .= Ferramentas::linha('foreach ($chavePrimariaTabela as $chave) {', 4);
        $codigo .= Ferramentas::linha('$this->db->where($chave, $valores[$chave]);', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('$this->db->update($entidade::TABELA, $valores);', 4);
        $codigo .= Ferramentas::linha('return $entidade;', 4);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 3);
        $codigo .= Ferramentas::linha('throw $sex;', 4);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 3);
        $codigo .= Ferramentas::linha('throw $ex;', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('} else {', 2);
        $codigo .= Ferramentas::linha('/* Busca a chave de ligacao entre a tabela principal e a de linguagem */', 3);
        $codigo .= Ferramentas::linha('$chavePrimariaTabelaLinguagem = $this->chavePrimaria()[$entidade::TABELA_LINGUAGEM];', 3);
        $codigo .= Ferramentas::linha('foreach ($chavePrimariaTabelaLinguagem as $valor) {', 3);
        $codigo .= Ferramentas::linha('if (strpos($valor, $entidade::TABELA)) {', 4);
        $codigo .= Ferramentas::linha('$chaveLinguagem = $valor;', 5);
        $codigo .= Ferramentas::linha('break;', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('/* Se possui chave de ligacao entao */', 3);
        $codigo .= Ferramentas::linha('if (isset($chaveLinguagem)) {', 3);
        $codigo .= Ferramentas::linha('/* Atualiza a tabela principal */', 4);
        $codigo .= Ferramentas::linha('try {', 4);
        $codigo .= Ferramentas::linha('foreach ($chavePrimariaTabela as $chave) {', 5);
        $codigo .= Ferramentas::linha('$this->db->where($chave, $valores[$entidade::TABELA][$chave]);', 6);
        $codigo .= Ferramentas::linha('}', 5);
        $codigo .= Ferramentas::linha('$this->db->update($entidade::TABELA, $valores[$entidade::TABELA]);', 5);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 4);
        $codigo .= Ferramentas::linha('throw $sex;', 5);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 4);
        $codigo .= Ferramentas::linha('throw $ex;', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('$get = Conversao::campoParaGet($chaveLinguagem, $entidade::PREFIXO);', 4);
        $codigo .= Ferramentas::linha('if (!empty($entidade->{$get}())) {', 4);
        $codigo .= Ferramentas::linha('/* Atualiza a tabela de linguagem */', 5);
        $codigo .= Ferramentas::linha('try {', 5);
        $codigo .= Ferramentas::linha('foreach ($chavePrimariaTabelaLinguagem as $chave) {', 6);
        $codigo .= Ferramentas::linha('$this->db->where($chave, $valores[$entidade::TABELA_LINGUAGEM][$chave]);', 7);
        $codigo .= Ferramentas::linha('}', 6);
        $codigo .= Ferramentas::linha('/* Se possui o registro na tabela de linguagem */', 6);
        $codigo .= Ferramentas::linha('if ($this->db->count_all_results($entidade::TABELA_LINGUAGEM)) {', 6);
        $codigo .= Ferramentas::linha('/* Atualiza na tabela de linguagem */', 7);
        $codigo .= Ferramentas::linha('try {', 7);
        $codigo .= Ferramentas::linha('foreach ($chavePrimariaTabelaLinguagem as $chave) {', 8);
        $codigo .= Ferramentas::linha('$this->db->where($chave, $valores[$entidade::TABELA_LINGUAGEM][$chave]);', 9);
        $codigo .= Ferramentas::linha('}', 8);
        $codigo .= Ferramentas::linha('$this->db->update($entidade::TABELA_LINGUAGEM, $valores[$entidade::TABELA_LINGUAGEM]);', 8);
        $codigo .= Ferramentas::linha('return $entidade;', 8);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 7);
        $codigo .= Ferramentas::linha('throw $sex;', 8);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 7);
        $codigo .= Ferramentas::linha('throw $ex;', 8);
        $codigo .= Ferramentas::linha('}', 7);
        $codigo .= Ferramentas::linha('} else {', 6);
        $codigo .= Ferramentas::linha('/* Nao possui registro, entao, Insere na tabela de linguagem */', 7);
        $codigo .= Ferramentas::linha('try {', 7);
        $codigo .= Ferramentas::linha('$this->db->insert($entidade::TABELA_LINGUAGEM, $valores[$entidade::TABELA_LINGUAGEM]);', 8);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 7);
        $codigo .= Ferramentas::linha('throw $sex;', 8);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 7);
        $codigo .= Ferramentas::linha('throw $ex;', 8);
        $codigo .= Ferramentas::linha('}', 7);
        $codigo .= Ferramentas::linha('}', 6);
        $codigo .= Ferramentas::linha('return $entidade;', 6);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 5);
        $codigo .= Ferramentas::linha('throw $sex;', 6);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 5);
        $codigo .= Ferramentas::linha('throw $ex;', 6);
        $codigo .= Ferramentas::linha('}', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('return $entidade;', 4);
        $codigo .= Ferramentas::linha('} else {', 3);
        $codigo .= Ferramentas::linha('/* Nao possui chave de ligacao, entao, atualiza apenas a tabela principal */', 4);
        $codigo .= Ferramentas::linha('try {', 4);
        $codigo .= Ferramentas::linha('foreach ($chavePrimariaTabela as $chave) {', 5);
        $codigo .= Ferramentas::linha('$this->db->where($chave, $valores[$entidade::TABELA][$chave]);', 6);
        $codigo .= Ferramentas::linha('}', 5);
        $codigo .= Ferramentas::linha('$this->db->update($entidade::TABELA, $valores[$entidade::TABELA]);', 5);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 4);
        $codigo .= Ferramentas::linha('throw $sex;', 5);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 4);
        $codigo .= Ferramentas::linha('throw $ex;', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('return $entidade;', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('}', 2);
        $codigo .= Ferramentas::linha('}', 1, 2);

        return $codigo;
    }

    private function gerarAtualizarPost()
    {
        $codigo = Ferramentas::linha('/**', 1);
        $codigo .= Ferramentas::linha(' * Metodo gerado automaticamente em ' . date('d/m/Y H:i:s'), 1);
        $codigo .= Ferramentas::linha(' * @param array $post', 1);
        $codigo .= Ferramentas::linha(" * @return {$this->repositorio}Entidade[]", 1);
        $codigo .= Ferramentas::linha(' * @throws Exception', 1);
        $codigo .= Ferramentas::linha(' * @throws SqlException', 1);
        $codigo .= Ferramentas::linha(' */', 1);
        $codigo .= Ferramentas::linha('public function atualizarPost($post)', 1);
        $codigo .= Ferramentas::linha('{', 1);
        $codigo .= Ferramentas::linha('$resultado = array();', 2);
        $codigo .= Ferramentas::linha('if (isset($post[\'linguagens\'])) {', 2);
        $codigo .= Ferramentas::linha('foreach ($post[\'linguagens\'] as $linguagem) {', 3);
        $codigo .= Ferramentas::linha("\$entidade = new {$this->repositorio}Entidade();", 4);
        $codigo .= Ferramentas::linha('foreach ($entidade->colunas()[$entidade::TABELA] as $coluna) {', 4);
        $codigo .= Ferramentas::linha('$set = Conversao::campoParaSet($coluna, $entidade::PREFIXO);', 5);
        $codigo .= Ferramentas::linha('if (isset($post[$coluna])) {', 5);
        $codigo .= Ferramentas::linha('$entidade->{$set}($post[$coluna]);', 6);
        $codigo .= Ferramentas::linha('}', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('foreach ($entidade->colunas()[$entidade::TABELA_LINGUAGEM] as $coluna) {', 4);
        $codigo .= Ferramentas::linha('if (isset($linguagem[$coluna])) {', 5);
        $codigo .= Ferramentas::linha('$set = Conversao::campoParaSet($coluna, $entidade::PREFIXO);', 6);
        $codigo .= Ferramentas::linha('$entidade->{$set}($linguagem[$coluna]);', 6);
        $codigo .= Ferramentas::linha('}', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('try {', 4);
        $codigo .= Ferramentas::linha('$resultado[] = $this->atualizar($entidade);', 5);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 4);
        $codigo .= Ferramentas::linha('throw $sex;', 5);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 4);
        $codigo .= Ferramentas::linha('throw $ex;', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('} else {', 2);
        $codigo .= Ferramentas::linha("\$entidade = new {$this->repositorio}Entidade();", 3);
        $codigo .= Ferramentas::linha('foreach ($entidade->colunas()[$entidade::TABELA] as $coluna) {', 3);
        $codigo .= Ferramentas::linha('if (isset($post[$coluna])) {', 4);
        $codigo .= Ferramentas::linha('$set = Conversao::campoParaSet($coluna, $entidade::PREFIXO);', 5);
        $codigo .= Ferramentas::linha('$entidade->{$set}($post[$coluna]);', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('try {', 3);
        $codigo .= Ferramentas::linha('$resultado[] = $this->atualizar($entidade);', 4);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 3);
        $codigo .= Ferramentas::linha('throw $sex;', 4);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 3);
        $codigo .= Ferramentas::linha('throw $ex;', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('}', 2);
        $codigo .= Ferramentas::linha('return $resultado;', 2);
        $codigo .= Ferramentas::linha('}', 1);

        return $codigo;
    }

    private function gerarBuscar()
    {
        $codigo = Ferramentas::linha('/**', 1);
        $codigo .= Ferramentas::linha(' * Metodo gerado automaticamente em ' . date('d/m/Y H:i'), 1);
        $codigo .= Ferramentas::linha('* @param string $campo <p>Campo do banco de dados (aceita \'campo >\', \'campo <=\')</p>>', 1);
        $codigo .= Ferramentas::linha('* @param string $valor <p>Valor do campo do banco de dados</p>>', 1);
        $codigo .= Ferramentas::linha('* @param boolean $like <p>Se a pesquisa usara like \'%valor%\'</p>>', 1);
        $codigo .= Ferramentas::linha("* @return {$this->repositorio}Entidade[]", 1);
        $codigo .= Ferramentas::linha('* @throws Exception', 1);
        $codigo .= Ferramentas::linha('* @throws SqlException', 1);
        $codigo .= Ferramentas::linha('*/', 1);
        $codigo .= Ferramentas::linha('public function buscar($campo=null, $valor=null, $like = false)', 1);
        $codigo .= Ferramentas::linha('{', 1);
        $codigo .= Ferramentas::linha('if ($campo && $valor) {', 2);
        $codigo .= Ferramentas::linha('if (!$like) {', 2);
        $codigo .= Ferramentas::linha('$this->db->where($campo, $valor);', 3);
        $codigo .= Ferramentas::linha('} else {', 2);
        $codigo .= Ferramentas::linha('$posicao = Conversao::posicaoDoLike($valor);', 3);
        $codigo .= Ferramentas::linha('if ($posicao === null) {', 3);
        $codigo .= Ferramentas::linha('$this->db->like($campo, $valor);', 4);
        $codigo .= Ferramentas::linha('} else {', 3);
        $codigo .= Ferramentas::linha('$this->db->like($campo, str_replace(\'%\', \'\', $valor), $posicao);', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('}', 2);
        $codigo .= Ferramentas::linha('}', 2);
        $codigo .= Ferramentas::linha("\$chavePrimaria = current(\$this->chavePrimaria()[{$this->repositorio}Entidade::TABELA]);", 2);

        $codigo .= Ferramentas::linha("//Se NÃO tiver tabela de linguagem", 2);
        $codigo .= Ferramentas::linha("if (!{$this->repositorio}Entidade::TABELA_LINGUAGEM) {", 2);
        $codigo .= Ferramentas::linha('try {', 3);
        $codigo .= Ferramentas::linha('$result = $this->db', 4);
        $codigo .= Ferramentas::linha("->get({$this->repositorio}Entidade::TABELA)", 5);
        $codigo .= Ferramentas::linha('->result();', 5);
        $codigo .= Ferramentas::linha('$output = array();', 4);
        $codigo .= Ferramentas::linha('foreach ($result as $row) {', 4);
        $codigo .= Ferramentas::linha("\$output[] = Conversao::objetoParaEntidade(\$row, new {$this->repositorio}Entidade());", 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('return $output;', 4);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 3);
        $codigo .= Ferramentas::linha('return array();', 4);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 3);
        $codigo .= Ferramentas::linha('return array();', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('}', 3);

        $codigo .= Ferramentas::linha('//Caso tenha tabela de linguagem', 3);
        $codigo .= Ferramentas::linha("if ({$this->repositorio}Entidade::TABELA_LINGUAGEM) {", 2);
        $codigo .= Ferramentas::linha('try {', 3);
        $codigo .= Ferramentas::linha("\$chaves = \$this->chavePrimaria()[{$this->repositorio}Entidade::TABELA_LINGUAGEM];", 4);
        $codigo .= Ferramentas::linha('foreach ($chaves as $value) {', 4);
        $codigo .= Ferramentas::linha("if (strpos(\$value, {$this->repositorio}Entidade::TABELA)) {", 5);
        $codigo .= Ferramentas::linha('$chaveJoinLinguagem = $value;', 6);
        $codigo .= Ferramentas::linha('break;', 6);
        $codigo .= Ferramentas::linha('}', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('', 0);
        $codigo .= Ferramentas::linha('foreach ($chaves as $value) {', 4);
        $codigo .= Ferramentas::linha('if (strpos($value, \'linguagem\')) {', 5);
        $codigo .= Ferramentas::linha('$chaveIdLinguagem = $value;', 6);
        $codigo .= Ferramentas::linha('break;', 6);
        $codigo .= Ferramentas::linha('}', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('if (isset($chaveJoinLinguagem) && isset($chaveIdLinguagem)) {', 4);
        $codigo .= Ferramentas::linha('$idLinguagemAtual = Linguagem::getIdLinguagemAtual();', 5);
        $codigo .= Ferramentas::linha('try {', 5);
        $codigo .= Ferramentas::linha('$result = $this->db', 6);
        $codigo .= Ferramentas::linha("->from({$this->repositorio}Entidade::TABELA)", 7);
        $codigo .= Ferramentas::linha("->join({$this->repositorio}Entidade::TABELA_LINGUAGEM, \"{\$chaveJoinLinguagem} = {\$chavePrimaria} AND {\$chaveIdLinguagem} = {\$idLinguagemAtual}\", 'left', false)", 7);
        $codigo .= Ferramentas::linha('->get()', 7);
        $codigo .= Ferramentas::linha('->result();', 7);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 5);
        $codigo .= Ferramentas::linha('throw $sex;', 6);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 5);
        $codigo .= Ferramentas::linha('throw $ex;', 6);
        $codigo .= Ferramentas::linha('}', 5);
        $codigo .= Ferramentas::linha('} else {', 4);
        $codigo .= Ferramentas::linha('try {', 5);
        $codigo .= Ferramentas::linha('$result = $this->db', 6);
        $codigo .= Ferramentas::linha("->get({$this->repositorio}Entidade::TABELA)", 7);
        $codigo .= Ferramentas::linha('->result();', 7);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 5);
        $codigo .= Ferramentas::linha('throw $sex;', 6);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 5);
        $codigo .= Ferramentas::linha('throw $ex;', 6);
        $codigo .= Ferramentas::linha('}', 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('$output = array();', 4);
        $codigo .= Ferramentas::linha('foreach ($result as $row) {', 4);
        $codigo .= Ferramentas::linha("\$output[] = Conversao::objetoParaEntidade(\$row, new {$this->repositorio}Entidade());", 5);
        $codigo .= Ferramentas::linha('}', 4);
        $codigo .= Ferramentas::linha('return $output;', 4);
        $codigo .= Ferramentas::linha('} catch (SqlException $sex) {', 3);
        $codigo .= Ferramentas::linha('throw $sex;', 4);
        $codigo .= Ferramentas::linha('} catch (Exception $ex) {', 3);
        $codigo .= Ferramentas::linha('throw $ex;', 4);
        $codigo .= Ferramentas::linha('}', 3);
        $codigo .= Ferramentas::linha('}', 2);
        $codigo .= Ferramentas::linha('}', 1);

        return $codigo;
    }

    private function gerarChavePrimaria()
    {
        $codigo = Ferramentas::linha();
        $codigo .= Ferramentas::linha('/**', 1);
        $codigo .= Ferramentas::linha(' * Metodo gerado automaticamente em ' . date('d/m/Y H:i:s'), 1);
        $codigo .= Ferramentas::linha(' * @return array Chave(s) Primaria(s) da(s) Tabela(s)', 1);
        $codigo .= Ferramentas::linha(' */', 1);
        $codigo .= Ferramentas::linha('public function chavePrimaria() ', 1);
        $codigo .= Ferramentas::linha('{', 1);
        $codigo .= Ferramentas::linha('return array(', 2);

        foreach ($this->chavesPrimarias as $key => $chaves) {
            $codigo .= Ferramentas::linha("'{$key}' => array(", 3);
            foreach ($chaves as $chave) {
                $codigo .= Ferramentas::linha("'{$chave}',", 4);
            }
            $codigo .= Ferramentas::linha('),', 3);
        }

        $codigo .= Ferramentas::linha(');', 2);
        $codigo .= Ferramentas::linha('}', 1);

        return $codigo;
    }
}