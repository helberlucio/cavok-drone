<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 28/08/2016
 * Time: 13:48
 */

namespace Modulos\Criadores\Entidades\Controllers;

use Aplicacao\Url;
use Core\Controlador\Controlador;
use Modulos\Criadores\Entidades\Models\EntidadesRN;

class Index extends Controlador
{

    /**
     * @param bool|false $sucesso
     * @return string|void
     * @descricao Gerador de Código  Entidades e Repositórios
     * @escritorio desenvolvedor
     */
    public function index($sucesso = false)
    {
        /* Dados da visao */
        $data = array();
        /* Lista de modulos */
        $data['modulos'] = array();
        $pastasModulos = new \DirectoryIterator('src/Modulos');
        foreach ($pastasModulos as $pasta) {
            if ($pasta->isDir() && !$pasta->isDot()) {
                $data['modulos'][] = $pasta->getFilename();
            }
        }
        sort($data['modulos']);
        /* Lista de Tabelas do Banco de Dados */
        $data['tabelas'] = array();

        try {
            $result = $this->db->select('`table_name`')
                ->where('`table_schema`', LI_CONNECT_DATABASE)
                ->get('`information_schema`.`tables`')
                ->result();
            foreach ($result as $row) {
                $data['tabelas'][] = $row->table_name;
            }
        } catch (\SQLiteException $e) {
            echo $e->getMessage();
        }
        sort($data['tabelas']);

        /* Lista de Ações */
        $data['acoes'] = array(
            'adicionar' => 'Adicionar',
            'remover' => 'Remover',
            'atualizar' => 'Atualizar',
            'buscar' => 'Buscar',
            'salvar' => 'Salvar',
            'buscar_por_id' => 'Buscar por ID',
        );
        $data['sucesso'] = $sucesso;
        $data['pagina'] = 'index';

        return $this->load->AdminView('Home/index', $data);
    }

    public function modulosAJAX()
    {
        if (!isset($_POST['grupoModulo'])) {
            echo json_encode(array());
        }
        if (isset($_POST['grupoModulo'])) {
            $grupoModulo = $_POST['grupoModulo'];
        }

        $data['modulos'] = array();
        $pastasModulos = new \DirectoryIterator('src/Modulos/' . $grupoModulo);
        foreach ($pastasModulos as $pasta) {
            if ($pasta->isDir() && !$pasta->isDot()) {
                $data['modulos'][] = $pasta->getFilename();
            }
        }
        sort($data['modulos']);
        echo json_encode($data['modulos']);
    }

    /**
     * @param $tabela
     * @return array
     */
    private function getCamposTabela($tabela)
    {
        $output = array();
        $result = $this->db->select('`column_name`')
            ->where('`table_schema`', LI_CONNECT_DATABASE)
            ->where('`table_name`', $tabela)
            ->get('`information_schema`.`columns`')
            ->result();
        foreach ($result as $row) {
            $output[] = $row->column_name;
        }
        return $output;
    }

    /**
     * @param null $tabela
     * @param null $grupoModulo
     * @param null $modulo
     * @param null $raiz
     * @return string|void
     */
    public function gerar($tabela = null, $grupoModulo = null, $modulo = null, $raiz = null)
    {

        if(!input_post()){
            $this->index();
        }

        $grupoModulo = isset($_POST['grupoModulo']) ? $_POST['grupoModulo'] : $grupoModulo;
        $modulo = isset($_POST['modulo']) ? $_POST['modulo'] : $modulo;
        $raiz = isset($_POST['raiz']) ? $_POST['raiz'] : $raiz;

        if (!$raiz && (!$grupoModulo && !$modulo)) {
            echo json_encode(array('level' => 1, 'mensagem' => 'Antes de gerar sua entidade, selecione o diretório onde será gerada.'));
            exit;
        }
        $diretorio = '';
        if (($grupoModulo && $modulo) && !$raiz) {
            $diretorio = 'Modulos/'.$grupoModulo . '/' . $modulo.'/Models/Entidades/';
        }
        if ($raiz) {
            $diretorio = $raiz;
        }

        $tabela = isset($_POST['tabela']) ? $_POST['tabela'] : $tabela;

        if (!$tabela) {
            echo json_encode(array('level' => 1, 'mensagem' => 'Antes de gerar sua entidade, selecione a tabela.'));
            exit;
        }

        $this->setTabelasLigacao($tabela);
        $Entidades = new EntidadesRN();

        foreach ($this->entidades_compos_relacionados as $tabelaEntidade => $entidadesRelacionadas) {
            $campos = $this->getCamposTabela($tabelaEntidade);
            $Entidades
                ->setModulo($diretorio)
                ->setTabela($tabelaEntidade)
                ->setRelacionamentos($entidadesRelacionadas)
                ->setTodosRelacionamentosPossiveis($this->entidades_compos_relacionados)
                ->setCampos($campos)
                ->gerar()
                ->salvar();
        }
        echo json_encode(array('level' => 0, 'mensagem' => 'Entidades no padrão ORM Criadas com sucesso em: <b>'.$diretorio.'</b>'));
    }

    private $entidades_compos_relacionados = array();

    private function setTabelasLigacao($tabela)
    {
        $colunas = $this->db->select('COLUMN_NAME, TABLE_NAME')
            ->where('`table_schema`', LI_CONNECT_DATABASE)
            ->where('COLUMN_NAME', $tabela . '_id')
            ->get('`information_schema`.`COLUMNS`')
            ->result();

        $ligacoes = array();
        foreach ($colunas as $coluna) {
            $ligacoes[] = array('tabela' => $coluna->TABLE_NAME, 'coluna' => $coluna->COLUMN_NAME);
            $this->setTabelasLigacao($coluna->TABLE_NAME);
        }
        $this->entidades_compos_relacionados[$tabela] = $ligacoes;
    }

    /**
     * @descricao Imprimir Código
     * @escritorio desenvolvedor
     */

    public function codigo()
    {
        $codigo = '';
        if (isset($_POST['codigo'])) {
            $codigo = $_POST['codigo'];
            echo "<pre>";
            foreach (explode(PHP_EOL, $codigo) as $linha) {
                $tabs = floor((strlen($linha) - strlen(ltrim($linha))) / 4);
                $linha = str_replace("'", "\\'", $linha);
                $linha = str_replace('<p>', '&lt;p&gt;', $linha);
                $linha = str_replace('</p>', '&lt;/p&gt;>', $linha);
                echo "\$codigo .= Ferramentas::linha('" . ltrim($linha) . "', {$tabs});" . PHP_EOL;
            }
            echo "</pre>";
        }
        echo "<form method='post'>
                <textarea name='codigo' style='width: 100%' rows='10'>" . $codigo . "</textarea>
                <input type='submit' value='Enviar' />
                </form>";
        exit;
    }
}