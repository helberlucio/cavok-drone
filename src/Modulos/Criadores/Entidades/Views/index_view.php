<?php

/**
 * @author Gabriel
 * @var $modulos []
 * @var $tabelas []
 * @var $acoes []
 */

use Aplicacao\Url;

?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-cog"></i>
            Gerador
            <i class="fa fa-angle-right"></i>
            Entidades Relacionadas
        </div>
        <div class="panel-body">
            <!-- ## Parte 1 ## -->
            <div class="col-md-12 GeradorEntidades-tipo-diretorio">
                <div class="form-group GeradorEntidades-tipo-escritorio">
                    <label for="form-modulo" class="col-sm-2 control-label">
                        Tipo de diretório
                    </label>
                    <div class="col-sm-10" style="padding: 0 5px;">
                        <div class="form-group">
                            <select onchange="ChangeTipoDiretorio($(this).val())" class="form-control"
                                    id="tipo-diretorio"
                                    name="tipo_diretorio">
                                <option value="0">
                                    Selecionar
                                </option>
                                <option value="1">
                                    Diretório de Módulos
                                </option>
                                <option value="2">
                                    Diretório de a escolher
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group GeradorEntidades-tipo-escritorio">
                    <label for="form-modulo" class="col-sm-2 control-label">
                        Tabela para entidade.
                    </label>
                    <div class="col-sm-10" style="padding: 0 5px;">
                        <div class="form-group">
                            <select class="form-control" name="tabela" id="form-tabelas" style="margin-top:10px;">
                                <option value="">-- <?php echo 'Selecione'; ?></option>
                                <?php foreach ($tabelas as $tabela) { ?>
                                    <option value="<?php echo $tabela; ?>">
                                        <?php echo $tabela; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ## Parte 2 ## -->
            <div class="col-md-12 GeradorEntidades-raiz-modulo" data-open="close">
                <form class="form-horizontal" method="post" data-formulario="diretorio-dinamico">
                    <div class="form-group">
                        <label for="form-modulo" class="col-sm-2 control-label">
                            <?php echo 'Grupos de Módulo'; ?>
                        </label>
                        <div class="col-sm-10">
                            <select onchange="aoMudarGrupoModulos(this)" class="form-control" name="grupoModulo"
                                    id="form-grupo-modulo">
                                <option>-- <?php echo 'Selecione'; ?></option>
                                <?php foreach ($modulos as $modulo) { ?>
                                    <option value="<?php echo $modulo; ?>">
                                        <?php echo $modulo; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="form-modulo" class="col-sm-2 control-label">
                            <?php echo 'Módulo'; ?>
                        </label>
                        <div class="col-sm-10">
                            <select class="form-control" name="modulo" id="form-modulo">
                                <option>-- <?php echo 'Selecione'; ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-play"></i>
                                <?php echo 'Gerar'; ?>
                            </button>
                        </div>
                    </div>
                </form>
            </div>

            <!-- ## Parte 3 ## -->
            <div class="col-md-12 GeradorEntidades-raiz-dinamica" data-open="close">
                <form class="form-horizontal" method="post" data-formulario="diretorio-dinamico">
                    <div class="form-group">
                        <label>Especifique o diretório.
                            <small>Lembrando que inicia-se apartir da pasta "src/"</small>
                        </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-share"></i>
                             </span>
                            <input style="min-width: 135px;" type="text" name="raiz" class="form-control"
                                   placeholder="Raiz do local EX: Modulo/Criadores/Entidades...">
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-play"></i>
                            <?php echo 'Gerar'; ?>
                        </button>
                    </div>

                </form>
            </div>

            <div class="col-md-12 call-back" style="padding: 0;"></div>

        </div>
    </div>
    <script>
        var url_modulos = '<?php echo Url::ModuloAdmin('Criadores/Entidades/Index/modulosAJAX');?>';
        var url_gerador = '<?php echo Url::ModuloAdmin('Criadores/Entidades/Index/gerar'); ?>';
    </script>
<?php
echo Aplicacao\Ferramentas\GetAssets::JS('EntidadeRepositorio', 'Criadores', 'Entidades');
echo Aplicacao\Ferramentas\GetAssets::CSS('EntidadeRepositorio', 'Criadores', 'Entidades');
?>