<?php

namespace Modulos\Criadores\Menu\Models\Entidades;

use Aplicacao\Conversao;
use Aplicacao\GenericRepository;
use Core\Modelos\ModelagemDb;
class CmsMenu
{
    const TABELA = 'cms_menu';
    const PREFIXO = '';
    const NAMESPACE_FILE = 'Modulos\Criadores\Menu\Models\Entidades';
    const FILE_NAME = 'CmsMenu';

    private $id;
    private $menu;
    private $icone;
    private $link;
    private $target;
    private $submenu_id;
    private $ativo;

    public function __construct()
    {
    }

    /**
     * @return id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return CmsMenu
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return menu
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * @param string $menu
     * @return CmsMenu
     */
    public function setMenu($menu)
    {
        $this->menu = $menu;
        return $this;
    }

    /**
     * @return icone
     */
    public function getIcone()
    {
        return $this->icone;
    }

    /**
     * @param string $icone
     * @return CmsMenu
     */
    public function setIcone($icone)
    {
        $this->icone = $icone;
        return $this;
    }

    /**
     * @return link
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return CmsMenu
     */
    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    /**
     * @return target
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param string $target
     * @return CmsMenu
     */
    public function setTarget($target)
    {
        $this->target = $target;
        return $this;
    }

    /**
     * @return submenu_id
     */
    public function getSubmenuId()
    {
        return $this->submenu_id;
    }

    /**
     * @param string $submenu_id
     * @return CmsMenu
     */
    public function setSubmenuId($submenu_id)
    {
        $this->submenu_id = $submenu_id;
        return $this;
    }

    /**
     * @return ativo
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * @param string $ativo
     * @return CmsMenu
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
        return $this;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return int <p>Quantidade maxima de caracteres permitidos</p>
     */
    public function tamanhoMaximo($coluna)
    {
        $tamanhos = array(
            'id' => 0,
            'menu' => 0,
            'icone' => 0,
            'link' => 0,
            'target' => 0,
            'submenu_id' => 0,
            'ativo' => 0,
        );

        return isset($tamanhos[$coluna]) ? $tamanhos[$coluna] : 0;
    }

    /**
     * @param string <p>Nome da coluna do banco de dados</p>
     * @return string <p>Tipo da coluna no banco de dados</p>
     */
    public function tipos($coluna)
    {
        $tipos = array(
            'id' => 'string',
            'menu' => 'string',
            'icone' => 'string',
            'link' => 'string',
            'target' => 'string',
            'submenu_id' => 'string',
            'ativo' => 'string',
        );
        return isset($tipos[$coluna]) ? $tipos[$coluna] : null;
    }

    /**
     * @return array <p>Lista de colunas do banco de dados</p>
     */
    public function colunas()
    {
        return array(
            'cms_menu' => array(
                'id',
                'menu',
                'icone',
                'link',
                'target',
                'submenu_id',
                'ativo',
            ),
        );

    }

    
    /**
    * @param ModelagemDb $objectFilter
    * @return $this
    */
    public function getRow(ModelagemDb $objectFilter)
    {
        $obj = $objectFilter->get(self::TABELA)->row();
        $thisClass = "\\". self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $classe = Conversao::objetoParaClasse($obj, new $thisClass);
        return $classe;
    }

    /**
    * @param ModelagemDb $objectFilter
    * @return $this[]
    */
    public function getRows(ModelagemDb $objectFilter)
    {
        $classe = array();
        $objects = $objectFilter->get(self::TABELA)->result();
        $thisClass = "\\". self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        foreach ($objects as $row)
        {
            $classe[] = Conversao::objetoParaClasse($row, new $thisClass);
        }
        return $classe;
    }

    public function update()
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $data = GenericRepository::createSaveData($thisClass, $this);
        $db->update(self::TABELA, $data);
    }

    public function delete()
    {
         $db = new ModelagemDb();
        $data["id"] = $this->getId();
        $db->delete(self::TABELA, $data);
    }

    /**
    * @return int
    */
    public function insert()
    {
        $db = new ModelagemDb();
        $thisClass = "\\" . self::NAMESPACE_FILE . "\\" . self::FILE_NAME;
        $data = GenericRepository::createSaveData($thisClass, $this);
        return $db->insert(self::TABELA, $data);
    }

}

