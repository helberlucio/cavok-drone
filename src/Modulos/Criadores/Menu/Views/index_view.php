<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-cog"></i>
        Gerador
        <i class="fa fa-angle-right"></i>
        Menu
    </div>
    <div class="panel-body">
        <div class="col-md-12" style="margin-top:35px;margin: 0 -15px !important;">
            <div class="row">
                <form name="form1" class="formularioModulo form-horizontal insert-form" enctype="multipart/form-data"
                      role="form"
                      id="validar_formulario"
                      method="post">
                    <div class="buttons" style="text-align: left">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Icone do Menu: </label>
                            <div class="col-sm-10">
                                <input name="icone" type="text" class="form-control" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nome do Menu: </label>
                            <div class="col-sm-10">
                                <input name="menu" type="text" class="form-control" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Target: </label>
                            <div class="col-sm-10">
                                <select class="form-control" name="target">
                                    <option value=" ">-- NONE --</option>
                                    <option value="_blank">_blank</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Link Externo: </label>
                            <div class="col-sm-10">
                                <input name="link" type="text" class="form-control" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Sub-Menu: </label>
                            <div class="col-sm-10">
                                <select class="form-control" name="submenu_id">
                                    <option value="0">-- NONE --</option>
                                    <?php
                                    $menus = $this->db->get('cms_menu')->result();
                                    foreach ($menus as $menu) {
                                        ?>
                                        <option value="<?php echo $menu->id; ?>"><?php echo $menu->menu; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="buttons col-md-12" style="text-align: right;margin-top:30px;">
                        <a class="btn btn-primary" style="color:#FFF;"><i class="icon-remove-circle"></i> Fechar</a>
                        <input type="submit" class="btn btn-success" value="Salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('.formularioModulo').submit(function () {
            $('.load-gear').fadeIn('fast');
            var serialized = $(this).serialize();
            var url = '<?php echo Aplicacao\Url::ModuloAdmin('Criadores/Menu/Index/Criar');?>';
            $.post(url, serialized, function (data) {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-bottom-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };
                $('.load-gear').fadeOut('slow');
                if (data.level == 0) {
                    toastr.success(data.mensagem);
                }
                if (data.level == 1) {
                    toastr.warning(data.mensagem);
                }
            }, 'json');
            return false;
        });
    });
</script>