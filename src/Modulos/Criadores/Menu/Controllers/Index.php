<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 07/11/2016
 * Time: 20:57
 */

namespace Modulos\Criadores\Menu\Controllers;


use Aplicacao\Ferramentas\Ajax;
use Core\Controlador\Controlador;
use Modulos\Criadores\Menu\Models\Entidades\CmsMenu;

class Index extends Controlador
{

    public function index()
    {
        $data['menu'] = '';
        $data['pagina'] = 'index';
        $this->load->AdminView('Home/index', $data);
    }


    /**
     * @description Cria o menu se o mesmo não existir
     */
    public function Criar()
    {
        if ($this->menuAlreadyExist(input_post('menu'))) {
            $callback = array('level' => 1, 'mensagem' => 'Menu já existe! Tente outro ou habilite esse.');
            Ajax::retorno($callback);
        }
        $cmsMenu = new CmsMenu();
        $cmsMenu->setIcone(input_post('icone'))
            ->setMenu(input_post('menu'))
            ->setTarget(input_post('target'))
            ->setLink(input_post('link'))
            ->setAtivo(1)
            ->setSubmenuId(input_post('submenu_id') ? input_post('submenu_id') : 0)
            ->insert();
        $callback = array('level' => 0, 'mensagem' => 'Menu Criado com sucesso!');
        Ajax::retorno($callback);
    }

    /**
     * @param $menu string
     * @return bool
     */
    private function menuAlreadyExist($menu)
    {
        $modelagem = $this->db->where('menu', $menu);
        $menu = new CmsMenu();
        $menuExistent = $menu->getRow($modelagem);
        return $menuExistent->getId() ? true : false;
    }

}