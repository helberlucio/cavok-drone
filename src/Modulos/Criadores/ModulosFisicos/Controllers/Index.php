<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 13/11/2016
 * Time: 22:37
 */

namespace Modulos\Criadores\ModulosFisicos\Controllers;

use Core\Controlador\Controlador;
use Modulos\Criadores\ModulosFisicos\Models\GerarModuloFisico;

class Index extends Controlador
{

    public function index()
    {
        $data['pagina'] = 'index';
        $this->load->AdminView('Home/index', $data);
    }

    public function gerarAJAX()
    {
        $grupoModulo = input_post('grupo_modulo');
        $nomeModulo = input_post('nome_modulo');
        $forcarCriacao = input_post('forcar_criacao') ? input_post('forcar_criacao') : false;
        $eAPI = input_post('modulo_api') ? true : false;

        $regraDeNegocio = new GerarModuloFisico();
        echo json_encode($regraDeNegocio->gerar($grupoModulo, $nomeModulo, $forcarCriacao, $eAPI));

    }

}