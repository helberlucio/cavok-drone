<?php

/**
 * @author Gabriel
 * @var $modulos []
 * @var $tabelas []
 * @var $acoes []
 */

use Aplicacao\Url;

?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-cog"></i>
            Criador
            <i class="fa fa-angle-right"></i>
            Módulos Físicos
            <small>Criar Pastas e arquivos necessários</small>
        </div>
        <div class="panel-body">
            <!-- ## Parte 1 ## -->
            <div class="col-md-12 GeradorEntidades-tipo-diretorio">
                <form method="post">
                    <div class="form-group GeradorEntidades-tipo-escritorio">
                        <label for="form-modulo" class="col-sm-2 control-label">
                            Grupo do módulo
                        </label>
                        <div class="col-sm-10" style="padding: 0 5px;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="grupo_modulo"
                                       placeholder="Digite o grupo do Módulo EX: Banners.">
                            </div>
                        </div>
                    </div>
                    <div class="form-group GeradorEntidades-tipo-escritorio">
                        <label for="form-modulo" class="col-sm-2 control-label">
                            Nome do Módulo
                        </label>
                        <div class="col-sm-10" style="padding: 0 5px;">
                            <div class="form-group">
                                <input class="form-control" type="text" name="nome_modulo"
                                       placeholder="Nome do Módulo EX: FullBasic">
                            </div>
                        </div>
                    </div>
                    <div class="form-group GeradorEntidades-tipo-escritorio">
                        <label for="form-modulo" class="col-sm-2 control-label">
                            É Módulo de API ?
                        </label>
                        <div class="col-sm-10" style="padding: 0 5px;">
                            <div class="form-group">
                                <input type="checkbox" name="modulo_api">
                            </div>
                        </div>
                    </div>
                    <div class="form-group GeradorEntidades-tipo-escritorio">
                        <label for="form-modulo" class="col-sm-2 control-label">
                            Forçar Criação do Módulo
                        </label>
                        <div class="col-sm-10" style="padding: 0 5px;">
                            <div class="form-group">
                                <label><input type="checkbox" name="forcar_criacao">
                                    <small>Mesmo que o módulo já exista, será recriado.</small>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group GeradorEntidades-tipo-escritorio">
                        <div class="col-sm-10" style="padding: 0 5px;">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-play-circle" aria-hidden="true"></i> Criar
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12 call-back" style="padding: 0;"></div>
        </div>
    </div>
    <script>
        var url_gerador = '<?php echo Url::ModuloAdmin('Criadores/ModulosFisicos/Index/gerarAJAX'); ?>';
    </script>
<?php
Aplicacao\Ferramentas\GetAssets::JS('modulosfisicos', 'Criadores', 'ModulosFisicos');
Aplicacao\Ferramentas\GetAssets::CSS('modulosfisicos', 'Criadores', 'ModulosFisicos');
?>