<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 14/11/2016
 * Time: 10:12
 */

namespace Modulos\Criadores\ModulosFisicos\Models;


use Aplicacao\Conversao;
use Aplicacao\Ferramentas\Criacao;

class GerarModuloFisico
{

    private $error = array();
    private $classeName;

    public function gerar($grupoModulo, $nomeModulo, $forcarCriacao = false, $api = false)
    {
        $diretorioModulos = LI_DIR_MODULOS;

        if ($api) {
            $grupoModulo = $grupoModulo . 'API';
        }

        $grupoModulo = Conversao::underscoreToCamelCase($grupoModulo);
        $nomeModulo = Conversao::underscoreToCamelCase($nomeModulo);

        $validacao = $this->validarEspecificacoesDoModulo($diretorioModulos, $grupoModulo, $nomeModulo, $forcarCriacao);
        if (!$validacao) {
            return $this->error;
        }

        $this->classeName = Conversao::underscoreToCamelCase($grupoModulo) . Conversao::underscoreToCamelCase($nomeModulo);

        $diretoriosACriar = array(
            $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/",
            $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Assets/",
            $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Assets/js/",
            $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Assets/css/",
            $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Assets/Images/",
            $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Assets/Global/",
            $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Controllers/",
            $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Models/",
            $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Models/RegrasDeNegocio/",
            $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Models/Entidades/",
            $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Models/Repositorios/",
            $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Models/Gadgets/",
            $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Migracao/",
            $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Migracao/Tabelas/",
            $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Migracao/Dados/",
            $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Views/",
            $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Views/Gadgets/"
        );
        if ($api) {
            unset($diretoriosACriar[0]);
            unset($diretoriosACriar[1]);
            unset($diretoriosACriar[2]);
            unset($diretoriosACriar[3]);
            unset($diretoriosACriar[4]);
            unset($diretoriosACriar[5]);
            unset($diretoriosACriar[12]);
            unset($diretoriosACriar[13]);
            unset($diretoriosACriar[14]);
            unset($diretoriosACriar[15]);
            unset($diretoriosACriar[16]);
        }

        foreach ($diretoriosACriar as $diretorio) {
            if (!is_dir($diretorio)) {
                mkdir($diretorio, 0777, true);
            }
            @unlink($diretorio . '.gitkeep');

            if ($diretorio == $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Controllers/") {
                $this->CriarControlador($grupoModulo, $nomeModulo, $diretorio);
            } elseif ($diretorio == $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Views/") {
                $this->CriarView($grupoModulo, $nomeModulo, $diretorio);
            } elseif ($diretorio == $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Models/RegrasDeNegocio/") {
                $this->CriarRegrasDeNegocio($grupoModulo, $nomeModulo, $diretorio);
            } elseif ($diretorio == $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Models/Repositorios/") {
                $this->CriarRepositorio($grupoModulo, $nomeModulo, $diretorio);
            } elseif ($diretorio == $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Assets/Global/") {
                $this->CriarGlobal($grupoModulo, $nomeModulo, $diretorio);
            } elseif ($diretorio == $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Assets/css/") {
                $this->CriarCss($grupoModulo, $nomeModulo, $diretorio);
            } elseif ($diretorio == $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Models/Gadgets/") {
                $this->CriarGadget($grupoModulo, $nomeModulo, $diretorio);
            } elseif ($diretorio == $diretorioModulos . $grupoModulo . '/' . $nomeModulo . "/Views/Gadgets/") {
                $this->CriarView($grupoModulo, $nomeModulo, $diretorio);
            } else {
                $this->CriarPadrao($grupoModulo, $nomeModulo, $diretorio);
            }
        }
        $this->error = array('level' => 0, 'mensagem' => "Módulo físico criado com sucesso!");
        return $this->error;

    }

    private function validarEspecificacoesDoModulo($diretorioModulos, $grupoModulo, $nomeModulo, $forcarCriacao)
    {
        /**
         * Validando
         */
        if (preg_match_all('/[A-z0-9]/', $nomeModulo) != strlen($nomeModulo)) {
            $this->error = array('level' => 1, 'mensagem' => "Nome do modulo invalido, nao aceita caracteres epeciais\n");
            return false;
        }

        if (!$grupoModulo) {
            $this->error = array('level' => 1, 'mensagem' => "Informe o grupo do modulo\n");
            return false;
        }
        if (!$nomeModulo) {
            $this->error = array('level' => 1, 'mensagem' => "Informe o nome do modulo\n");
            return false;
        }
        if (strpos('Modulo' . $nomeModulo, '_')) {
            $this->error = array('level' => 1, 'mensagem' => "Nao e permitido utilizar _ no grupo do modulo\n");
            return false;
        }
        if (strpos('Modulo' . $nomeModulo, '_')) {
            $this->error = array('level' => 1, 'mensagem' => "Nao e permitido utilizar _ no nome do modulo\n");
            return false;
        }
        if (is_numeric(substr($grupoModulo, 0, 1))) {
            $this->error = array('level' => 1, 'mensagem' => "Nao e permitido numero iniciando o grupo do modulo\n");
            return false;
        }
        if (is_numeric(substr($nomeModulo, 0, 1))) {
            $this->error = array('level' => 1, 'mensagem' => "Nao e permitido numero iniciando o nome do modulo\n");
            return false;
        }
        if (is_dir($diretorioModulos . $grupoModulo . '/' . $nomeModulo) && !$forcarCriacao) {
            $this->error = array('level' => 1, 'mensagem' => "Módulo já existe! Você pode forçar a criação do módulo, caso realmente queira perder as alteraçõe");
            return false;
        }
        return true;
    }


    /**
     * @param $grupoModulo
     * @param $nomeModulo
     */
    private function CriarView($grupoModulo, $nomeModulo, $diretorio)
    {
        $classe = $grupoModulo . $nomeModulo;
        $controladorCodigo = '';
        $controladorCodigo .= Criacao::linha('<?php ', 0, 1);
        $controladorCodigo .= Criacao::linha("\\Aplicacao\\Ferramentas\\GetAssets::CSS('" . $classe . "', '" . $grupoModulo . "', '" . $nomeModulo . "');", 0, 1);
        $controladorCodigo .= Criacao::linha('?> ', 0, 1);
        $controladorCodigo .= Criacao::linha('<div class="container-fluid ' . $classe . '-fluid ' . $classe . '">', 0, 1);
        $controladorCodigo .= Criacao::linha('<div class="row">', 1, 1);
        $controladorCodigo .= Criacao::linha('<div class="container ' . $classe . '-container">', 2, 1);
        $controladorCodigo .= Criacao::linha('<h1>Módulo ' . $classe . ' Criado com sucesso</h1>', 3, 1);
        $controladorCodigo .= Criacao::linha('<small>' . $diretorio . '</small>', 3, 1);
        $controladorCodigo .= Criacao::linha('</div>', 2, 1);
        $controladorCodigo .= Criacao::linha('</div>', 1, 1);
        $controladorCodigo .= Criacao::linha('</div>', 0, 1);
        $arquivo = str_replace('\\', '/', $diretorio) . 'index_view' . EXT;

        file_put_contents($arquivo, $controladorCodigo);
        @chmod($arquivo, 0777);

    }

    public function CriarCss($grupoModulo, $nomeModulo, $diretorio)
    {
        $controladorCodigo = '';
        $controladorCodigo .= Criacao::linha('.' . $grupoModulo . $nomeModulo . '{', 0, 1);
        $controladorCodigo .= Criacao::linha('', 0, 1);
        $controladorCodigo .= Criacao::linha('}', 0, 1);
        $arquivo = str_replace('\\', '/', $diretorio) . $grupoModulo . $nomeModulo . '.css';
        file_put_contents($arquivo, $controladorCodigo);
        @chmod($arquivo, 0777);
    }

    public function CriarPadrao($grupoModulo, $nomeModulo, $diretorio)
    {
        $controladorCodigo = '';
        $controladorCodigo .= Criacao::linha('<?php', 0, 1);
        $controladorCodigo .= Criacao::linha('echo "' . $grupoModulo . $nomeModulo . '";', 0, 1);
        $arquivo = str_replace('\\', '/', $diretorio) . $grupoModulo . $nomeModulo . EXT;
        file_put_contents($arquivo, $controladorCodigo);
        @chmod($arquivo, 0777);
    }

    public function CriarGlobal($grupoModulo, $nomeModulo, $diretorio)
    {
        $controladorCodigo = '';
        $controladorCodigo .= Criacao::linha('index', 0, 1);
        $arquivo = str_replace('\\', '/', $diretorio) . $grupoModulo . $nomeModulo . '.html';
        file_put_contents($arquivo, $controladorCodigo);
        @chmod($arquivo, 0777);
    }

    /**
     * @param $grupoModulo
     * @param $nomeDoModulo
     * @param $diretorio
     */
    private function CriarControlador($grupoModulo, $nomeDoModulo, $diretorio)
    {
        $namespace = str_replace('src\\', '', str_replace('/', '\\', substr($diretorio, 0, -1)));
        $controladorCodigo = '';
        $controladorCodigo .= Criacao::linha('<?php', 0, 1);
        $controladorCodigo .= Criacao::linha('namespace ' . $namespace . ';', 0, 2);
        $controladorCodigo .= Criacao::linha('use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;', 0, 1);
        $controladorCodigo .= Criacao::linha('use Modulos\\' . $grupoModulo . '\\' . $nomeDoModulo . '\\Models\Repositorios\\' . $grupoModulo . $nomeDoModulo . 'RP;', 0, 2);
        $controladorCodigo .= Criacao::linha('class Index extends ControladorPadraoAbsHelp{', 0, 2);
        $controladorCodigo .= Criacao::linha('public $admin = false;', 1, 2);
        $controladorCodigo .= Criacao::linha('public function index($view = \'index\', $data = array(), $admin = false){', 1, 1);
        $controladorCodigo .= Criacao::linha('echo ' . $grupoModulo . $nomeDoModulo . 'RP::getIndexPage($view, $data, $admin);', 2, 1);
        $controladorCodigo .= Criacao::linha('}', 1, 1);
        $controladorCodigo .= Criacao::linha('}', 0, 0);

        $arquivo = str_replace('\\', '/', $diretorio) . 'Index' . EXT;

        file_put_contents($arquivo, $controladorCodigo);
        @chmod($arquivo, 0777);
    }

    public function CriarRepositorio($grupoModulo, $nomeModulo, $diretorio)
    {
        $namespace = str_replace('src\\', '', str_replace('/', '\\', substr($diretorio, 0, -1)));
        $controladorCodigo = '';
        $controladorCodigo .= Criacao::linha('<?php', 0, 1);
        $controladorCodigo .= Criacao::linha('namespace ' . $namespace . ';', 0, 2);
        $controladorCodigo .= Criacao::linha('use ModuloHelp\PadraoCriacao\RepositorioPadrao\Abstracts\RepositorioPadraoAbsHelp;', 0, 2);
        $controladorCodigo .= Criacao::linha('class ' . $this->classeName . 'RP extends RepositorioPadraoAbsHelp{', 0, 2);
        $controladorCodigo .= Criacao::linha('const GRUPO_MODULO = \'' . $grupoModulo . '\';', 1, 1);
        $controladorCodigo .= Criacao::linha('const NOME_MODULO = \'' . $nomeModulo . '\';', 1, 2);
        $controladorCodigo .= Criacao::linha('public static function getUrlModulo($controlador = \'Index\')', 1, 1);
        $controladorCodigo .= Criacao::linha('{', 1, 1);
        $controladorCodigo .= Criacao::linha('return \'Modulo/\'.self::GRUPO_MODULO.\'/\'.self::NOME_MODULO.\'/\'.$controlador; ', 2, 1);
        $controladorCodigo .= Criacao::linha('}', 1, 2);

        $controladorCodigo .= Criacao::linha('public static function getIndexPage($view = \'index\', $data = array(), $admin = false)', 1, 1);
        $controladorCodigo .= Criacao::linha('{', 1, 1);
        $controladorCodigo .= Criacao::linha('$repositorio = new ' . $this->classeName . 'RP(); ', 2, 1);
        $controladorCodigo .= Criacao::linha('return $repositorio->getModulo($view, $data, $admin); ', 2, 1);
        $controladorCodigo .= Criacao::linha('}', 1, 2);

        $controladorCodigo .= Criacao::linha('public function getModulo($view = \'index\', $data = array(), $admin = false)', 1, 1);
        $controladorCodigo .= Criacao::linha('{', 1, 1);
        $controladorCodigo .= Criacao::linha('return parent::getModulo($view, $data, $admin); // TODO: Change the autogenerated stub', 2, 1);
        $controladorCodigo .= Criacao::linha('}', 1, 1);

        $controladorCodigo .= Criacao::linha('}', 0, 2);

        $arquivo = str_replace('\\', '/', $diretorio) . $this->classeName . 'RP' . EXT;

        file_put_contents($arquivo, $controladorCodigo);
        @chmod($arquivo, 0777);
    }


    public function CriarRegrasDeNegocio($grupoModulo, $nomeModulo, $diretorio)
    {
        $namespace = str_replace('src\\', '', str_replace('/', '\\', substr($diretorio, 0, -1)));
        $controladorCodigo = '';
        $controladorCodigo .= Criacao::linha('<?php', 0, 1);
        $controladorCodigo .= Criacao::linha('namespace ' . $namespace . ';', 0, 1);
        $controladorCodigo .= Criacao::linha('use ModuloHelp\PadraoCriacao\ModelRegraDeNegocio\Abstracts\ModelRegraDeNegocioPadraoAbsHelp;', 0, 2);
        $controladorCodigo .= Criacao::linha('class ' . $this->classeName . 'RN extends ModelRegraDeNegocioPadraoAbsHelp{', 0, 2);
        $controladorCodigo .= Criacao::linha("public function getModulo(" . '$viewName' . "=" . '"index"' . "," . '$data = array(), $admin = false, $minifyHtml = true, $viewDeAtualizacao = false){', 1, 1);
        $controladorCodigo .= Criacao::linha('return parent::getModulo($viewName, $data, $admin, $minifyHtml, $viewDeAtualizacao); // TODO: Change the autogenerated stub', 2, 1);
        $controladorCodigo .= Criacao::linha('}', 1, 1);
        $controladorCodigo .= Criacao::linha('}', 0, 1);

        $arquivo = str_replace('\\', '/', $diretorio) . $this->classeName . 'RN' . EXT;

        file_put_contents($arquivo, $controladorCodigo);
        @chmod($arquivo, 0777);
    }

    public function CriarGadget($grupoModulo, $nomeModulo, $diretorio)
    {
        $namespace = str_replace('src\\', '', str_replace('/', '\\', substr($diretorio, 0, -1)));
        $controladorCodigo = '';
        $controladorCodigo .= Criacao::linha('<?php', 0, 1);
        $controladorCodigo .= Criacao::linha('namespace ' . $namespace . ';', 0, 1);
        $controladorCodigo .= Criacao::linha('use ModuloHelp\PadraoCriacao\ModelGadgets\Abstracts\ModelGadgetsPadraoAbsHelp;', 0, 2);
        $controladorCodigo .= Criacao::linha('class ' . $this->classeName . 'Gadget extends ModelGadgetsPadraoAbsHelp{', 0, 2);

        $controladorCodigo .= Criacao::linha('public $enable_gadget = false;', 1, 1);
        $controladorCodigo .= Criacao::linha('public $ecritorios = array();', 1, 2);

        $controladorCodigo .= Criacao::linha("public function __construct(){", 1, 1);
        $controladorCodigo .= Criacao::linha('parent::__construct();', 2, 1);
        $controladorCodigo .= Criacao::linha('$this->enable_gadget = false;', 2, 1);
        $controladorCodigo .= Criacao::linha('}', 1, 2);

        $controladorCodigo .= Criacao::linha("public function getGadget(" . '$viewName' . "=" . '"index"' . "," . '$data = array(), $admin = true){', 1, 1);
        $controladorCodigo .= Criacao::linha('return parent::getGadget($viewName, $data, $admin); // TODO: Change the autogenerated stub', 2, 1);
        $controladorCodigo .= Criacao::linha('}', 1, 1);
        $controladorCodigo .= Criacao::linha('}', 0, 1);


        $arquivo = str_replace('\\', '/', $diretorio) . $this->classeName . 'Gadget' . EXT;

        file_put_contents($arquivo, $controladorCodigo);
        @chmod($arquivo, 0777);
    }


}