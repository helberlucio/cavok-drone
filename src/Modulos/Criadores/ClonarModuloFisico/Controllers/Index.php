<?php

namespace Modulos\Criadores\ClonarModuloFisico\Controllers;

use Core\Controlador\Controlador;
use Modulos\Criadores\ClonarModuloFisico\Models\RegrasDeNegocio\CriadoresClonarModuloFisicoRN;
use Modulos\Criadores\ClonarModuloFisico\Models\Repositorios\CriadoresClonarModuloFisicoRP;

class Index extends Controlador
{

    public function index()
    {

        $args = func_get_args();
        if(empty($args) || (count($args) < 4)){
            die('É necessário ter no mínimo 4 argumento, Grupo do modulo para clonar 1, Modulo Para Clonar 2, Grupo Modulo Para Criar 3, Modulo para criar 4. Os argumentos dados foram '.implode(', ', $args));
        }
        (new CriadoresClonarModuloFisicoRN())->clonarModulo(
            $args[0],
            $args[1],
            $args[2],
            $args[3]
        );
    }

}