<?php

namespace Modulos\Criadores\ClonarModuloFisico\Models\RegrasDeNegocio;

use Aplicacao\Diretorios;
use ModuloHelp\PadraoCriacao\ModelRegraDeNegocio\Abstracts\ModelRegraDeNegocioPadraoAbsHelp;

class CriadoresClonarModuloFisicoRN extends ModelRegraDeNegocioPadraoAbsHelp
{

    private $diretorioDoModulo;
    private $grupoDoModulo;
    private $nomeDoModulo;
    private $novoGrupoDeModulo;
    private $novoNomeDoModulo;

    private $arquivoAtualDaLeitura;
    private $diretorioAtualDaLeitura;

    public function clonarModulo($grupoDoModulo, $nomeDoModulo, $novoGrupoDeModulo, $novoNomeDoModulo)
    {
        $this->grupoDoModulo = $grupoDoModulo;
        $this->nomeDoModulo = $nomeDoModulo;
        $this->novoGrupoDeModulo = $novoGrupoDeModulo;
        $this->novoNomeDoModulo = $novoNomeDoModulo;

        $diretorioDoModulo = 'src/Modulos/' . $grupoDoModulo . '/' . $nomeDoModulo;
        $this->diretorioDoModulo = $diretorioDoModulo;

        echo '<pre>';
        if (!is_dir($diretorioDoModulo)) {
            die('Diretorio não encontrado!');
        }
        $this->diretorioAtualDaLeitura = $this->diretorioDoModulo;
        $listagemsDePastasEArquivos = Diretorios::Escaniar($diretorioDoModulo);
        $this->percorrerDiretorioEmBuscaDeArquivo($listagemsDePastasEArquivos);
    }

    private function percorrerDiretorioEmBuscaDeArquivo($iteratorDiretorio)
    {
        foreach ($iteratorDiretorio as $key => $pastaArquivo) {
            if ($pastaArquivo['type'] == 'file') {
                $this->recreateFileOnNewModule($pastaArquivo);
            }
            if ($pastaArquivo['type'] == 'dir') {
                $this->percorrerDiretorioEmBuscaDeArquivo($pastaArquivo['subFiles']);
            }
        }
    }

    private function recreateFileOnNewModule($pastaArquivo)
    {
        echo '<hr>';
        $diretorioDoArquivo = substr(str_replace(realpath('./'), '', $pastaArquivo['diretorio']), 1);
        $novoDiretorioDoArquivo = str_replace($this->nomeDoModulo, $this->novoNomeDoModulo, str_replace($this->grupoDoModulo, $this->novoGrupoDeModulo, $diretorioDoArquivo));

        $arquivo = $diretorioDoArquivo . '\\' . $pastaArquivo['baseName'];
        $novoArquivo = $pastaArquivo['baseName'];
        if (!is_dir($novoDiretorioDoArquivo)) {
            mkdir($novoDiretorioDoArquivo, 777, true);
        }
        if (!is_file($novoArquivo)) {
            $contentArquivo = file_get_contents($arquivo);
            $novoArquivo = $novoDiretorioDoArquivo.'\\'.$this->replaceKeyWords($pastaArquivo['baseName'], true);
            file_put_contents($novoArquivo, $contentArquivo, 0777);
        }
        if (!is_file($novoArquivo)) {
            die('Não foi possivel criar arquivo: ' . $novoArquivo);
        }
        echo '<hr>';
        $contagemTotalDeLinhas = 0;
        $file = file($novoArquivo);
        $linhasDeProgramacao = '';

        foreach ($file as $lineRead => $contentLine) {
            $contagemTotalDeLinhas++;
            if ($contagemTotalDeLinhas == 1 && ($pastaArquivo['extension'] == 'php')) {
                $linhasDeProgramacao .= $contentLine;
                continue;
            }
            $replace = $this->replaceKeyWords($contentLine);
            $linhasDeProgramacao .= $replace;
        }

        print_r($arquivo);
        print_r(str_replace('<?php', '', $linhasDeProgramacao));
        $file_handle = fopen($novoArquivo, 'w');
        fwrite($file_handle, $linhasDeProgramacao);
        fclose($file_handle);
    }

    private function replaceKeyWords($content = '', $isFileName = false)
    {
        $replaces = array(
            LI_DIR_MODULOS . $this->grupoDoModulo . '/' . $this->nomeDoModulo => LI_DIR_MODULOS . $this->novoGrupoDeModulo . '/' . $this->novoNomeDoModulo,
            'Modulos\\' . $this->grupoDoModulo . '\\' . $this->nomeDoModulo => 'Modulos\\' . $this->novoGrupoDeModulo . '\\' . $this->novoNomeDoModulo,
            'namespace Modulos\\' . $this->grupoDoModulo . '\\' . $this->nomeDoModulo => 'namespace Modulos\\' . $this->novoGrupoDeModulo . '\\' . $this->novoNomeDoModulo,
            $this->grupoDoModulo . '/' . $this->nomeDoModulo => $this->novoGrupoDeModulo . '/' . $this->novoNomeDoModulo,
            "'" . $this->grupoDoModulo . "'" => "'" . $this->novoGrupoDeModulo . "'",
            '"' . $this->grupoDoModulo . "'" => '"' . $this->novoGrupoDeModulo . '"',
            "'" . $this->nomeDoModulo . "'" => "'" . $this->novoNomeDoModulo . "'",
            '"' . $this->nomeDoModulo . '"' => '"' . $this->novoNomeDoModulo . '"',
            "'" . $this->grupoDoModulo . $this->nomeDoModulo . "'" => "'" . $this->novoGrupoDeModulo . $this->novoNomeDoModulo . "'",
            $this->grupoDoModulo . $this->nomeDoModulo => $this->novoGrupoDeModulo . $this->novoNomeDoModulo,
            'ModulosAdministraveis\\Models\Entidades\\' . $this->novoGrupoDeModulo . $this->novoNomeDoModulo . '[]' => 'ModulosAdministraveis\\Models\Entidades\\' . $this->grupoDoModulo . $this->nomeDoModulo . '[]'
        );
        $replacesContentFile = array();
        if (!$isFileName) {
            $replacesContentFile = array(
                $this->grupoDoModulo => '#' . $this->grupoDoModulo . '#',
                $this->nomeDoModulo => '#' . $this->nomeDoModulo . '#',
                "\\Modulos\\#".$this->grupoDoModulo."#\\"=> "\\Modulos\\".$this->novoGrupoDeModulo."\\",
                "Repositorios\\#".$this->grupoDoModulo."#" => "Repositorios\\".$this->novoGrupoDeModulo,
                '#' . $this->grupoDoModulo . '#' => $this->grupoDoModulo,
                '#' . $this->nomeDoModulo . '#'=>$this->nomeDoModulo
            );
        }
        $replaces = array_merge($replaces, $replacesContentFile);
        foreach ($replaces as $replace => $subject) {
            $content = str_replace($replace, $subject, $content);
        }
        return $content;
    }

}
