<?php

namespace Modulos\AdminAutoDesenvolve\Escritorios\Controllers;

use Aplicacao\Url;
use Core\Controlador\Controlador;
use Modulos\AdminAutoDesenvolve\Escritorios\Models\Repositorios\AdminAutoDesenvolveEscritoriosRP;

class Index extends Controlador
{

    public function index()
    {
        AdminAutoDesenvolveEscritoriosRP::setEscritorioAtual();
        header('Location:' . Url::Admin());
    }

    public function sairDoEscritorio()
    {
        AdminAutoDesenvolveEscritoriosRP::resetarEscritorioAtual();
        header('Location:' . Url::Admin());
    }
}