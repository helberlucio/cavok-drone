<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 15/12/2017
 * Time: 14:31
 */

namespace Modulos\AdminAutoDesenvolve\Escritorios\Models\Constantes;


class Escritorios
{
    const emissao_de_notas_fiscais  = 1;
    const compras  = 2;
    const vendas  = 3;
    const frente_de_caixa  = 4;
    const fluxo_de_caixa  = 5;
    const estoque  = 6;
    const recurssos_humanos  = 7;
    const cobranca  = 8;
    const contabilidade  = 9;
    const gerenciador_site  = 10;
    const tarefas  = 11;



    const configuracoes_gerais  = 10;
    const gerenciar_produtos  = 101;
    const gerenciar_balanco_de_estoque  = 102;
    const estatisticas_da_loja  = 103;



}