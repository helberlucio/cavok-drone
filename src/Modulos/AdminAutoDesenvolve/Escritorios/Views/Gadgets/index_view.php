<?php
/**
 * @var $escritorios \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysEscritoriosEntidade[]
 */
$escritoriosRelacionadosUsuario = sessao('admin_escritorios');

\Aplicacao\Ferramentas\GetAssets::CSS('AdminAutoDesenvolveEscritorios', 'AdminAutoDesenvolve', 'Escritorios');
?>
    <div class="container-fluid AdminAutoDesenvolveEscritorios-fluid AdminAutoDesenvolveEscritorios">
        <div class="row">
            <div class="col-md-12" style="padding: 0 10px;">

                <section class="panel page-search-form">
                    <header class="panel-heading">
                        <h2 class="panel-title">Escolha o departamento</h2>
                    </header>
                </section>
            </div>
            <?php
            foreach ($escritorios as $escritorio) {
                ?>
                <div class="col-md-4" style="padding:0 10px;cursor: pointer;">
                    <section class="panel panel-featured-left panel-featured-success"
                             style="border-color:<?php echo $escritorio->getCor(); ?>">
                        <div class="panel-body">
                            <div class="AdminAutoDesenvolveEscritorios-ferramentas">
                                <div class="col-md-3" style="padding: 0;">
                                    <div class="col-md-1" style="padding: 0;color:#CCC;cursor: pointer;text-align: right;">
                                        <i class="fa fa-cog" aria-hidden="true" onclick="editarConfiguracoes('<?php echo $escritorio->getId(); ?>','<?php echo $escritorio->getCor(); ?>', '<?php echo $escritorio->getOrder(); ?>', '<?php echo $escritorio->getTitulo(); ?>');"></i>
                                    </div>
                                    <div class="col-md-1" style="text-align: left;">
                                        <?php
                                        if (isset($escritoriosRelacionadosUsuario[$escritorio->getId()])) {
                                            ?>
                                            <i style="color:#47a447; font-size: 15px;" class="fa fa-unlock" aria-hidden="true"></i>
                                            <?php
                                        } else {
                                            ?>
                                            <i style="color:#d2322d;font-size: 15px;" class="fa fa-lock" aria-hidden="true"></i>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>

                            </div>

                            <div
                                    onclick="goToNiceLink('<?php echo \Aplicacao\Url::Base() . \Aplicacao\Url::Apelido('Admin/' . \Modulos\AdminAutoDesenvolve\Escritorios\Models\Repositorios\AdminAutoDesenvolveEscritoriosRP::getUrlModulo('Index/escolherEscritorio/' . $escritorio->getId()), 'admin/escolher-escritorio/' . \Aplicacao\Url::Title($escritorio->getTitulo())); ?>')"
                                    class="widget-summary widget-summary-md">
                                <div class="col-md-12"
                                     style="display: flex;align-items: center;justify-content: center;height: 70px;">
                                    <div class="summary-icon bg-success"
                                         style="background-color: transparent !important;
                                                 box-shadow: 0px 0px 10px <?php echo $escritorio->getCor(); ?>;
                                                 margin:0 !important;color:<?php echo $escritorio->getCor(); ?>">
                                        <?php echo $escritorio->getIcone(); ?>
                                    </div>
                                </div>
                                <div class="col-md-12" style="text-align: center !important;padding:20px 0 !important;">
                                    <div class="summary">
                                        <strong class="amount">
                                            <?php echo $escritorio->getTitulo(); ?>
                                        </strong>
                                        <div class="info">
                                            <h4 class="title">
                                                Acessar departamento de <?php echo $escritorio->getTitulo(); ?>
                                                <i class="fa fa-external-link-square" aria-hidden="true"></i>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <?php
            }
            ?>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="escritorio_configuracao_modal" role="dialog"
         aria-labelledby="Configuração de departamento"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="exampleModalLabel">Editar cor e ordenação do departamento de:
                        <span class="departamento-title-modal"></span>
                    </h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="text" value="" name="id" class="modal_escritorios_hidden_id" hidden="">
                        <div class="col-md-12 form-group">
                            <label>
                                <b>Escolha a cor e confirme</b>
                            </label>
                            <br>
                            <!-- ######### COLOR PICKER ############## -->
                            <input type='text' id="full"/>
                            <input type="text" hidden class="colorPicked">
                            <!-- ######### COLOR PICKER ############## -->
                        </div>
                        <div class="col-md-12 form-group">
                            <label>
                                Ordem ascendente:
                            </label>
                            <input type="number" name="order" class="order form-control"
                                   placeholder="EX: 1(Primeiro), 2(Segundo), 3(terceiro)">
                        </div>
                        <div class="col-md-12 callbackplace_escritorios-popup" style="padding: 0;"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" onclick="salvarConfiguracao();">Salvar configuração
                    </button>
                </div>
            </div>
        </div>
    </div>
    <script>
        var token = '<?php echo sessao('admin')->getToken();?>';
    </script>
<?php
\Aplicacao\Ferramentas\GetAssets::JS('gadgetEscolhaDeEscritorios', 'AdminAutoDesenvolve', 'Escritorios');