<?php

namespace Modulos\AdminAutoDesenvolve\Gadgets;

use ModuloHelp\PadraoCriacao\ModelGadgets\Abstracts\ModelGadgetsPadraoAbsHelp;
use Modulos\AdminAutoDesenvolve\Escritorios\Models\Gadgets\AdminAutoDesenvolveEscritoriosGadget;
use Modulos\AdminAutoDesenvolve\Escritorios\Models\Repositorios\AdminAutoDesenvolveEscritoriosRP;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysEscritoriosEntidade;

class AdminAutoDesenvolveGadgets
{
    /**
     * @var ModelGadgetsPadraoAbsHelp[]
     */
    private $arquivos = array();

    private $escritorio_id;

    public function __construct()
    {
        $this->setGadgetsFiles();
    }

    /**
     * @return ModelGadgetsPadraoAbsHelp[]
     */
    public function getGadgetsFiles()
    {
        ksort($this->arquivos);
        return $this->arquivos;
    }

    private function setGadgetsFiles($caminho = 'src/Modulos')
    {
        $this->escritorio_id = 0;
        if (!isset($_SESSION['admin_escritorio_atual']) || !$_SESSION['admin_escritorio_atual']) {
            $this->arquivos = array((new AdminAutoDesenvolveEscritoriosGadget()));
        }
        if (isset($_SESSION['admin_escritorio_atual']) && ($_SESSION['admin_escritorio_atual'] instanceof SysEscritoriosEntidade)) {
            $escritoriosRP = (AdminAutoDesenvolveEscritoriosRP::getEscritorioAtual());
            $this->escritorio_id = $escritoriosRP->getId();
        }
        $diretorios = \Aplicacao\Diretorios::Escaniar($caminho);
        foreach ($diretorios as $key => $path) {
            $diretorioAtual = $caminho;
            if (!isset($path['type']) && $path['type'] == 'dir') {
                continue;
            }
            if (isset($path['type']) && $path['type'] == 'dir') {
                if (isset($path['baseName'])) {
                    $diretorioAtual = $caminho . '/' . $path['baseName'];
                    if (!($path['baseName'] === 'Gadgets')) {
                        if (isset($path['subFiles'])) {
                            $this->setGadgetsFiles($diretorioAtual);
                        }
                    }
                    if ($path['baseName'] == 'Gadgets') {
                        $this->setGadgetsFiles($diretorioAtual);
                        $this->getGadgetsClassByGadgetsModule($diretorioAtual);
                    }
                }
            }
        }
    }

    private function getGadgetsClassByGadgetsModule($diretorioGadgets)
    {
        $arquivos = \Aplicacao\Diretorios::Escaniar($diretorioGadgets);
        foreach ($arquivos as $arquivo) {
            if ($arquivo['type'] == 'file' &&
                isset($arquivo['baseName']) &&
                isset($arquivo['extension']) &&
                $arquivo['extension'] == 'php'
            ) {
                $arquivoPhp = $diretorioGadgets . '/' .
                    substr($arquivo['baseName'], 0, -strlen('.' . $arquivo['extension']));
                $classeDir = str_replace('/', '\\', str_replace('src/', '', $arquivoPhp));
                $isGadget = substr($classeDir, strlen($classeDir) - strlen('Gadget'));
                if ($isGadget !== 'Gadget') {
                    continue;
                }
                if (!(class_exists($classeDir))) {
                    continue;
                }
                $instancia = new $classeDir();
                if (!($instancia instanceof \ModuloHelp\PadraoCriacao\ModelGadgets\Abstracts\ModelGadgetsPadraoAbsHelp)) {
                    continue;
                }
                if (!$instancia->enable_gadget) {
                    continue;
                }
                if (empty($instancia->ecritorios)) {
                    continue;
                }
                if (!isset($instancia->ecritorios[$this->escritorio_id])) {
                    continue;
                }
                if ($instancia instanceof AdminAutoDesenvolveEscritoriosGadget) {
                    continue;
                }

                $order = $this->getOrdemCorreta($instancia->order);
                $this->arquivos[$order] = $instancia;
            }
        }
    }

    /**
     * @param $order
     * @return int
     */
    private function getOrdemCorreta($order)
    {
        if (!isset($this->arquivos[$order])) {
            return $order;
        }
        $this->getOrdemCorreta(($order + 1));
    }
}