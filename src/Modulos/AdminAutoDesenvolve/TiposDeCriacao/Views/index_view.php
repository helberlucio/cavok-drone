<?php
\Aplicacao\Ferramentas\GetAssets::CSS('AdminAutoDesenvolveTiposDeCriacao', 'AdminAutoDesenvolve', 'TiposDeCriacao');
\Aplicacao\Ferramentas\GetAssets::JS('AdminAutoDesenvolveTiposDeCriacao', 'AdminAutoDesenvolve', 'TiposDeCriacao');
?>
<div class="col-md-12 AdminAutoDesenvolveTiposDeCriacao">
    <!-- BOX 3 -->
    <div class="col-md-4 escolha-de-itens box1">
        <div class="col-md-12 icone-area">
            <img src="<?php echo \Aplicacao\Url::Base().\Aplicacao\Ferramentas\GetAssets::IMAGE('escolhaDeCriacao/website.png', 'AdminAutoDesenvolve', 'TiposDeCriacao'); ?>">
        </div>
        <div class="col-md-12 button-area">
            <a href="<?php echo \Aplicacao\Url::Admin('Modulo/Criadores/Aplicacao/Website/?page_name=Criador de Website');?>">
                <button>CRIAR WEBSITE</button>
            </a>
        </div>
    </div>
    <!-- BOX 2 -->
    <div class="col-md-4 escolha-de-itens box2">
        <div class="col-md-12 icone-area">
            <img src="<?php echo \Aplicacao\Url::Base().\Aplicacao\Ferramentas\GetAssets::IMAGE('escolhaDeCriacao/loja.png', 'AdminAutoDesenvolve', 'TiposDeCriacao'); ?>">
        </div>
        <div class="col-md-12 button-area">
            <a href="<?php echo \Aplicacao\Url::Admin('Modulo/Criadores/Aplicacao/Loja/?page_name=Criador de Loja');?>">
                <button>CRIAR LOJA</button>
            </a>
        </div>
    </div>
    <!-- BOX 3 -->
    <div class="col-md-4 escolha-de-itens box3">
        <div class="col-md-12 icone-area">
            <img src="<?php echo \Aplicacao\Url::Base().\Aplicacao\Ferramentas\GetAssets::IMAGE('escolhaDeCriacao/blog.png', 'AdminAutoDesenvolve', 'TiposDeCriacao'); ?>">
        </div>
        <div class="col-md-12 button-area">
            <a href="<?php echo \Aplicacao\Url::Admin('Modulo/Criadores/Aplicacao/Blog/?page_name=Criador de Blog');?>">
                <button>CRIAR BLOG</button>
            </a>
        </div>
    </div>
</div>
