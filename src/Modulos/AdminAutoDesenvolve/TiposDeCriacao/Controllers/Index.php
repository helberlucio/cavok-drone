<?php
namespace Modulos\AdminAutoDesenvolve\TiposDeCriacao\Controllers;

use Core\Controlador\Controlador;
use Modulos\AdminAutoDesenvolve\TiposDeCriacao\Models\Repositorios\AdminAutoDesenvolveTiposDeCriacaoRP;

class Index extends Controlador{

    public function index($view = 'index', $data = array(), $admin = false){
        echo AdminAutoDesenvolveTiposDeCriacaoRP::getIndexPage($view, $data, $admin);
    }
}