<?php
namespace Modulos\AdminAutoDesenvolve\BreadCrumb\Controllers;

use Core\Controlador\Controlador;
use Modulos\AdminAutoDesenvolve\BreadCrumb\Models\Repositorios\AdminAutoDesenvolveBreadCrumbRP;

class Index extends Controlador{

    public function index($view = 'index', $data = array(), $admin = false){
        echo AdminAutoDesenvolveBreadCrumbRP::getIndexPage($view, $data, $admin);
    }
}