<?php
/**
 * Created by PhpStorm.
 * User: Gabriel PHP
 * Date: 27/05/2017
 * Time: 14:06
 */

namespace Modulos\AdminAutoDesenvolve\BreadCrumb\Models\RegrasDeNegocio;


use Aplicacao\Url;
use Modulos\AdminAutoDesenvolve\Escritorios\Models\Repositorios\AdminAutoDesenvolveEscritoriosRP;

class BreadCrumbGenarator
{

    public function getBreadCrumb()
    {
        $this->startBreadCrumb();
        $this->setBreadCrumb();
        return $this->getListaHtmlBreadCrumb();
    }

    private function startBreadCrumb()
    {
        if (!isset($_SESSION['autodesenvolve_admin_breadCrumb']) || !is_array($_SESSION['autodesenvolve_admin_breadCrumb'])) {
            $_SESSION['autodesenvolve_admin_breadCrumb'] = array();
        }
    }

    /**
     *
     */
    private function setBreadCrumb()
    {
        if (!input_get('page_name')) {
            return;
        }
        $nameBreadCrumb = input_get('page_name');
        if(!($nameBreadCrumb != '')){
            return;
        }
        $icon = input_get('icon') ? input_get('icon') : '<i class="fa fa-circle-o-notch" aria-hidden="true"></i>';
        $jaExistePaginaAcessadaEmBreadCrumb = $this->verificarSeJaExiste($nameBreadCrumb);
        if ($jaExistePaginaAcessadaEmBreadCrumb) {
            $this->unsetBreadCrumbExistente($nameBreadCrumb);
        }
        $this->quantidadeMaximaDeBreadCrumb(4);
        $_SESSION['autodesenvolve_admin_breadCrumb'][] = array('icon'=>$icon, 'name' => $nameBreadCrumb, 'url' => Url::Current());
    }

    /**
     * @param $breadCrumbName
     * @return bool
     */
    private function verificarSeJaExiste($breadCrumbName)
    {
        foreach ($_SESSION['autodesenvolve_admin_breadCrumb'] as $breadCrumb) {
            if (!isset($breadCrumb['name'])) {
                continue;
            }
            if ($breadCrumb['name'] == $breadCrumbName) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $breadCrumbName
     */
    private function unsetBreadCrumbExistente($breadCrumbName)
    {
        foreach ($_SESSION['autodesenvolve_admin_breadCrumb'] as $ky => $breadCrumb) {
            if (!isset($breadCrumb['name'])) {
                continue;
            }
            if ($breadCrumb['name'] == $breadCrumbName) {
                unset($_SESSION['autodesenvolve_admin_breadCrumb'][$ky]);
            }
        }
    }

    /**
     * @param int $maximoDeBreadCrumb
     */
    private function quantidadeMaximaDeBreadCrumb($maximoDeBreadCrumb = 2)
    {
        if (!(count($_SESSION['autodesenvolve_admin_breadCrumb']) >= $maximoDeBreadCrumb)) {
            return;
        }
        foreach ($_SESSION['autodesenvolve_admin_breadCrumb'] as $ky => $breadCrumb) {
            unset($_SESSION['autodesenvolve_admin_breadCrumb'][$ky]);
            return;
        }
    }

    /**
     * @return string
     */
    private function getListaHtmlBreadCrumb()
    {
    	$escritoriosRP = AdminAutoDesenvolveEscritoriosRP::getEscritorioAtual();
        if(!$escritoriosRP->getId()){
            return '';
        }
        if(empty($_SESSION['autodesenvolve_admin_breadCrumb'])){
            $_SESSION['autodesenvolve_admin_breadCrumb'][] = array('icon'=>'<i class="fa fa-home"></i>', 'name'=>'Home', 'url'=>Url::Admin());
        }
        $html = '<ul>';
        foreach ($_SESSION['autodesenvolve_admin_breadCrumb'] as $ky => $breadCrumb) {
            if(!isset($breadCrumb['name']) || !isset($breadCrumb['url']) || !isset($breadCrumb['icon'])){
                continue;
            }
            $html .= '<li>';
            $html .= '<a href="'.$breadCrumb['url'].'">';
            $html .= $breadCrumb['icon'].' '.$breadCrumb['name'] .' <i class="fa fa-angle-right" aria-hidden="true"></i>';
            $html .= '</a>';
            $html .= '</li>';
        }
        $html .= '</ul>';

        return $html;
    }

}