<?php
/**
 * @var $lista_breadCrumb string
 */
$escritorioAtual = \Modulos\AdminAutoDesenvolve\Escritorios\Models\Repositorios\AdminAutoDesenvolveEscritoriosRP::getEscritorioAtual();
\Aplicacao\Ferramentas\GetAssets::CSS('AdminAutoDesenvolveBreadCrumb', 'AdminAutoDesenvolve', 'BreadCrumb');
?>
<div class="col-md-12 bread-crumb-area gradient-purple-blue">
    <div class="col-md-9" style="padding: 0;">
        <?php echo $lista_breadCrumb; ?>
    </div>
    <?php
    if ($escritorioAtual->getTitulo()) {
        ?>
        <div class="col-md-3 box-troca-departamentos">
            <div class="col-md-9 col-md-offset-3 troca-departamentos"
                 onclick='window.location = "<?php echo \Aplicacao\Url::Admin('Modulo/AdminAutoDesenvolve/Escritorios/Index/sairDoEscritorio'); ?>"'>
                <i class="fa fa-refresh" aria-hidden="true"></i> Trocar de departamento
            </div>
        </div>
        <?php
    }
    ?>
</div>
