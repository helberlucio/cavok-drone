<?php
namespace Modulos\AdminAutoDesenvolve\Footer\Controllers;

use Core\Controlador\Controlador;
use Modulos\AdminAutoDesenvolve\Footer\Models\Repositorios\AdminAutoDesenvolveFooterRP;

class Index extends Controlador{

    public function index($view = 'index', $data = array(), $admin = false){
        echo AdminAutoDesenvolveFooterRP::getIndexPage($view, $data, $admin);
    }
}