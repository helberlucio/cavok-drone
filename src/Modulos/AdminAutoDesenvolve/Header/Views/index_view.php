<?php
/**
 *
 *
 * @var $escritorio \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysEscritoriosEntidade
 * @var $usuarioLogado \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsUsuariosEntidade
 * @var $stiloAdmin \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysDesignAdmin
 * @var $WebsiteGeralEntidade \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\WebsiteGeralEntidade
 */

use Aplicacao\Url;

\Aplicacao\Ferramentas\GetAssets::JS('AdminAutoDesenvolveHeader', 'AdminAutoDesenvolve', 'Header', false);
\Aplicacao\Ferramentas\GetAssets::CSS('AdminAutoDesenvolveHeader', 'AdminAutoDesenvolve', 'Header');
?>
<div id="header-autodesenvolve" class="header-autodesenvolve container-fluid">
    <div id="logo-header-autodesenvolve" class="logo-header-autodesenvolve col-md-3">
        <a href="<?php echo Url::Admin(); ?>">
            <img src="<?php echo Url::Base($WebsiteGeralEntidade->getLogo()) ?>"
                 style="max-height: 80%;height: 75px;margin-top:8px;">
        </a>
    </div>
    <?php
    if ($escritorio->getId()) {
        ?>
        <div class="col-md-3 autodesenvolve-escritorio">
            <?php
            echo $escritorio->getIcone() . ' &nbsp; ' . $escritorio->getTitulo();
            ?>
        </div>
        <?php
    }
    ?>
    <div class="col-md-2 pull-right informacoes-usuario-header-autodesenvolve-area">
        <div class="col-md-12 informacoes-usuario-header-autodesenvolve">
            <ul>
                <li class="area-informacoes-usuario" data-open="false"
                    style="height: 58px;justify-content: center;align-items: center;display: flex;">
                    <div class="col-md-3 foto-area-header-autodesenvolve">
                        <i class="fa fa-user" aria-hidden="true" style="font-size: 16px;"></i>
<!--                        <img class="foto-header-autodesenvolve"-->
<!--                             src="--><?php //echo Url::Base(\Aplicacao\Imagem::resize($usuarioLogado->getImagem(), 50, 50, '#a2a2a2', 'jpg')); ?><!--">-->
                    </div>
                    <div class="col-md-5 nome-usuario-header-autodesenvolve">
                        <?php echo $usuarioLogado->getNome(); ?>
                    </div>
                    <div class="col-md-1 nome-usuario-header-autodesenvolve arrow-status-info-usuario">
                        <img
                                data-arrow-open="<?php echo Url::Base(\Aplicacao\Ferramentas\GetAssets::IMAGE('arrow-down.png', 'AdminAutoDesenvolve', 'Header')); ?>"
                                data-arrow-close="<?php echo Url::Base(\Aplicacao\Ferramentas\GetAssets::IMAGE('arrow-right.png', 'AdminAutoDesenvolve', 'Header')); ?>"
                                src="<?php echo Url::Base(\Aplicacao\Ferramentas\GetAssets::IMAGE('arrow-right.png', 'AdminAutoDesenvolve', 'Header')); ?>">
                    </div>
                    <ul>
                        <li>
                            <a href="<?php echo Url::Admin('Modulo/AdminAutoDesenvolve/MeuPerfil/Index/'); ?>">
                                <div class="col-md-1 usuario-informacoes-icone-area">
                                    <i style="font-size: 16px;" class="fa fa-user" aria-hidden="true"></i>
                                </div>
                                <div class="col-md-11 usuario-informacoes-descricao-area">
                                    Meu Perfil
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo Url::Admin('Modulo/AdminAutoDesenvolve/SugestoesMelhorias/Index/'); ?>">
                                <div class="col-md-1 usuario-informacoes-icone-area">
                                    <i style="font-size: 16px;" class="fa fa-dropbox" aria-hidden="true"></i>
                                </div>
                                <div class="col-md-11 usuario-informacoes-descricao-area">
                                    Sugestões de melhoria
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo Url::Admin('Modulo/AdminAutoDesenvolve/Suporte/Index/'); ?>">
                                <div class="col-md-1 usuario-informacoes-icone-area">
                                    <i style="font-size: 16px;" class="fa fa-wrench" aria-hidden="true"></i>
                                </div>
                                <div class="col-md-11 usuario-informacoes-descricao-area">
                                    Solicitar Suporte
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo Url::Admin('Modulo/AdminAutoDesenvolve/Escritorios/Index/sairDoEscritorio'); ?>">
                                <div class="col-md-1 usuario-informacoes-icone-area">
                                    <i class="fa fa-exchange" aria-hidden="true"></i>
                                </div>
                                <div class="col-md-11 usuario-informacoes-descricao-area">
                                    Trocar de departamento
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo Url::Admin('Login/logout'); ?>">
                                <div class="col-md-1 usuario-informacoes-icone-area">
                                    <i style="font-size: 16px;" class="fa fa-power-off" aria-hidden="true"></i>
                                </div>
                                <div class="col-md-11 usuario-informacoes-descricao-area">
                                    Sair
                                </div>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
