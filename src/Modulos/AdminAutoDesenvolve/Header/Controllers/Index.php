<?php
namespace Modulos\AdminAutoDesenvolve\Header\Controllers;

use Core\Controlador\Controlador;
use Modulos\AdminAutoDesenvolve\Header\Models\Repositorios\AdminAutoDesenvolveHeaderRP;

class Index extends Controlador{

    public function index($view = 'index', $data = array(), $admin = false){
        echo AdminAutoDesenvolveHeaderRP::getIndexPage($view, $data, $admin);
    }
}