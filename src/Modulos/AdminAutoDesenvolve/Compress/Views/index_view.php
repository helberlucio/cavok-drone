<?php
\Aplicacao\Ferramentas\GetAssets::CSS('AdminAutoDesenvolveCompress', 'AdminAutoDesenvolve', 'Compress');
?>
<div class="col-md-12 AdminAutoDesenvolveCompress">
    <div class="row">
        <div class="col-md-12">
            <h1>Compressor de arquivos</h1>
        </div>
        <div class="col-md-12 form-area">
            <div class="col-md-12 callback" style="padding: 0;">

            </div>
            <input type="text" placeholder="Selecionar raiz de compressao" name="selecao_raiz_para_compressao"
                   id="selecao_raiz_para_compressao" class="form-control selecao_raiz_para_compressao">
            <label><input type="checkbox" name="mostrar_raiz" id="mostrar_raiz" value="1">Mostrar Raiz</label><br>
            <button class="btn btn-primary confirm-button" data-showraiz="false">Confirmar</button>
        </div>
    </div>
</div>
<script>
    var dataApi = [
        {id: '0', value: 'Selecionar raiz de compressao'},
        {id: '1', value: 'Todos os arquivos'},
        {id: '2', value: 'Todos os arquivos menos administracao'},
        {id: '3', value: 'Modulo específico'}
    ];
    $(function () {
        adicionarAutoCompleteFieldApi('selecao_raiz_para_compressao', dataApi);
        $('#mostrar_raiz').change(function () {
            var mostrarRaiz = $(this);
            if (mostrarRaiz[0].checked === true) {
                $('.confirm-button').empty().append('Mostrar arquivos da raiz').attr('data-showraiz', 'true');
            } else {
                $('.confirm-button').empty().append('Confirmar').attr('data-showraiz', 'false');
            }
        });
        $('.confirm-button').click(function () {
            var valorSelect = $('input[name="selecao_raiz_para_compressao"]').val();
            var formAreaCallback = $('.form-area .callback');
            if (valorSelect === '0') {
                formAreaCallback.empty().append('<div class="alert alert-info" style="padding:10px;">Primeiro  selecione a raiz de compressão</div>');
                setTimeout(function () {
                    formAreaCallback.empty();
                }, 3500);
                return false;
            }
            if (valorSelect !== '0' && $(this).attr('data-showraiz') === 'true') {
                mostrarRaiz();
            } else {
                executarCompressao();
            }
        });
    });


    function mostrarRaiz() {
        alert('Mostrar raiz');
        $.post(base_url('<?php echo \Modulos\AdminAutoDesenvolve\Compress\Models\Repositorios\AdminAutoDesenvolveCompressRP::getUrlModulo('Index/mostrarRaiz');?>'));
    }

    function executarCompressao() {
        alert('executar compressao');
    }
</script>