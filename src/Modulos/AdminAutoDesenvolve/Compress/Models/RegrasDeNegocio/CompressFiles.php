<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 28/02/2018
 * Time: 12:41
 */

namespace Modulos\AdminAutoDesenvolve\Compress\Models\RegrasDeNegocio;


use Aplicacao\Diretorios;
use MatthiasMullie\Minify;
use Spatie\ImageOptimizer\OptimizerChainFactory;

class CompressFiles
{

    /**
     * @var array
     */
    private $files_notfound = array();

    /**
     * @var array
     */
    private $files_bytes = array();

    /**
     * @var int
     */
    private $total_bytes_save = 0;

    public function getRaiz($path = './src/Modulos/Site/')
    {
        $paths = array(
            './src/Modulos/Site/',
            './src/Assets/'
        );
        ini_set('max_execution_time', 0);
        $yeldScanDir = Diretorios::Escaniar($path);
        foreach ($yeldScanDir as $scan) {
            if ($scan['type'] === 'dir') {
                $this->getRaiz($path . $scan['baseName'] . '/');
            }
            if ($scan['type'] === 'file') {
                if ($scan['extension'] == 'css') {
                    $this->minifyCssFiles($scan['fileDir']);
                }
                if ($scan['extension'] == 'js') {
                    $this->minifyJsFiles($scan['fileDir']);
                }
                if (
                    $scan['extension'] == 'jpg' ||
                    $scan['extension'] == 'JPG' ||
                    $scan['extension'] == 'jpeg' ||
                    $scan['extension'] == 'JPEG' ||
                    $scan['extension'] == 'png' ||
                    $scan['extension'] == 'PNG' ||
                    $scan['extension'] == 'gif' ||
                    $scan['extension'] == 'GIF' ||
                    $scan['extension'] == 'bmp' ||
                    $scan['extension'] == 'BMP'
                ) {
                    $this->minifyImageFiles($scan['fileDir']);
                }
            }
        }
    }

    private function compressFile($file)
    {
        if (!is_file($file)) {
            $this->files_notfound[] = $file;
            return false;
        }

        $totalSizeSize = filesize($file);
        $fileBytes['original'] = $totalSizeSize;

        $content = file_get_contents($file);

        $search = array('/\>[^\S ]+/s', '/[^\S ]+\</s', '/(\s)+/s', '/<!--(.|\s)*?-->/');
        $replace = array('>', '<', '\\1', '');
        $newContent = preg_replace($search, $replace, $content);
        file_put_contents($file, $newContent);

        $totalCompressFileSize = filesize($file);
        $fileBytes['compressed'] = $totalCompressFileSize;
        $this->files_bytes[] = array('file_name' => $file, 'size' => $fileBytes);
        $this->total_bytes_save = $this->total_bytes_save + ($totalSizeSize - $totalCompressFileSize);

        return true;
    }

    private function minifyImageFiles($path)
    {
        if (!is_file($path)) {
            die($path);
        }
        $optimizerChain = OptimizerChainFactory::create();
        $optimizerChain->optimize($path, $path);
    }

    private function minifyCssFiles($path)
    {
        $minifier = new Minify\CSS($path);
        $minifier->add($path);
        file_put_contents($path, 'escrevendo');
        $minifier->minify($path);
    }

    private function minifyJsFiles($path)
    {
        $minifier = new Minify\JS($path);
        $minifier->add($path);
        file_put_contents($path, 'escrevendo');
        $minifier->minify($path);
    }
}