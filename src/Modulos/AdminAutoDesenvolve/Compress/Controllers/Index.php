<?php

namespace Modulos\AdminAutoDesenvolve\Compress\Controllers;

use Aplicacao\Ferramentas\Response;
use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;
use Modulos\AdminAutoDesenvolve\Compress\Models\RegrasDeNegocio\CompressFiles;
use Modulos\AdminAutoDesenvolve\Compress\Models\Repositorios\AdminAutoDesenvolveCompressRP;

class Index extends ControladorPadraoAbsHelp
{

    public $admin = false;

    public function index($view = 'index', $data = array(), $admin = false)
    {
        (new CompressFiles())->getRaiz();
    }

    public function mostrarRaiz()
    {
        (new CompressFiles())->getRaiz();
    }
}