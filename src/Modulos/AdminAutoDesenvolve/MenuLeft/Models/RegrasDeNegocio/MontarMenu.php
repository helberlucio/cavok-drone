<?php
/**
 * Created by PhpStorm.
 * User: Gabriel PHP
 * Date: 25/05/2017
 * Time: 18:14
 */

namespace Modulos\AdminAutoDesenvolve\MenuLeft\Models\RegrasDeNegocio;

use Aplicacao\Url;
use ModuloHelp\PadraoCriacao\ModelRegraDeNegocio\Abstracts\ModelRegraDeNegocioPadraoAbsHelp;
use Modulos\AdminAutoDesenvolve\Escritorios\Models\Repositorios\AdminAutoDesenvolveEscritoriosRP;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\AutodesenvolveMenuEntidade;

class MontarMenu extends ModelRegraDeNegocioPadraoAbsHelp
{

	private $aberturaEstrutura   = '<ul>';
	private $fechamentoEstrutura = '</ul>';

	public function getListagem()
	{
		$menuPrimeiraLinha = $this->getPrimeiraLinhaDeMenu();
		$html = '<ul>';
		$html .= $this->gerarLista($html, $menuPrimeiraLinha);
		$html .= '</ul>';

		return $html;
	}

	private function gerarLista(&$html, $menus, $submenu = false)
	{
		/**
		 * @var $menus AutodesenvolveMenuEntidade[]
		 */
		if ($submenu)
		{
			$html .= '<ul>';
		}
		foreach ($menus as $key => $menu)
		{
			$temSubmenuu = false;
			if ($this->temSubMenu($menu))
			{
				$temSubmenuu = true;
			}
			$linkFalse = 'disableLink';
			$html .= '<li onclick="menuHandler(this)" id="side-menu-' . $key . '" class="' . ($temSubmenuu ? 'drop-menu' : 'single-menu') . '">';
			if ($menu->getLink() && $menu->getLink() != '#' && $menu->getLink() != '')
			{
				$linkFalse = '';
			}
			$urlSelf = $menu->getUrlAmigavel() != '' ? Url::Base() . Url::Apelido('Admin/' . $menu->getLink() . '&icon=' . $menu->getIcone() . '&page_name=' . $menu->getNome(), 'Admin/' . $menu->getUrlAmigavel()) : Url::Admin($menu->getLink() . '&icon=' . $menu->getIcone() . '&page_name=' . $menu->getNome());
			$html .= '<a class="' . $linkFalse . '" taget="' . $menu->getTarget() . '" href="' . ($menu->getTarget() == '_self' ? $urlSelf : $menu->getLink()) . '">';
			$html .= '<div class="col-md-2 listagem-menu-icone-area">' . $menu->getIcone() . '</div>' .
				'<div class="col-md-10 listagem-menu-titulo-area">' . $menu->getNome() . ($temSubmenuu ? '<i class="fa fa-sort-desc" aria-hidden="true"></i>' : '') . '</div> ';
			$html .= '</a>';
			if ($temSubmenuu)
			{
				$html .= $this->gerarLista($html, $this->getSubMenus($menu), true);
			}
			$html .= '</li>';
		}
		if ($submenu)
		{
			$html .= '</ul>';
		}
	}

	/**
	 * @param AutodesenvolveMenuEntidade $menu
	 * @return bool
	 */
	private function temSubMenu(AutodesenvolveMenuEntidade $menu)
	{
		$quantidadeSubMenuQuery = $this->db->select('count(1) as quantidade')
			->where('ativo', 1)
			->where('submenu', $menu->getId())->get('autodesenvolve_menu')->row();
		if (isset($quantidadeSubMenuQuery->quantidade) && $quantidadeSubMenuQuery->quantidade >= 1)
		{
			return true;
		}

		return false;
	}

	/**
	 * @param AutodesenvolveMenuEntidade $menu
	 * @return AutodesenvolveMenuEntidade[]
	 */
	private function getSubMenus(AutodesenvolveMenuEntidade $menu)
	{
		return (new AutodesenvolveMenuEntidade())->getRows(
			$this->db
				->order_by('ordem', 'ASC')
				->where('ativo', 1)
				->where('submenu', $menu->getId()));
	}

	/**
	 * @return AutodesenvolveMenuEntidade[]
	 */
	private function getPrimeiraLinhaDeMenu()
	{
		$escritorioAtual = (AdminAutoDesenvolveEscritoriosRP::getEscritorioAtual());
		$escritorioAtualID = $escritorioAtual->getId();

		return (new AutodesenvolveMenuEntidade())->getRows($this->db->select('autodesenvolve_menu.*, (select count(1) from autodesenvolve_menu am2 WHERE am2.id = autodesenvolve_menu.submenu) as quantidade_submenu')
			->where('ativo >', 0)
			->where('(escritorio = ' . $escritorioAtualID . ' OR escritorio is null OR escritorio = "")', '', true)
			->order_by('ordem', 'ASC')
			->having('quantidade_submenu < 1'), false);
	}

}