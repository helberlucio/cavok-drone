<?php
namespace Modulos\AdminAutoDesenvolve\MenuLeft\Controllers;

use Core\Controlador\Controlador;
use Modulos\AdminAutoDesenvolve\MenuLeft\Models\Repositorios\AdminAutoDesenvolveMenuLeftRP;

class Index extends Controlador{

    public function index($view = 'index', $data = array(), $admin = false){
        echo AdminAutoDesenvolveMenuLeftRP::getIndexPage($view, $data, $admin);
    }
}