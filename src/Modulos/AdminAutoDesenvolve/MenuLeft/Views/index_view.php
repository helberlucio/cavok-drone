<?php
/**
 * @var $usuarioLogado \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsUsuariosEntidade
 * @var $listagem_html string
 */

\Aplicacao\Ferramentas\GetAssets::CSS('AdminAutoDesenvolveMenuLeft', 'AdminAutoDesenvolve', 'MenuLeft');
\Aplicacao\Ferramentas\GetAssets::JS('AdminAutoDesenvolveMenuLeft', 'AdminAutoDesenvolve', 'MenuLeft');
?>
<div class="col-md-3 menu-left-autodesenvolve">
    <div class="col-md-12 area-tre-list-icon" onclick="toggleInXs();">
        <div class="tre-list-icon" style="display: none;">
            <div class="col-md-12 fit-box">
                <div class="fit-aside">
                </div>
                <div class="fit-side">
                </div>
            </div>
            <div class="col-md-12 fit-box">
                <div class="fit-aside">
                </div>
                <div class="fit-side">
                </div>
            </div>
            <div class="col-md-12 fit-box">
                <div class="fit-aside">
                </div>
                <div class="fit-side">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 menu-area">
        <?php echo $listagem_html; ?>
    </div>
</div>
