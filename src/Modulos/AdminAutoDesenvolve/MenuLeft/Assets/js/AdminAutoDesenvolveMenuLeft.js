var menuReduzido = !1;

function AtivarDesativarMenuVersaoReduzida() {
    $('.tre-list-icon').click(function () {
        if (menuReduzido === !0) {
            menuReduzido = !1;
            $('.menu-left-autodesenvolve').removeClass('versao-minima');
            $('.logo-header-autodesenvolve').removeClass('versao-minima');
            $('.autodesenvolve-escritorio').removeClass('versao-minima');
            $('.box-troca-departamentos').removeClass('versao-minima');
            medirTamanhoLarguraMenu(220);
        } else {
            menuReduzido = !0;
            $('.menu-left-autodesenvolve').addClass('versao-minima');
            $('.logo-header-autodesenvolve').addClass('versao-minima');
            $('.autodesenvolve-escritorio').addClass('versao-minima');
            $('.box-troca-departamentos').addClass('versao-minima');
            medirTamanhoLarguraMenu(56);
        }
    })
}

var windowWidth = $(window).width();

function dropDownClickEvent() {
    $('.menu-left-autodesenvolve .menu-area li.drop-menu a').on('click', function () {
        if (!$(this).parent('li').hasClass('active')) {
            $(this).parent('li').parent('ul').find('li').each(function () {
                $(this).removeClass('active');
                $(this).children('ul').slideUp(150);
                $(this).addClass('small');
            });
            $(this).parent('li').removeClass('small');
            $(this).parent('li').addClass('active');
            $(this).parent('li').children('ul').slideDown(150);
        } else {
            $(this).parent('li').parent('ul').find('li').removeClass('small');
            $(this).parent('li').removeClass('active');
            $(this).parent('li').children('ul').slideUp(150);
            $(this).parent('li').parent('ul').find('li').each(function () {
                $(this).removeClass('active');
                $(this).children('ul').slideUp(150);
            });
        }
    })
}

function toggleInXs() {
    if (windowWidth < 1000) {
        $('.search-area').slideToggle(200);
        $('.cms-area').slideToggle(200);
        $('.menu-area').slideToggle(200);
    }
}

$(function () {
    if (windowWidth > 1000) {
        AtivarDesativarMenuVersaoReduzida();
    }
    dropDownClickEvent();
});