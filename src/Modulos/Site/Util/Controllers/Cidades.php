<?php

namespace Modulos\Site\Util\Controllers;

use Aplicacao\Ferramentas\Response;
use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteAreaCoberturaEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysBairrosEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysCidadesEntidade;

class Cidades extends ControladorPadraoAbsHelp {

    public $admin = false;

    public function index() {
        $search = input_get('search');
        $response = ['results' => []];
        if (!$search) {
            Response::json($response);
        }
        $results = (new SysCidadesEntidade())->getRows(
            $this->db
                ->like('titulo', $search, 'left')
                ->order_by('sigla_uf', 'ASC')
                ->order_by('titulo', 'ASC')
        );
        $group = null;
        $data = [];
        foreach ($results as $row) {
            if ($row->getSiglaUf() != $group) {
                $group = $row->getSiglaUf();
                $data[ $group ] = [
                    'text'     => $group,
                    'children' => [],
                ];
            }
            $data[ $group ]['children'][] = ['id'=> $row->getId(), 'text'=> $row->getTitulo()];
        }
        $response['results'] = array_values($data);
        Response::json($response);
    }

    public function bairros() {
        $cidade = input_get('cidade');
        $search = input_get('search');
        $response = ['results' => []];
        if (!$cidade) {
            Response::json($response);
        }
        if (!$search) {
            $results = (new SiteAreaCoberturaEntidade())->getRows(
                $this->db
                    ->where('cidade', $cidade)
                    ->group_by('bairro')
                    ->order_by('bairro', 'ASC')
                    ->limit(5)
            );
        } else {
            $results = (new SiteAreaCoberturaEntidade())->getRows(
                $this->db
                    ->like('bairro', $search)
                    ->where('cidade', $cidade)
                    ->group_by('bairro')
                    ->order_by('bairro', 'ASC')
            );
        }
        foreach ($results as $row) {
            $response['results'][] = [
                'id'   => $row->getId(),
                'text' => $row->getBairro(),
            ];
        }
        Response::json($response);
    }

}