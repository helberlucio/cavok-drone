<?php
/**
 * @var SysPagesEntidade     $page
 * @var SiteCursosEntidade   $item
 * @var SiteAgendaEntidade[] $agenda
 * @var array                $mensagem
 * @var array                $form
 */

use Aplicacao\Conversao;
use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteAgendaEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursosEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysPagesEntidade;

$form->action = $form->action ?? 'cadastro';

?>
<div class="detalhes">
    <div class="banner" style='background-image: url("<?= Url::Base($page->getBanner()) ?>")'>
        <div class="container pt-4 pb-4">
            <h4 class="text-white text-center mb-5 mt-3"><?= $page->getTitulo() ?> de <?= $item->getNome() ?></h4>
            <div class="row justify-content-center">
                <div class="col-md-2 text-center text-white mb-4">
                    <img src="<?= Url::Gerenciador('Cursos/cavok-drone-service-curso-icone-calendario.png') ?>">
                    <div class="titulo mt-3">Data</div>
                    <hr>
                    <div class="descricao">
						<?= count($agenda)
							? (date('d', strtotime(current($agenda)->getData())) . ' / ' . date('m', strtotime(current($agenda)->getData())))
							: '' ?>
                    </div>
                </div>
                <div class="col-md-2 text-center text-white mb-4">
                    <img src="<?= Url::Gerenciador('Cursos/cavok-drone-service-curso-icone-duracao.png') ?>">
                    <div class="titulo mt-3">Duração</div>
                    <hr>
                    <div class="descricao"><?= $item->getDuracaoTurmaDias() ?> dias</div>
                </div>
                <div class="col-md-2 text-center text-white mb-4">
                    <img src="<?= Url::Gerenciador('Cursos/cavok-drone-service-curso-icone-local.png') ?>">
                    <div class="titulo mt-3">Local</div>
                    <hr>
                    <div class="descricao"><?= count($agenda) ? current($agenda)->getCidade() : '' ?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="formulario pt-5 pb-5">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-7">
                    <h5 class="mb-4 text-primary">Descrição do Curso <?= $item->getNome() ?></h5>
                    <p><?= nl2br($item->getDescricao()) ?></p>
                    <div class="row mb-4">
                        <div class="col-md-7">
                            <h5 class="mb-4 text-primary">Público Alvo</h5>
                            <p><?= nl2br($item->getPublicAlvo()) ?></p>
                        </div>
                        <div class="col-md-5">
                            <h5 class="mb-4 text-primary">Objetivo</h5>
                            <p><?= nl2br($item->getObjetivo()) ?></p>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-md-7">
                            <h5 class="mb-4 text-primary">Certificado</h5>
                            <p><?= nl2br($item->getCertificado()) ?></p>
                        </div>
                        <div class="col-md-5">
                            <h5 class="mb-4 text-primary">Metodologia</h5>
                            <p><?= nl2br($item->getMetodologia()) ?></p>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-md-7">
                            <h5 class="mb-4 text-primary">Valor do investimento</h5>
                            <p class="valores">
                                Curso Online Teórico Com Aula Prática de Pilotagem \ <?= $item->getDuracaoTurma() ?> hs \
                                R$ <?= Conversao::formatarDinheiro($item->getValorTurma()) ?>
                                <br>
                                Curso Teórico Online \ <?= $item->getDuracaoIndividual() ?> hs \
                                R$ <?= Conversao::formatarDinheiro($item->getValorIndividual()) ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <h5 class="text-white mb-4 bg-dark text-white p-3 text-center">Faça sua matrícula</h5>
                    <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link <?= $form->action == 'cadastro' ? 'active' : '' ?>" id="cadastro-tab"
                               data-toggle="tab" href="#cadastro" role="tab"
                               aria-controls="cadastro" aria-selected="<?= $form->action == 'cadastro' ? 'true' : 'false' ?>">Cadastrar</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link <?= $form->action == 'login' ? 'active' : '' ?>" id="login-tab"
                               data-toggle="tab" href="#login" role="tab"
                               aria-controls="login"
                               aria-selected="<?= $form->action == 'login' ? 'true' : 'false' ?>">Entrar</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade <?= $form->action == 'cadastro' ? 'show active' : '' ?>" id="cadastro"
                             role="tabpanel"
                             aria-labelledby="cadastro-tab">
                            <form id="form-matricula" method="post" action="">
                                <input type="hidden" name="curso" value="<?= $id ?>">
                                <input type="hidden" name="action" value="cadastro">
                                <div class="form-group">
                                    <label class="text-primary" for="input-dia">* Escolha o dia</label>
                                    <select class="form-control rounded-sm" name="dia" id="input-dia" required>
                                        <option value="" disabled hidden selected></option>
										<?php foreach ($agenda as $evento): ?>
                                            <option value="<?= $evento->getId() ?>"><?= Conversao::formatarDataBR($evento->getData()) ?>
                                                | <?= $evento->getHorario() ?></option>
										<?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="text-primary" for="input-individual">* Modo do curso</label>
                                    <select class="form-control rounded-sm" name="individual" id="input-individual" required>
                                        <option value="" hidden disabled selected></option>
                                        <option value="0">Online Teórico Com Aula Prática de Pilotagem</option>
                                        <option value="1">Teórico Online</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="text-primary" for="input-nome">* Nome completo</label>
                                    <input type="text" class="form-control rounded-sm" name="nome" id="input-nome"
                                           value="<?= isset($form->nome) ? $form->nome : '' ?>"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label class="text-primary" for="input-cpf">* CPF</label>
                                    <input type="text" class="form-control rounded-sm mask-cpf" name="cpf"
                                           id="input-cpf"
                                           value="<?= isset($form->cpf) ? $form->cpf : '' ?>"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label class="text-primary" for="input-telefone">* Telefone ou Whatsapp</label>
                                    <input type="tel" class="form-control rounded-sm mask-telefone" name="telefone"
                                           id="input-telefone"
                                           value="<?= isset($form->telefone) ? $form->telefone : '' ?>"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label class="text-primary" for="input-email">* Seu e-mail</label>
                                    <input type="email" class="form-control rounded-sm" name="email" id="input-email"
                                           value="<?= isset($form->email) ? $form->email : '' ?>"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label class="text-primary" for="input-senha">* Senha</label>
                                    <input type="password" class="form-control rounded-sm" name="senha" id="input-senha"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label class="text-primary" for="input-senha2">* Repetir Senha</label>
                                    <input type="password" class="form-control rounded-sm" name="senha2"
                                           id="input-senha2" required>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block pt-3 pb-3 rounded-0">
                                    Confirmar matrícula
                                </button>
                            </form>
                        </div>
                        <div class="tab-pane fade <?= $form->action == 'login' ? 'show active' : '' ?>" id="login"
                             role="tabpanel" aria-labelledby="login-tab">
                            <form id="form-matricula" method="post" action="">
                                <input type="hidden" name="curso" value="<?= $id ?>">
                                <input type="hidden" name="action" value="login">
                                <div class="form-group">
                                    <label class="text-primary" for="input-dia">* Escolha o dia</label>
                                    <select class="form-control rounded-sm" name="dia" id="input-dia" required>
                                        <option value="" disabled hidden selected></option>
										<?php foreach ($agenda as $evento): ?>
                                            <option value="<?= $evento->getId() ?>"><?= Conversao::formatarDataBR($evento->getData()) ?>
                                                | <?= $evento->getHorario() ?></option>
										<?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="text-primary" for="input-individual">* Modo do curso</label>
                                    <select class="form-control rounded-sm" name="individual" id="input-individual" required>
                                        <option value="" hidden disabled selected></option>
                                        <option value="0">Online Teórico Com Aula Prática de Pilotagem</option>
                                        <option value="1">Teórico Online</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="text-primary" for="login-input-email">* Seu e-mail</label>
                                    <input type="email" class="form-control rounded-sm" name="email"
                                           id="login-input-email"
                                           value="<?= isset($form->email) ? $form->email : '' ?>"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label class="text-primary" for="login-input-senha">* Senha</label>
                                    <input type="password" class="form-control rounded-sm" name="senha"
                                           id="login-input-senha" required>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block pt-3 pb-3 rounded-0">
                                    Confirmar matrícula
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
		<?php if (isset($mensagem)): ?>
		<?php if ($mensagem['sucesso']): ?>
        Swal.fire({
            title: 'Sucesso!',
            text: '<?= $mensagem['mensagem'] ?>',
            type: 'success',
        });
		<?php else: ?>
        Swal.fire({
            title: 'Erro!',
            text: '<?= $mensagem['mensagem'] ?>',
            type: 'error',
        });
		<?php endif; ?>
		<?php endif; ?>
    });
</script>