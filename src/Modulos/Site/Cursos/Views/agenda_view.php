<?php
/**
 * @var SysPagesEntidade           $page
 * @var SiteCursosEntidade[]       $items
 * @var SiteCursosOnlineEntidade[] $items_online
 * @var array                      $agenda
 */

use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursosEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursosOnlineEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysPagesEntidade;
use Modulos\Site\Cursos\Models\Repositorios\SiteCursosRP;

function corData($dia)
{
	if ($dia['esgotado'] === null)
	{
		return 'text-muted';
	}

	return 'text-dark';
}

function corTexto($dia)
{
	if ($dia['esgotado'] === null)
	{
		return 'text-danger';
	}

	return 'text-dark';
}

function texto($dia)
{
	if ($dia['esgotado'] === null)
	{
		return 'text-muted';
	}

	return 'text-dark';
}

?>
<div class="agenda">
    <h5 class="text-primary text-center mt-5"><?= $page->getTitulo() ?></h5>
    <div class="container mt-4 mb-5">
        <div class="row">
			<?php foreach ($items_online as $item_online): ?>
                <div class="col-md-4  mb-3">
                    <div class="item border border-gray h-100">
                        <h5 class="text-white bg-primary p-3 text-center"><?= $item_online->getNome() ?></h5>
                        <div class="pl-5 pr-5 pt-3 pb-4">
                            <h6 class="text-primary text-center mb-4 text-nowrap">Curso online</h6>
                            <p class="mt-4 mb-4 text-center descricao"><?= nl2br($page->getDescricao()) ?></p>
                            <a href="<?= Url::ModuloApelido(
								SiteCursosRP::class,
								"Index/detalhes_online?menu=cursos&id={$item_online->getId()}",
								Url::Title($item_online->getNome())
								['curso-online']) ?>"
                               class="btn btn-primary btn-block pt-2 pb-2">
                                Agendar matrícula
                            </a>
                        </div>
                    </div>
                </div>
			<?php endforeach; ?>
			<?php foreach ($items as $item): ?>
                <div class="col-md-4  mb-3">
                    <div class="item border border-gray h-100">
                        <h5 class="text-white bg-primary p-3 text-center"><?= $item->getNome() ?></h5>
                        <div class="pl-5 pr-5 pt-3 pb-4">
							<?php if ($item->getDuracaoTurmaDias()): ?>
                                <h6 class="text-primary text-center mb-4 text-nowrap"> Duração do
                                    curso: <?= $item->getDuracaoTurmaDias() ?> dias</h6>
							<?php endif; ?>
                            <ul class="list-unstyled">
								<?php if (isset($agenda[ $item->getId() ])): ?>
									<?php foreach ($agenda[ $item->getId() ] as $dia): ?>
                                        <li class="mb-2">
                                            <span class="<?= $dia['esgotado'] === null ? 'text-gray-dark' : 'text-dark' ?>"><?= $dia['data'] ?></span>&nbsp;
											<?php if ($dia['esgotado'] !== null): ?>
                                                <span class="<?= $dia['esgotado'] === false ? 'text-primary' : 'text-danger' ?>">
													<?= $dia['esgotado'] === false ? '*Em aberto' : '*Esgotado' ?>
                                                </span>
											<?php endif; ?>
                                        </li>
									<?php endforeach; ?>
								<?php else: ?>
                                    <p class="text-center text-muted">Nenhuma data futura cadastrada</p>
								<?php endif; ?>
                            </ul>
                            <p class="mt-4 mb-4 text-center descricao"><?= nl2br($page->getDescricao()) ?></p>
                            <a href="<?= Url::ModuloApelido(
								SiteCursosRP::class,
								"Index/detalhes?menu=cursos&id={$item->getId()}",
								Url::Title($item->getNome()),
								['curso']) ?>"
                               class="btn btn-primary btn-block pt-2 pb-2">
                                Agendar matrícula
                            </a>
                        </div>
                    </div>
                </div>
			<?php endforeach; ?>
        </div>
    </div>
</div>
