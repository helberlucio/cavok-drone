<?php
/**
 * @var SysPagesEntidade         $page
 * @var SiteCursosOnlineEntidade $item
 * @var SiteAgendaEntidade[]     $agenda
 * @var array                    $mensagem
 * @var array                    $form
 */

use Aplicacao\Conversao;
use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteAgendaEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursosOnlineEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysPagesEntidade;

$form->action = $form->action ?? 'cadastro';

?>
<div class="detalhes">
    <div class="banner" style='background-image: url("<?= Url::Base($page->getBanner()) ?>")'>
        <div class="container pt-4 pb-4">
            <h4 class="text-white text-center mb-3 mt-3"><?= $item->getNome() ?></h4>
        </div>
    </div>
    <div class="formulario pt-5 pb-5">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-7">
                    <h5 class="mb-4 text-primary">Descrição do Curso <?= $item->getNome() ?></h5>
                    <p><?= nl2br($item->getDescricao()) ?></p>
                    <div class="row mb-4">
                        <div class="col-md-7">
                            <h5 class="mb-4 text-primary">Público Alvo</h5>
                            <p><?= nl2br($item->getPublicAlvo()) ?></p>
                        </div>
                        <div class="col-md-5">
                            <h5 class="mb-4 text-primary">Objetivo</h5>
                            <p><?= nl2br($item->getObjetivo()) ?></p>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-md-7">
                            <h5 class="mb-4 text-primary">Certificado</h5>
                            <p><?= nl2br($item->getCertificado()) ?></p>
                        </div>
                        <div class="col-md-5">
                            <h5 class="mb-4 text-primary">Metodologia</h5>
                            <p><?= nl2br($item->getMetodologia()) ?></p>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-md-7">
                            <h5 class="mb-4 text-primary">Valor do investimento</h5>
                            <p class="valores">
                                Curso Online: R$ <?= Conversao::formatarDinheiro($item->getValor()) ?>
                            </p>
                            <p class="valores">
                                Curso Online e Presencial:
                                R$ <?= Conversao::formatarDinheiro($item->getValorPresencial()) ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <h5 class="text-white mb-3 bg-dark text-white p-3 text-center">Faça sua matrícula</h5>
                    <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link <?= $form->action == 'cadastro' ? 'active' : '' ?>" id="cadastro-tab"
                               data-toggle="tab" href="#cadastro" role="tab"
                               aria-controls="cadastro"
                               aria-selected="<?= $form->action == 'cadastro' ? 'true' : 'false' ?>">Cadastrar</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link <?= $form->action == 'login' ? 'active' : '' ?>" id="login-tab"
                               data-toggle="tab" href="#login" role="tab"
                               aria-controls="login"
                               aria-selected="<?= $form->action == 'login' ? 'true' : 'false' ?>">Entrar</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade <?= $form->action == 'cadastro' ? 'show active' : '' ?>" id="cadastro"
                             role="tabpanel"
                             aria-labelledby="cadastro-tab">
                            <form id="form-matricula" method="post" action="">
                                <input type="hidden" name="curso" value="<?= $id ?>">
                                <input type="hidden" name="action" value="cadastro">
                                <div class="form-group">
                                    <label class="text-primary" for="input-nome">* Nome completo</label>
                                    <input type="text" class="form-control rounded-sm" name="nome" id="input-nome"
                                           value="<?= isset($form->nome) ? $form->nome : '' ?>"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label class="text-primary" for="input-cpf">* CPF</label>
                                    <input type="text" class="form-control rounded-sm mask-cpf" name="cpf"
                                           id="input-cpf"
                                           value="<?= isset($form->cpf) ? $form->cpf : '' ?>"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label class="text-primary" for="input-telefone">* Telefone ou Whatsapp</label>
                                    <input type="tel" class="form-control rounded-sm mask-telefone" name="telefone"
                                           id="input-telefone"
                                           value="<?= isset($form->telefone) ? $form->telefone : '' ?>"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label class="text-primary" for="input-email">* Seu e-mail</label>
                                    <input type="email" class="form-control rounded-sm" name="email" id="input-email"
                                           value="<?= isset($form->email) ? $form->email : '' ?>"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label class="text-primary" for="input-senha">* Senha</label>
                                    <input type="password" class="form-control rounded-sm" name="senha" id="input-senha"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label class="text-primary" for="input-senha2">* Repetir Senha</label>
                                    <input type="password" class="form-control rounded-sm" name="senha2"
                                           id="input-senha2" required>
                                </div>
                                <label class="text-primary">* Tipo do curso</label>
                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="presencial"
                                               id="input-presencial-nao" value="0" checked>
                                        <label class="form-check-label" for="input-presencial-nao">Somente
                                            Online</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="presencial"
                                               id="input-presencial-sim" value="1">
                                        <label class="form-check-label" for="input-presencial-sim">Online e
                                            Presencial</label>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block pt-3 pb-3 rounded-0">
                                    Confirmar matrícula
                                </button>
                            </form>
                        </div>
                        <div class="tab-pane fade <?= $form->action == 'login' ? 'show active' : '' ?>" id="login"
                             role="tabpanel" aria-labelledby="login-tab">
                            <form id="form-matricula" method="post" action="">
                                <input type="hidden" name="curso" value="<?= $id ?>">
                                <input type="hidden" name="action" value="login">
                                <div class="form-group">
                                    <label class="text-primary" for="login-input-email">* Seu e-mail</label>
                                    <input type="email" class="form-control rounded-sm" name="email"
                                           id="login-input-email"
                                           value="<?= isset($form->email) ? $form->email : '' ?>"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label class="text-primary" for="login-input-senha">* Senha</label>
                                    <input type="password" class="form-control rounded-sm" name="senha"
                                           id="login-input-senha" required>
                                </div>
                                <label class="text-primary">* Tipo do curso</label>
                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="presencial"
                                               id="login-input-presencial-nao" value="0" checked>
                                        <label class="form-check-label" for="login-input-presencial-nao">Somente
                                            Online</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="presencial"
                                               id="login-input-presencial-sim" value="1">
                                        <label class="form-check-label" for="login-input-presencial-sim">Online e
                                            Presencial</label>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block pt-3 pb-3 rounded-0">
                                    Confirmar matrícula
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
		<?php if (isset($mensagem)): ?>
		<?php if ($mensagem['sucesso']): ?>
        Swal.fire({
            title: 'Sucesso!',
            text: '<?= $mensagem['mensagem'] ?>',
            type: 'success',
        });
		<?php if (isset($mensagem['redirect'])): ?>
        window.setTimeout(function () {
            window.document.location.href = '<?= $mensagem['redirect'] ?>';
        }, 1000);
		<?php endif; ?>
		<?php else: ?>
        Swal.fire({
            title: 'Erro!',
            text: '<?= $mensagem['mensagem'] ?>',
            type: 'error',
        });
		<?php endif; ?>
		<?php endif; ?>
    });
</script>