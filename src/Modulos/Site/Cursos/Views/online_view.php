<?php
/**
 * @var $curso    \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursosOnlineEntidade
 * @var $conteudo \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursoConteudoEntidade
 */

use Aplicacao\Url;
use Modulos\Site\Cursos\Models\Repositorios\SiteCursosRP;

?>
<div class="container mt-5 mb-5">
	<?php if (isset($mensagem)): ?>
        <p class="alert alert-danger text-center"><?= $mensagem ?></p>
	<?php endif; ?>
	<?php if (isset($curso)): ?>
        <h2 class="text-primary text-center mt-2"><?= $curso->getNome() ?></h2>
	<?php endif; ?>
	<?php if (isset($conteudo)): ?>
        <h5 class="text-center mt-2 mb-2"><?= $conteudo->getTitulo() ?></h5>
		<?php if (!empty($conteudo->getAudio())): ?>
            <div class="my-5">
                <audio controls controlsList="nodownload">
                    <source src="<?= Url::Base($conteudo->getAudio()) ?>" type="audio/mpeg">
                    Seu browser não suporta o elemento de áudio.
                </audio>
            </div>
		<?php endif; ?>
		<?php if (!empty($conteudo->getTexto())): ?>
            <div class="my-5">
				<?= $conteudo->getTexto() ?>
            </div>
		<?php endif; ?>

        <nav class="mt-5">
            <ul class="pagination justify-content-center">
				<?php if (isset($pa) && $pa != 0): ?>
                    <li class="page-item">
                        <a class="page-link"
                           href="<?= Url::ModuloApelido(
							   SiteCursosRP::class,
							   "Online/index?menu=cursos&c={$curso->getId()}&p={$pa}",
							   $pa,
							   ['conteudo', Url::Title($curso->getNome())]) ?>">Anterior</a>
                    </li>
				<?php else: ?>
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Anterior</a>
                    </li>
				<?php endif; ?>
                <li class="page-item active" aria-current="page">
                    <a class="page-link"
                       href="<?= Url::ModuloApelido(
						   SiteCursosRP::class,
						   "Online/index?menu=cursos&c={$curso->getId()}&p={$p}",
						   $p,
						   ['conteudo', Url::Title($curso->getNome())]) ?>"><?= $p ?></a>
                </li>
				<?php if (isset($pp) && $pp != 0): ?>
                    <li class="page-item">
                        <a class="page-link"
                           href="<?= Url::ModuloApelido(
							   SiteCursosRP::class,
							   "Online/index?menu=cursos&c={$curso->getId()}&p={$pp}",
							   $pp,
							   ['conteudo', Url::Title($curso->getNome())]) ?>">Próximo</a>
                    </li>
				<?php else: ?>
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Próximo</a>
                    </li>
				<?php endif; ?>
            </ul>
        </nav>
	<?php endif; ?>
</div>
<div align="center">
    <noscript>
        <div style="position:fixed; top:0px; left:0px; z-index:3000; height:100%; width:100%; background-color:#FFFFFF">
            <div style="font-family: Trebuchet MS; font-size: 14px; background-color:#FFF000; padding: 10pt;">In order
                for you to see this content, you should enable Javascript!
            </div>
        </div>
    </noscript>
</div>

<script type="text/javascript">
    function disableSelection(e) {
        if (typeof e.onselectstart != "undefined")
            e.onselectstart = function () {
                return false
            };
        else if (typeof e.style.MozUserSelect != "undefined") e.style.MozUserSelect = "none";
        else e.onmousedown = function () {
                return false
            };
        e.style.cursor = "default"
    }

    window.onload = function () {
        disableSelection(document.body)
    }
</script>

<script type="text/javascript">
    document.oncontextmenu = function (e) {
        var t = e || window.event;
        var n = t.target || t.srcElement;
        if (n.nodeName != "A") return false
    };
    document.ondragstart = function () {
        return false
    };
</script>
<style type="text/css" media="print">
    #pagina-cursos { display: none !important; }
</style>
<style type="text/css">
        *:

    (
    input, textarea

    )
    {
        -webkit-touch-callout: none
    ;
        -webkit-user-select: none
    ;
    }

    img {
        -webkit-touch-callout: none;
        -webkit-user-select: none;
    }
</style>

<script type="text/javascript">
    window.addEventListener("keydown", function (e) {
        if (e.ctrlKey && (e.which == 65 || e.which == 66 || e.which == 67 || e.which == 70 || e.which == 73 || e.which == 80 || e.which == 83 || e.which == 85 || e.which == 86)) {
            e.preventDefault()
        }
    });
    document.keypress = function (e) {
        if (e.ctrlKey && (e.which == 65 || e.which == 66 || e.which == 70 || e.which == 67 || e.which == 73 || e.which == 80 || e.which == 83 || e.which == 85 || e.which == 86)) {
        }
        return false;
    }
</script>

<script type="text/javascript">
    document.onkeydown = function (e) {
        e = e || window.event;
        if (e.keyCode == 123 || e.keyCode == 18) {
            return false
        }
    }
</script>