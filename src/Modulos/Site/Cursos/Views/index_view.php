<?php
/**
 * @var SysPagesEntidade           $page
 * @var SiteCursosEntidade[]       $items
 * @var SiteCursosOnlineEntidade[] $items_online
 */

use Aplicacao\Conversao;
use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursosEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursosOnlineEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysPagesEntidade;
use Modulos\Site\Cursos\Models\Repositorios\SiteCursosRP;

?>
<div class="index">
    <div class="banner"
         style="background-image: url('<?= Url::Base($page->getBanner()) ?>'); min-height: <?= $page->getAlturaBanner() ?>px;">
        <h3 class="titulo text-white text-center text-uppercase"><?= $page->getTitulo() ?></h3>
    </div>
    <div class="container mt-5 mb-5">
        <div class="row">
			<?php foreach ($items_online as $item): ?>
                <div class="col-md-4  mb-3">
                    <div class="item border border-gray h-100">
                        <img src="<?= Url::Base($item->getImagem()) ?>" alt="<?= $item->getNome() ?>"
                             class="img-fluid">
                        <div class="p-4">
                            <h5 class="text-primary text-center mb-4"><?= $item->getNome() ?></h5>
                            <h5 class="text-dark text-center mb-4 text-nowrap">
                                R$ <?= Conversao::formatarDinheiro($item->getValor()) ?>
                            </h5>
                            <p class="text-center mb-4 text-muted"
                               title="<?= $item->getDescricao() ?>"><?= nl2br(mb_substr($item->getDescricao(), 0, 135)) ?>
                                ...</p>
                            <a href="<?= Url::ModuloApelido(
								SiteCursosRP::class,
								"Index/detalhes_online?menu=cursos&id={$item->getId()}",
								Url::Title($item->getNome()),
								['curso-online']) ?>"
                               class="btn btn-primary btn-block pt-2 pb-2">
                                Consultar curso
                            </a>
                        </div>
                    </div>
                </div>
			<?php endforeach; ?>
        </div>
    </div>
</div>
