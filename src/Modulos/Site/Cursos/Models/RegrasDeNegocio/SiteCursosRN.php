<?php

namespace Modulos\Site\Cursos\Models\RegrasDeNegocio;

use Aplicacao\Conversao;
use Exception;
use ModuloHelp\PadraoCriacao\ModelRegraDeNegocio\Abstracts\ModelRegraDeNegocioPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteAgendaEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteAlunosEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteClienteEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursosEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursosOnlineEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteMatriculasEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteMatriculasOnlineEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysPagesEntidade;
use Modulos\Site\AreaDoCliente\Models\RegrasDeNegocio\SiteAreaDoClienteRN;
use stdClass;

class SiteCursosRN extends ModelRegraDeNegocioPadraoAbsHelp
{

	public function getModulo($viewName = "index", $data = [], $admin = false, $minifyHtml = true, $viewDeAtualizacao = false)
	{
		if ($data['pagina'] == "index")
		{
			$data['page'] = (new SysPagesEntidade())->getRow($this->db->where('pagina', 'cursos'));
//			$data['items'] = (new SiteCursosEntidade())->getRows($this->db->order_by('ordem', 'asc'));
			$data['items_online'] = (new SiteCursosOnlineEntidade())->getRows($this->db->order_by('ordem', 'asc'));
		}
		if ($data['pagina'] == "agenda")
		{
			$data['page'] = (new SysPagesEntidade())->getRow($this->db->where('pagina', 'agenda'));
			$data['items'] = (new SiteCursosEntidade())->getRows($this->db->order_by('ordem', 'asc'));
			$data['items_online'] = (new SiteCursosOnlineEntidade())->getRows($this->db->order_by('ordem', 'asc'));

			$ultimaData = [];
			$tamanho = 0;
			$agenda = [];
			foreach ((new SiteAgendaEntidade())->getRows($this->db->where('site_agenda.data >=', date('Y-m-d'))->order_by('site_agenda.data', 'asc')) as $row)
			{
				if (isset($ultimaData[ $row->getCurso() ]))
				{
					$ultimaDt = $ultimaData[ $row->getCurso() ];
					$atualDt = strtotime($row->getData());
					if ($atualDt > $ultimaDt)
					{
						$proximaDt = strtotime('+1 day', $ultimaDt);
						while ($proximaDt < $atualDt)
						{
							$agenda[ $row->getCurso() ][] = [
								'data'     => date('d/m/Y', $proximaDt),
								'esgotado' => null,
							];
							$proximaDt = strtotime('+1 day', $proximaDt);
						}
					}
				}
				$agenda[ $row->getCurso() ][] = [
					'data'     => Conversao::formatarDataBR($row->getData()),
					'esgotado' => $row->getInscritos() >= $row->getVagas(),
				];
				$t = count($agenda[ $row->getCurso() ]);
				if ($t > $tamanho)
				{
					$tamanho = $t;
				}
				$ultimaData[ $row->getCurso() ] = strtotime($row->getData());
			}
			$tamanho = $tamanho < 6 ? 6 : $tamanho;
			foreach ($agenda as $curso => $row)
			{
				$t = count($agenda[ $curso ]);
				$ultimaDt = $ultimaData[ $curso ];
				$proximaDt = strtotime('+1 day', $ultimaDt);
				while ($t < $tamanho)
				{
					$agenda[ $curso ][] = [
						'data'     => date('d/m/Y', $proximaDt),
						'esgotado' => null,
					];
					$proximaDt = strtotime('+1 day', $proximaDt);
					$t++;
				}
			}
			$data['agenda'] = $agenda;
		}
		if ($data['pagina'] == "detalhes")
		{
			$data['page'] = (new SysPagesEntidade())->getRow($this->db->where('pagina', 'curso-detalhes'));
			$data['item'] = (new SiteCursosEntidade())->getRow($this->db->where('id', $data['id']));
			$data['agenda'] = (new SiteAgendaEntidade())->getRows(
				$this->db
					->select('site_agenda.*, CONCAT(sys_cidades.titulo,"-",sys_cidades.sigla_uf) as cidade')
					->where('site_agenda.curso', $data['id'])
					->join('sys_cidades', 'site_agenda.cidade = sys_cidades.id')
					->where('site_agenda.data >=', date('Y-m-d'))
					->where('site_agenda.vagas > site_agenda.inscritos', null, true)
					->order_by('site_agenda.data', 'asc')
			);
		}
		if ($data['pagina'] == "detalhes_online")
		{
			$data['page'] = (new SysPagesEntidade())->getRow($this->db->where('pagina', 'curso-detalhes'));
			$data['item'] = (new SiteCursosOnlineEntidade())->getRow($this->db->where('id', $data['id']));
			$data['agenda'] = (new SiteAgendaEntidade())->getRows(
				$this->db
					->select('site_agenda.*, CONCAT(sys_cidades.titulo,"-",sys_cidades.sigla_uf) as cidade')
					->where('site_agenda.curso', $data['id'])
					->join('sys_cidades', 'site_agenda.cidade = sys_cidades.id')
					->where('site_agenda.data >=', date('Y-m-d'))
					->where('site_agenda.vagas > site_agenda.inscritos', null, true)
					->order_by('site_agenda.data', 'asc')
			);
			$aluno = (new SiteAreaDoClienteRN())->getUser();
			if ($aluno instanceof SiteClienteEntidade && $aluno->getId())
			{
				$data['form'] = (object)['email' => $aluno->getEmail(), 'action' => 'login'];
			}
		}
		if ($data['pagina'] == "agenda")
		{
			$data['page'] = (new SysPagesEntidade())->getRow($this->db->where('pagina', 'cursos-agenda'));
		}

		return parent::getModulo($viewName, $data, $admin, $minifyHtml, $viewDeAtualizacao); // TODO: Change the autogenerated stub
	}

	/**
	 * @param stdClass $form
	 * @param bool     $cadastro
	 * @return SiteAgendaEntidade
	 * @throws Exception
	 */
	public function validateForm(stdClass $form, $cadastro = true)
	{
		if (!isset($form->curso) || empty($form->curso))
		{
			throw new Exception('Curso não informado!');
		}
		$curso = (new SiteCursosEntidade())->getRow($this->db->where('id', $form->curso));
		if (!$curso->getId())
		{
			throw new Exception('Curso não encontrado!');
		}
		if (!isset($form->dia) || empty($form->dia))
		{
			throw new Exception('Dia não informado!');
		}
		$agenda = (new SiteAgendaEntidade())->getRow($this->db->where('id', $form->dia));
		if (!$agenda->getId())
		{
			throw new Exception('Data não encontrada para o curso!');
		}
		if ((int)$agenda->getInscritos() >= (int)$agenda->getVagas())
		{
			throw new Exception('Excedido o número de vagas para essa data!');
		}
		if ($cadastro)
		{
			if (!isset($form->nome) || empty($form->nome))
			{
				throw new Exception('Nome não informado!');
			}
			if (!isset($form->cpf) || empty($form->cpf))
			{
				throw new Exception('CPF não informado!');
			}
			if (!isset($form->telefone) || empty($form->telefone))
			{
				throw new Exception('Telefone ou Whatsapp não informado!');
			}
			if (!isset($form->email) || empty($form->email))
			{
				throw new Exception('E-mail não informado!');
			}
			if (!isset($form->senha) || empty($form->senha))
			{
				throw new Exception('Senha não informada!');
			}
			if (!isset($form->senha2) || ($form->senha != $form->senha2))
			{
				throw new Exception('As senhas não são iguais!');
			}
		}

		return $agenda;
	}

	/**
	 * @param stdClass $form
	 * @param bool     $cadastro
	 * @throws Exception
	 */
	public function validateFormOnline(stdClass $form, $cadastro = true)
	{
		if (!isset($form->curso) || empty($form->curso))
		{
			throw new Exception('Curso não informado!');
		}
		$curso = (new SiteCursosOnlineEntidade())->getRow($this->db->where('id', $form->curso));
		if (!$curso->getId())
		{
			throw new Exception('Curso não encontrado!');
		}
		if (!isset($form->presencial))
		{
			throw new Exception('Tipo do curso não informado!');
		}
		if ($cadastro)
		{
			if (!isset($form->nome) || empty($form->nome))
			{
				throw new Exception('Nome não informado!');
			}
			if (!isset($form->cpf) || empty($form->cpf))
			{
				throw new Exception('CPF não informado!');
			}
			if (!isset($form->telefone) || empty($form->telefone))
			{
				throw new Exception('Telefone ou Whatsapp não informado!');
			}
			if (!isset($form->email) || empty($form->email))
			{
				throw new Exception('E-mail não informado!');
			}
			if (!isset($form->senha) || empty($form->senha))
			{
				throw new Exception('Senha não informada!');
			}
			if (!isset($form->senha2) || ($form->senha != $form->senha2))
			{
				throw new Exception('As senhas não são iguais!');
			}
		}
	}

	/**
	 * @param stdClass $form
	 * @return SiteAlunosEntidade
	 */
	public function cadastroAluno(stdClass $form)
	{
		$aluno = (new SiteAlunosEntidade())->getRow($this->db->where('cpf', Conversao::somenteNumeros($form->cpf)));
		if ($aluno->getId()) return $aluno;

		$id = $aluno->setNome($form->nome)
			->setCpf(Conversao::somenteNumeros($form->cpf))
			->setTelefone(Conversao::somenteNumeros($form->telefone))
			->setEmail($form->email)
			->setAtivo(1)
			->insert();

		return (new SiteAlunosEntidade())->getRow($this->db->where('id', $id));
	}

	/**
	 * @param stdClass $form
	 * @return SiteAlunosEntidade
	 * @throws Exception
	 */
	public function cadastroAlunoOnline(stdClass $form)
	{
		$aluno = (new SiteAlunosEntidade())->getRow($this->db->where('cpf', Conversao::somenteNumeros($form->cpf)));
		if ($aluno->getId())
		{
			throw new Exception('Já existe um cadastro com esse CPF!');
		}
		$aluno = (new SiteAlunosEntidade())->getRow($this->db->where('email', $form->email));
		if ($aluno->getId())
		{
			throw new Exception('Já existe um cadastro com esse E-mail!');
		}

		$id = $aluno->setNome($form->nome)
			->setCpf(Conversao::somenteNumeros($form->cpf))
			->setTelefone(Conversao::somenteNumeros($form->telefone))
			->setEmail($form->email)
			->setSenha(sha1($form->senha))
			->setAtivo(1)
			->insert();

		return (new SiteAlunosEntidade())->getRow($this->db->where('id', $id));
	}

	/***
	 * @param stdClass $form
	 * @return SiteAlunosEntidade
	 * @throws Exception
	 */
	public function loginAlunoOnline(stdClass $form)
	{
		$rn = new SiteAreaDoClienteRN();
		$user = $rn->searchUser($form->email, $form->senha);
		if (!$user->getId())
		{
			throw new Exception('Usuário e/ou senha inválidos');
		}
		$rn->login($user, false);

		return $user;
	}

	/**
	 * @param SiteAlunosEntidade $aluno
	 * @param SiteAgendaEntidade $agenda
	 * @throws Exception
	 */
	public function mastricular(SiteAlunosEntidade $aluno, SiteAgendaEntidade $agenda)
	{
		$id = (new SiteMatriculasEntidade())
			->setAluno($aluno->getId())
			->setCurso($agenda->getCurso())
			->setData($agenda->getId())
			->setCidade($agenda->getCidade())
			->insert();
		if (!$id)
		{
			throw  new Exception('Ocorreu um erro ao realizar a matrícula. Entre em contato conosco!.');
		}
	}

	/**
	 * @param SiteAlunosEntidade $aluno
	 * @param                    $curso
	 * @param int                $presencial
	 * @throws Exception
	 */
	public function matricular_online(SiteAlunosEntidade $aluno, $curso, $presencial = 0)
	{
//		$matricula = (new SiteMatriculasOnlineEntidade())->getRow($this->db->where('aluno', $aluno->getId())->where('curso', $curso));
//		if ($matricula->getId())
//		{
//			throw new Exception('Você já está matriculado para este curso!');
//		}

		$id = (new SiteMatriculasOnlineEntidade())
			->setAluno($aluno->getId())
			->setCurso($curso)
			->setData(date('Y-m-d'))
			->setPresencial((int)$presencial)
			->setConfirmado(0)
			->setPago(0)
			->setSituacaoPagamento('Aguardando pagamento.')
			->insert();

		if (!$id)
		{
			throw  new Exception('Ocorreu um erro ao realizar a matrícula. Entre em contato conosco!.');
		}
	}
}
