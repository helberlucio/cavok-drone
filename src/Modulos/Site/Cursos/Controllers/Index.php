<?php

namespace Modulos\Site\Cursos\Controllers;

use Aplicacao\Url;
use Exception;
use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;
use Modulos\Site\AreaDoCliente\Models\RegrasDeNegocio\SiteAreaDoClienteRN;
use Modulos\Site\AreaDoCliente\Models\Repositorios\SiteAreaDoClienteRP;
use Modulos\Site\Cursos\Models\RegrasDeNegocio\SiteCursosRN;
use Modulos\Site\Cursos\Models\Repositorios\SiteCursosRP;
use stdClass;

class Index extends ControladorPadraoAbsHelp
{

	public $admin = false;

	public function index($view = 'index', $data = [], $admin = false)
	{
		$view = 'Home/index';
		$data['pagina'] = 'index';

		echo SiteCursosRP::getIndexPage($view, $data, $admin);
	}

	public function agenda($view = 'index', $data = [], $admin = false)
	{
		$view = 'Home/index';
		$data['pagina'] = 'agenda';

		echo SiteCursosRP::getIndexPage($view, $data, $admin);
	}

	public function detalhes($view = 'index', $data = [], $admin = false)
	{
		if (input_post())
		{
			$data['form'] = (object)input_post();
			$data['mensagem'] = $this->matricula($data['form']);
		}

		$view = 'Home/index';
		$data['pagina'] = 'detalhes';
		$data['id'] = input_get('id');
		$data['form'] = $data['form'] ?? (new stdClass());

		echo SiteCursosRP::getIndexPage($view, $data, $admin);
	}

	public function detalhes_online($view = 'index', $data = [], $admin = false)
	{
		if (input_post())
		{
			$data['form'] = (object)input_post();
			$data['mensagem'] = $this->matricula_online($data['form']);
		}

		$view = 'Home/index';
		$data['pagina'] = 'detalhes_online';
		$data['id'] = input_get('id');
		$data['form'] = $data['form'] ?? (new stdClass());

		echo SiteCursosRP::getIndexPage($view, $data, $admin);
	}

	private function matricula_online(stdClass &$form)
	{
		try
		{
			$rn = new SiteCursosRN();
			if ($form->action == 'login')
			{
				$rn->validateFormOnline($form, false);
				$aluno = $rn->loginAlunoOnline($form);
				$rn->matricular_online($aluno, $form->curso, $form->presencial);
			}
			if ($form->action == 'cadastro')
			{
				$rn->validateFormOnline($form);
				$aluno = $rn->cadastroAlunoOnline($form);
				(new SiteAreaDoClienteRN())->login($aluno, false);
				$rn->matricular_online($aluno, $form->curso, $form->presencial);
			}

			return [
				'sucesso'  => true,
				'mensagem' => 'Matrícula realizada com sucesso. Aguarde confirmação!',
				'redirect' => Url::ModuloApelido(
					SiteAreaDoClienteRP::class,
					'Admin/matriculas?menu=painel-do-aluno',
					'matriculas',
					['painel-do-aluno']
				),
			];
		} catch (Exception $ex)
		{
			return [
				'sucesso'  => false,
				'mensagem' => $ex->getMessage(),
			];
		}
	}

	public function matricula(stdClass $form)
	{
		try
		{
			$rn = new SiteCursosRN();
			if ($form->action == 'login')
			{
				$agenda = $rn->validateForm($form, false);
				$aluno = $rn->loginAlunoOnline($form);
				$rn->mastricular($aluno, $agenda);
			}
			if ($form->action == 'cadastro')
			{
				$agenda = $rn->validateForm($form);
				$aluno = $rn->cadastroAlunoOnline($form);
				(new SiteAreaDoClienteRN())->login($aluno, false);
				$rn->mastricular($aluno, $agenda);
			}

			return [
				'sucesso'  => true,
				'mensagem' => 'Matrícula realizada com sucesso. Aguarde confirmação!',
			];
		} catch (Exception $ex)
		{
			return [
				'sucesso'  => false,
				'mensagem' => $ex->getMessage(),
			];
		}
	}
}