<?php

namespace Modulos\Site\Cursos\Controllers;

use Exception;
use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursoConteudoEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursosOnlineEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteMatriculasOnlineEntidade;
use Modulos\Site\AreaDoCliente\Models\RegrasDeNegocio\SiteAreaDoClienteRN;
use Modulos\Site\Cursos\Models\Repositorios\SiteCursosRP;

class Online extends ControladorPadraoAbsHelp
{
	public function index($view = 'index', $data = [], $admin = false)
	{
		try
		{
			$aluno = (new SiteAreaDoClienteRN())->getUser();
			if (!$aluno->getId())
			{
				throw new Exception('Nenhum aluno logado!');
			}

			$id = input_get('c');
			$pagina = input_get('p');

			if (!$id)
			{
				throw new Exception('Curso não informado!');
			}

			$curso = (new SiteCursosOnlineEntidade())->getRow($this->db->where('id', $id));
			if (!$curso->getId())
			{
				throw new Exception('Curso não encontrado!');
			}

			$matricula = (new SiteMatriculasOnlineEntidade())->getRow($this->db->where('curso', $curso->getId())->where('aluno', $aluno->getId()));
			if (!$matricula->getId())
			{
				throw new Exception('Aluno não matriculado no curso.');
			}
			if (!$matricula->getConfirmado())
			{
				throw new Exception('Aluno matriculado mas não confirmado.');
			}
			if (!$matricula->getPago())
			{
				throw new Exception("Aluno matriculado e confirmado.<br/><small>Situação: {$matricula->getSituacaoPagamento()}</small>");
			}

			$pagina = $pagina ? $pagina : 1;

			$data['pa'] = $pagina == 1 ? 0 : ($pagina - 1);
			$data['p'] = $pagina;
			$data['conteudo'] = (new SiteCursoConteudoEntidade())->getRow($this->db->where('curso', $curso->getId())->where('ordem', $pagina));
			$proximo = (new SiteCursoConteudoEntidade())->getRow($this->db->where('curso', $curso->getId())->where('ordem', $pagina + 1));
			$data['pp'] = $proximo->getOrdem() ? $proximo->getOrdem() : 0;
			$data['curso'] = $curso;
		} catch (Exception $ex)
		{
			$data['mensagem'] = $ex->getMessage();
		}

		$view = 'Home/index';
		$data['pagina'] = 'online';

		echo SiteCursosRP::getIndexPage($view, $data, $admin);
	}
}