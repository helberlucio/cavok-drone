<?php

namespace Modulos\Site\Footer\Controllers;

use Aplicacao\Ferramentas\Response;
use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteServidorEmailEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysCidadesEntidade;
use Modulos\Envio\Email\Models\RegrasDeNegocio\EnvioMailer;
use Modulos\Site\Footer\Models\Repositorios\SiteFooterRP;

class Index extends ControladorPadraoAbsHelp {

    public $admin = false;

    public function index($view = 'index', $data = [], $admin = false) {
        echo SiteFooterRP::getIndexPage($view, $data, $admin);
    }

    public function enviar() {
        $nome = input_post('nome');
        $telefone = input_post('telefone');
        $email = input_post('email');
        $cidade_id = input_post('cidade');
        $mensagem = input_post('mensagem');

        $cidade = (new SysCidadesEntidade())->getRow($this->db->where('id', $cidade_id));

        $servidor = (new SiteServidorEmailEntidade())->getRow($this->db);

        $to = $servidor->getEmailDestinatario();
        $subject = "[Envie-nos uma mensagem] " . $nome;

        $EnvioMailer = new EnvioMailer();

        $result = $EnvioMailer->enviarEmailSMTP([
            'seguranca' => $servidor->getSeguranca(),
            'host'      => $servidor->getHost(),
            'porta'     => $servidor->getPorta(),
            'username'  => $servidor->getUsername(),
            'password'  => $servidor->getPassword(),
            'from'      => $servidor->getFrom(),
            'nome_from' => $servidor->getFromName(),
            'reply'     => $servidor->getReply(),
        ], $to,
            '',
            $subject,
            nl2br("Nome: {$nome}" . PHP_EOL .
                "Telefone: {$telefone}" . PHP_EOL .
                "E-mail: {$email}" . PHP_EOL .
                "Cidade: {$cidade->getTitulo()} / {$cidade->getSiglaUf()}" . PHP_EOL .
                "Mensagem: {$mensagem}")
        );

        Response::json($result);
    }
}