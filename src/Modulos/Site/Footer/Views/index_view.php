<?php

/**
 * @var WebsiteGeralEntidade         $configuracaoGeral
// * @var SiteCursosEntidade[]         $cursos
 * @var SiteCursosOnlineEntidade[]   $cursos_online
 * @var SiteFormaPagamentoEntidade[] $fp
 */

use Aplicacao\Conversao;
use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursosEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursosOnlineEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteFormaPagamentoEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\WebsiteGeralEntidade;
use Modulos\Site\Cursos\Models\Repositorios\SiteCursosRP;

?>
<div class="contact-whatsapp">
    <a target="_blank" href="https://wa.me/55<?= Conversao::somenteNumeros($configuracaoGeral->getWhatsapp()) ?>"
       class="pulse">
        <i class="fab fa-whatsapp hvr-icon"></i>
    </a>
</div>
<footer id="Footer" class="mt-auto bg-black text-white">
    <div class="container">
        <div class="row">
            <div class="col-md-4 mb-3">
                <h5 class="text-primary">Cursos</h5>
				<?php foreach ($cursos_online as $curso): ?>
                    <a href="<?= Url::ModuloApelido(
						SiteCursosRP::class,
						"Index/detalhes_online?menu=cursos&id={$curso->getId()}",
						Url::Title($curso->getNome()),
						['curso-online']) ?>"
                       class="text-white text-decoration-none"><?= $curso->getNome() ?></a><br>
				<?php endforeach; ?>
            </div>
            <div class="col-md-3 mb-3">
                <h5 class="text-primary">Contato</h5>
                <a target="_blank"
                   href="https://wa.me/55<?= Conversao::somenteNumeros($configuracaoGeral->getWhatsapp()) ?>"
                   class="text-white text-decoration-none"><?= $configuracaoGeral->getWhatsapp() ?></a>
            </div>
            <div class="col-md-5 mb-3">
                <h5 class="text-primary">Formas de pagamento</h5>
                <div class="fp"><?php foreach ($fp as $item): ?><img
                        src="<?= Url::Base($item->getImagem()) ?>"><?php endforeach; ?></div>
            </div>
            <div class="col-md-4 mb-3">
                <h5 class="text-primary">Horário</h5>
                <p><?= nl2br($configuracaoGeral->getHorario()) ?></p>
            </div>
            <div class="col-md-3 mb-3">
                <h5 class="text-primary">Endereço</h5>
                <p><?= nl2br($configuracaoGeral->getEndereco()) ?></p>
            </div>
            <div class="col-md-5 mb-3">
                <h5 class="text-primary">Siga-nos nas redes sociais</h5>
                <a class="social text-decoration-none" href="<?= $configuracaoGeral->getFacebook() ?>" target="_blank">
                    <img src="<?= Url::Gerenciador('Footer/cavok-drone-service-facebook.png') ?>">
                </a>
                <a class="social text-decoration-none" href="<?= $configuracaoGeral->getInstagram() ?>" target="_blank">
                    <img src="<?= Url::Gerenciador('Footer/cavok-drone-service-instagram.png') ?>">
                </a>
                <a class="social text-decoration-none" href="<?= $configuracaoGeral->getYoutube() ?>" target="_blank">
                    <img src="<?= Url::Gerenciador('Footer/cavok-drone-service-youtube.png') ?>">
                </a>
            </div>
        </div>
    </div>
    <div class="footer-brand bg-primary text-white">
        <div class="container text-white">
            <div class="row align-itens-center">
                <div class="col py-3"><?= date('Y') ?> © <?= $configuracaoGeral->getEmpresa() ?>. Todos os direitos
                    reservados
                </div>
                <div class="col py-2 text-right">
                    <a href="https://ligadoscomunicacao.com.br" class="text-white text-decoration-none" target="_blank">
                        Desenvolvido por: <img src="<?= Url::Gerenciador("Footer/ligados.png") ?>"
                                               alt="Ligados Comunicação">
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>