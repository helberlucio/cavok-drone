<?php
/**
 * @var SiteHomeSaibaMaisEntidade[] $items
 * @var SiteCursosOnlineEntidade[]  $cursos_online
 */

use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursosOnlineEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteHomeSaibaMaisEntidade;
use Modulos\Site\Cursos\Models\Repositorios\SiteCursosRP;

?>
    <div class="mt-5 container-fluid">
        <div class="saiba-mais row">
			<?php foreach ($items as $item): ?>
                <div class="pb-5 pt-5 col-md-<?= ceil(12 / count($items)) ?>"
                     style="background-image: url('<?= Url::Base($item->getImagem()) ?>')">
                    <div class="item text-white">
                        <h1 class="mb-4 pl-4 pr-4"><?= nl2br($item->getTitulo()) ?></h1>
                        <hr>
                        <p class="mb-4"><?= nl2br($item->getDescricao()) ?></p>
                        <div class="text-center">
							<?php if (!empty($item->getCursoOnline())): ?>
                                <a href="<?= Url::ModuloApelido(
									SiteCursosRP::class,
									"Index/detalhes_online?menu=cursos&id={$item->getCursoOnline()}",
									Url::Title($cursos_online[ $item->getCursoOnline() ]->getNome()),
									['curso-online']) ?>"
                                   class="btn btn-outline-light pl-5 pr-5 pt-3 pb-3">
                                    Saiba mais
                                </a>
							<?php elseif (!empty($item->getUrl())): ?>
                                <a href="<?= $item->getUrl() ?>" class="btn btn-outline-light pl-5 pr-5 pt-3 pb-3">
									<?= $item->getBotao() ?>
                                </a>
							<?php endif; ?>
                        </div>
                    </div>
                </div>
			<?php endforeach; ?>
        </div>
    </div>
<?php foreach ($cursos_online as $item): ?>
    <a href="<?= Url::ModuloApelido(
		SiteCursosRP::class,
		"Index/detalhes_online?menu=cursos&id={$item->getId()}",
		Url::Title($item->getNome()),
		['curso-online']) ?>"
       style="display: none"></a>
<?php endforeach; ?>