<?php
/**
 * @var SiteHomeBemVindoEntidade $item
 */

use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteHomeBemVindoEntidade;

?>
<div class="bem-vindo mt-5">
    <div class="missao" style="background-image: url('<?= Url::Base($item->getImgMissao()) ?>')">
        <img class="d-block d-md-none img-fluid" src="<?= Url::Base($item->getImgMissao()) ?>" alt="Missão">
        <div class="container">
            <div class="row">
                <div class="col-md-6 pt-5 pb-5">
                    <h1><?= $item->getTituloMissao() ?></h1>
                    <p class="mb-4"><?= nl2br($item->getTextoMissao()) ?></p>
                    <div class="text-center">
                        <a href="<?= $item->getUrlMissao() ?>"
                           class="btn btn-outline btn-outline-light pl-5 pr-5">Saiba +</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cursos" style="background-image: url('<?= Url::Base($item->getImgCursos()) ?>')">
        <img class="d-block d-md-none img-fluid" src="<?= Url::Base($item->getImgCursos()) ?>" alt="Cursos">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 offset-md-6 pt-5 pb-5">
                    <h1><?= $item->getTituloCursos() ?></h1>
                    <p class="mb-4"><?= nl2br($item->getTextoCursos()) ?></p>
                    <div class="text-center">
                        <a href="<?= $item->getUrlCursos() ?>"
                           class="btn btn-primary pl-5 pr-5">Confira nossos cursos</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>