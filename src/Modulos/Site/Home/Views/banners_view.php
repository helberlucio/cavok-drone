<?php
/**
 * @var SiteBannerEntidade[] $items
 */

use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteBannerEntidade;

?>
<div id="home-banners" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <?php foreach ($items as $key => $item): ?>
            <li data-target="#home-banners" data-slide-to="<?= $key ?>" class="<?= !$key ? 'active' : '' ?>"></li>
        <?php endforeach; ?>
    </ol>
    <div class="carousel-inner">
        <?php foreach ($items as $key => $item): ?>
            <div class="carousel-item <?= !$key ? 'active' : '' ?>">
                <?php if (!empty($item->getUrl())): ?>
                <a href="<?= $item->getUrl() ?>" <?= $item->getUrlExterna() ? 'target="_blank"' : '' ?>>
                    <?php endif; ?>
                    <img src="<?= Url::Base($item->getImagem()) ?>" class="d-none d-sm-block w-100">
                    <img src="<?= Url::Base($item->getImagemMobile()) ?>" class="d-block d-sm-none w-100">
                    <?php if (!empty($item->getUrl())): ?>
                </a>
            <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
    <a class="carousel-control-prev" href="#home-banners" role="button" data-slide="prev">
        <div class="wrapper">
            <img src="<?= Url::Gerenciador('Home/arrow-left.png') ?>">
        </div>
    </a>
    <a class="carousel-control-next" href="#home-banners" role="button" data-slide="next">
        <div class="wrapper">
            <img src="<?= Url::Gerenciador('Home/arrow-right.png') ?>">
        </div>
    </a>
</div>