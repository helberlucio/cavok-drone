<?php
/**
 * @var SiteHomeSolucoesEntidade[] $items
 */

use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteHomeSolucoesEntidade;

?>
<div class="container mt-5">
    <h5 class="text-info text-center mb-5">Nós temos a solução certa para atendê-lo.</h5>
    <div class="solucoes row">
        <?php foreach ($items as $item): ?>
            <div class="item text-center col-md-<?= ceil(12 / count($items)) ?>">
                <img src="<?= Url::Base($item->getImagem()) ?>" class="mb-4">
                <h5 class="text-primary mb-4"><?= $item->getTitulo() ?></h5>
                <p><?= nl2br($item->getDescricao()) ?></p>
                <a href="<?= $item->getUrl() ?>" class="btn btn-outline-info pl-5 pr-5 mb-5">
                    Saiba +
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>