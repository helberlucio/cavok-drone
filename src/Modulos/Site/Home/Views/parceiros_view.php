<?php
/**
 * @var SiteHomeParceirosEntidade[] $items
 */

use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteHomeParceirosEntidade;

?>
<div class="my-5 container">
    <h5 class="text-primary text-center mb-4">Confira alguns de nossos parceiros</h5>
    <div class="parceiros owl-carousel owl-theme">
        <?php foreach ($items as $item): ?>
            <a href="<?= $item->getUrl() ?>" target="_blank" class="item text-center">
                <img class="border border-gray rounded" src="<?= Url::Base($item->getImagem()) ?>" alt="">
            </a>
        <?php endforeach; ?>
    </div>
</div>

<script>
    $(function () {
        $('.parceiros').owlCarousel({
            loop: false,
            margin: 10,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 2,
                    nav: false,
                    dots: true
                },
                600: {
                    items: 3,
                    nav: false,
                    dots: true
                },
                1000: {
                    items: 5,
                    nav: false,
                    dots: true
                }
            }
        });
    });
</script>