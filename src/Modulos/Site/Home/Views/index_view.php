<?php


use Modulos\Site\Home\Models\Repositorios\SiteHomeRP;
use Modulos\Site\LigamosParaVoce\Models\Repositorios\SiteLigamosParaVoceRP;
use Modulos\Site\Planos\Models\Repositorios\SitePlanosRP;

?>
<?= SiteHomeRP::getIndexPage('banners'); ?>
<?= SiteHomeRP::getIndexPage('solucoes'); ?>
<?= SiteHomeRP::getIndexPage('bem_vindo'); ?>
<?= SiteHomeRP::getIndexPage('saiba_mais'); ?>
<?= SiteHomeRP::getIndexPage('parceiros'); ?>
