<?php
namespace Modulos\Site\Home\Controllers;

use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;
use Modulos\Site\Home\Models\Repositorios\SiteHomeRP;

class Index extends ControladorPadraoAbsHelp{

    public $admin = false;

    public function index($view = 'index', $data = array(), $admin = false){
        echo SiteHomeRP::getIndexPage($view, $data, $admin);
    }
}