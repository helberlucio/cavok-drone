<?php
/**
 * @var WebsiteGeralEntidade $configuracaoGeral
 * @var string               $mapa
 * @var SysPagesEntidade     $page
 * @var string               $nome
 * @var string               $email
 * @var string               $telefone
 * @var string               $whatsapp
 */

use Aplicacao\Conversao;
use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysPagesEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\WebsiteGeralEntidade;

?>
<div class="formulario mt-5 mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="telefone mb-5">
                    <img src="<?= Url::Gerenciador('FaleConosco/telefone.png') ?>" class="mb-3 mr-3 float-left">
                    <h5 class="text-primary mb-2">Fale conosco via telefone</h5>
                    <p class="mask-telefone text-muted"><?= $configuracaoGeral->getContatoTelefone() ?></p>
                </div>
                <a class="whatsapp mb-5 d-block text-decoration-none" target="_blank"
                   href="https://wa.me/55<?= Conversao::somenteNumeros($configuracaoGeral->getWhatsapp()) ?>">
                    <img src="<?= Url::Gerenciador('FaleConosco/whatsapp.png') ?>" class="mb-3 mr-3 float-left">
                    <h5 class="text-primary mb-2">Fale conosco via Whatsapp</h5>
                    <p class="mask-telefone text-muted"><?= $configuracaoGeral->getWhatsapp() ?></p>
                </a>
                <div class="endereco mb-5">
                    <img src="<?= Url::Gerenciador('FaleConosco/endereco.png') ?>" class="mb-5 mr-3 float-left">
                    <h5 class="text-primary mb-2">Nosso Endereço</h5>
                    <p class="text-muted"><?= nl2br($configuracaoGeral->getEndereco()) ?></p>
                </div>
            </div>
            <div class="col-md-7">
                <h5 class="text-primary mb-4"><?= $page->getTitulo() ?></h5>
                <form id="form-fale-conosco" method="post" action="">
                    <div class="form-group">
                        <input type="text" class="form-control" name="nome" placeholder="Seu nome"
                               value="<?= isset($nome) ? $nome : '' ?>"
                               required>
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control mask-telefone" name="telefone"
                               value="<?= isset($telefone) ? $telefone : '' ?>"
                               placeholder="Telefone/Whatsapp"
                               required>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" placeholder="Seu e-mail"
                               value="<?= isset($email) ? $email : '' ?>"
                               required>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="mensagem" placeholder="Escreva sua mensagem aqui"
                                  required></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Enviar mensagem</button>
                    <?php if (isset($form)): ?>
                        <div class="row justify-content-center mb-3 mt-3">
                            <div class="col-md-12">
                                <?php if ($form['sucesso']): ?>
                                    <div class="alert alert-success text-center">
                                        E-mail enviado com sucesso!
                                    </div>
                                <?php else: ?>
                                    <div class="alert alert-danger text-center">
                                        Ocorreu um erro ao enviar o e-mail. Tente novamente mais tarde ou entre em
                                        contato conosco via telefone/whatsaap.
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </form>
            </div>
        </div>
    </div>
</div>