<?php

namespace Modulos\Site\FaleConosco\Controllers;

use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;
use Modulos\Site\AreaDoCliente\Models\RegrasDeNegocio\SiteAreaDoClienteRN;
use Modulos\Site\FaleConosco\Models\RegrasDeNegocio\SiteFaleConoscoRN;
use Modulos\Site\FaleConosco\Models\Repositorios\SiteFaleConoscoRP;

class Index extends ControladorPadraoAbsHelp {

    public $admin = false;

    public function index($view = 'index', $data = [], $admin = false) {
        $view = 'Home/index';
        $data['pagina'] = 'index';

        if (!empty(input_post())) {
            $data['form'] = (new SiteFaleConoscoRN())->enviar(
                input_post('nome'),
                input_post('telefone'),
                input_post('email'),
                input_post('mensagem')
            );
        }

        echo SiteFaleConoscoRP::getIndexPage($view, $data, $admin);
    }
}