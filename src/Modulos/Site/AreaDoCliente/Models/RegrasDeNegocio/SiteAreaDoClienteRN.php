<?php

namespace Modulos\Site\AreaDoCliente\Models\RegrasDeNegocio;

use Aplicacao\Url;
use ModuloHelp\PadraoCriacao\ModelRegraDeNegocio\Abstracts\ModelRegraDeNegocioPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteAlunosEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteClienteEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SitePessoaFisicaEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysPagesEntidade;
use Modulos\Site\AreaDoCliente\Models\Repositorios\SiteAreaDoClienteRP;

class SiteAreaDoClienteRN extends ModelRegraDeNegocioPadraoAbsHelp
{

	private static $SESSION_NAME = 'site_area_cliente';

	public function getModulo($viewName = "index", $data = [], $admin = false, $minifyHtml = true, $viewDeAtualizacao = false)
	{
		$data['page'] = (new SysPagesEntidade())->getRow($this->db->where('pagina', "area-do-cliente"));

		return parent::getModulo($viewName, $data, $admin, $minifyHtml, $viewDeAtualizacao); // TODO: Change the autogenerated stub
	}

	public function perfil(&$data) {
		$post = (object)input_post();
		$user = $this->getUser();
		if (sha1($post->senha) != $user->getSenha()) {
			$data['mensagem'] = ['tipo' => 'danger', 'texto' => 'Senha atual inválida!'];
		} else {
			$post->telefone = preg_replace('/\D/', '', $post->telefone);

			$user = (new SiteAlunosEntidade())->getRow($this->db->where('id', $user->getId()));
			$user->setNome($post->nome)
				->setTelefone($post->telefone);
			$user->update();
			$this->update($user);
			$data['mensagem'] = ['tipo' => 'success', 'texto' => 'Perfil atualizado com sucesso!'];
		}

		return $user;
	}

	public function senha(&$data) {
		$post = (object)input_post();
		$user = $this->getUser();
		if (sha1($post->senha) != $user->getSenha()) {
			$data['mensagem'] = ['tipo' => 'danger', 'texto' => 'Senha atual inválida!'];
		} else {
			$user = (new SiteAlunosEntidade())->getRow($this->db->where('id', $user->getId()));
			$user->setSenha(sha1($post->novasenha))->update();
			$this->update($user);
			$data['mensagem'] = ['tipo' => 'success', 'texto' => 'Senha atualizada com sucesso!'];
		}
	}

	public function update($user) {
		$_SESSION[ self::$SESSION_NAME ] = serialize($user);
	}

	public function logout()
	{
		unset($_SESSION[ self::$SESSION_NAME ]);
	}

	public function login(SiteAlunosEntidade $user, $redirect = true)
	{
		$_SESSION[ self::$SESSION_NAME ] = serialize($user);
		if ($redirect)
		{
			Url::Redirecionar(Url::ModuloApelido(SiteAreaDoClienteRP::class, 'Index?menu=painel-do-aluno', 'painel-do-aluno'));
		}
	}

	/**
	 * @return SiteClienteEntidade
	 */
	public function getUser()
	{
		return isset($_SESSION[ self::$SESSION_NAME ]) ? unserialize($_SESSION[ self::$SESSION_NAME ]) : (new SiteClienteEntidade());
	}

	public static function isLogged()
	{
		return isset($_SESSION[ self::$SESSION_NAME ]) && !empty($_SESSION[ self::$SESSION_NAME ]);
	}

	public function checkLogin()
	{
		if (!self::isLogged())
		{
			Url::Redirecionar(Url::ModuloApelido(SiteAreaDoClienteRP::class, 'Login?menu=painel-do-aluno', 'painel-do-aluno-login'));
		}

		return $this;
	}

	/**
	 * @param $email
	 * @param $senha
	 * @return SiteAlunosEntidade
	 */
	public function searchUser($email, $senha)
	{
		return (new SiteAlunosEntidade())->getRow($this->db->where('email', $email)->where('senha', sha1($senha)));
	}
}
