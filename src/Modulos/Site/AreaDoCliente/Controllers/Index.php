<?php

namespace Modulos\Site\AreaDoCliente\Controllers;

use Aplicacao\Ferramentas\Response;
use Aplicacao\Url;
use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteServidorEmailEntidade;
use Modulos\Envio\Email\Models\RegrasDeNegocio\EnvioMailer;
use Modulos\Site\AreaDoCliente\Models\RegrasDeNegocio\SiteAreaDoClienteRN;
use Modulos\Site\AreaDoCliente\Models\Repositorios\SiteAreaDoClienteRP;

class Index extends ControladorPadraoAbsHelp
{

	public $admin = false;

	public function index($view = 'index', $data = [], $admin = false)
	{
		(new SiteAreaDoClienteRN())->checkLogin();

		$view = 'Home/index';
		$data['pagina'] = 'index';
		$data['option'] = input_get('option');

		echo SiteAreaDoClienteRP::getIndexPage($view, $data, $admin);
	}

	public function contrato()
	{
		$file_url = Url::Base(LI_DIR_GERENCIADOR . 'AreaDoCliente/contrato.pdf');
		$pdfname = 'agile-segunda-via-do-contrato-' . time() . '.pdf';
		header('Content-Type: application/pdf');
		header("Content-Transfer-Encoding: Binary");
		header("Content-disposition: attachment; filename=" . $pdfname);
		readfile($file_url);
	}

	public function segunda_via_boleto()
	{
		if (!SiteAreaDoClienteRN::isLogged())
		{
			Response::json(['redirect' => Url::ModuloApelido(SiteAreaDoClienteRP::class, 'Login?menu=painel-do-aluno&option=segunda-via', 'login', ['painel-do-aluno'])]);
		}

		$cliente = (new SiteAreaDoClienteRN())->getUser();

		$servidor = (new SiteServidorEmailEntidade())->getRow($this->db);

		$to = $servidor->getEmailDestinatario();
		$subject = "[Segunda via do boleto] " . $cliente->getNome();

		$EnvioMailer = new EnvioMailer();

		$result = $EnvioMailer->enviarEmailSMTP([
			'seguranca' => $servidor->getSeguranca(),
			'host'      => $servidor->getHost(),
			'porta'     => $servidor->getPorta(),
			'username'  => $servidor->getUsername(),
			'password'  => $servidor->getPassword(),
			'from'      => $servidor->getFrom(),
			'nome_from' => $servidor->getFromName(),
			'reply'     => $servidor->getReply(),
		], $to,
			'',
			$subject,
			nl2br("Pedido de segunda via de boleto por:" . PHP_EOL . PHP_EOL .
				"Nome: {$cliente->getNome()}" . PHP_EOL .
				"CPF: {$cliente->getCpf()}" . PHP_EOL .
				"E-mail: {$cliente->getEmail()}")
		);

		Response::json($result);
	}
}