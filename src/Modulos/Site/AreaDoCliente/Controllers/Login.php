<?php

namespace Modulos\Site\AreaDoCliente\Controllers;

use Aplicacao\Url;
use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteAlunosEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteClienteEntidade;
use Modulos\Site\AreaDoCliente\Models\RegrasDeNegocio\SiteAreaDoClienteRN;
use Modulos\Site\AreaDoCliente\Models\Repositorios\SiteAreaDoClienteRP;

class Login extends ControladorPadraoAbsHelp {

    public $admin = false;

    public function index($view = 'index', $data = [], $admin = false) {
        if (SiteAreaDoClienteRN::isLogged()) {
            Url::Redirecionar(Url::ModuloApelido(SiteAreaDoClienteRP::class, 'Index?menu=painel-do-aluno', 'painel-do-aluno'));
        }

        if (!empty(input_post())) {
            $this->login($data);
        }

        $view = 'Home/index';
        $data['pagina'] = 'login';

        echo SiteAreaDoClienteRP::getIndexPage($view, $data, $admin);
    }

    private function login(&$data) {
        $email = input_post('email');
        if (!$email) {
            $data['error'] = 'E-mail não informado!';

            return;
        }
        $senha = input_post('senha');
        if (!$email) {
            $data['error'] = 'Senha não informada!';

            return;
        }

        $aluno = (new SiteAlunosEntidade())->getRow($this->db
            ->where('email', $email)
            ->where('senha', sha1($senha))
        );

        if (!$aluno->getId()) {
            $data['error'] = 'E-mail ou Senha inválidos!';

            return;
        }

        (new SiteAreaDoClienteRN())->login($aluno);
        Url::Redirecionar(
            Url::ModuloApelido(
                SiteAreaDoClienteRP::class,
                'Admin?menu=painel-do-aluno',
                'painel-do-aluno'
            )
        );
    }

    public function logout() {
        (new SiteAreaDoClienteRN())->logout();
        Url::Redirecionar(Url::ModuloApelido(SiteAreaDoClienteRP::class, 'Login?menu=painel-do-aluno', 'login', ['painel-do-aluno']));
    }
}