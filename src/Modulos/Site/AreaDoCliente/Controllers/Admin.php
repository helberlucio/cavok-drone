<?php

namespace Modulos\Site\AreaDoCliente\Controllers;

use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursosEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursosOnlineEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteMatriculasOnlineEntidade;
use Modulos\Site\AreaDoCliente\Models\RegrasDeNegocio\SiteAreaDoClienteRN;
use Modulos\Site\AreaDoCliente\Models\Repositorios\SiteAreaDoClienteRP;

class Admin extends ControladorPadraoAbsHelp
{

	public $admin = false;

	public function index($view = 'index', $data = [], $admin = false)
	{
		$rn = new SiteAreaDoClienteRN();
		$rn->checkLogin();
		$user = $rn->getUser();

		if (input_post())
		{
			$user = $rn->perfil($data);
		}

		$view = 'Home/index';
		$data['submenu'] = 'index';
		$data['pagina'] = 'admin/index';
		$data['user'] = $user;

		echo SiteAreaDoClienteRP::getIndexPage($view, $data, $admin);
	}

	public function seguranca($view = 'index', $data = [], $admin = false)
	{
		$rn = new SiteAreaDoClienteRN();
		$rn->checkLogin();

		if (input_post())
		{
			$rn->senha($data);
		}

		$view = 'Home/index';
		$data['submenu'] = 'seguranca';
		$data['pagina'] = 'admin/seguranca';

		echo SiteAreaDoClienteRP::getIndexPage($view, $data, $admin);
	}

	public function matriculas($view = 'index', $data = [], $admin = false)
	{
		$rn = new SiteAreaDoClienteRN();
		$rn->checkLogin();
		$aluno = $rn->getUser();

//		$matriculas = (new SiteMatriculasEntidade())->getRows(
//			$this->db
//				->select("site_matriculas.id,
//				site_matriculas.aluno,
//				site_matriculas.curso,
//				CONCAT(site_agenda.data, ' ', site_agenda.horario) as data,
//				CONCAT(sys_cidades.titulo,' - ',sys_cidades.sigla_uf) as cidade,
//				site_matriculas.confirmado")
//				->join('site_agenda', 'site_agenda.id = site_matriculas.data')
//				->join('sys_cidades', 'site_agenda.cidade = sys_cidades.id')
//				->where('aluno', $aluno->getId())
//		);

//		/**    @var SiteCursosEntidade[] $cursos */
//		$cursos = (new SiteCursosEntidade())->getRows($this->db);
//		$cursosIndexed = [];
//		foreach ($cursos as $curso)
//		{
//			$cursosIndexed[ $curso->getId() ] = $curso;
//		}

		/**    @var SiteCursosOnlineEntidade[] $cursos_online */
		$matriculas_online = (new SiteMatriculasOnlineEntidade())->getRows($this->db->where('aluno', $aluno->getId()));
		$cursos_online = (new SiteCursosOnlineEntidade())->getRows($this->db);
		$cursosOnlineIndexed = [];
		foreach ($cursos_online as $curso)
		{
			$cursosOnlineIndexed[ $curso->getId() ] = $curso;
		}

//		$data['matriculas'] = $matriculas;
//		$data['cursos'] = $cursosIndexed;
		$data['matriculas_online'] = $matriculas_online;
		$data['cursos_online'] = $cursosOnlineIndexed;

		$view = 'Home/index';
		$data['submenu'] = 'matriculas';
		$data['pagina'] = 'admin/matriculas';

		echo SiteAreaDoClienteRP::getIndexPage($view, $data, $admin);
	}
}