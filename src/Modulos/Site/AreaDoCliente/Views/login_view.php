<?php
/**
 * @var SysPagesEntidade $page
 */

use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysPagesEntidade;
use Modulos\Site\AreaDoCliente\Models\Repositorios\SiteAreaDoClienteRP;

?>
<div id="login">
    <div class="container mt-5 mb-5">
        <h5 class="text-muted text-center mb-0"><?= $page->getTitulo() ?></h5>
        <h3 class="text-primary text-center mb-4"><?= $page->getDescricao() ?></h3>
        <div class="row justify-content-center">
            <div class="col-md-5">
                <form class="border rounded bg-white pl-5 pr-4 pt-4 pb-4" method="post"
                      action="<?= Url::ModuloApelido(SiteAreaDoClienteRP::class, 'Login?menu=painel-do-aluno', 'login', ['painel-do-aluno']) ?>">
                    <h4 class="text-primary text-center">Painel do Aluno</h4>
                    <br>
                    <div class="form-group">
                        <input type="email" class="form-control" id="input-email" name="email" placeholder="E-mail"
                               required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="input-senha" name="senha" placeholder="Senha"
                               required>
                    </div>
					<?php if (!empty($error)): ?>
                        <div class="alert alert-danger text-center"><?= $error ?></div>
					<?php endif; ?>
                    <button type="submit" class="btn btn-primary btn-block">Entrar</button>
                </form>
            </div>
        </div>
    </div>


</div>