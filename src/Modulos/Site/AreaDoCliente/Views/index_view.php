<?php
/**
 * @var \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysPagesEntidade $page
 */

use Aplicacao\Url;
use Modulos\Site\AreaDoCliente\Models\Repositorios\SiteAreaDoClienteRP;
use Modulos\Site\FaleConosco\Models\Repositorios\SiteFaleConoscoRP;

?>
<div id="painel" style="background-image: url('<?= $page->getBanner() ?>')">
    <div class="container mt-5 mb-5 pt-3 pb-3">
        <div class="row justify-content-center">
            <div class="col-md-3">
                <a class="d-block mb-3 bg-white p-4 text-center border-bottom border-primary"
                   onclick="SegundaViaBoleto()"
                   style="border-bottom-width: 4px !important; cursor: pointer">
                    <img src="<?= Url::Base(LI_DIR_GERENCIADOR . "AreaDoCliente/boleto.png") ?>">
                    <div class="text-decoration-none text-dark text-uppercase mt-3 text-weight-bold">2ª Via do Boleto
                    </div>
                </a>
            </div>
            <div class="col-md-3">
                <a class="d-block mb-3 bg-white p-4 text-center border-bottom border-primary"
                   href="<?= Url::Base("Modulo/Site/AreaDoCliente/Index/contrato") ?>"
                   style="border-bottom-width: 4px !important;">
                    <img src="<?= Url::Base(LI_DIR_GERENCIADOR . "AreaDoCliente/contrato.png") ?>">
                    <div class="text-decoration-none text-dark text-uppercase mt-3 text-weight-bold">2ª Via do
                        Contrato
                    </div>
                </a>
            </div>
            <div class="col-md-3">
                <a class="d-block mb-3 bg-white p-4 text-center border-bottom border-primary"
                   href="<?= Url::ModuloApelido(SiteFaleConoscoRP::class, 'Index?menu=contato&option=area-do-cliente', 'contato-area-do-cliente') ?>"
                   style="border-bottom-width: 4px !important;">
                    <img src="<?= Url::Base(LI_DIR_GERENCIADOR . "AreaDoCliente/atendimento.png") ?>">
                    <div class="text-decoration-none text-dark text-uppercase mt-3 text-weight-bold">Enviar mensagem
                    </div>
                </a>
            </div>
            <div class="col-md-3">
                <a class="d-block mb-3 bg-white p-4 text-center border-bottom border-primary"
                   href="<?= Url::ModuloApelido(SiteAreaDoClienteRP::class, 'Login/logout', 'area-do-cliente-sair') ?>"
                   style="border-bottom-width: 4px !important;">
                    <img src="<?= Url::Base(LI_DIR_GERENCIADOR . "AreaDoCliente/logout.png") ?>">
                    <div class="text-decoration-none text-dark text-uppercase mt-3 text-weight-bold">Sair</div>
                </a>
            </div>
        </div>
    </div>
</div>
<?php if (isset($option)): ?>
    <?php if ($option == 'segunda-via'): ?>
        <script>
            $(function () {
                SegundaViaBoleto();
            });
        </script>
    <?php endif; ?>
<?php endif; ?>
