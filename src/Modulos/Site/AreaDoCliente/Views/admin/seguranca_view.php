<?php

/**
 * @var object $cidade
 * @var object $tipo
 */

use Modulos\Site\AreaDoCliente\Models\Repositorios\SiteAreaDoClienteRP;

?>
<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group">
                <?= SiteAreaDoClienteRP::getIndexPage('admin/menu', ['submenu' => $submenu]); ?>
            </div>
        </div>
        <div class="col-md-9">
            <?php if (isset($mensagem)): ?>
                <div class="alert alert-<?= $mensagem['tipo'] ?> text-center">
                    <?= $mensagem['texto'] ?>
                </div>
            <?php endif; ?>
            <form method="post" id="form-seguranca">
                <div class="alert alert-warning text-center d-none">As senhas devem ser iguais!</div>
                <div class="border rounded">
                    <div class="pt-4 pl-4 pr-4">
                        <div class="form-group row">
                            <label for="input-senha" class="col-sm-3 col-form-label">Nova Senha</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="password" id="input-senha" name="novasenha"
                                       required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="input-senha2" class="col-sm-3 col-form-label">Repetir Nova Senha</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="password" id="input-senha2"
                                       required>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="pt-4 pl-4 pr-4">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Senha atual</label>
                            <div class="col-md-6 mb-3">
                                <input type="password" class="form-control" name="senha"
                                       required>
                            </div>
                            <div class="col-md-3">
                                <button class="btn btn-info btn-block">Atualizar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $("#form-seguranca").submit(function (event) {
        let s1 = $("#input-senha");
        let s2 = $("#input-senha2");
        let wa = $(this).find(".alert-warning");
        s1.removeClass('is-invalid');
        s2.removeClass('is-invalid');
        wa.addClass('d-none');
        if (s1.val() !== s2.val()) {
            s1.addClass('is-invalid');
            s2.addClass('is-invalid');
            wa.removeClass('d-none');
            event.preventDefault();
        }
    });
</script>