<?php
/**
 * @var string              $submenu
 * @var string              $lotes_menu
 * @var SiteLotesEntidade[] $lotes
 * @var array               $fotos
 */

use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteLotesEntidade;
use Modulos\Site\PessoaFisica\Models\Repositorios\SitePessoaFisicaRP;

?>
<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group">
                <?= SitePessoaFisicaRP::getIndexPage('admin/menu', ['submenu' => $submenu]); ?>
            </div>
        </div>
        <div class="col-md-9 lista">
            <?php if (isset($mensagem)): ?>
                <div class="alert alert-<?= $mensagem['tipo'] ?> text-center">
                    <?= $mensagem['texto'] ?>
                </div>
            <?php endif; ?>
            <div class="row">
                <?php if (count($lotes)): ?>
                    <?php foreach ($lotes as $lote): ?>
                        <div class="col-md-4">
                            <a class="card"
                               href="<?= Url::ModuloApelido(SitePessoaFisicaRP::class, "Lotes/editar?menu=area-da-pessoa-fisica&lote={$lote->getId()}", "area-da-pessoa-fisica-lotes-editar-{$lote->getId()}") ?>">
                                <?php if ($lote->getAtivo()): ?>
                                    <div class="ativo">ativo</div>
                                <?php endif; ?>
                                <?php if (isset($fotos[ $lote->getId() ])): ?>
                                    <div style="background-image: url('<?= $fotos[ $lote->getId() ] ?>')"
                                         class="card-img-top"></div>
                                <?php else: ?>
                                    <div class="card-img-top placeholder"><i class="fa fa-3x fa-camera"></i></div>
                                <?php endif; ?>
                                <div class="card-body">
                                    <h5 class="card-title"><?= $lote->getTitulo() ?></h5>
                                    <div class="card-text">
                                        <h6>R$ <?= number_format($lote->getValor(), 2, ',', '.') ?></h6>
                                        <span class="text-nowrap">Setor <?= $lote->getBairro() ?></span>,<br><span
                                                class="text-nowrap"><?= $lote->getCidade() ?></span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="col text-center">
                        Nenhum lote cadastrado. <br> Publique um agora!
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>