<?php
/**
 * @var string $submenu
 * @var string $lotes_menu
 * @var object $cidade
 * @var object $bairro
 * @var SiteLotesEntidade $lote
 */

use Aplicacao\Ferramentas\GetAssets;
use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteLotesEntidade;
use Modulos\Site\PessoaFisica\Models\Repositorios\SitePessoaFisicaRP;

?>
<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group">
                <?= SitePessoaFisicaRP::getIndexPage('admin/menu', ['submenu' => $submenu]); ?>
            </div>
        </div>
        <div class="col-md-9 editar">
            <?php if (isset($mensagem)): ?>
                <div class="alert alert-<?= $mensagem['tipo'] ?> text-center">
                    <?= $mensagem['texto'] ?>
                </div>
            <?php endif; ?>
            <div class="border p-4">
                <form id="formPublicar" method="post">
                    <input type="hidden" name="id" value="<?= $lote->getId() ?>">
                    <div class="form-row">
                        <div class="form-group col-md-8">
                            <label for="input-titulo" class="muted m-0">
                                <small>Título</small>
                            </label>
                            <input type="text" class="form-control" name="titulo" id="input-titulo"
                                   placeholder="Título - Ex: Lote à venda" value="<?= $lote->getTitulo() ?>" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="input-vendedor" class="muted m-0">
                                <small>Anunciante</small>
                            </label>
                            <input type="text" class="form-control" name="vendedor" id="input-vendedor"
                                   placeholder="Nome do anunciante do lote" value="<?= $lote->getVendedor() ?>"
                                   required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="input-cidade" class="muted m-0">
                                <small>Cidade</small>
                            </label>
                            <select class="form-control" name="cidade" id="input-cidade"
                                    placeholder="Cidade" required></select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="input-bairro" class="muted m-0">
                                <small>Bairro</small>
                            </label>
                            <select class="form-control" name="bairro" id="input-bairro"
                                    placeholder="Bairro" required></select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="input-rua" class="muted m-0">
                                <small>Rua</small>
                            </label>
                            <input type="text" class="form-control" name="rua" id="input-rua"
                                   placeholder="Rua" value="<?= $lote->getRua() ?>" required>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="input-quadra" class="muted m-0">
                                <small>Quadra</small>
                            </label>
                            <input type="text" class="form-control" name="quadra" id="input-quadra"
                                   placeholder="Quadra" value="<?= $lote->getQuadra() ?>" required>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="input-lote" class="muted m-0">
                                <small>Lote</small>
                            </label>
                            <input type="text" class="form-control" name="lote" id="input-lote"
                                   placeholder="Lote" value="<?= $lote->getLote() ?>" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="input-quartos" class="muted m-0">
                                <small>Frente (m)</small>
                            </label>
                            <input type="text" class="form-control mask-number" name="frente" id="input-frente"
                                   placeholder="Frente" value="<?= $lote->getFrente() ?>" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="input-vagas" class="muted m-0">
                                <small>Lateral (m)</small>
                            </label>
                            <input type="text" class="form-control mask-number" name="lateral" id="input-lateral"
                                   placeholder="Lateral" value="<?= $lote->getLateral() ?>" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="input-area-total" class="muted m-0">
                                <small>Área total (m²)</small>
                            </label>
                            <input type="text" class="form-control mask-number" name="area_total" id="input-area-total"
                                   placeholder="Área total" value="<?= $lote->getAreaTotal() ?>" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="input-valor" class="muted m-0">
                                <small>Valor</small>
                            </label>
                            <input type="text" class="form-control mask-money" name="valor" id="input-valor"
                                   placeholder="Valor" value="<?= number_format($lote->getValor(), 2,',', '.') ?>" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="input-telefone" class="muted m-0">
                                <small>Telefone</small>
                            </label>
                            <input type="text" class="form-control mask-telefone" name="telefone" id="input-telefone"
                                   placeholder="Telefone" value="<?= $lote->getTelefone() ?>" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="input-whatsapp" class="muted m-0">
                                <small>Whatsapp</small>
                            </label>
                            <input type="text" class="form-control mask-telefone" name="whatsapp" id="input-whatsapp"
                                   placeholder="Whatsapp" value="<?= $lote->getWhatsapp() ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-sobre" class="muted m-0">
                            <small>Sobre</small>
                        </label>
                        <textarea class="form-control" name="sobre" id="input-sobre"
                                  placeholder="Sobre"><?= $lote->getSobre() ?></textarea>
                    </div>
                </form>
                <div id="fotos" class="d-none">
                    <small class="mt-3 d-block">Fotos atuais</small>
                    <div class="lista border rounded"></div>
                </div>
                <small class="mt-3 d-block">Adicionar Fotos</small>
                <form id="formFotos" method="post" class="dropzone border rounded">
                    <div class="fallback">
                        <input name="file" type="file" multiple/>
                    </div>
                    <div class="dz-message">Clique aqui para selecionar ou arraste as fotos para cá</div>
                </form>
                <br>
                <button type="submit" id="button-submit" form="formPublicar" class="btn btn-primary btn-block">Salvar
                </button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= GetAssets::Plugins('Dropzone/dropzone.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?= GetAssets::Plugins('Dropzone/dropzone.css') ?>"/>
<script>
    Dropzone.options.formFotos = {
        url: '<?= Url::Base('Modulo/Site/PessoaFisica/Lotes/upload') ?>?lote=<?= $lote->getId() ?>',
        complete: function (file) {
            this.removeFile(file);
            Fotos.busca();
        }
    };

    const Fotos = {
        lista: [],
        busca: function () {
            $.get('<?= Url::Base('Modulo/Site/PessoaFisica/Lotes/getFotos') ?>?lote=<?= $lote->getId() ?>', function (data) {
                Fotos.lista = data;
                Fotos.exibir();
            });
        },
        remover: function (el) {
            if (confirm("Tem certeza que deseja remover a foto?")) {
                $.get('<?= Url::Base('Modulo/Site/PessoaFisica/Lotes/remFotos') ?>?lote=<?= $lote->getId() ?>&foto=' + $(el).attr('data-id'), function (data) {
                    Fotos.busca();
                });
            }
        },
        exibir: function () {
            const fotos = $('#fotos');
            if (Fotos.lista.length === 0) {
                fotos.addClass('d-none');
            } else {
                fotos.removeClass('d-none');
                const lista = fotos.find('.lista').empty();
                $.each(Fotos.lista, function (i) {
                    let item = Fotos.lista[i];
                    let foto = $('<div />')
                        .addClass('foto')
                        .attr('data-id', item.id)
                        .css('background-image', 'url("' + item.imagem + '")')
                        .on('click', function () {
                            Fotos.remover(this);
                        })
                        .appendTo(lista);
                    let rem = $('<div />')
                        .addClass('rem')
                        .appendTo(foto);
                    $('<i />')
                        .addClass('fas')
                        .addClass('fa-trash-alt')
                        .addClass('fa-2x')
                        .appendTo(rem);
                });
            }
        }
    };

    $(function () {
        Fotos.busca();

        $('#input-cidade').select2({
            data: [<?= json_encode($cidade) ?>],
            theme: 'bootstrap4',
            placeholder: "Cidade",
            allowClear: true,
            language: "pt-BR",
            ajax: {
                url: '<?= Url::Base("Modulo/Site/Util/Cidades/index") ?>',
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term,
                    };
                }
            }
        });
        $('#input-cidade').val(<?= $cidade->id ?>).trigger('change');
        $('#input-cidade').on('select2:select', function (e) {
            $('#input-bairro').val(null).trigger('change');
            $('#input-bairro').removeAttr('disabled');
        }).on('select2:clear', function (e) {
            $('#input-bairro').val(null).trigger('change');
            $('#input-bairro').attr('disabled', true);
        });

        $('#input-bairro').select2({
            data: [<?= json_encode($bairro) ?>],
            theme: 'bootstrap4',
            placeholder: "Bairro",
            allowClear: true,
            language: "pt-BR",
            ajax: {
                url: '<?= Url::Base("Modulo/Site/Util/Cidades/bairros") ?>',
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term,
                        cidade: $("#input-cidade").val()
                    };
                }
            }
        });
        $('#input-bairro').val(<?= $bairro->id ?>).trigger('change');
    });
</script>