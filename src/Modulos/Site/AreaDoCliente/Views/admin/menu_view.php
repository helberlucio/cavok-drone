<?php
/**
 * @var string $submenu
 */

use Aplicacao\Url;
use Modulos\Site\AreaDoCliente\Models\Repositorios\SiteAreaDoClienteRP;

?>
<div class="list-group">
    <a href="<?= Url::ModuloApelido(
		SiteAreaDoClienteRP::class,
		'Admin/index?menu=painel-do-aluno',
		'painel-do-aluno'
	) ?>"
       class="list-group-item list-group-item-action <?= $submenu == 'index' ? 'active' : '' ?>">
        <i class="fa fa-user fa-fw"></i>
        Perfil
    </a>
    <a href="<?= Url::ModuloApelido(
		SiteAreaDoClienteRP::class,
		'Admin/matriculas?menu=painel-do-aluno',
		'matriculas',
		['painel-do-aluno']
	) ?>"
       class="list-group-item list-group-item-action <?= $submenu == 'matriculas' ? 'active' : '' ?>">
        <i class="fa fa-pencil-alt fa-fw"></i>
        Matrículas
    </a>
    <a href="<?= Url::ModuloApelido(
		SiteAreaDoClienteRP::class,
		'Admin/seguranca?menu=painel-do-aluno',
		'seguranca',
		['painel-do-aluno']
	) ?>"
       class="list-group-item list-group-item-action <?= $submenu == 'seguranca' ? 'active' : '' ?>">
        <i class="fa fa-lock fa-fw"></i>
        Segurança
    </a>
    <a href="<?= Url::ModuloApelido(
		SiteAreaDoClienteRP::class,
		'Login/logout',
		'sair',
		['painel-do-aluno']
	) ?>"
       class="list-group-item list-group-item-action">
        <i class="fa fa-sign-out-alt fa-fw"></i>
        Sair
    </a>
</div>