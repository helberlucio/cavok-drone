<?php

/**
 * // * @var SiteMatriculasEntidade[] $matriculas
 * @var SiteMatriculasOnlineEntidade[] $matriculas_online
 * // * @var SiteCursosEntidade[] $cursos
 * @var SiteCursosOnlineEntidade[]     $cursos_online
 * @var string                         $submenu
 */

use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursosOnlineEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteMatriculasOnlineEntidade;
use Modulos\Site\AreaDoCliente\Models\Repositorios\SiteAreaDoClienteRP;
use Modulos\Site\Avaliacao\Models\Repositorios\SiteAvaliacaoRP;
use Modulos\Site\Cursos\Models\Repositorios\SiteCursosRP;

?>
<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group">
				<?= SiteAreaDoClienteRP::getIndexPage('admin/menu', ['submenu' => $submenu]); ?>
            </div>
        </div>
        <div class="col-md-9">
			<?php if (isset($mensagem)): ?>
                <div class="alert alert-<?= $mensagem['tipo'] ?> text-center">
					<?= $mensagem['texto'] ?>
                </div>
			<?php endif; ?>
			<?php if (count($matriculas_online)): ?>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Curso</th>
                        <th>Tipo</th>
                        <th>Data da Matrícula</th>
                        <th>Pago</th>
                        <th>Confirmado</th>
                        <th width="1">Conteúdo</th>
                        <th width="1">Avaliação</th>
                    </tr>
                    </thead>
                    <tbody>
					<?php foreach ($matriculas_online as $matricula): ?>
                        <tr>
                            <td><?= $cursos_online[ $matricula->getCurso() ]->getNome(); ?></td>
                            <td><?= $matricula->getPresencial() ? 'Online e Presencial' : 'Online'; ?></td>
                            <td><?= date('d/m/Y', strtotime($matricula->getData())) ?></td>
                            <td><?= $matricula->getPago() ? 'Sim' : 'Não' ?>
								<?php if (!$matricula->getPago() && !empty($cursos_online[ $matricula->getCurso() ]->getLinkPagamento()))
									echo $matricula->getPresencial()
										? "<a class='btn btn-sm btn-primary float-right' href='{$cursos_online[$matricula->getCurso()]->getLinkPagamentoPresencial()}' target='_blank'>Pagar</a>"
										: "<a class='btn btn-sm btn-primary float-right' href='{$cursos_online[$matricula->getCurso()]->getLinkPagamento()}' target='_blank'>Pagar</a>";
								?></td>
                            <td><?= $matricula->getConfirmado() ? 'Sim' : 'Não' ?></td>
                            <td>
								<?php if ($matricula->getConfirmado() && $matricula->getPago()): ?>
                                    <a href="<?= Url::ModuloApelido(
										SiteCursosRP::class,
										"Online/index?menu=cursos&c={$matricula->getCurso()}&p=1",
										'1',
										['conteudo', Url::Title($cursos_online[ $matricula->getCurso() ]->getNome())]) ?>"
                                       class="btn btn-primary btn-sm btn-block"
                                       target="_blank">Ver
                                    </a>
								<?php else: ?>
                                    <button class="btn btn-primary btn-sm btn-block" disabled
                                            title="Aguardando Confirmação/Pagamento">Ver
                                    </button>
								<?php endif; ?>
                            </td>
                            <td>
								<?php if ($matricula->getConfirmado() && $matricula->getPago()): ?>
                                    <a href="<?= Url::ModuloApelido(
										SiteAvaliacaoRP::class,
										"Online/curso?menu=avaliacao&matricula={$matricula->getId()}",
										Url::Title($cursos_online[ $matricula->getCurso() ]->getNome()),
										['avaliacao']
									) ?>"
                                       class="btn btn-primary btn-sm btn-block" target="_blank">Fazer</a>
								<?php else: ?>
                                    <button class="btn btn-primary btn-sm btn-block" disabled
                                            title="Aguardando Confirmação/Pagamento">Fazer
                                    </button>
								<?php endif; ?>
                            </td>
                        </tr>
					<?php endforeach; ?>
                    </tbody>
                </table>
			<?php endif; ?>
			<?php if (!count($matriculas_online)): ?>
                <h5 class="text-primary mb-3">Nenhuma matrícula</h5>
			<?php endif; ?>
        </div>
    </div>
</div>