<?php

/**
 * @var SiteAlunosEntidade $user
 * @var object                   $cidade
 * @var object                   $tipo
 */

use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteAlunosEntidade;
use Modulos\Site\AreaDoCliente\Models\Repositorios\SiteAreaDoClienteRP;

?>
<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group">
                <?= SiteAreaDoClienteRP::getIndexPage('admin/menu', ['submenu' => $submenu]); ?>
            </div>
        </div>
        <div class="col-md-9">
            <?php if (isset($mensagem)): ?>
                <div class="alert alert-<?= $mensagem['tipo'] ?> text-center">
                    <?= $mensagem['texto'] ?>
                </div>
            <?php endif; ?>
            <form method="post">
                <div class="border rounded">
                    <div class="pt-4 pl-4 pr-4">
                        <div class="form-group row">
                            <label for="input-nome" class="col-sm-2 col-form-label">Nome</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" id="input-nome" name="nome"
                                       value="<?= $user->getNome() ?>"
                                       required>
                            </div>
                        </div>
                        <div class="form-group row align-items-center">
                            <label for="input-email" class="col-sm-2 col-form-label">CPF</label>
                            <div class="col-sm-10 mask-cpf-cnpj">
                                <?= $user->getCpf() ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="input-telefone" class="col-sm-2 col-form-label">Telefone</label>
                            <div class="col-sm-10">
                                <input class="form-control mask-telefone" type="tel" id="input-telefone" name="telefone"
                                       value="<?= $user->getTelefone() ?>" required>
                            </div>
                        </div>
                        <div class="form-group row align-items-center">
                            <label for="input-email" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <?= $user->getEmail() ?>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="pt-4 pl-4 pr-4">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Senha atual</label>
                            <div class="col-md-7 mb-3">
                                <input type="password" class="form-control" name="senha" required>
                            </div>
                            <div class="col-md-3">
                                <button class="btn btn-info btn-block">Atualizar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>