<?php

namespace Modulos\Site\Consultoria\Controllers;

use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;
use Modulos\Site\Consultoria\Models\Repositorios\SiteConsultoriaRP;
use Modulos\Site\FaleConosco\Models\RegrasDeNegocio\SiteFaleConoscoRN;

class Index extends ControladorPadraoAbsHelp {

    public $admin = false;

    public function index($view = 'index', $data = [], $admin = false) {
        $view = 'Home/index';
        $data['pagina'] = 'index';

        if (!empty(input_post())) {
            $data['form'] = (new SiteFaleConoscoRN())->enviar(
                input_post('nome'),
                input_post('telefone'),
                input_post('email'),
                input_post('mensagem'),
                '[Consultoria]'
            );
        }

        echo SiteConsultoriaRP::getIndexPage($view, $data, $admin);
    }
}