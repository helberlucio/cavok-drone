<?php
/**
 * @var SysPagesEntidade          $page
 * @var SiteConsultoriaEntidade[] $items
 */

use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteConsultoriaEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysPagesEntidade;

?>
<div class="formulario pt-5 pb-5"
     style="background-image: url('<?= Url::Base($page->getBanner()) ?>'); background-repeat: repeat; background-size: cover">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-5 text-white">
                <h1 class="mb-4"><?= $page->getTitulo() ?></h1>
                <p><?= nl2br($page->getDescricao()) ?></p>
            </div>
            <div class="col-md-6">
                <h5 class="text-white mb-4">Preencha o formulário abaixo com uma breve explanação
                    da sua necessidade.</h5>
                <form id="form-fale-conosco" method="post" action="">
                    <div class="form-group">
                        <label class="text-white" for="input-nome">* Seu nome</label>
                        <input type="text" class="form-control rounded-sm" name="nome" id="input-nome"
                               value="<?= isset($nome) ? $nome : '' ?>"
                               required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" for="input-telefone">* Seu contato</label>
                        <input type="tel" class="form-control rounded-sm mask-telefone" name="telefone"
                               id="input-telefone"
                               value="<?= isset($telefone) ? $telefone : '' ?>"
                               required>
                    </div>
                    <div class="form-group">
                        <label class="text-white" class="text-white" for="input-email">* Seu e-mail</label>
                        <input type="email" class="form-control rounded-sm" name="email" id="input-email"
                               value="<?= isset($email) ? $email : '' ?>"
                               required>
                    </div>
                    <div class="form-group">
                        <label class="text-white"<label for="input-mensagem">Mensagem</label>
                        <textarea class="form-control rounded-sm" name="mensagem" id="input-mensagem" rows="6"
                                  required></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block pt-3 pb-3 rounded-sm">
                        Enviar solicitação
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="container mb-5">
    <h5 class="text-primary text-center mt-5 mb-5">Confira o resultado de algumas de nossas consultorias!</h5>
    <div class="galeria owl-carousel owl-theme">
        <?php foreach ($items as $item): ?>
            <a href="<?= $item->getUrl() ?>" target="_blank" title="<?= $item->getNome() ?>">
                <img class="border border-gray rounded" src="<?= Url::Base($item->getImagem()) ?>"
                     alt="<?= $item->getNome() ?>">
            </a>
        <?php endforeach; ?>
    </div>
</div>
<script>
    $(function () {
        $('.galeria').owlCarousel({
            loop: false,
            margin: 10,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 2,
                    nav: false,
                    dots: true
                },
                600: {
                    items: 3,
                    nav: false,
                    dots: true
                },
                1000: {
                    items: 6,
                    nav: false,
                    dots: true
                }
            }
        });

        <?php if (isset($form)): ?>
        <?php if ($form['sucesso']): ?>
        Swal.fire({
            title: 'Sucesso!',
            text: 'Entraremos em contato em breve!',
            type: 'success',
        });
        <?php else: ?>
        Swal.fire({
            title: 'Erro!',
            text: 'Não foi possível realizar a solicitação. Tente novamente mais tarde ou entre em contato com nosso suporte via telefone.',
            type: 'error',
        });
        <?php endif; ?>
        <?php endif; ?>
    });
</script>