<?php
namespace Modulos\Site\Header\Controllers;

use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;
use Modulos\Site\Header\Models\Repositorios\SiteHeaderRP;

class Index extends ControladorPadraoAbsHelp{

    public $admin = false;

    public function index($view = 'index', $data = array(), $admin = false){
        echo SiteHeaderRP::getIndexPage($view, $data, $admin);
    }
}