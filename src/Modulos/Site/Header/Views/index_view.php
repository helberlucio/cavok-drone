<?php

/**
 * @var WebsiteGeralEntidade $configuracaoGeral
 * @var string               $menu
 */

use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\WebsiteGeralEntidade;
use Modulos\Site\AreaDoCliente\Models\RegrasDeNegocio\SiteAreaDoClienteRN;
use Modulos\Site\AreaDoCliente\Models\Repositorios\SiteAreaDoClienteRP;
use Modulos\Site\Assine\Models\Repositorios\SiteAssineRP;
use Modulos\Site\Avaliacao\Models\Repositorios\SiteAvaliacaoRP;
use Modulos\Site\Blog\Models\Repositorios\SiteBlogRP;
use Modulos\Site\Consultoria\Models\Repositorios\SiteConsultoriaRP;
use Modulos\Site\Cursos\Models\Repositorios\SiteCursosRP;
use Modulos\Site\Duvidas\Models\Repositorios\SiteDuvidasRP;
use Modulos\Site\FaleConosco\Models\Repositorios\SiteFaleConoscoRP;
use Modulos\Site\Institucional\Models\Repositorios\SiteInstitucionalRP;
use Modulos\Site\Planos\Models\Repositorios\SitePlanosRP;
use Modulos\Site\SeguroReta\Models\Repositorios\SiteSeguroRetaRP;

?>
<header>
    <div class="fixed-top">
        <div id="nav-top" class="bg-black d-block w-100">
            <div class="container">
                <ul class="nav justify-content-md-end justify-content-sm-center">
                    <li class="nav-item">
                        <div class="nav-link text-white">
                            <img data-toggle="tooltip" data-placement="bottom"
                                 title="<?= $configuracaoGeral->getContatoTelefone() ?>"
                                 src="<?= Url::Gerenciador('Header/ico-phone.png') ?>"><span
                                    class="d-none d-sm-inline-block">&nbsp;<?= $configuracaoGeral->getContatoTelefone() ?></span>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link text-white" target="_blank">
                            <img src="<?= Url::Gerenciador('Header/ico-whatsapp.png') ?>"><span
                                    class="d-none d-sm-inline-block">&nbsp;<?= $configuracaoGeral->getWhatsapp() ?></span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= $configuracaoGeral->getFacebook() ?>" class="nav-link text-white" target="_blank">
                            <img src="<?= Url::Gerenciador('Header/ico-facebook.png') ?>">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= $configuracaoGeral->getInstagram() ?>" class="nav-link text-white" target="_blank">
                            <img src="<?= Url::Gerenciador('Header/ico-instagram.png') ?>">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= $configuracaoGeral->getYoutube() ?>" class="nav-link text-white" target="_blank">
                            <img src="<?= Url::Gerenciador('Header/ico-youtube.png') ?>">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <nav id="nav-main" class="navbar navbar-expand-md bg-white navbar-light">
            <div class="container">
                <a class="navbar-brand" href="<?= Url::Base() ?>"><img
                            src="<?= Url::Base($configuracaoGeral->getLogo()) ?>"
                            alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                        aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse flex-column" id="navbarCollapse">
                    <ul class="navbar-nav mr-auto nav-fill w-100" id="nav-bottom">
                        <li class="nav-item <?= !$menu ? 'active' : '' ?>">
                            <a class="nav-link" href="<?= Url::Base() ?>">
                                <div class="nav-link-text">Home</div>
                            </a>
                        </li>
                        <li class="nav-item <?= $menu == 'institucional' ? 'active' : '' ?>">
                            <a class="nav-link"
                               href="<?= Url::ModuloApelido(SiteInstitucionalRP::class, 'Index?menu=institucional', 'institucional') ?>">
                                <div class="nav-link-text">Institucional</div>
                            </a>
                        </li>
                        <li class="nav-item <?= $menu == 'cursos' ? 'active' : '' ?>">
                            <a class="nav-link"
                               href="<?= Url::ModuloApelido(SiteCursosRP::class, 'Index?menu=cursos', 'cursos') ?>">
                                <div class="nav-link-text">Cursos</div>
                            </a>
                        </li>
                        <li class="nav-item <?= $menu == 'consultoria' ? 'active' : '' ?>">
                            <a class="nav-link"
                               href="<?= Url::ModuloApelido(SiteConsultoriaRP::class, 'Index?menu=consultoria', 'consultoria') ?>">
                                <div class="nav-link-text">Consultoria</div>
                            </a>
                        </li>
                        <li class="nav-item <?= $menu == 'seguro-reta' ? 'active' : '' ?>">
                            <a class="nav-link"
                               href="<?= Url::ModuloApelido(SiteSeguroRetaRP::class, 'Index?menu=seguro-reta', 'seguro-reta') ?>">
                                <div class="nav-link-text">Seguro Reta</div>
                            </a>
                        </li>
                        <li class="nav-item <?= $menu == 'duvidas' ? 'active' : '' ?>">
                            <a class="nav-link"
                               href="<?= Url::ModuloApelido(SiteDuvidasRP::class, 'Index?menu=duvidas', 'duvidas') ?>">
                                <div class="nav-link-text">Dúvidas</div>
                            </a>
                        </li>
                        <li class="nav-item <?= $menu == 'blog' ? 'active' : '' ?>">
                            <a class="nav-link"
                               href="<?= Url::ModuloApelido(SiteBlogRP::class, 'Index?menu=blog', 'blog') ?>">
                                <div class="nav-link-text">Blog</div>
                            </a>
                        </li>
                        <li class="nav-item <?= $menu == 'contato' ? 'active' : '' ?>">
                            <a class="nav-link"
                               href="<?= Url::ModuloApelido(SiteFaleConoscoRP::class, 'Index?menu=contato', 'contato') ?>">
                                <div class="nav-link-text">Contato</div>
                            </a>
                        </li>
                        <li class="nav-item faca-sua-matricula <?= $menu == 'faca-sua-matricula' ? 'active' : '' ?>">
                            <a class="nav-link"
                               href="<?= Url::ModuloApelido(SiteCursosRP::class, 'Index?menu=cursos', 'cursos') ?>">
                                <div class="<?= $menu == 'faca-sua-matricula' ? 'nav-link-text' : 'nav-link-btn btn btn-primary' ?>">Faça sua matrícula</div>
                            </a>
                        </li>
                        <li class="nav-item painel-do-aluno <?= $menu == 'painel-do-aluno' ? 'active' : '' ?>">
							<?php if (SiteAreaDoClienteRN::isLogged()): ?>
                            <a class="nav-link ml-sm-1"
                               href="<?= Url::ModuloApelido(SiteAreaDoClienteRP::class, 'Admin?menu=painel-do-aluno', 'painel-do-aluno') ?>">
                                <div class="<?= $menu == 'painel-do-aluno' ? 'nav-link-text' : 'nav-link-btn btn btn-outline-primary' ?>">Painel do Aluno</div>
                            </a>
							<?php else: ?>
                                <a class="nav-link ml-sm-1"
                                   href="<?= Url::ModuloApelido(SiteAreaDoClienteRP::class, 'Login?menu=painel-do-aluno', 'login',['painel-do-aluno']) ?>">
                                    <div class="<?= $menu == 'painel-do-aluno' ? 'nav-link-text' : 'nav-link-btn btn btn-outline-primary' ?>">Entrar</div>
                                </a>
							<?php endif; ?>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip({'trigger': 'click'})
    });
</script>