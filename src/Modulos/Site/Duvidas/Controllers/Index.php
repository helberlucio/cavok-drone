<?php

namespace Modulos\Site\Duvidas\Controllers;

use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;
use Modulos\Site\Duvidas\Models\Repositorios\SiteDuvidasRP;

class Index extends ControladorPadraoAbsHelp {

    public $admin = false;

    public function index($view = 'index', $data = [], $admin = false) {
        $view = 'Home/index';
        $data['pagina'] = 'index';

        echo SiteDuvidasRP::getIndexPage($view, $data, $admin);
    }
}