<?php
/**
 * @var SysPagesEntidade      $page
 * @var SiteDuvidasEntidade[] $duvidas
 */

use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteDuvidasEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysPagesEntidade;

?>
<div class="fundo mb-5" style="background-image: url('<?= Url::Base($page->getBanner()) ?>'); height: <?= $page->getAlturaBanner() ?>px">
    <h3 class="text-center text-white text-uppercase" style="line-height: <?= $page->getAlturaBanner() ?>px"><?= $page->getTitulo() ?></h3>
</div>
<div class="container mb-5">
    <div class="accordion" id="duvidas">
        <?php foreach ($duvidas as $duvida): ?>
            <div class="item border-bottom">
                <div class="titulo pl-0 pt-3 pb-3" id="titulo-<?= $duvida->getId() ?>">
                    <div class="ancora text-dark collapsed" style="cursor: pointer"
                         data-toggle="collapse"
                         data-target="#descricao-<?= $duvida->getId() ?>"
                         aria-expanded="false"
                         aria-controls="descricao-<?= $duvida->getId() ?>">
                        <img src="<?= Url::Gerenciador( 'Duvidas/plus.png') ?>" alt="+"
                             style="margin-left: 10px; float: right">
                        <?= $duvida->getTitulo() ?>
                    </div>
                </div>
                <div id="descricao-<?= $duvida->getId() ?>" class="descricao collapse"
                     aria-labelledby="titulo-<?= $duvida->getId() ?>"
                     data-parent="#duvidas">
                    <div class="pt-4 pb-5"><?= nl2br($duvida->getDescricao()) ?></div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<script>
    var swapIcon = function (obj) {
        var ancoras = $(obj).find('.ancora');
        ancoras.each(function (index) {
            var ancora = ancoras[index];
            var img = $(ancora).find('img');
            if ($(ancora).hasClass('collapsed')) {
                $(ancora).removeClass('text-primary').addClass('text-dark');
                $(img).attr('src', "<?= Url::Gerenciador( 'Duvidas/plus.png') ?>");
            } else {
                $(ancora).removeClass('text-dark').addClass('text-primary');
                $(img).attr('src', "<?= Url::Gerenciador( 'Duvidas/minus.png') ?>");
            }
        });
    };
    $('#duvidas')
        .on('shown.bs.collapse', function () {
            swapIcon(this);
        })
        .on('hidden.bs.collapse', function () {
            swapIcon(this);
        });
</script>