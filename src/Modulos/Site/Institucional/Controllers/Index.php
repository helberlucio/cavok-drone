<?php

namespace Modulos\Site\Institucional\Controllers;

use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteInstitucionalEntidade;
use Modulos\Site\Institucional\Models\Repositorios\SiteInstitucionalRP;

class Index extends ControladorPadraoAbsHelp {

    public $admin = false;

    public function index($view = 'index', $data = [], $admin = false) {
        $view = 'Home/index';
        $data['pagina'] = 'index';

        echo SiteInstitucionalRP::getIndexPage($view, $data, $admin);
    }
}