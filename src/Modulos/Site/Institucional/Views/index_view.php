<?php
/**
 * @var SysPagesEntidade            $page
 * @var SiteInstitucionalEntidade[] $fotos
 */

use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteInstitucionalEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysPagesEntidade;

?>
<div class="container mt-5 mb-4">
    <div class="row">
        <div class="col-md-2">
            <h1 class="text-primary text-center text-md-right"><?= $page->getTitulo() ?></h1>
        </div>
        <div class="col-md-10">
            <p><?= nl2br($page->getDescricao()) ?></p>
        </div>
    </div>
    <div class="row d-none d-md-block">
        <div class="col">
            <div id="current-foto"
                 style="background-image: url('<?= Url::Base(current($fotos)->getImagem()) ?>')"></div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="galeria owl-carousel owl-theme">
                <?php foreach ($fotos as $foto): ?>
                    <img src="<?= Url::Base($foto->getImagem()) ?>" style="cursor: pointer"
                         onclick='changeFoto("<?= Url::Base($foto->getImagem()) ?>")'>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
<script>
    const changeFoto = function (url) {
        $('#current-foto').css('background-image', "url('" + url + "')");
    };

    $(function () {
        $('.galeria').owlCarousel({
            loop: true,
            margin: 10,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: false,
                    dots: true
                },
                600: {
                    items: 3,
                    nav: false,
                    dots: true
                },
                1000: {
                    items: 6,
                    nav: false,
                    dots: true
                }
            }
        });
    });
</script>