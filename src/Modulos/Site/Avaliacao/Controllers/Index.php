<?php

namespace Modulos\Site\Avaliacao\Controllers;

use Aplicacao\Conversao;
use Aplicacao\Url;
use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteAgendaEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteAlunosEntidade;
use Modulos\Site\Avaliacao\Models\RegrasDeNegocio\SiteAvaliacaoRN;
use Modulos\Site\Avaliacao\Models\Repositorios\SiteAvaliacaoRP;

class Index extends ControladorPadraoAbsHelp
{

	public $admin = false;

	public function index($view = 'index', $data = [], $admin = false)
	{
		if (input_post())
		{
			$this->login($data);
		}

		$view = 'Home/index';
		$data['pagina'] = 'index';

		echo SiteAvaliacaoRP::getIndexPage($view, $data, $admin);
	}

	public function cursos($view = 'index', $data = [], $admin = false)
	{
		$rn = new SiteAvaliacaoRN();
		if (input_get('agenda'))
		{
			$rn->setCurso($rn->setAgenda(input_get('agenda'))->getAgenda()->getCurso());
			Url::Redirecionar(Url::ModuloApelido(SiteAvaliacaoRP::class, 'Index/perguntas?menu=avaliacao', 'avaliacao-pergunta-1'));
		}
		$view = 'Home/index';
		$data['pagina'] = 'cursos';
		$data['items'] = $rn->getCursos();

		echo SiteAvaliacaoRP::getIndexPage($view, $data, $admin);
	}

	public function resultado($view = 'index', $data = [], $admin = false)
	{
		$data['resposta'] = input_post('resposta');

		$view = 'Home/index';
		$data['pagina'] = 'resultado';

		echo SiteAvaliacaoRP::getIndexPage($view, $data, $admin);
	}

	public function certificado()
	{
		return (new SiteAvaliacaoRN())->gerarCertificado();
	}

	public function perguntas($view = 'index', $data = [], $admin = false)
	{
		$data['atual'] = input_get('atual');
		$data['resposta'] = input_post('resposta');

		$view = 'Home/index';
		$data['pagina'] = 'perguntas';

		echo SiteAvaliacaoRP::getIndexPage($view, $data, $admin);
	}

	private function login(&$data)
	{
		$user = (new SiteAlunosEntidade())->getRow($this->db->where('cpf', Conversao::somenteNumeros(input_post('cpf'))));
		if (!$user->getId())
		{
			$data['error'] = 'Não existe nenhuma matrícula com este CPF.';

			return;
		};
		$agenda = (new SiteAgendaEntidade())->getRows(
			$this->db
				->select('site_agenda.*')
				->join('site_matriculas', 'site_matriculas.data = site_agenda.id')
				->where('site_matriculas.aluno', $user->getId())
				->where('site_agenda.data <', date('Y-m-d'))
				->order_by('site_agenda.data', 'desc')
//				->limit(1)
		);
		if (!count($agenda))
		{
			$data['error'] = 'Você ainda não realizou nenhum curso.';

			return;
		}
		$cursos = [];
		foreach ($agenda as $a)
		{
			$cursos[] = $a;
		}
		(new SiteAvaliacaoRN())->login($user, $cursos);
	}
}