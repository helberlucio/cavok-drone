<?php

namespace Modulos\Site\Avaliacao\Controllers;

use Aplicacao\Url;
use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursosEntidade;
use Modulos\Site\Avaliacao\Models\RegrasDeNegocio\SiteAvaliacaoOnlineRN;
use Modulos\Site\Avaliacao\Models\Repositorios\SiteAvaliacaoRP;

class Online extends ControladorPadraoAbsHelp
{

	public $admin = false;

	public function index($view = 'index', $data = [], $admin = false)
	{
		if (input_post())
		{
			$this->login($data);
		}

		$view = 'Home/index';
		$data['pagina'] = 'index';

		echo SiteAvaliacaoRP::getIndexPage($view, $data, $admin);
	}

	public function curso()
	{
		$matricula = input_get('matricula');
		if (!$matricula) die('Matrícula não informada!');

		$rn = new SiteAvaliacaoOnlineRN();
		$rn->clear();
		$rn->setMatricula($matricula);
		$matricula = $rn->getMatricula();
		if (!$matricula->getId()) die('Matrícula não encontrada!');

		$rn->setCurso($matricula->getCurso());

		Url::Redirecionar(
			Url::ModuloApelido(
				SiteAvaliacaoRP::class,
				'Online/perguntas?menu=avaliacao',
				1,
				['avaliacao', Url::Title($rn->getCurso()->getNome())]
			)
		);
	}

	public function resultado($view = 'index', $data = [], $admin = false)
	{
		$data['resposta'] = input_post('resposta');

		$view = 'Home/index';
		$data['pagina'] = 'resultado_online';

		echo SiteAvaliacaoRP::getIndexPage($view, $data, $admin);
	}

	public function certificado()
	{
		return (new SiteAvaliacaoOnlineRN())->gerarCertificado();
	}

	public function perguntas($view = 'index', $data = [], $admin = false)
	{
		$data['atual'] = input_get('atual');
		$data['resposta'] = input_post('resposta');

		$view = 'Home/index';
		$data['pagina'] = 'perguntas_online';

		echo SiteAvaliacaoRP::getIndexPage($view, $data, $admin);
	}
}