<?php
namespace Modulos\Site\Avaliacao\Models\Repositorios;

use ModuloHelp\PadraoCriacao\RepositorioPadrao\Abstracts\RepositorioPadraoAbsHelp;

class SiteAvaliacaoRP extends RepositorioPadraoAbsHelp{

    const GRUPO_MODULO = 'Site';
    const NOME_MODULO = 'Avaliacao';

    public static function getUrlModulo($controlador = 'Index')
    {
        return 'Modulo/'.self::GRUPO_MODULO.'/'.self::NOME_MODULO.'/'.$controlador; 
    }

    public static function getIndexPage($view = 'index', $data = array(), $admin = false)
    {
        $repositorio = new SiteAvaliacaoRP(); 
        return $repositorio->getModulo($view, $data, $admin); 
    }

    public function getModulo($view = 'index', $data = array(), $admin = false)
    {
        return parent::getModulo($view, $data, $admin); // TODO: Change the autogenerated stub
    }
}

