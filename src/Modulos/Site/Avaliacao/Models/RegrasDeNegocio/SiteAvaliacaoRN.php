<?php

namespace Modulos\Site\Avaliacao\Models\RegrasDeNegocio;

use Aplicacao\Url;
use ModuloHelp\PadraoCriacao\ModelRegraDeNegocio\Abstracts\ModelRegraDeNegocioPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteAgendaEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteAlunosEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursosEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteQuestoesEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteQuestoesOnlineEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysPagesEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\WebsiteGeralEntidade;
use Modulos\Site\AreaDoCliente\Models\RegrasDeNegocio\SiteAreaDoClienteRN;
use Modulos\Site\Avaliacao\Models\Repositorios\SiteAvaliacaoRP;
use Mpdf\Mpdf;
use Mpdf\MpdfException;
use setasign\Fpdi\PdfParser\PdfParserException;

class SiteAvaliacaoRN extends ModelRegraDeNegocioPadraoAbsHelp
{

	private static $SESSION_NAME      = 'site_aluno';
	private static $SESSION_CURSO     = 'site_curso';
	private static $SESSION_AGENDA    = 'site_agenda';
	private static $SESSION_CURSOS    = 'site_cursos';
	private static $SESSION_PERGUNTAS = 'site_perguntas';
	private static $SESSION_RESPOSTAS = 'site_respostas';

	public function getModulo($viewName = "index", $data = [], $admin = false, $minifyHtml = true, $viewDeAtualizacao = false)
	{
		if ($data['pagina'] == "index")
		{
			$data['page'] = (new SysPagesEntidade())->getRow($this->db->where('pagina', 'avaliacao-login'));
		}
		if ($data['pagina'] == "resultado")
		{
			$data['page'] = (new SysPagesEntidade())->getRow($this->db->where('pagina', 'avaliacao-resultado'));
			$atual = $this->getAtualPergunta();
			if ($data['resposta'])
			{
				$this->addResposta($atual->getId(), $data['resposta']);
			}
			$perguntas = (new SiteQuestoesEntidade())->getRows(
				$this->db
					->where('curso', $this->getCurso()->getId())
					->order_by('ordem', 'asc')
			);
			$respostas = $this->getRepostas();
			$total = count($perguntas);
			$corretas = 0;
			foreach ($perguntas as $pergunta)
			{
				if (strtolower($pergunta->getRespostaCorreta()) == strtolower($respostas[ $pergunta->getId() ]))
				{
					$corretas++;
				}
			}
			$data['porcentagem'] = ceil($corretas * 100 / $total);
			$data['aluno'] = $this->getUser()->getNome();
		}
		if ($data['pagina'] == "resultado_online")
		{
			$SiteAvaliacaoOnlineRN = new SiteAvaliacaoOnlineRN();
			$data['page'] = (new SysPagesEntidade())->getRow($this->db->where('pagina', 'avaliacao-resultado'));
			$atual = $SiteAvaliacaoOnlineRN->getAtualPergunta();
			if ($data['resposta'])
			{
				$SiteAvaliacaoOnlineRN->addResposta($atual->getId(), $data['resposta']);
			}
			$perguntas = (new SiteQuestoesOnlineEntidade())->getRows(
				$this->db
					->where('curso', $SiteAvaliacaoOnlineRN->getCurso()->getId())
					->order_by('ordem', 'asc')
			);
			$respostas = $SiteAvaliacaoOnlineRN->getRepostas();
			$total = count($perguntas);
			$corretas = 0;
			foreach ($perguntas as $pergunta)
			{
				if (strtolower($pergunta->getRespostaCorreta()) == strtolower($respostas[ $pergunta->getId() ]))
				{
					$corretas++;
				}
			}
			$data['porcentagem'] = ceil($corretas * 100 / $total);
			$data['aluno'] = (new SiteAreaDoClienteRN())->getUser()->getNome();
		}
		if ($data['pagina'] == "perguntas")
		{
			$data['page'] = (new SysPagesEntidade())->getRow($this->db->where('pagina', 'avaliacao-perguntas'));
			if (!$data['atual'])
			{
				$items = (new SiteQuestoesEntidade())->getRows(
					$this->db
						->where('curso', $this->getCurso()->getId())
						->order_by('ordem', 'asc')
						->limit(2)
				);
				$this->setAnteriorPergunta()->setProximaPergunta();
				foreach ($items as $key => $item)
				{
					if (!$key)
					{
						$this->setAtualPergunta($item);
					} else
					{
						$this->setProximaPergunta($item);
					}
				}
			} else
			{
				$atual = (new SiteQuestoesEntidade())->getRow(
					$this->db
						->where('curso', $this->getCurso()->getId())
						->where('id', $data['atual'])
				);
				$this->setAtualPergunta($atual);

				$proxima = (new SiteQuestoesEntidade())->getRow(
					$this->db
						->where('curso', $this->getCurso()->getId())
						->where('ordem >', $atual->getOrdem())
						->order_by('ordem', 'asc')
						->limit(1)
				);
				$this->setProximaPergunta($proxima);

				$anterior = (new SiteQuestoesEntidade())->getRow(
					$this->db->where('curso', $this->getCurso()->getId())
						->where('ordem <', $atual->getOrdem())
						->order_by('ordem', 'desc')
						->limit(1)
				);
				$this->setAnteriorPergunta($anterior);
			}
			if ($data['resposta'])
			{
				$this->addResposta($this->getAnteriorPergunta()->getId(), $data['resposta']);
			}
			$data['selecionado'] = $this->getResposta($this->getAtualPergunta()->getId());
			$data['curso'] = $this->getCurso()->getNome();
		}
		if ($data['pagina'] == "perguntas_online")
		{
			$SiteAvaliacaoOnlineRN = new SiteAvaliacaoOnlineRN();
			$data['page'] = (new SysPagesEntidade())->getRow($this->db->where('pagina', 'avaliacao-perguntas'));
			if (!$data['atual'])
			{
				$items = (new SiteQuestoesOnlineEntidade())->getRows(
					$this->db
						->where('curso', $SiteAvaliacaoOnlineRN->getCurso()->getId())
						->order_by('ordem', 'asc')
						->limit(2)
				);
				$SiteAvaliacaoOnlineRN->setAnteriorPergunta()->setProximaPergunta();
				foreach ($items as $key => $item)
				{
					if (!$key)
					{
						$SiteAvaliacaoOnlineRN->setAtualPergunta($item);
					} else
					{
						$SiteAvaliacaoOnlineRN->setProximaPergunta($item);
					}
				}
			} else
			{
				$atual = (new SiteQuestoesOnlineEntidade())->getRow(
					$this->db
						->where('curso', $SiteAvaliacaoOnlineRN->getCurso()->getId())
						->where('id', $data['atual'])
				);
				$SiteAvaliacaoOnlineRN->setAtualPergunta($atual);

				$proxima = (new SiteQuestoesOnlineEntidade())->getRow(
					$this->db
						->where('curso', $SiteAvaliacaoOnlineRN->getCurso()->getId())
						->where('ordem >', $atual->getOrdem())
						->order_by('ordem', 'asc')
						->limit(1)
				);
				$SiteAvaliacaoOnlineRN->setProximaPergunta($proxima);

				$anterior = (new SiteQuestoesOnlineEntidade())->getRow(
					$this->db->where('curso', $SiteAvaliacaoOnlineRN->getCurso()->getId())
						->where('ordem <', $atual->getOrdem())
						->order_by('ordem', 'desc')
						->limit(1)
				);
				$SiteAvaliacaoOnlineRN->setAnteriorPergunta($anterior);
			}
			if ($data['resposta'])
			{
				$SiteAvaliacaoOnlineRN->addResposta($SiteAvaliacaoOnlineRN->getAnteriorPergunta()->getId(), $data['resposta']);
			}
			$data['selecionado'] = $SiteAvaliacaoOnlineRN->getResposta($SiteAvaliacaoOnlineRN->getAtualPergunta()->getId());
			$data['curso'] = $SiteAvaliacaoOnlineRN->getCurso()->getNome();
		}

		return parent::getModulo($viewName, $data, $admin, $minifyHtml, $viewDeAtualizacao); // TODO: Change the autogenerated stub
	}

	public function addResposta($pergunta, $resposta)
	{
		$_SESSION[ self::$SESSION_RESPOSTAS ][ $pergunta ] = $resposta;

		return $this;
	}

	public function getRepostas()
	{
		return isset($_SESSION[ self::$SESSION_RESPOSTAS ]) ? $_SESSION[ self::$SESSION_RESPOSTAS ] : [];
	}

	public function getResposta($pergunta)
	{
		return isset($_SESSION[ self::$SESSION_RESPOSTAS ][ $pergunta ]) ? $_SESSION[ self::$SESSION_RESPOSTAS ][ $pergunta ] : null;
	}

	private function setProximaPergunta(SiteQuestoesEntidade $item = null)
	{
		if (!$item) $item = new SiteQuestoesEntidade();
		$_SESSION[ self::$SESSION_PERGUNTAS ]['proxima'] = serialize($item);

		return $this;
	}

	private function setAtualPergunta(SiteQuestoesEntidade $item = null)
	{
		if (!$item) $item = new SiteQuestoesEntidade();
		$_SESSION[ self::$SESSION_PERGUNTAS ]['atual'] = serialize($item);

		return $this;
	}

	private function setAnteriorPergunta(SiteQuestoesEntidade $item = null)
	{
		if (!$item) $item = new SiteQuestoesEntidade();
		$_SESSION[ self::$SESSION_PERGUNTAS ]['anterior'] = serialize($item);

		return $this;
	}

	private function setAnteriorPerguntaOnline(SiteQuestoesOnlineEntidade $item = null)
	{
		if (!$item) $item = new SiteQuestoesOnlineEntidade();
		$_SESSION[ SiteAvaliacaoOnlineRN::$SESSION_PERGUNTAS ]['anterior'] = serialize($item);

		return $this;
	}

	/**
	 * @return SiteQuestoesEntidade
	 */
	public function getProximaPergunta()
	{
		return isset($_SESSION[ self::$SESSION_PERGUNTAS ]) ? unserialize($_SESSION[ self::$SESSION_PERGUNTAS ]['proxima']) : new SiteQuestoesEntidade();
	}

	/**
	 * @return SiteQuestoesEntidade
	 */
	public function getAtualPergunta()
	{
		return isset($_SESSION[ self::$SESSION_PERGUNTAS ]) ? unserialize($_SESSION[ self::$SESSION_PERGUNTAS ]['atual']) : new SiteQuestoesEntidade();
	}

	/**
	 * @return SiteQuestoesEntidade
	 */
	public function getAnteriorPergunta()
	{
		return isset($_SESSION[ self::$SESSION_PERGUNTAS ]) ? unserialize($_SESSION[ self::$SESSION_PERGUNTAS ]['anterior']) : new SiteQuestoesEntidade();
	}

	public function logout()
	{
		unset($_SESSION[ self::$SESSION_NAME ]);
	}

	public function login(SiteAlunosEntidade $user, $cursos = [])
	{
		$_SESSION[ self::$SESSION_NAME ] = serialize($user);
		$_SESSION[ self::$SESSION_CURSOS ] = serialize($cursos);
		unset($_SESSION[ self::$SESSION_CURSO ]);
		unset($_SESSION[ self::$SESSION_AGENDA ]);
		unset($_SESSION[ self::$SESSION_PERGUNTAS ]);
		unset($_SESSION[ self::$SESSION_RESPOSTAS ]);
//		Url::Redirecionar(Url::ModuloApelido(SiteAvaliacaoRP::class, 'Index/perguntas?menu=avaliacao', 'avaliacao-pergunta-1'));
		Url::Redirecionar(Url::ModuloApelido(SiteAvaliacaoRP::class, 'Index/cursos?menu=avaliacao', 'avaliacao-cursos'));
	}

	/**
	 * @return SiteAgendaEntidade[]
	 */
	public function getCursos()
	{
		$cursos = [];
		foreach ((new SiteCursosEntidade())->getRows($this->db) as $curso)
		{
			$cursos[ $curso->getId() ] = $curso;
		}
		$agendas = [];
		/** @var SiteAgendaEntidade $agenda */
		foreach (unserialize($_SESSION[ self::$SESSION_CURSOS ]) as $agenda)
		{
			$agenda->setCurso($cursos[ $agenda->getCurso() ]);
			$agendas[] = $agenda;
		}

		return $agendas;
	}

	/**
	 * @return SiteCursosEntidade
	 */
	public function getCurso()
	{
		return unserialize($_SESSION[ self::$SESSION_CURSO ]);
	}

	public function setCurso($curso)
	{
		$_SESSION[ self::$SESSION_CURSO ] = serialize((new SiteCursosEntidade())->getRow($this->db->where('id', $curso)));

		return $this;
	}

	/**
	 * @return SiteAgendaEntidade
	 */
	public function getAgenda()
	{
		return unserialize($_SESSION[ self::$SESSION_AGENDA ]);
	}

	public function setAgenda($agenda)
	{
		$_SESSION[ self::$SESSION_AGENDA ] = serialize((new SiteAgendaEntidade())->getRow($this->db->where('id', $agenda)));

		return $this;
	}

	/**
	 * @return SiteAlunosEntidade
	 */
	public function getUser()
	{
		return unserialize($_SESSION[ self::$SESSION_NAME ]);
	}

	public static function isLogged()
	{
		return isset($_SESSION[ self::$SESSION_NAME ]) && !empty($_SESSION[ self::$SESSION_NAME ]);
	}

	public function checkLogin()
	{
		if (!self::isLogged())
		{
			Url::Redirecionar(Url::ModuloApelido(SiteAvaliacaoRP::class, 'Index?menu=avaliacao', 'avaliacao'));
		}

		return $this;
	}

	/**
	 *
	 */
	public function gerarCertificado()
	{
		$srcFile = "src/Assets/certificado_frente.pdf";
		$srcFile2 = "src/Assets/certificado_fundo.pdf";
		if (file_exists($srcFile))
		{
			$configuracaoGeral = (new WebsiteGeralEntidade())->getRow($this->db);
			$ultimoNumero = $configuracaoGeral->getNumUltimoCertificado();
			try
			{
				$ultimoNumero++;
				$numeroLabel = 'N° do Certificado';
				$numero = str_pad($ultimoNumero, 9, '0', STR_PAD_LEFT);
				$certificado = 'C E R T I F I C A D O';
				$declaracao1 = 'Em conformidade com a legislação educacional brasileira,';
				$declaracao2 = 'conforme decreto n° 5.154, de 23 de julho de 2004, este';
				$declaracao3 = 'documento certifica para os devidos fins que,';
				$curso = 'Curso: ' . mb_strtoupper($this->getCurso()->getNome());
				$carga = 'Carga Horária: ' . $this->getCurso()->getDuracaoTurma() . ' Horas';
				$aluno = mb_strtoupper($this->getUser()->getNome());
				$aluno_short = explode(' ', $aluno);
				if (count($aluno_short) > 2)
				{
					$aluno_short = array_reverse($aluno_short);
					$aluno_short[1] = substr($aluno_short[1], 0, 1) . '.';
					$aluno_short = array_reverse($aluno_short);
				}
				$aluno_short = implode(' ', $aluno_short);
				$concluiuLabel = 'Concluiu com êxito:';
				$instituicaoLabel = 'INSTITUIÇÃO';
				$cavokLabel = 'CAVOK DRONE SERVICE BRASIL';
				$instituicaoCavokLabel = $instituicaoLabel . ': ' . $cavokLabel;
				$mes = [1  => 'Janeiro', 2 => 'Fevereiro', 3 => 'Março',
						4  => 'Abril', 5 => 'Maio', 6 => 'Junho', 7 => 'Julho',
						8  => 'Agosto', 9 => 'Setembro', 10 => 'Outubro', 11 => 'Novembro',
						12 => 'Dezembro',][ date('n', strtotime($this->getAgenda()->getData())) ];
				$ano = date('Y', strtotime($this->getAgenda()->getData()));
				$dia1 = date('d', strtotime($this->getAgenda()->getData()));
				$dia2 = date('d', strtotime('+1 day', strtotime($this->getAgenda()->getData())));
				$dataLabel = "Goiânia, {$dia1} e {$dia2} de {$mes} de {$ano}.";
				$concluinteLabel = "CONCLUINTE";

				$mpdf = new Mpdf(['orientation' => 'L']);
				$pageCount = $mpdf->setSourceFile($srcFile);
				$tplId = $mpdf->ImportPage($pageCount);
				$mpdf->UseTemplate($tplId);
				$mpdf->SetFont('Arial');

				$mpdf->SetFontSize(23);
				$mpdf->SetTextColor(255, 255, 255);
				$yOffset = 4;
				$mpdf->Text(94 - ($mpdf->GetStringWidth($certificado) / 2), 19 + $yOffset, $certificado);

				$mpdf->SetTextColor(93, 148, 177);
				$mpdf->Text(94 - ($mpdf->GetStringWidth($aluno) / 2), 79 + $yOffset, $aluno);

				$mpdf->SetTextColor(0, 0, 0);
				$mpdf->SetFontSize(13);
				$mpdf->Text(270 - ($mpdf->GetStringWidth($numeroLabel) / 2), 33 + $yOffset, $numeroLabel);
				$mpdf->Text(270 - ($mpdf->GetStringWidth($numero) / 2), 39 + $yOffset, $numero);

				$mpdf->Text(94 - ($mpdf->GetStringWidth($declaracao1) / 2), 44 + $yOffset, $declaracao1);
				$mpdf->Text(94 - ($mpdf->GetStringWidth($declaracao2) / 2), 51 + $yOffset, $declaracao2);
				$mpdf->Text(94 - ($mpdf->GetStringWidth($declaracao3) / 2), 57 + $yOffset, $declaracao3);

				$mpdf->Text(94 - ($mpdf->GetStringWidth($concluiuLabel) / 2), 105 + $yOffset, $concluiuLabel);

				$mpdf->Text(94 - ($mpdf->GetStringWidth($curso) / 2), 122 + $yOffset, $curso);

				$mpdf->Text(94 - ($mpdf->GetStringWidth($carga) / 2), 130 + $yOffset, $carga);

				$mpdf->Text(94 - ($mpdf->GetStringWidth($instituicaoCavokLabel) / 2), 139 + $yOffset, $instituicaoCavokLabel);

				$mpdf->Text(94 - ($mpdf->GetStringWidth($dataLabel) / 2), 156 + $yOffset, $dataLabel);

				$mpdf->Text(94 - ($mpdf->GetStringWidth($cavokLabel) / 2), 191 + $yOffset, $cavokLabel);

				$mpdf->Text(94 - ($mpdf->GetStringWidth($instituicaoLabel) / 2), 199 + $yOffset, $instituicaoLabel);

				$mpdf->Text(240 - ($mpdf->GetStringWidth($aluno_short) / 2), 191 + $yOffset, $aluno_short);

				$mpdf->Text(240 - ($mpdf->GetStringWidth($concluinteLabel) / 2), 199 + $yOffset, $concluinteLabel);

				$configuracaoGeral->setNumUltimoCertificado($ultimoNumero)->update();

				$mpdf->AddPage();
				$pageCount = $mpdf->setSourceFile($srcFile2);
				$tplId = $mpdf->ImportPage($pageCount);
				$mpdf->UseTemplate($tplId);

				$nome = Url::Title($aluno);
				$mpdf->Output("certificado-{$nome}.pdf", true);
			} catch (PdfParserException $e)
			{
				dd($e->getMessage());
			} catch (MpdfException $e)
			{
				dd($e->getMessage());
			}

		}
	}
}
