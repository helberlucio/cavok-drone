<?php

namespace Modulos\Site\Avaliacao\Models\RegrasDeNegocio;

use Aplicacao\Url;
use ModuloHelp\PadraoCriacao\ModelRegraDeNegocio\Abstracts\ModelRegraDeNegocioPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteAgendaEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteCursosOnlineEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteMatriculasOnlineEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteQuestoesOnlineEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\WebsiteGeralEntidade;
use Modulos\Site\AreaDoCliente\Models\RegrasDeNegocio\SiteAreaDoClienteRN;
use Mpdf\Mpdf;
use Mpdf\MpdfException;
use setasign\Fpdi\PdfParser\PdfParserException;

class SiteAvaliacaoOnlineRN extends ModelRegraDeNegocioPadraoAbsHelp
{

	public static $SESSION_CURSO     = 'site_curso_online';
	public static $SESSION_MATRICULA = 'site_matricula_online';
	public static $SESSION_CURSOS    = 'site_cursos_online';
	public static $SESSION_PERGUNTAS = 'site_perguntas_online';
	public static $SESSION_RESPOSTAS = 'site_respostas_online';

	public function clear()
	{
		unset($_SESSION[ self::$SESSION_CURSO ]);
		unset($_SESSION[ self::$SESSION_MATRICULA ]);
		unset($_SESSION[ self::$SESSION_CURSOS ]);
		unset($_SESSION[ self::$SESSION_PERGUNTAS ]);
		unset($_SESSION[ self::$SESSION_RESPOSTAS ]);
	}

	public function addResposta($pergunta, $resposta)
	{
		$_SESSION[ self::$SESSION_RESPOSTAS ][ $pergunta ] = $resposta;

		return $this;
	}

	public function getRepostas()
	{
		return isset($_SESSION[ self::$SESSION_RESPOSTAS ]) ? $_SESSION[ self::$SESSION_RESPOSTAS ] : [];
	}

	public function getResposta($pergunta)
	{
		return isset($_SESSION[ self::$SESSION_RESPOSTAS ][ $pergunta ]) ? $_SESSION[ self::$SESSION_RESPOSTAS ][ $pergunta ] : null;
	}

	public function setProximaPergunta(SiteQuestoesOnlineEntidade $item = null)
	{
		if (!$item) $item = new SiteQuestoesOnlineEntidade();
		$_SESSION[ self::$SESSION_PERGUNTAS ]['proxima'] = serialize($item);

		return $this;
	}

	public function setAtualPergunta(SiteQuestoesOnlineEntidade $item = null)
	{
		if (!$item) $item = new SiteQuestoesOnlineEntidade();
		$_SESSION[ self::$SESSION_PERGUNTAS ]['atual'] = serialize($item);

		return $this;
	}

	public function setAnteriorPergunta(SiteQuestoesOnlineEntidade $item = null)
	{
		if (!$item) $item = new SiteQuestoesOnlineEntidade();
		$_SESSION[ self::$SESSION_PERGUNTAS ]['anterior'] = serialize($item);

		return $this;
	}

	/**
	 * @return SiteQuestoesOnlineEntidade
	 */
	public function getProximaPergunta()
	{
		return isset($_SESSION[ self::$SESSION_PERGUNTAS ]) ? unserialize($_SESSION[ self::$SESSION_PERGUNTAS ]['proxima']) : new SiteQuestoesOnlineEntidade();
	}

	/**
	 * @return SiteQuestoesOnlineEntidade
	 */
	public function getAtualPergunta()
	{
		return isset($_SESSION[ self::$SESSION_PERGUNTAS ]) ? unserialize($_SESSION[ self::$SESSION_PERGUNTAS ]['atual']) : new SiteQuestoesOnlineEntidade();
	}

	/**
	 * @return SiteQuestoesOnlineEntidade
	 */
	public function getAnteriorPergunta()
	{
		return isset($_SESSION[ self::$SESSION_PERGUNTAS ]) ? unserialize($_SESSION[ self::$SESSION_PERGUNTAS ]['anterior']) : new SiteQuestoesOnlineEntidade();
	}

	/**
	 * @return SiteAgendaEntidade[]
	 */
	public function getCursos()
	{
		$cursos = [];
		foreach ((new SiteCursosOnlineEntidade())->getRows($this->db) as $curso)
		{
			$cursos[ $curso->getId() ] = $curso;
		}
		$agendas = [];
		/** @var SiteAgendaEntidade $agenda */
		foreach (unserialize($_SESSION[ self::$SESSION_CURSOS ]) as $agenda)
		{
			$agenda->setCurso($cursos[ $agenda->getCurso() ]);
			$agendas[] = $agenda;
		}

		return $agendas;
	}

	/**
	 * @return SiteCursosOnlineEntidade
	 */
	public function getCurso()
	{
		return isset($_SESSION[ self::$SESSION_CURSO ])
			? unserialize($_SESSION[ self::$SESSION_CURSO ])
			: new SiteCursosOnlineEntidade();
	}

	public function setCurso($curso_id)
	{
		$_SESSION[ self::$SESSION_CURSO ] = serialize((new SiteCursosOnlineEntidade())->getRow($this->db->where('id', $curso_id)));

		return $this;
	}

	/**
	 * @return SiteMatriculasOnlineEntidade
	 */
	public function getMatricula()
	{
		return isset($_SESSION[ self::$SESSION_MATRICULA ])
			? unserialize($_SESSION[ self::$SESSION_MATRICULA ])
			: new SiteMatriculasOnlineEntidade();
	}

	public function setMatricula($matricula_id)
	{
		$_SESSION[ self::$SESSION_MATRICULA ] = serialize((new SiteMatriculasOnlineEntidade())->getRow($this->db->where('id', $matricula_id)));

		return $this;
	}

	/**
	 *
	 */
	public function gerarCertificado()
	{
		$srcFile = $this->getCurso()->getCertificadoPdfFrente();
		$srcFile2 = $this->getCurso()->getCertificadoPdfFundo();
		if (file_exists($srcFile) && file_exists($srcFile2))
		{
			$configuracaoGeral = (new WebsiteGeralEntidade())->getRow($this->db);
			$ultimoNumero = $configuracaoGeral->getNumUltimoCertificado();
			try
			{
				$ultimoNumero++;
				$numeroLabel = 'N° do Certificado';
				$numero = str_pad($ultimoNumero, 9, '0', STR_PAD_LEFT);
				$certificado = 'C E R T I F I C A D O';
				$declaracao1 = 'Em conformidade com a legislação educacional brasileira,';
				$declaracao2 = 'conforme decreto n° 5.154, de 23 de julho de 2004, este';
				$declaracao3 = 'documento certifica para os devidos fins que,';
				$curso = 'Curso: ' . mb_strtoupper($this->getCurso()->getNome());
				$carga = '';
				$aluno = mb_strtoupper((new SiteAreaDoClienteRN())->getUser()->getNome());
				$aluno_short = explode(' ', $aluno);
				if (count($aluno_short) > 2)
				{
					$aluno_short = array_reverse($aluno_short);
					$aluno_short[1] = substr($aluno_short[1], 0, 1) . '.';
					$aluno_short = array_reverse($aluno_short);
				}
				$aluno_short = implode(' ', $aluno_short);
				$concluiuLabel = 'Concluiu com êxito:';
				$instituicaoLabel = 'INSTITUIÇÃO';
				$cavokLabel = 'CAVOK DRONE SERVICE BRASIL';
				$instituicaoCavokLabel = $instituicaoLabel . ': ' . $cavokLabel;
				$mes = [1  => 'Janeiro', 2 => 'Fevereiro', 3 => 'Março',
						4  => 'Abril', 5 => 'Maio', 6 => 'Junho', 7 => 'Julho',
						8  => 'Agosto', 9 => 'Setembro', 10 => 'Outubro', 11 => 'Novembro',
						12 => 'Dezembro',][ date('n') ];
				$ano = date('Y');
				$dia = date('d');
				$dataLabel = "Goiânia, {$dia} de {$mes} de {$ano}.";
				$concluinteLabel = "CONCLUINTE";

				$mpdf = new Mpdf(['orientation' => 'L']);
				$pageCount = $mpdf->setSourceFile($srcFile);
				$tplId = $mpdf->ImportPage($pageCount);
				$mpdf->UseTemplate($tplId);
				$mpdf->SetFont('Arial');

				$mpdf->SetFontSize(23);
				$mpdf->SetTextColor(255, 255, 255);
				$yOffset = 4;
				$mpdf->Text(94 - ($mpdf->GetStringWidth($certificado) / 2), 19 + $yOffset, $certificado);

				$mpdf->SetTextColor(93, 148, 177);
				$mpdf->Text(94 - ($mpdf->GetStringWidth($aluno) / 2), 79 + $yOffset, $aluno);

				$mpdf->SetTextColor(0, 0, 0);
				$mpdf->SetFontSize(13);
				$mpdf->Text(270 - ($mpdf->GetStringWidth($numeroLabel) / 2), 33 + $yOffset, $numeroLabel);
				$mpdf->Text(270 - ($mpdf->GetStringWidth($numero) / 2), 39 + $yOffset, $numero);

				$mpdf->Text(94 - ($mpdf->GetStringWidth($declaracao1) / 2), 44 + $yOffset, $declaracao1);
				$mpdf->Text(94 - ($mpdf->GetStringWidth($declaracao2) / 2), 51 + $yOffset, $declaracao2);
				$mpdf->Text(94 - ($mpdf->GetStringWidth($declaracao3) / 2), 57 + $yOffset, $declaracao3);

				$mpdf->Text(94 - ($mpdf->GetStringWidth($concluiuLabel) / 2), 105 + $yOffset, $concluiuLabel);

				$mpdf->Text(94 - ($mpdf->GetStringWidth($curso) / 2), 122 + $yOffset, $curso);

				$mpdf->Text(94 - ($mpdf->GetStringWidth($carga) / 2), 130 + $yOffset, $carga);

				$mpdf->Text(94 - ($mpdf->GetStringWidth($instituicaoCavokLabel) / 2), 139 + $yOffset, $instituicaoCavokLabel);

				$mpdf->Text(94 - ($mpdf->GetStringWidth($dataLabel) / 2), 156 + $yOffset, $dataLabel);

				$mpdf->Text(94 - ($mpdf->GetStringWidth($cavokLabel) / 2), 191 + $yOffset, $cavokLabel);

				$mpdf->Text(94 - ($mpdf->GetStringWidth($instituicaoLabel) / 2), 199 + $yOffset, $instituicaoLabel);

				$mpdf->Text(240 - ($mpdf->GetStringWidth($aluno_short) / 2), 191 + $yOffset, $aluno_short);

				$mpdf->Text(240 - ($mpdf->GetStringWidth($concluinteLabel) / 2), 199 + $yOffset, $concluinteLabel);

				$configuracaoGeral->setNumUltimoCertificado($ultimoNumero)->update();

				$mpdf->AddPage();
				$pageCount = $mpdf->setSourceFile($srcFile2);
				$tplId = $mpdf->ImportPage($pageCount);
				$mpdf->UseTemplate($tplId);

				$nome = Url::Title($aluno);
				$mpdf->Output("certificado-{$nome}.pdf", true);
			} catch (PdfParserException $e)
			{
				dd($e->getMessage());
			} catch (MpdfException $e)
			{
				dd($e->getMessage());
			}

		}
	}
}
