<?php
/**
 * @var SysPagesEntidade $page
 * @var int              $porcentagem
 * @var string           $aluno
 */

use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysPagesEntidade;
use Modulos\Site\Avaliacao\Models\Repositorios\SiteAvaliacaoRP;

?>
<div class="container mb-5 text-center">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1 class="text-primary mt-5 mb-5">Você atingiu <?= $porcentagem ?>%</h1>
			<?php if ($porcentagem < 70): ?>
                <h1 class="mb-5"><?= $aluno ?></h1>
                <h5 class="mb-5">Você não foi aprovado na avaliação da Cavok Drone Service.</h5>
			<?php else: ?>
                <h1 class="mb-5">Parabéns, <?= $aluno ?></h1>
                <h5 class="mb-5">Você foi aprovado na avaliação da Cavok Drone Service. Para baixar o seu certificado
                    basta clicar no botão
                    abaixo “Fazer download do certificado”.</h5>
                <div class="text-center">
                    <a class="btn btn-primary px-5"
                       href="<?= Url::ModuloApelido(SiteAvaliacaoRP::class, 'Online/certificado', 'certificado-online-download') ?>">Fazer
                        download do certificado</a>
                </div>
			<?php endif; ?>
        </div>
    </div>
</div>