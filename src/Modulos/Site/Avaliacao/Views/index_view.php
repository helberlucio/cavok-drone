<?php
/**
 * @var SysPagesEntidade $page
 */

use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysPagesEntidade;

?>
<div class="banner mb-5 container-fluid"
     style="background-image: url('<?= Url::Base($page->getBanner()) ?>'); min-height: <?= $page->getAlturaBanner() ?>px;">
    <div class="row h-100 align-items-center">
        <div class="col">
            <h3 class="titulo text-white text-center text-uppercase"><?= $page->getTitulo() ?></h3>
        </div>
    </div>
</div>
<div class="container mb-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h5 class="mb-5 text-center text-dark"><?= nl2br($page->getDescricao()) ?></h5>
            <form action="" method="post">
                <div class="form-group">
                    <input class="form-control form-control-lg mask-cpf text-center rounded-0" type="text"
                           id="input-cpf"
                           name="cpf"
                           placeholder="*Digite seu CPF aqui para logar na plataforma" required>
                    <?php if (isset($error)): ?>
                        <div class="alert alert-danger text-center  rounded-0 border-top-0"><?= $error ?></div>
                    <?php endif; ?>
                </div>
                <div class="row justify-content-center mt-5">
                    <div class="col-md-8">
                        <button type="submit" class="btn btn-primary btn-lg btn-block" style="border-radius: 24px">
                            Iniciar prova
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>