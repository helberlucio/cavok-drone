<?php
/**
 * @var \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteAgendaEntidade[] $items
 */

use Aplicacao\Url;
use Modulos\Site\Avaliacao\Models\Repositorios\SiteAvaliacaoRP;

?>
<div class="index">
    <div class="container mt-5 mb-5">
        <div class="row justify-content-center">
			<?php foreach ($items as $item): ?>
                <div class="col-md-4  mb-3">
                    <div class="item border border-gray h-100">
                        <img src="<?= Url::Base($item->getCurso()->getImagem()) ?>"
                             alt="<?= $item->getCurso()->getNome() ?>"
                             class="img-fluid">
                        <div class="p-4">
                            <h5 class="text-primary text-center mb-4"><?= $item->getCurso()->getNome() ?></h5>
                            <h5 class="text-center mb-4"><?= date('d/m/Y', strtotime($item->getData())) ?></h5>
                            <a href="<?= Url::ModuloApelido(SiteAvaliacaoRP::class, "Index/cursos?menu=avaliacao&agenda={$item->getId()}", 'avaliacao-' . Url::Title($item->getCurso()->getNome())) ?>"
                               class="btn btn-primary btn-block pt-2 pb-2">
                                Selecionar curso
                            </a>
                        </div>
                    </div>
                </div>
			<?php endforeach; ?>
        </div>
    </div>
</div>
