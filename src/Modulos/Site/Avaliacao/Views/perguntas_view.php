<?php
/**
 * @var SysPagesEntidade $page
 * @var string           $selecionado
 * @var string           $curso
 */

use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysPagesEntidade;
use Modulos\Site\Avaliacao\Models\RegrasDeNegocio\SiteAvaliacaoRN;
use Modulos\Site\Avaliacao\Models\Repositorios\SiteAvaliacaoRP;

$rn = new SiteAvaliacaoRN();
$anterior = $rn->getAnteriorPergunta();
$atual = $rn->getAtualPergunta();
$proxima = $rn->getProximaPergunta();

?>
<div class="perguntas container mb-5">
    <h6 class="titulo text-primary text-center mt-5 mb-5"><?= $page->getTitulo() ?></h6>
    <h6 class="text-primary mb-4">
        <small class="text-muted float-right">Curso: <?= $curso ?></small>
        Questão <?= str_pad($atual->getOrdem(), 2, '0', STR_PAD_LEFT) ?>
    </h6>
    <p class="mb-5"><?= nl2br($atual->getPergunta()) ?></p>
    <form method="post" action="<?= $proxima->getId()
		? Url::ModuloApelido(SiteAvaliacaoRP::class, 'Index/perguntas?menu=avaliacao&atual=' . $proxima->getId(), 'avaliacao-pergunta-' . $proxima->getOrdem())
		: Url::ModuloApelido(SiteAvaliacaoRP::class, 'Index/resultado?menu=avaliacao', 'avaliacao-resultado') ?>">
        <div class="form-check mb-4">
            <input class="form-check-input" type="radio" name="resposta" id="resposta-a" value="a"
                   required <?= $selecionado == 'a' ? 'checked' : '' ?>>
            <label class="form-check-label" for="resposta-a">
                <h5 class="text-primary d-inline-block pl-3 pr-3">A</h5><?= nl2br($atual->getRespostaA()) ?>
            </label>
        </div>
        <div class="form-check mb-4">
            <input class="form-check-input" type="radio" name="resposta" id="resposta-b" value="b"
                   required <?= $selecionado == 'b' ? 'checked' : '' ?>>
            <label class="form-check-label" for="resposta-b">
                <h5 class="text-primary d-inline-block pl-3 pr-3">B</h5><?= nl2br($atual->getRespostaB()) ?>
            </label>
        </div>
		<?php if (!empty($atual->getRespostaC())): ?>
            <div class="form-check mb-4">
                <input class="form-check-input" type="radio" name="resposta" id="resposta-c" value="c"
                       required <?= $selecionado == 'c' ? 'checked' : '' ?>>
                <label class="form-check-label" for="resposta-c">
                    <h5 class="text-primary d-inline-block pl-3 pr-3">C</h5><?= nl2br($atual->getRespostaC()) ?>
                </label>
            </div>
		<?php endif; ?>
		<?php if (!empty($atual->getRespostaD())): ?>
            <div class="form-check mb-4">
                <input class="form-check-input" type="radio" name="resposta" id="resposta-d" value="d"
                       required <?= $selecionado == 'd' ? 'checked' : '' ?>>
                <label class="form-check-label" for="resposta-d">
                    <h5 class="text-primary d-inline-block pl-3 pr-3">D</h5><?= nl2br($atual->getRespostaD()) ?>
                </label>
            </div>
		<?php endif; ?>
        <div class="row justify-content-center">
			<?php if ($anterior->getId()): ?>
                <div class="col-md-3">
                    <a href="<?= Url::ModuloApelido(SiteAvaliacaoRP::class, 'Index/perguntas?menu=avaliacao&atual=' . $anterior->getId(), 'avaliacao-pergunta-' . $anterior->getOrdem()) ?>"
                       class="btn btn-gray btn-block">Voltar</a>
                </div>
			<?php endif; ?>
            <div class="col-md-3">
                <button type="submit"
                        class="btn btn-primary btn-block"><?= $proxima->getId() ? 'Próximo' : 'Finalizar' ?></button>
            </div>
        </div>
    </form>
</div>