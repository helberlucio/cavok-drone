<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 23/04/2018
 * Time: 19:24
 */

namespace Modulos\Site\Blog\Models\RegrasDeNegocio;


use ModuloHelp\PadraoCriacao\ModelRegraDeNegocio\Abstracts\ModelRegraDeNegocioPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\BlogPostagensEntidade;

class Pesquisar extends ModelRegraDeNegocioPadraoAbsHelp
{

    public function getResultadoPesquisaPorCategoria()
    {
        $palavraChave = input_get('pesquisa_por_categoria');
        $palavraChave = '%' . str_replace(' ', '%', $palavraChave) . '%';
        $palavraChave = str_replace('%%', '%', $palavraChave);
        $db = $this->db->select('blog_postagens.*')
            ->join('blog_categorias bc', 'bc.id = blog_postagens.categoria', 'LEFT')
            ->where("(bc.titulo LIKE '" . $palavraChave . "')", '', true)
            ->order_by('blog_postagens.ordem', 'DESC');
        return (new BlogPostagensEntidade())->getRows($db, false);
    }

    public function getResultadoPesquisa()
    {
        $busca = input_get('pesquisa');
        $busca = '%' . str_replace(' ', '%', $busca) . '%';
        $busca = str_replace('%%', '%', $busca);
        return $this->getResultado($busca);
    }

    private function getResultado($busca)
    {
        return (new BlogPostagensEntidade())->getRows($this->getCondicionaisPesquisa($busca), false);
    }

    private function getCondicionaisPesquisa($palavraChave)
    {
        $db = $this->db->select('blog_postagens.*')
            ->join('blog_categorias bc', 'bc.id = blog_postagens.categoria', 'LEFT')
            ->where("(blog_postagens.palavras_chaves LIKE '" . $palavraChave . "' OR blog_postagens.titulo LIKE '" . $palavraChave . "' OR blog_postagens.descricao LIKE '" . $palavraChave . "' OR blog_postagens.chamada LIKE '" . $palavraChave . "' OR bc.titulo LIKE '" . $palavraChave . "')", '', true)
            ->order_by('blog_postagens.ordem', 'DESC');
        return $db;
    }
}