<?php
/**
 * @var $blog_comunicacao \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\ComunicacaoBlogEntidade
 */

use Aplicacao\Ferramentas\GetAssets;
use Aplicacao\Url;
use Modulos\Blog\Blog\Models\Repositorios\BlogBlogRP;

GetAssets::CSS('SiteBlog', 'Site', 'Blog');

?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 d-none d-sm-block SiteBlog-banner"
                 style="background-image: url(<?php echo Url::Base() . $blog_comunicacao->getBanner(); ?>);<?php echo 'height: ' . ($blog_comunicacao->getAlturaBanner() >= 100 ? $blog_comunicacao->getAlturaBanner() : '400') . 'px'; ?>">
            </div>
            <div class="col-md-12 d-sm-none SiteBlog-banner"
                 style="background-image: url(<?php echo Url::Base() . $blog_comunicacao->getBannerMobile(); ?>);<?php echo 'height: ' . ($blog_comunicacao->getBanner() >= 100 ? $blog_comunicacao->getAlturaBannerMobile() : '400') . 'px'; ?>;">
            </div>
        </div>
        <div class="row">
            <div class="container">
                <div class="col-md-12 SiteBlog-titulo">
                    <h1 class="text-primary text-center mt-4 mb-2 text-uppercase"><?php echo $blog_comunicacao->getTitulo(); ?></h1>
                    <p class="text-center mb-5"><?php echo $blog_comunicacao->getSubTitulo(); ?></p>
                </div>
            </div>
        </div>
    </div>

<?php echo BlogBlogRP::getIndexPage();