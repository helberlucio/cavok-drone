<?php

namespace Modulos\Site\Blog\Controllers;

use Aplicacao\Url;
use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;
use Modulos\Site\Blog\Models\Repositorios\SiteBlogRP;

class Index extends ControladorPadraoAbsHelp
{

    public $admin = false;

    public function index($view = 'index', $data = array(), $admin = false)
    {
        echo SiteBlogRP::getIndexPage('index', array(), false);
    }

    public function pesquisar()
    {
        echo SiteBlogRP::getIndexPage('index', array(), false);
    }

    public function refazerPesquisa()
    {
        $pesquisa = input_get('pesquisa');
        if (!$pesquisa) {
            $urlToGo = Url::Base() . Url::Apelido(SiteBlogRP::getUrlModulo('Index'), 'blog');
            Header('Location:' . $urlToGo);
        }
        $pesquisa = str_replace('#', '', $pesquisa);
        
        $urlPesquisa = \Aplicacao\Url::Base() .
            \Aplicacao\Url::Apelido(
                \Modulos\Site\Blog\Models\Repositorios\SiteBlogRP::getUrlModulo('Index/pesquisar') . '?pesquisa=' .
                urlencode($pesquisa), \Aplicacao\Url::Title($pesquisa),
                array('noticias', 'pesquisa')
            );
        Header('Location:' . $urlPesquisa);
    }
}