<?php
/**
 * @var SysPagesEntidade          $page
 * @var SiteConsultoriaEntidade[] $items
 */

use Aplicacao\Url;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SiteConsultoriaEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysPagesEntidade;

?>
<img src="<?= Url::Base($page->getBanner()) ?>" alt="<?= $page->getTitulo() ?>" class="img-fluid">
<div class="formulario pt-5 pb-5">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-7">
                <h5 class="mb-4 text-primary"><?= $page->getTitulo() ?></h5>
                <p><?= nl2br($page->getDescricao()) ?></p>
            </div>
            <div class="col-md-4">
                <h5 class="text-white mb-4 bg-dark text-white p-3 text-center">Faça seu orçamento</h5>
                <form id="form-fale-conosco" method="post" action="">
                    <div class="form-group">
                        <label class="text-primary" for="input-nome">* Nome</label>
                        <input type="text" class="form-control rounded-sm" name="nome" id="input-nome"
                               value="<?= isset($nome) ? $nome : '' ?>"
                               required>
                    </div>
                    <div class="form-group">
                        <label class="text-primary" for="input-cpf">* CPF</label>
                        <input type="text" class="form-control rounded-sm mask-cpf" name="cpf" id="input-cpf"
                               value="<?= isset($cpf) ? $cpf : '' ?>"
                               required>
                    </div>
                    <div class="form-group">
                        <label class="text-primary" for="input-drone">* Nome do drone</label>
                        <input type="text" class="form-control rounded-sm" name="drone" id="input-drone"
                               value="<?= isset($drone) ? $drone : '' ?>"
                               required>
                    </div>
                    <div class="form-group">
                        <label class="text-primary" for="input-modelo">* Modelo</label>
                        <input type="text" class="form-control rounded-sm" name="modelo" id="input-modelo"
                               value="<?= isset($modelo) ? $modelo : '' ?>"
                               required>
                    </div>
                    <div class="form-group">
                        <label class="text-primary" for="input-marca">* Marca</label>
                        <input type="text" class="form-control rounded-sm" name="marca" id="input-marca"
                               value="<?= isset($marca) ? $marca : '' ?>"
                               required>
                    </div>
                    <div class="form-group">
                        <label class="text-primary" for="input-telefone">* Telefone ou Whatsapp</label>
                        <input type="tel" class="form-control rounded-sm mask-telefone" name="telefone"
                               id="input-telefone"
                               value="<?= isset($telefone) ? $telefone : '' ?>"
                               required>
                    </div>
                    <div class="form-group">
                        <label class="text-primary" for="input-email">* Seu e-mail</label>
                        <input type="email" class="form-control rounded-sm" name="email" id="input-email"
                               value="<?= isset($email) ? $email : '' ?>"
                               required>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block pt-3 pb-3 rounded-0">
                        Enviar solicitação de orçamento
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        <?php if (isset($form)): ?>
        <?php if ($form['sucesso']): ?>
        Swal.fire({
            title: 'Sucesso!',
            text: 'Entraremos em contato em breve!',
            type: 'success',
        });
        <?php else: ?>
        Swal.fire({
            title: 'Erro!',
            text: 'Não foi possível realizar a solicitação. Tente novamente mais tarde ou entre em contato com nosso suporte via telefone.',
            type: 'error',
        });
        <?php endif; ?>
        <?php endif; ?>
    });
</script>