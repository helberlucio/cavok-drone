<?php

namespace Modulos\Site\SeguroReta\Controllers;

use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;
use Modulos\Site\SeguroReta\Models\RegrasDeNegocio\SiteSeguroRetaRN;
use Modulos\Site\SeguroReta\Models\Repositorios\SiteSeguroRetaRP;

class Index extends ControladorPadraoAbsHelp {

    public $admin = false;

    public function index($view = 'index', $data = [], $admin = false) {
        $view = 'Home/index';
        $data['pagina'] = 'index';

        if (!empty(input_post())) {
            $data['form'] = (new SiteSeguroRetaRN())->enviar(input_post());
        }

        echo SiteSeguroRetaRP::getIndexPage($view, $data, $admin);
    }
}