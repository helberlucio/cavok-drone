<?php

namespace Modulos\Authentications\TokenAdmin\Models\RegrasDeNegocio;

use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\ModelRegraDeNegocio\Abstracts\ModelRegraDeNegocioPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsUsuariosEntidade;

class AuthenticationsTokenAdminRN
{

    private $db;

    public function __construct()
    {
        $this->db = new ModelagemDb();
    }

    /**
     * @param $token
     * @return $this|CmsUsuariosEntidade
     */
    public function VerificarToken($token)
    {
        $usserByToken = (new CmsUsuariosEntidade())->getRow($this->db->where('token', $token));
        if (!$usserByToken->getId()) {
            return (new CmsUsuariosEntidade());
        }
        return $usserByToken;
    }
}
