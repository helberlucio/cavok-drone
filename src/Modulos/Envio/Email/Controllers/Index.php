<?php
namespace Modulos\Envio\Email\Controllers;

use Aplicacao\Ferramentas\Ajax;
use Core\Controlador\Controlador;
use Modulos\Envio\Email\Models\RegrasDeNegocio\EnvioEmailRN;
use Modulos\Envio\Email\Models\Repositorios\CapturaMetricasEmailMarketing;
use Modulos\Envio\Email\Models\Repositorios\EnvioEmailRP;

class Index extends Controlador
{
    private $regraDeNegocio;

    public function __construct()
    {
        $this->regraDeNegocio = new EnvioEmailRN();
        parent::__construct();
    }

    public function index()
    {
        echo EnvioEmailRP::getIndexPage();
    }

    public function dispararEmailCapanha()
    {
        $idCampanha = (int)input_post('id_campanha');
        $enviarDesabilitado = input_post('enviar_independente_habilitado') ? true : false;
        $enviarForaDaData = input_post('enviar_independente_da_data') ? true : false;

        $callback = $this->regraDeNegocio->dispararEmailCapanha($idCampanha, $enviarDesabilitado, $enviarForaDaData);
        Ajax::retorno($callback);
    }

    public function getCampanhaPorId($idCampanha = 0)
    {
        $this->regraDeNegocio->getCampanhaPorId($idCampanha);
    }

    public function Capturar_metricas($email = null)
    {
        $sImage = 'src/Assets/Gerenciador/autodesenvolvelogo.png';
        $im = imagecreatefrompng($sImage);
        header('Content-Type: image/png');
        imagepng($im);
        imagedestroy($im);
        CapturaMetricasEmailMarketing::init($email);
    }
}