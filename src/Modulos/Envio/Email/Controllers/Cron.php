<?php
/**
 * Created by PhpStorm.
 * User: Gabriel PHP
 * Date: 15/05/2017
 * Time: 17:14
 */

namespace Modulos\Envio\Email\Controllers;


use ModuloHelp\PadraoCriacao\ControladorPadrao\Abstracts\ControladorPadraoAbsHelp;
use Modulos\Envio\Email\Models\RegrasDeNegocio\CampanhasParaEnviar;
use Modulos\Envio\Email\Models\RegrasDeNegocio\RealizarEnvios;

    class Cron extends ControladorPadraoAbsHelp
{

    public function index()
    {
        $envios = (new RealizarEnvios())->iniciar();
        var_dump($envios);
    }

}