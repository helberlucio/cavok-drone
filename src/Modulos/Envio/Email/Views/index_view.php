<?php
/**
 * @var $campanhas \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\EmailCampanhas[]
 * @var $campanha \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\EmailCampanhas
 * @var $emails
 * @var $anexos array
 */
\Aplicacao\Ferramentas\GetAssets::CSS('EnvioEmail', 'Envio', 'Email');
?>
<div class="container-fluid" style="padding: 0;">
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-envelope"></i>
            Envio de email
            <i class="fa fa-angle-right"></i>
            Enviar Lista de email
            <small>lista de acordo com a campanha selecionada</small>
        </div>
        <div class="panel-body" style="min-height:120px;padding: 0; padding-top: 25px;">
            <form class="form_campanhas">
                <div class="col-md-12 form-group">
                    <label>
                        <b>Selecionar Camapanha para disparo.</b>
                    </label>
                    <select name="camapanha" class="form-control">
                        <optgroup label="Camapanhas">
                            <?php
                            foreach ($campanhas as $campanhaDados) {
                                ?>
                                <option id="<?php echo $campanhaDados->getId(); ?>">
                                    <?php echo $campanhaDados->getTituloIdentificador(); ?>
                                </option>
                                <?php
                            }
                            ?>
                        </optgroup>
                    </select>
                    <button type="submit" style="margin-top:15px;" class="btn btn-success">Carregar Campanha</button>
                </div>
            </form>
            <div class="col-md-12"
                 style="border:1px solid #212121;padding:15px;margin-top:20px;max-height: 200px;overflow-y: auto;">
                <?php
                $contadorDeEmailsDaCampanha = 0;
                array_reverse($emails);
                foreach ($emails as $email) {

                    if (\Aplicacao\Ferramentas\Identificadores::eEmail(isset($email[0]) ? $email[0] : '')) {
                        $contadorDeEmailsDaCampanha++;
                        if ($contadorDeEmailsDaCampanha > 1000) {
                            break;
                        }

                        ?>
                        <div class="col-md-12 lista-email">
                            <label>
                                <input type="checkbox" name="emailSelecionado[]" checked="checked">
                                <?php echo isset($email[0]) ? $email[0] : 'Não identificado'; ?>
                            </label>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <div class="col-md-12" style="padding: 15px;margin-top:20px;">
                <b>Titulo:</b> <?php echo $campanha->getTitulo(); ?>
            </div>
            <div class="col-md-12" style="margin-top:20px;">
                <b>Corpo do e-mail:</b><br><br>
                <?php
                echo $campanha->getDescricao();
                ?>
            </div>
            <div style="padding: 0;" class="col-md-12">
                <label>
                    <b>
                        Anexos da campanha abaixo:
                    </b>
                </label>
            </div>
            <div class="col-md-12" style="padding: 0;">
                <?php
                foreach ($anexos as $anexo) {
                    ?>
                    <img src="<?php echo \Aplicacao\Url::Base($anexo['file']); ?>" width="185px">
                    <?php
                }
                ?>
            </div>
            <form method="post" class="form_detalhes">
                <div class="col-md-12" style="margin-top:15px">
                    <label><b>Data Para iniciar os
                            disparos:</b></label> <?php echo date('d/m/Y', strtotime($campanha->getDataDisparo())); ?>
                </div>
                <div class="col-md-12" style="margin-top:15px">
                    <label><input type="checkbox" name="enviar_independente_da_data" value="1">Enviar Agora independente
                        da data de
                        envio</label>
                </div>
                <div class="col-md-12" style="margin-top:15px">
                    <label><b>Habilitado:</b></label> <?php echo $campanha->getHabilitado() ? 'Sim' : ' Não'; ?>
                </div>
                <div class="col-md-12" style="margin-top:15px">
                    <label><input type="checkbox" name="enviar_independente_habilitado" value="1">Enviar independente de
                        estar habilitado</label>
                </div>
                <div class="col-md-12" style="margin-top:20px;margin-bottom:20px;">
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-paper-plane" aria-hidden="true"></i> Enviar E-mail
                    </button>
                </div>
                <input style="opacity:0;" type="text" value="<?php echo $campanha->getId(); ?>" name="id_campanha">
            </form>
            <div class="col-md-12 callback" style="margin-top:20px;">

            </div>
        </div>

    </div>
</div>
<script>

    function enviarEmail() {
        $('.form_detalhes').submit(function () {
            var serial = $(this).serialize();
            $('.callback').empty().append('<div class="alert alert-success">Enviando e-mails</div>');
            $.post(base_url('Modulo/Envio/Email/Index/dispararEmailCapanha/'), serial, function (data) {
                $('.callback').empty().append('<div class="alert alert-success"></div>');
                var resultados = data.emails_enviados;
                console.log(data.emails_enviados);
                $.each(resultados, function (key, val) {
                    var mensagem = resultados[key].mensagem;
                    $('.callback .alert-success').append(mensagem + '<br>');
                });

            }, 'json');
            return false;
        });
    }

    function getCampanha() {
        $('.form_campanhas').submit(function (data) {
            var serial = $(this).serialize();
            $.post(base_url('Modulo/Envio/Email/Index/getCampanhaPorId'), serial, function (data) {

            }, 'joson');
            return false;
        });
    }

    $(function () {
        enviarEmail();
    });

</script>
