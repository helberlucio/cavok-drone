<?php
/**
 * Created by PhpStorm.
 * User: Gabriel PHP
 * Date: 15/05/2017
 * Time: 22:36
 */

namespace Modulos\Envio\Email\Models\RegrasDeNegocio;


use Aplicacao\Conversao;
use Aplicacao\Ferramentas\Identificadores;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\ModelRegraDeNegocio\Abstracts\ModelRegraDeNegocioPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\EmailCadastrarEnvio;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\EmailCadastroServidores;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\EmailConfiguracaoAnexo;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\EmailConfiguracaoServidores;
use Modulos\Envio\Email\Models\Entidades\CampanhaEntidade;

class RealizarEnvios extends ModelRegraDeNegocioPadraoAbsHelp
{

    /**
     * @var EmailConfiguracaoAnexo
     */
    private $anexoConfiguracao;

    /**
     * @var EmailCadastroServidores[]
     */
    private $servidores = array();

    /**
     * @var int
     */
    private $quantidadeDeServidores = 0;

    /**
     * @var EmailConfiguracaoServidores
     */
    private $configuracoesServidores;

    /**
     * @var CampanhaEntidade
     */
    private $campanha;

    /**
     * @var ModelagemDb
     */
    public $db;

    public function __construct()
    {
        $this->db = new ModelagemDb();
        $this->servidores = (new EmailCadastroServidores())->getRows((new ModelagemDb()));
        $this->quantidadeDeServidores = (count($this->servidores) - 1);

        $emailConfiguracaoServidores = (new EmailConfiguracaoServidores())->getRow($this->db);

        $this->tempoDeDescansoEmMinutos = ($emailConfiguracaoServidores->getQuantidadeEnvioParaDescanso() * 60);
        $this->quantidadeMaximaDeEnviosParaDescansoDoServidor = $emailConfiguracaoServidores->getQuantidadeEnvioParaDescanso();
        $this->quantidadeMaximaParaTrocaDeServidor = $emailConfiguracaoServidores->getQuantidadeLoop();
        $this->quantidadeMaximaDeEnviosPorServidor = $emailConfiguracaoServidores->getQuantidadeEnvioPorDia();

        $this->anexoConfiguracao = (new EmailConfiguracaoAnexo())->getRow($this->db);

        parent::__construct();
    }

    public function iniciar()
    {
        $retornosDosEnvios = array();
        $campanhasParaEnvio = (new CampanhasParaEnviar())->get();
        foreach ($campanhasParaEnvio as $campanha) {
            $this->campanha = $campanha;
            $retornosDosEnvios[] = $this->enviarCampanha();
        }
        return $retornosDosEnvios;
    }

    private $quantidadeTotalDeEnvios;
    private $registrosDeEnvos = array();

    /**
     * @return array
     */
    private function enviarCampanha()
    {
        echo '<pre>';
        $emailsParaEnviar = $this->campanha->getEmailsParaEnviarDaCampanha();
        $envioMailer = new EnvioMailer();
        $envios = array();
        foreach ($emailsParaEnviar as $email) {
            echo '<hr>';
            if (!isset($email[0])) {
                continue;
            }
            $dadosDoServidor = $this->getDadosDoServidor();
            $tituloParaEnvio = $this->getTituloDaCampanha();
            $anexosParaEnvio = $this->getAnexosDaCampanha();
            var_dump('Email para envio: ');
            echo '<hr>';
            var_dump($email[0]);
            var_dump($tituloParaEnvio);
            var_dump($anexosParaEnvio);
            var_dump($dadosDoServidor);
            $envios[] = $envioMailer->enviarEmailSMTP($dadosDoServidor, $email[0], substr($email[0], 0, 8),
                $tituloParaEnvio,
                $this->campanha->getInformacoesCampanha()->getCorpo(),
                $anexosParaEnvio);
            $this->quantidadeTotalDeEnvios += 1;
            echo '<hr>';
        }
        echo '<hr>';
        var_dump('Quantidade total de envios realizados: ' . $this->quantidadeTotalDeEnvios);
        $this->finalizarDisparo();
        return $envios;
    }

    private function finalizarDisparo()
    {
        $disparo_id = $this->campanha->getDisparoId();
        $cadastroDeEnvio = (new EmailCadastrarEnvio())->getRow($this->db->where('id', $disparo_id));
        if($cadastroDeEnvio->getId() != $disparo_id){
            die('Erro ao encontrar disparo para finalizar');
        }
        $cadastroDeEnvio->setFinalizado(1)->update();
    }

    private $offsetsRegionaisTitulosAnexos = array();

    /**
     * @return array
     */
    private function getAnexosDaCampanha()
    {
        $anexos = $this->campanha->getAnexosDaCampanha();
        $anexosParaEnvio = array();
        foreach ($anexos as $key => $anexo) {
            $offsetIdentificacao = Conversao::underscoreToCamelCase($anexo->getInformacoesAnexo()->getIdentificador());
            if (!isset($this->offsetsRegionaisTitulosAnexos[$offsetIdentificacao]['enviados'])) {
                $this->offsetsRegionaisTitulosAnexos[$offsetIdentificacao]['enviados'] = 0;
            }
            if (!isset($this->offsetsRegionaisTitulosAnexos[$offsetIdentificacao]['offset'])) {
                $this->offsetsRegionaisTitulosAnexos[$offsetIdentificacao]['offset'] = 0;
            }

            if ($this->offsetsRegionaisTitulosAnexos[$offsetIdentificacao]['enviados'] >= $this->anexoConfiguracao->getLoop()) {
                $this->offsetsRegionaisTitulosAnexos[$offsetIdentificacao]['enviados'] = 0;
                $this->offsetsRegionaisTitulosAnexos[$offsetIdentificacao]['offset'] += 1;
            }

            if (count($anexo->getTitulosAnexo()) <= $this->offsetsRegionaisTitulosAnexos[$offsetIdentificacao]['offset']) {
                $this->offsetsRegionaisTitulosAnexos[$offsetIdentificacao]['offset'] = 0;
            }

            $this->offsetsRegionaisTitulosAnexos[$offsetIdentificacao]['enviados'] += 1;

            $data['file'] = $anexo->getInformacoesAnexo()->getAnexo();
            $data['name'] = isset($anexo->getTitulosAnexo()[$this->offsetsRegionaisTitulosAnexos[$offsetIdentificacao]['offset']])
            && isset($anexo->getTitulosAnexo()[$this->offsetsRegionaisTitulosAnexos[$offsetIdentificacao]['offset']]->titulo)
                ? $anexo->getTitulosAnexo()[$this->offsetsRegionaisTitulosAnexos[$offsetIdentificacao]['offset']]->titulo : 'Sem Titulo';
            $anexosParaEnvio[] = $data;
        }
        return $anexosParaEnvio;
    }

    private $totalDeEnviosComMesmoTitulo = 0;
    private $offsetDoTitulo = 0;

    private function getTituloDaCampanha()
    {
        $titulos = $this->campanha->getTitulosDaCampanha();
        $quantidadeDeTitulos = count($titulos);
        $this->totalDeEnviosComMesmoTitulo += 1;

        if ($this->campanha->getInformacoesCampanha()->getQuantidadeTrocaTitulo() <= $this->totalDeEnviosComMesmoTitulo) {
            $this->offsetDoTitulo += 1;
            $this->totalDeEnviosComMesmoTitulo = 0;
        }

        if ($this->offsetDoTitulo >= $quantidadeDeTitulos) {
            $this->offsetDoTitulo = 0;
        }

        return $titulos[$this->offsetDoTitulo];
    }

    private $quantidadeMaximaDeEnviosPorServidor = 20000;
    private $quantidadeMaximaDeEnviosParaDescansoDoServidor = 5000;
    private $quantidadeMaximaParaTrocaDeServidor = 400;
    private $tempoDeDescansoEmMinutos = 1;
    private $offsetServidorEmAndamento = 0;
    private $quantidadesEnviosRealizadosPeloServidor = 0;
    private $servidoresEmDescanso = array();

    private function getDadosDoServidor()
    {
        $this->quantidadesEnviosRealizadosPeloServidor += 1;

        if ($this->quantidadeMaximaParaTrocaDeServidor <= $this->quantidadesEnviosRealizadosPeloServidor) {
            $this->offsetServidorEmAndamento += 1;
            $this->quantidadesEnviosRealizadosPeloServidor = 0;
        }

        if ($this->quantidadeMaximaDeEnviosParaDescansoDoServidor <= $this->quantidadesEnviosRealizadosPeloServidor) {
            $this->servidoresEmDescanso[$this->offsetServidorEmAndamento] = date('Y-m-d H:i:s', (time() + $this->tempoDeDescansoEmMinutos));
        }

        if (isset($this->servidoresEmDescanso[$this->offsetServidorEmAndamento])) {
            if ($this->servidoresEmDescanso[$this->offsetServidorEmAndamento] > date('Y-m-d H:i:s')) {
                $this->offsetServidorEmAndamento += 1;
            }
        }

        if ($this->quantidadeDeServidores < $this->offsetServidorEmAndamento) {
            $this->offsetServidorEmAndamento = 0;
        }

        $servidorAtual = $this->servidores[$this->offsetServidorEmAndamento];

        $dadosDoServidor['seguranca'] = $servidorAtual->getSeguranca();
        $dadosDoServidor['host'] = $servidorAtual->getHost();
        $dadosDoServidor['porta'] = $servidorAtual->getPorta();
        $dadosDoServidor['username'] = $servidorAtual->getUsername();
        $dadosDoServidor['password'] = $servidorAtual->getSenha();
        $dadosDoServidor['from'] = $servidorAtual->getFrom();
        $dadosDoServidor['nome_from'] = $servidorAtual->getNomeFrom();
        $dadosDoServidor['reply'] = $servidorAtual->getReply();
        return $dadosDoServidor;
    }

}