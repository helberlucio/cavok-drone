<?php
/**
 * Created by PhpStorm.
 * User: Gabriel PHP
 * Date: 15/05/2017
 * Time: 10:05
 */

namespace Modulos\Envio\Email\Models\RegrasDeNegocio;


use Aplicacao\Ferramentas\Identificadores;
use ModuloHelp\PadraoCriacao\ModelRegraDeNegocio\Abstracts\ModelRegraDeNegocioPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\EmailAnexosCampanha;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\EmailCadastroCampanhas;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\EmailsEnviados;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\RelationshipEmailAnexosCampanhaEmailTitulosAnexo;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\RelationshipEmailCadastroCampanhasEmailAnexosCampanha;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\RelationshipEmailCadastroCampanhasEmailTitulosCampanhas;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\RelationshipEmailCampanhasEmailAnexos;
use Modulos\Envio\Email\Models\Entidades\AnexoEntidade;
use Modulos\Envio\Email\Models\Entidades\CampanhaEntidade;

class CampanhasParaEnviar extends ModelRegraDeNegocioPadraoAbsHelp
{

    /**
     * @return CampanhaEntidade[]
     */
    public function get()
    {
        $campanhas = $this->getTodasCampanhasParaEnviar();
        $campanhasParaEnvio = array();
        foreach ($campanhas as $campanha) {
            $campanhasParaEnvio[] = $this->setCampanhaParaEnviar($campanha);
        }
        return $campanhasParaEnvio;
    }

    private function getTodasCampanhasParaEnviar()
    {
        $camapanhas = $this->db
            ->select('ecc.id, ece.lista_emails, ece.id as disparo_id')
            ->join('email_cadastro_campanhas ecc', 'ecc.id = ece.campanha')
            ->where('(ece.finalizado != 1 or ece.finalizado is null)', '', true)
            ->where("concat(ece.data_envio, ' ',ece.horario_envio) <= ", date('Y-m-d H:i:s'))
            ->get('email_cadastrar_envio ece')->result(false);
        return $camapanhas;
    }

    /**
     * @param $campanhaObj
     * @return CampanhaEntidade
     */
    private function setCampanhaParaEnviar($campanhaObj)
    {
        $this->validarObjetoCampanha($campanhaObj);
        $campanhaEntidade = new CampanhaEntidade();
        $campanhaEntidade->setInformacoesCamapanha((new EmailCadastroCampanhas())->getRow($this->db->where('id', $campanhaObj->id)));
        $campanhaEntidade->setEmailsParaEnviarDaCampanha($this->getEmailsParaEnviarDaCampanha($campanhaObj->lista_emails, $campanhaObj->id));
        $campanhaEntidade->setTitulosDaCampanha($this->getTitulosDaCampanha($campanhaObj->id));
        $campanhaEntidade->setAnexosDaCampanha($this->getAnexosDaCampanha($campanhaObj->id));
        $campanhaEntidade->setDisparoId($campanhaObj->disparo_id);
        return $campanhaEntidade;
    }

    private function getEmailsParaEnviarDaCampanha($listaDeEmailsArquivo, $campanhaId)
    {
        if (!is_file($listaDeEmailsArquivo)) {
            return array();
        }
        $arquivoCSV = array_map('str_getcsv', file($listaDeEmailsArquivo));
        return $this->validarEmails($arquivoCSV, $campanhaId);
    }

    /**
     * @param array $listaDeEmails
     * @param $campanhaId
     * @return array
     */
    private function validarEmails($listaDeEmails = array(), $campanhaId)
    {
        $emailsValidos = array();
        foreach ($listaDeEmails as $email) {
            if (!isset($email) || !Identificadores::eEmail($email[0])) {
                continue;
            }
            if ($this->verificarEmailEnviado($email[0], $campanhaId)) {
                continue;
            }
            $emailsValidos[] = $email;
        }
        return $emailsValidos;
    }

    private function verificarEmailEnviado($email, $campanhaId)
    {
        $emailEnviado = (new EmailsEnviados())->getRow(
            $this->db->where('email', $email)
                ->where('campanha_id', $campanhaId));

        if ($emailEnviado->getId()) {
            return true;
        }
        return false;
    }

    private function getTitulosDaCampanha($campanhaId = 0)
    {
        $relacaoCadastroDeTitulosParaCampanha = (new RelationshipEmailCadastroCampanhasEmailTitulosCampanhas())
            ->getRows($this->db->where('email_cadastro_campanhas_id', $campanhaId));
        $titulosDaCampanha = array();
        foreach ($relacaoCadastroDeTitulosParaCampanha as $tituloDaCampanhaRelacao) {
            $tituloCampanha = $this->db->select('titulo')
                ->where('id', $tituloDaCampanhaRelacao->getEmailTitulosCampanhasId())
                ->get('email_titulos_campanhas')
                ->row();
            if (!isset($tituloCampanha->titulo)) {
                continue;
            }
            $titulosDaCampanha[] = $tituloCampanha->titulo;
        }
        return $titulosDaCampanha;
    }

    private function validarObjetoCampanha($campanhaObj)
    {
        if (!isset($campanhaObj->id)) {
            die('<code>Objeto da campanha sem id;</code>');
        } elseif (!isset($campanhaObj->lista_emails)) {
            die('<code>Objeto da campanha sem lista de emails;</code>');
        }
    }

    /**
     * @param int $campanhaId
     * @return EmailAnexosCampanha[]
     */
    private function getAnexosDaCampanha($campanhaId = 0)
    {
        $relacaoCampanhaAnexos =
            (new RelationshipEmailCadastroCampanhasEmailAnexosCampanha())->getRows($this->db
                ->where('email_cadastro_campanhas_id', $campanhaId));

        $anexos = array();
        foreach ($relacaoCampanhaAnexos as $campanhaAnexo) {
            $anexo = (new EmailAnexosCampanha())->getRow($this->db->where('id', $campanhaAnexo->getId()));
            $titulos = $this->getTitulosDoAnexo($anexo);
            $anexoEntidade = (new AnexoEntidade())->setInformacoesAnexo($anexo)->setTitulosAnexo($titulos);
            $anexos[] = $anexoEntidade;
        }
        return $anexos;
    }

    /**
     * @param EmailAnexosCampanha $anexo
     * @return array
     */
    private function getTitulosDoAnexo(EmailAnexosCampanha $anexo)
    {
        $relacaoTitulosAnexo = (new RelationshipEmailAnexosCampanhaEmailTitulosAnexo())->getRows($this->db
            ->where('email_anexos_campanha_id', $anexo->getId()));
        $titulos = array();
        foreach ($relacaoTitulosAnexo as $relacaoTituloAnexo) {
            $tituloDoAnexo = $this->db->where('id', $relacaoTituloAnexo->getEmailTitulosAnexoId())
                ->get('email_titulos_anexo')->row();
            $titulos[] = $tituloDoAnexo;
        }
        return $titulos;
    }
}