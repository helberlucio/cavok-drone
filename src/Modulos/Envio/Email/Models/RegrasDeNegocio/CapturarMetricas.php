<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Ariza
 * Date: 12/04/2017
 * Time: 09:05
 */

namespace Modulos\Envio\Email\Models\RegrasDeNegocio;


use Aplicacao\Url;
use ModuloHelp\PadraoCriacao\ModelRegraDeNegocio\Abstracts\ModelRegraDeNegocioPadraoAbsHelp;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\EmailsEnviados;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\EmailVisualizacoesUsuario;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\RelationshipEmailsEnviadosEmailVisualizacoesUsuario;

class CapturarMetricas extends ModelRegraDeNegocioPadraoAbsHelp
{

    private $email = false;

    public function init($email)
    {
        $this->email = $email;
        $sessaoExiste = $this->verificarSessaoExiste();
        if (!$sessaoExiste || (!$this->email == false && $this->email != $_SESSION['metricas_email_marketing'])) {
            $this->iniciarSessao();
            $sessaoExiste = $this->verificarSessaoExiste();
        }

        if (!$sessaoExiste) {
            return;
        }
        $this->email = $_SESSION['metricas_email_marketing'];
        $this->contabilizarVisitas();

    }

    private function verificarSessaoExiste()
    {
        if (!isset($_SESSION['metricas_email_marketing']) || !$_SESSION['metricas_email_marketing']) {
            return false;
        }
        return true;
    }

    private function iniciarSessao()
    {
        if (!isset($_SESSION['metricas_email_marketing']) || (!$_SESSION['metricas_email_marketing']) || $this->email) {
            $_SESSION['metricas_email_marketing'] = $this->email;
        }
    }

    private function contabilizarVisitas()
    {
        $inserirVisualizacao = $this->inserirVisualizacaoInicial();
        if (!$inserirVisualizacao) {
            $emailEnviado = (new EmailsEnviados())->getRow($this->db->where('email', $this->email));
            if($emailEnviado->getId()){
                $browser = $_SERVER['HTTP_USER_AGENT'] . "\n\n";
                $objetoPaginaVisitada = new \stdClass();
                $objetoPaginaVisitada->pagina_visitada = Url::Current();
                $objetoPaginaVisitada->data_visualizacao = date('Y-m-d H:i:s');
                $objetoPaginaVisitada->browser = $browser;//(isset($browser['browser']) ? $browser['browser'] : '') . (isset($browser['parent']) ? $browser['parent'] : '') . (isset($browser['platform']) ? $browser['platform'] : '');

                $visualizacaoEmailEnviado = (new EmailVisualizacoesUsuario());
                $visualizacaoEmailEnviado->setPaginaVisitada($objetoPaginaVisitada->pagina_visitada);
                $visualizacaoEmailEnviado->setDataVisualizacao($objetoPaginaVisitada->data_visualizacao);
                $visualizacaoEmailEnviado->setBrowser($objetoPaginaVisitada->browser);
                $paginaVisitadaId = $visualizacaoEmailEnviado->insert();
                if($paginaVisitadaId){
                    $associacaoEmailVisualizadoComPaginaVisualizada = (new RelationshipEmailsEnviadosEmailVisualizacoesUsuario());
                    $associacaoEmailVisualizadoComPaginaVisualizada->setEmailsEnviadosId($emailEnviado->getId());
                    $associacaoEmailVisualizadoComPaginaVisualizada->setEmailVisualizacoesUsuarioId($paginaVisitadaId);
                    return $associacaoEmailVisualizadoComPaginaVisualizada->insert();
                }
            }
            return false;
        }
        return false;
    }

    function inserirVisualizacaoInicial()
    {
        $emailEnviado = (new EmailsEnviados())->getRow($this->db->where('email', $this->email));
        if (!$emailEnviado->getVisualizado()) {
            $emailEnviado->setVisualizado(1);
            $emailEnviado->update();
            return $emailEnviado->getId();
        }
        return false;
    }
}