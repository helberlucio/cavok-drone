<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 22/03/2017
 * Time: 10:11
 */

namespace Modulos\Envio\Email\Models\RegrasDeNegocio;

use Aplicacao\Url;

require 'vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

class EnvioMailer
{

    public function enviarEmailSMTP($dadosServidor = array(),
                                    $emailQuemRecebe = 'myblenet@gmail.com',
                                    $nomeQuemRecebeEamil = 'Gabriel Ariza',
                                    $assuntoDoEmail = 'Comercial Ligados Comunicação',
                                    $corpoDoEmail = 'testando',
                                    $anexos = array())
    {
        $mail = new \PHPMailer;
        $mail->isSMTP();        // Ativar SMTP
        $mail->SMTPDebug = false;       // Debugar: 1 = erros e mensagens, 2 = mensagens apenas
        $mail->SMTPAuth = true;     // Autenticação ativada
        $mail->SMTPSecure = $dadosServidor['seguranca'];  // SSL REQUERIDO pelo GMail
        $mail->Host = $dadosServidor['host'];//'a2plcpnl0475.prod.iad2.secureserver.net';//'srv152.prodns.com.br'; // SMTP utilizado
        $mail->Port = $dadosServidor['porta'];//465;
        $mail->Username = $dadosServidor['username'];//'comercial@autodesenvolve.com.br';
        $mail->Password = $dadosServidor['password'];//'11141512_a*';
        $mail->CharSet = 'UTF-8';
        //$mail->addCustomHeader($dadosServidor['username']);

        $mail->setFrom($dadosServidor['from'], $dadosServidor['nome_from']);
        $mail->addAddress($emailQuemRecebe, $nomeQuemRecebeEamil);
        $mail->Subject = ($assuntoDoEmail);
        $mail->addReplyTo($dadosServidor['reply'], 'Retorno de email Replay');
        $mail->isHTML(true);
        $mail->msgHTML($corpoDoEmail . '<img alt="Autenticacao do email leed" src="' . Url::Base('Modulo/Envio/Email/Index/Capturar_metricas/' . $emailQuemRecebe . '/imagem/autodesenvolve.png') . '" width="1" height="1" style="opacity:0;display:none;">');


        foreach ($anexos as $addAttachment) {
            if (!isset($addAttachment['file'])) {
                continue;
            }
            if (!isset($addAttachment['name'])) {
                $mail->addAttachment($addAttachment['file']);
                continue;
            }
            $anexoFileExtension = pathinfo($addAttachment['file'], PATHINFO_EXTENSION);
            $mail->addAttachment($addAttachment['file'], $addAttachment['name'] . '.' . $anexoFileExtension);
        }
        $envio = $mail->send();
        if (!$envio || $envio == null) {
            return array('sucesso' => false, 'mensagem' => 'Mailer Error: ' . $mail->ErrorInfo);
        } else {
            return array('sucesso' => true, 'mensagem' => 'E-mail enviado com sucesso para: ' . $emailQuemRecebe);
        }
    }
}