<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 12/04/2017
 * Time: 09:08
 */

namespace Modulos\Envio\Email\Models\Repositorios;


use Modulos\Envio\Email\Models\RegrasDeNegocio\CapturarMetricas;

class CapturaMetricasEmailMarketing
{

    public static function init($email = null)
    {
        (new CapturarMetricas())->init($email);
    }

}