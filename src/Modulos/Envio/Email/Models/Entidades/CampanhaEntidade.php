<?php

/**
 * Created by PhpStorm.
 * User: Gabriel PHP
 * Date: 15/05/2017
 * Time: 11:49
 */

namespace Modulos\Envio\Email\Models\Entidades;

use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\EmailCadastroCampanhas;

class CampanhaEntidade
{
    /**
     * @var EmailCadastroCampanhas
     */
    private $informacoesDaCampanha;
    /**
     * @var array
     */
    private $emailsParaEnviarDaCampanha;

    /**
     * @var array
     */
    private $titulosDaCampanha;

    /**
     * @var array
     */
    private $anexosDaCampanha;

    /**
     * @var int
     */
    private $disparo_id;

    /**
     * @param EmailCadastroCampanhas $cadastroCampanhas
     * @return $this
     */
    public function setInformacoesCamapanha(EmailCadastroCampanhas $cadastroCampanhas)
    {
        $this->informacoesDaCampanha = $cadastroCampanhas;
        return $this;
    }

    /**
     * @return EmailCadastroCampanhas
     */
    public function getInformacoesCampanha()
    {
        return $this->informacoesDaCampanha;
    }

    /**
     * @param array $emailsParaCampanha
     * @return $this
     */
    public function setEmailsParaEnviarDaCampanha($emailsParaCampanha = array())
    {
        $this->emailsParaEnviarDaCampanha = $emailsParaCampanha;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmailsParaEnviarDaCampanha()
    {
        return $this->emailsParaEnviarDaCampanha;
    }

    /**
     * @param array $titulosDaCampanha
     * @return $this
     */
    public function setTitulosDaCampanha($titulosDaCampanha = array())
    {
        $this->titulosDaCampanha = $titulosDaCampanha;
        return $this;
    }

    /**
     * @param array $anexosDaCampanha
     * @return $this
     */
    public function setAnexosDaCampanha($anexosDaCampanha = array())
    {
        $this->anexosDaCampanha = $anexosDaCampanha;
        return $this;
    }

    /**
     * @return AnexoEntidade[]
     */
    public function getAnexosDaCampanha()
    {
        return $this->anexosDaCampanha;
    }

    /**
     * @return array
     */
    public function getTitulosDaCampanha()
    {
        return $this->titulosDaCampanha;
    }

    /**
     * @param int $disparo_id
     * @return $this
     */
    public function setDisparoId($disparo_id = 0)
    {
        $this->disparo_id = $disparo_id;
        return $this;
    }

    public function getDisparoId()
    {
        return $this->disparo_id;
    }

}