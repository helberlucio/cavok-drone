<?php
/**
 * Created by PhpStorm.
 * User: Gabriel PHP
 * Date: 17/05/2017
 * Time: 14:11
 */

namespace Modulos\Envio\Email\Models\Entidades;


use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\EmailAnexosCampanha;

class AnexoEntidade
{

    /**
     * @var $anexosCampanha
     */
    private $informacoes;

    /**
     * @var array
     */
    private $titulos;

    /**
     * @param EmailAnexosCampanha[]
     * @return $this
     */
    public function setInformacoesAnexo($anexosCampanha)
    {
        $this->informacoes = $anexosCampanha;
        return $this;
    }

    /**
     * @return EmailAnexosCampanha
     */
    public function getInformacoesAnexo()
    {
        return $this->informacoes;
    }

    /**
     * @param array $titulos
     * @return $this
     */
    public function setTitulosAnexo($titulos = array())
    {
        $this->titulos = $titulos;
        return $this;
    }

    /**
     * @return array
     */
    public function getTitulosAnexo()
    {
        return $this->titulos;
    }

}