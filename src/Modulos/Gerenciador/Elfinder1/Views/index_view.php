<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Gerenciador de arquivos</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2"/>
    <link rel="stylesheet" type="text/css"
          href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <?php Aplicacao\Ferramentas\GetAssets::CSS('elfinder.min', 'Gerenciador', 'Elfinder1'); ?>
    <?php Aplicacao\Ferramentas\GetAssets::CSS('theme', 'Gerenciador', 'Elfinder1'); ?>
    <?php Aplicacao\Ferramentas\GetAssets::JS('elfinder.full', 'Gerenciador', 'Elfinder1'); ?>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            $('#elfinder').elfinder({
                url: '<?php echo \Aplicacao\Url::ModuloAdmin('Gerenciador/Elfinder1/Connector/index');?>'  // connector URL (REQUIRED)
            });
        });
    </script>
</head>
<body>
<div id="elfinder"></div>
<script>
    $(function () {
        setTimeout(function () {
            $('.elfinder-toolbar').append('<div class="ui-widget-content ui-corner-all elfinder-buttonset">' +
                '<div class="ui-state-default elfinder-button" title="Full Screen" onclick="poenCapture();">' +
                '<i class="fa fa-camera" aria-hidden="true"></i>' +
                '</div>' +
                '</div>');
        }, 600);
    });

    function poenCapture() {
        window.open("<?php echo Aplicacao\Url::ModuloAdmin("Gerenciador/Webcam/Index/");?>", "tirar foto", "width=1300, height=600");
    }

    function reload(){
        location.reload();
    }

</script>
</body>
</html>
