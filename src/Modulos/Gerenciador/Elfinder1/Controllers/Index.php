<?php
namespace Modulos\Gerenciador\Elfinder1\Controllers;

use Aplicacao\Ferramentas\Response;
use Aplicacao\Ferramentas\ResponseCode;
use Core\Controlador\Controlador;

class Index extends Controlador
{

    public function index()
    {
        $this->load->AdminView("index", array());
    }
  
  public function decryptHashFile(){
    $fileHash = input('hash');
    \Aplicacao\Ferramentas\Response::json(array('hash'=>base64_decode(substr($fileHash, 3, 999))), ResponseCode::HTTP_OK);
  }
}

