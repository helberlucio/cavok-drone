<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 26/12/2016
 * Time: 09:25
 */

namespace Modulos\Gerenciador\Elfinder1\Controllers;

use Core\Controlador\Controlador;


class Connector extends Controlador
{
    public function index()
    {
        $connector = new \Modulos\Gerenciador\Elfinder1\Models\Connector();
        $connector->init();
    }
}
