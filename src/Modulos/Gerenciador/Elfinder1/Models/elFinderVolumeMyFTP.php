<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 26/12/2016
 * Time: 09:32
 */

namespace Modulos\Gerenciador\Elfinder1\Models;


class elFinderVolumeMyFTP extends \elFinderVolumeFTP
{
    protected function init()
    {
        $this->options = array_merge($this->options, $GLOBALS['netvolumeOpts']);
        return parent::init();
    }
}