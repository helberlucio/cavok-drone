<?php
/**
 * Created by PhpStorm.
 * User: OBJETO - Desenv
 * Date: 26/12/2016
 * Time: 09:54
 */

namespace Modulos\Gerenciador\Elfinder1\Models;

use Modulos\Gerenciador\Models\elFinderVolumeMyDropbox;


class Connector
{

    private $useFtpNetmout = true;

    private $urlElfinder = '';
    private $pathElfinder = '';

    public function __construct()
    {
        $this->pathElfinder = ELFINDER_ROOT_PATH . LI_DIR_GERENCIADOR . '/';
        $this->urlElfinder = ELFINDER_ROOT_URL . LI_DIR_GERENCIADOR;
        $diretorios_padroes = array(
            'blog/posts/',
            'loja/posts/',
            'fotos_web_cam/',
            'email_marketing/templates/',
            'email_marketing/lista_de_emails_em_csv/',
        );

        if (!is_dir(LI_DIR_GERENCIADOR . date('Y') . '/')) {
            foreach ($diretorios_padroes as $diretorio) {
                if (!is_dir(LI_DIR_GERENCIADOR . date('Y') . '/' . $diretorio)) {
                    mkdir(LI_DIR_GERENCIADOR . date('Y') . '/' . $diretorio, 777, true);
                }
            }
        }
    }

    public
    function init()
    {

        if ($this->useFtpNetmout) {

            new elFinderVolumeMyFTP();
            // overwrite net driver class name
            \elFinder::$netDrivers['ftp'] = 'MyFTP';
        }
        if (ELFINDER_DROPBOX_CONSUMERKEY && ELFINDER_DROPBOX_CONSUMERSECRET) {
            new elFinderVolumeMyDropbox();
            // overwrite net driver class name
            \elFinder::$netDrivers['dropbox'] = 'MyDropbox';
        }

        $connector = new \elFinderConnector(new \elFinder($this->getOpts()));
        $connector->run();
    }

    function access($attr, $path, $data, $volume)
    {
        return strpos(basename($path), '.') === 0       // if file/folder begins with '.' (dot)
            ? !($attr == 'read' || $attr == 'write')    // set read+write to false, other (locked+hidden) set to true
            : null;                                    // else elFinder decide it itself
    }

    private
    function getNetVolumeOpts()
    {
        $netvolumeOpts = array(
            'tmbURL' => ELFINDER_ROOT_URL . '/files/.tmb',
            'tmbPath' => ELFINDER_ROOT_PATH . '/files/.tmb',
            'syncMinMs' => 30000
        );
        return $netvolumeOpts;
    }

    private
    function getOpts()
    {
        $opts = array(
            'bind' => array(
                'mkdir.pre mkfile.pre rename.pre' => array(
                    'Plugin.Sanitizer.cmdPreprocess'
                ),
                'upload.presave' => array(
                    'Plugin.Sanitizer.onUpLoadPreSave'
                ),
                'upload rename' => array($this, 'sanitize')
            ),
            // global configure (optional)
            'plugin' => array(
                'Sanitizer' => array(
                    'enable' => true,
                    'targets' => array(' ', '\\', '/', ':', '*', '?', '"', '<', '>', '|'), // target chars
                    'replace' => '_'    // replace to this
                )
            ),
            'roots' => array(
                array(
                    'driver' => 'LocalFileSystem',           // driver for accessing file system (REQUIRED)
                    'path' => $this->pathElfinder, // path to files (REQUIRED)
                    'URL' => $this->urlElfinder, // URL to files (REQUIRED)
                    //'uploadDeny' => array('all'),                // All Mimetypes not allowed to upload
                    'uploadDeny' => array('php'),                // All Mimetypes not allowed to upload
                    //'uploadAllow' => array('image','zip', 'text/plain'),// Mimetype `image` and `text/plain` allowed to upload
                    'uploadOrder' => array('deny', 'allow'),      // allowed Mimetype `image` and `text/plain` only
                    'accessControl' => 'access'                     // disable and hide dot starting files (OPTIONAL)
                )
            )
        );
        return $opts;
    }
}