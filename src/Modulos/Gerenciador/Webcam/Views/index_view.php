<?php
\Aplicacao\Ferramentas\GetAssets::CSS('GerenciadorWebcam', 'Gerenciador', 'Webcam');
?>
<div class="container-fluid GerenciadorWebcam-fluid GerenciadorWebcam">
    <div class="row">
        <div class="col-md-12">
            <div style="padding: 0;" class="col-md-12 callback">

            </div>
            <div class="col-md-12" style="text-align: center;margin-top:10px;">
                <button onclick="snapshot();" class="btn btn-primary">
                    <i class="fa fa-camera" aria-hidden="true"></i>
                </button>
                <button id="save" class="btn btn-success" style="display: none;">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                </button>
                <button class="btn btn-default" onclick="restart();">
                    <i class="fa fa-recycle" aria-hidden="true"></i>
                </button>
            </div>
        </div>

        <div class="col-md-6" style="padding: 0;">

            <div class="col-md-12" style="padding: 0;text-align: center;background: #000;">
                <video onclick="snapshot(this);" width="100%" id="video" autoplay></video>
            </div>

        </div>
        <div class="col-md-6 viewPrintArea" style="background: #000;padding: 0 0 5px 0;opacity: 0;">
            <img src="" id="viewPrint" style="width: 100%;">
        </div>
        <div class="col-md-6" style="padding: 0;display: none;">
            <canvas id="myCanvas" width="320" height="240"></canvas>
        </div>


    </div>
</div>
<script>
    //--------------------
    // GET USER MEDIA CODE
    //--------------------
    navigator.getUserMedia = ( navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia);

    var video;
    var webcamStream;
    function restart(){
        $('.viewPrintArea').css({'opacity':'0'});
        $('#save').fadeOut();
        startWebcam();
    }
    function startWebcam() {
        if (navigator.getUserMedia) {
            navigator.getUserMedia(
                // constraints
                {
                    video: true,
                    audio: false
                },
                // successCallback
                function (localMediaStream) {
                    video = document.querySelector('video');
                    video.src = window.URL.createObjectURL(localMediaStream);
                    webcamStream = localMediaStream;
                },

                // errorCallback
                function (err) {
                    console.log("The following error occured: " + err);
                }
            );
        } else {
            console.log("getUserMedia not supported");
        }
    }

    //---------------------
    // TAKE A SNAPSHOT CODE
    //---------------------
    var canvas, ctx;

    function init() {
        canvas = document.getElementById("myCanvas");
        ctx = canvas.getContext('2d');
    }

    function snapshot() {
        $('#save').fadeIn();
        // Draws current image from the video element into the canvas
        ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
        var url = canvas.toDataURL();
        $('.viewPrintArea').css({'opacity':'1'});
        $('#viewPrint').attr('src', url);
    }

    $("#save").click(function () {
        $('.callback').empty().append('<div class="alert alert-success">Salvando arquivo de imagem...</div>');
        $.post(base_url('Modulo/Gerenciador/Webcam/Index/salvarImagem'), {imagem: canvas.toDataURL()}, function (data) {
            $('.callback').empty().append('<div class="alert alert-success">'+data.mensagem+'</div>');
            setTimeout(function(){
                $('.callback').empty();
                self.opener.reload();
                if(!confirm('Deseja continuar tirando fotos ?')){
                    window.close();
                }
            }, 1000);
        }, 'json');
    });

    $(function () {
        init();
        startWebcam();
    });
</script>
