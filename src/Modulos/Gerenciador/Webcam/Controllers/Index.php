<?php
namespace Modulos\Gerenciador\Webcam\Controllers;

use Aplicacao\Ferramentas\Ajax;
use Core\Controlador\Controlador;
use Modulos\Gerenciador\Webcam\Models\Repositorios\GerenciadorWebcamRP;

class Index extends Controlador
{

    public function index()
    {
        echo GerenciadorWebcamRP::getIndexPage();
    }

    public function salvarImagem()
    {
        $imagemBase64 = input('imagem');
        list($type, $imagemBase64) = explode(';', $imagemBase64);
        list(, $data)      = explode(',', $imagemBase64);
        $data = base64_decode($data);
        $diretorio = LI_DIR_GERENCIADOR.date('Y').'/fotos_web_cam/';
        if(!is_dir($diretorio)){
            mkdir($diretorio, 777,true);
        }
        $fileName = date('d-m-Y-H-i-s').'-'.rand(0,20).'.png';
        file_put_contents($diretorio.$fileName, $data);
        Ajax::retorno(array('level'=>0, 'mensagem'=>'Salvo com sucesso'));
    }
}