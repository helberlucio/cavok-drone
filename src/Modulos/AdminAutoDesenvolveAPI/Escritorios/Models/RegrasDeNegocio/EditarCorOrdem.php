<?php

namespace Modulos\AdminAutoDesenvolveAPI\Escritorios\Models\RegrasDeNegocio;

use Aplicacao\Ferramentas\Ajax;
use Core\Modelos\ModelagemDb;
use ModuloHelp\PadraoCriacao\ModelRegraDeNegocio\Abstracts\ModelRegraDeNegocioPadraoAbsHelp;
use Modulos\Authentications\TokenAdmin\Models\RegrasDeNegocio\AuthenticationsTokenAdminRN;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsUsuariosEntidade;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysEscritoriosEntidade;

class EditarCorOrdem
{


    private $id;
    private $cor;
    private $order;
    /**
     * @var CmsUsuariosEntidade
     */
    private $usuario;
    /**
     * @var array
     */
    private $error = array();
    /**
     * @var ModelagemDb
     */
    private $db;

    public function __construct()
    {
        $verificacaoDeToken = (new AuthenticationsTokenAdminRN())->VerificarToken(Ajax::getHeader('token'));
        if (!$verificacaoDeToken->getId()) {
            $this->error[] = 'Verificacao de token falha';
        }
        $this->db = new ModelagemDb();
        if (input('usuario_id')) {
            $usuario = (new CmsUsuariosEntidade())->getRow($this->db->where('id', input('usuario_id')));
            if ($usuario->getId()) {
                $verificacaoDeToken = $usuario;
            }
        }
        $this->usuario = $verificacaoDeToken;

        $this->id = input('id');
        $this->cor = input('cor');
        $this->order = input('order');

    }

    public function editar()
    {
        $escritorio = (new SysEscritoriosEntidade())->getRow($this->db->where('id', $this->id));
        if (!$escritorio->getId()) {
            $this->error[] = 'Escritório não encontrado para edição :(! Tente novamente :).';
            return $this->error;
        }
        $escritorio->setCor($this->cor)
            ->setOrder($this->order);
        $updateEscritorio = $escritorio->update();

        if (!$updateEscritorio) {
            $this->error[] = 'Falha ao tentar editar configuração do escritório :(! Por favor tente novamente :).';
            return $this->error;
        }
        return array();
    }
}

