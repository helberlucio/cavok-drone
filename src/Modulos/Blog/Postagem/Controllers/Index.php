<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 23/04/2018
 * Time: 12:23
 */

namespace Modulos\Blog\Postagem\Controllers;


use Modulos\Blog\Postagem\Models\Repositorios\BlogPostagemRP;

class Index
{
    public function index($view = 'index', $data = array(), $admin = false)
    {
        echo BlogPostagemRP::getIndexPage($view, $data, $admin);
    }

    public function pesquisar()
    {

    }
}