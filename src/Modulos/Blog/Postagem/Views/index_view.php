<?php
/**
 * @var $postagem        \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\BlogPostagensEntidade
 * @var $usuario_inseriu \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\CmsUsuarios
 */

use Aplicacao\Url;
use Modulos\Blog\SearchBar\Models\Repositorios\BlogSearchBarRP;

\Aplicacao\Ferramentas\GetAssets::CSS('BlogPostagem', 'Blog', 'Postagem');
\Aplicacao\Ferramentas\GetAssets::JS('BlogPostagem', 'Blog', 'Postagem');
?>
<div>
    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-3">
                <a href="" onclick="voltarPagina()" class="btn btn-warning text-primary btn-sm">
                    <strong class="small text-nowrap">
                        <i class="fa fa-arrow-left"></i> Voltar página
                    </strong>
                </a>
            </div>
            <div class="col-9">
                <?= BlogSearchBarRP::getIndexPage(); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="BlogPostagem-imagem"
                     style="background-image: url(<?php echo Url::Base($postagem->getBanner()); ?>);">
                </div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-md-3 text-center text-md-left text-primary">
                <?php
                if ($postagem->getCategoria()) {
                    $categoria = (new \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\BlogCategoriasEntidade())
                        ->getRow($this->db->where('id', $postagem->getCategoria()));
                    echo $categoria->getTitulo() ? $categoria->getTitulo() : 'Sem Categoria';
                }
                ?>
            </div>
            <div class="col-md-3 text-center text-md-right text-muted">
                <?php echo $usuario_inseriu->getNome(); ?>
            </div>
            <div class="col-md-3 text-center text-md-left text-muted">
                <?php echo date('d/m/Y', strtotime($postagem->getInsertData())); ?>
            </div>
            <div class="col-md-3 text-center text-md-right">
                <a class="text-social-facebook"
                   href="https://www.facebook.com/sharer/sharer.php?u=<?php echo Url::Current(); ?>">
                    <i class="fa fa-2x fa-facebook-square"></i>
                </a>
                <a class="text-social-twitter" href="https://twitter.com/home?status=<?php echo Url::Current(); ?>">
                    <i class="fa fa-2x fa-twitter-square"></i>
                </a>
                <a class="text-social-gplus" href="https://plus.google.com/share?url=<?php echo Url::Current(); ?>">
                    <i class="fa fa-2x fa-google-plus-square"></i>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h5 class="text-primary text-center pt-3 mt-4 mb-4">
                    <?= $postagem->getTitulo(); ?>
                </h5>
            </div>
            <div class="col-12">
                <?php echo $postagem->getDescricao(); ?>
            </div>
            <div class="col-12 mt-4 mb-4">
                <?php
                $tags = strpos($postagem->getPalavrasChaves(), ',') ? explode(', ', $postagem->getPalavrasChaves()) : [];
                foreach ($tags as $tag) {
                    echo '<a class="small text-warning"  title="' . $tag . '" href="' . Url::Base() .
                        Url::Apelido(
                            \Modulos\Site\Blog\Models\Repositorios\SiteBlogRP::getUrlModulo('Index/pesquisar') . '?pesquisa=' .
                            urlencode($tag), 'blog-pesquisa/' . Url::Title($tag)) . '">';
                    echo $tag;
                    echo '</a> ';
                }
                ?>
            </div>
        </div>
        <div id="disqus_thread" class="mt-4 mb-4" style="width: 100%">
            <script>
                $(function () {
                    var d = document, s = d.createElement('script');
                    s.src = 'https://ligadoscomunicacao-com-br.disqus.com/embed.js';
                    s.setAttribute('data-timestamp', +new Date());
                    (d.head || d.body).appendChild(s);
                });
            </script>
            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments
                    powered by Disqus.</a></noscript>
        </div>
    </div>
</div>