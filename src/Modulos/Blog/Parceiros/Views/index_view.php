<?php
/**
 * @var $parceiros \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\MusiorgBlogParceiros[]
 */
\Aplicacao\Ferramentas\GetAssets::CSS('BlogParceiros', 'Blog', 'Parceiros');
if(count($parceiros)) {
    ?>
    <div class="col-md-12 BlogParceiros">
        <div class="BlogParceiros-header col-md-12">
            <h1>PARCEIROS</h1>
        </div>
        <div class="col-md-12 BlogParceiros-body">
            <ul>
                <?php
                foreach ($parceiros as $parceiro) {
                    ?>
                    <li>
                        <a href="<?php echo $parceiro->getLink(); ?>" target="_blank">
                            <img width="100%" title="<?php echo $parceiro->getTitulo(); ?>"
                                 alt="<?php echo $parceiro->getTitulo(); ?>"
                                 src="<?php echo \Aplicacao\Url::Base($parceiro->getImagem()); ?>">
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
    </div>
    <?php
}