<?php

use Aplicacao\Ferramentas\GetAssets;
use Aplicacao\Url;

GetAssets::CSS('BlogSearchBar', 'Blog', 'SearchBar');
?>
<div class="BlogSearchBar">
    <form method="get" action="<?php echo Url::Base('Modulo/Site/Blog/Index/refazerPesquisa');?>">
        <input type="text" class="form-control bg-primary text-white" placeholder="Sua pesquisa..." name="pesquisa">
        <button class="bg-warning" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
    </form>
</div>
