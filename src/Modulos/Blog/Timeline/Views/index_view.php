<?php
/**
 * @var $postagensDestaque    \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\BlogPostagensEntidade[]
 * @var $postagens            \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\BlogPostagensEntidade[]
 * @var $quantidade_postagens int
 */

use Aplicacao\Ferramentas\GetAssets;
use Aplicacao\Url;
use Modulos\Blog\Postagem\Models\Repositorios\BlogPostagemRP;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\BlogCategoriasEntidade;

GetAssets::CSS('BlogTimeline', 'Blog', 'Timeline');

if (input_get('pesquisa')) {
    ?>
    <div class="BlogTimeline mb-2">
        <div class="BlogTimeline-box-postagens">
            <div class="BlogTimeline-header">
                <h5 class="text-info text-center pt-3">PESQUISA NO BLOG</h5>
            </div>
            <div class="BlogTimeline-body">
                    <p class="text-center">Sua pesquisa: <b><?= input_get('pesquisa'); ?></b>
                        Teve <?= $quantidade_postagens; ?> resultados</p>
            </div>
        </div>
    </div>
    <?php
}
if (!empty($postagensDestaque)) {
    ?>
    <div class="BlogTimeline">
        <div class="BlogTimeline-box-postagens border rounded border-gray shadow mb-4">
            <div class="BlogTimeline-header border-top border-primary rounded">
                <h5 class="text-info text-center pt-3">DESTAQUES DA SEMANA</h5>
            </div>
            <div class="BlogTimeline-body pl-md-5 pr-md-5 pl-2 pr-2 pt-4 pb-4">
                <?php
                foreach ($postagensDestaque as $key => $postagen) {
                    $closeRow = true;
                    if ($key % 2 == 0) {
                        echo '<div class="row">';
                        if (count($postagensDestaque) == ($key + 1)) {
                            $closeRow = true;
                        } else {
                            $closeRow = false;
                        }
                    }
                    ?>
                    <!-- ################### POSTAGEM ########################### -->
                    <div class="col-md-6 BlogTimeline-postagemvertical-area">
                        <a class="text-decoration-none"
                           href="<?= Url::Base() . Url::Apelido(BlogPostagemRP::getUrlModulo('Index') . '/' . $postagen->getId(), $postagen->getTitulo() . '-' . $postagen->getId(), ['blog', 'postagem']); ?>">
                            <div class="border border-gray rounded shadow BlogTimeline-postagemvertical-box">
                                <div class="BlogTimeline-postagemvertical-imagem border-bottom border-gray"
                                     style="background-image: url(<?= Url::Base($postagen->getThumb()); ?>);">
                                </div>
                                <div class="BlogTimeline-postagemvertical-descricoes">
                                    <div class="row justify-content-center">
                                        <div class="col-md-6 small text-warning text-left pt-2 pb-2">
                                            <?php
                                            if ($postagen->getCategoria()) {
                                                $categoria = (new BlogCategoriasEntidade())
                                                    ->getRow($this->db->where('id', $postagen->getCategoria()));
                                                echo $categoria->getTitulo() ? $categoria->getTitulo() : 'Sem Categoria';
                                            }
                                            ?>
                                        </div>
                                        <div class="col-md-3 small text-muted pt-2 pb-2">
                                            <?= date('d/m/Y', strtotime($postagen->getInsertData())); ?>
                                        </div>
                                    </div>
                                    <h5 class="text-primary text-center pt-3">
                                        <?= $postagen->getTitulo(); ?>
                                    </h5>
                                    <p class="text-dark small p-4 text-center">
                                        <?= substr($postagen->getChamada(), 0, 200) . '...'; ?>
                                    </p>
                                    <div class="row justify-content-center">
                                        <div class="col-11 mb-3">
                                            <button class="btn btn-primary btn-block text-white">Continue lendo <i
                                                        class="fa fa-eye"
                                                        aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- ################### POSTAGEM ########################### -->
                    <?php
                    if ($closeRow) {
                        echo '</div>';
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <?php
}
?>
<div class="BlogTimeline" style="<?= ($postagensDestaque ? 'margin-top:40px;' : '');
echo(empty($postagens) ? 'display:none;' : ''); ?>">
    <div class="BlogTimeline-box-postagens border rounded border-gray shadow mb-4">
        <div class="BlogTimeline-header border-top border-primary rounded">
            <h5 class="text-info text-center pt-3">MAIS POSTAGENS</h5>
        </div>
        <div class="BlogTimeline-body pl-md-5 pr-md-5 pl-2 pr-2 pt-4 pb-4">
            <?php
            foreach ($postagens as $keys => $postagen) {
                $closeRow = true;
                if ($keys % 2 == 0) {
                    echo '<div class="row">';
                    if (count($postagens) == ($keys + 1)) {
                        $closeRow = true;
                    } else {
                        $closeRow = false;
                    }
                }
                ?>
                <!-- ################### POSTAGEM ########################### -->
                <div class="col-md-6 BlogTimeline-postagemvertical-area">
                    <a class="text-decoration-none"
                       href="<?= Url::Base() . Url::Apelido(BlogPostagemRP::getUrlModulo('Index') . '/' . $postagen->getId(), $postagen->getTitulo() . '-' . $postagen->getId(), ['blog', 'postagem']); ?>">
                        <div class="border border-gray rounded shadow BlogTimeline-postagemvertical-box">
                            <div class="BlogTimeline-postagemvertical-imagem border-bottom border-gray"
                                 style="background-image: url(<?= Url::Base($postagen->getThumb()); ?>);">
                            </div>
                            <div class="BlogTimeline-postagemvertical-descricoes">
                                <div class="row justify-content-center">
                                    <div class="col-md-6 small text-warning text-left pt-2 pb-2">
                                        <?php
                                        if ($postagen->getCategoria()) {
                                            $categoria = (new BlogCategoriasEntidade())
                                                ->getRow($this->db->where('id', $postagen->getCategoria()));
                                            echo $categoria->getTitulo() ? $categoria->getTitulo() : 'Sem Categoria';
                                        }
                                        ?>
                                    </div>
                                    <div class="col-md-3 small text-muted pt-2 pb-2">
                                        <?= date('d/m/Y', strtotime($postagen->getInsertData())); ?>
                                    </div>
                                </div>
                                <h5 class="text-primary text-center pt-3">
                                    <?= $postagen->getTitulo(); ?>
                                </h5>
                                <p class="text-dark small p-4 text-center">
                                    <?= substr($postagen->getChamada(), 0, 200) . '...'; ?>
                                </p>
                                <div class="row justify-content-center">
                                    <div class="col-11 mb-3">
                                        <button class="btn btn-primary btn-block text-white">Continue lendo <i
                                                    class="fa fa-eye"
                                                    aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- ################### POSTAGEM ########################### -->
                <?php
                if ($closeRow) {
                    echo '</div>';
                }
            }
            ?>
        </div>
    </div>
</div>
