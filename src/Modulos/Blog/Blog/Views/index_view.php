<?php

use Modulos\Blog\Categorias\Models\Repositorios\BlogCategoriasRP;
use Modulos\Blog\Parceiros\Models\Repositorios\BlogParceirosRP;
use Modulos\Blog\Propagandas\Models\Repositorios\BlogPropagandasRP;
use Modulos\Blog\SearchBar\Models\Repositorios\BlogSearchBarRP;
use Modulos\Blog\Timeline\Models\Repositorios\BlogTimelineRP;

?>
<div class="container-fluid SiteBlog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?= BlogSearchBarRP::getIndexPage(); ?>
            </div>
        </div>
        <div class="row mt-3 mb-3">
            <div class="col-md-8">
                <?= BlogTimelineRP::getIndexPage(); ?>
            </div>
            <div class="col-md-4">
                <div>
                    <?= BlogCategoriasRP::getIndexPage(); ?>
                </div>
                <div>
                    <?= BlogParceirosRP::getIndexPage(); ?>
                </div>
                <div>
                    <?= BlogPropagandasRP::getIndexPage(); ?>
                </div>
            </div>
        </div>
    </div>
</div>