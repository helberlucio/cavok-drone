<?php
/**
 * @var $categorias \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\BlogCategoriasEntidade[]
 */

use Aplicacao\Url;
use Modulos\Site\Blog\Models\Repositorios\SiteBlogRP;

\Aplicacao\Ferramentas\GetAssets::CSS('BlogCategorias', 'Blog', 'Categorias');
?>
<div class="BlogCategorias  border rounded border-gray shadow">
    <div class="BlogCategorias-header border-top border-primary rounded">
        <h5 class="text-info text-center pt-3">CATEGORIAS</h5>
    </div>
    <div class="BlogCategorias-body">
        <div class="list-group list-group-flush">
            <?php foreach ($categorias as $categoria): ?>
                <a class="list-group-item list-group-item-action"
                   href="<?php echo Url::Base() . Url::Apelido(SiteBlogRP::getUrlModulo('Index/pesquisar') . '?pesquisa_por_categoria=' . urlencode($categoria->getTitulo()), Url::Title($categoria->getTitulo()), ['blog', 'pesquisa', 'categoria']); ?>">
                    <?= $categoria->getTitulo() . '&nbsp; (' . Modulos\Blog\Categorias\Models\Repositorios\BlogCategoriasRP::getQuantidadePostagensPorCategoriaId($categoria->getId()) . ')'; ?>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
</div>
