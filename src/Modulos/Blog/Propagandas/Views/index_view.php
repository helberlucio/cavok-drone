<?php
/**
 * @var $patrocinios \Modulos\Criadores\ModulosAdministraveis\Models\Entidades\BlogLinksPatrocinados[]
 */
\Aplicacao\Ferramentas\GetAssets::CSS('BlogPropagandas', 'Blog', 'Propagandas');
if(count($patrocinios)) {
    ?>
    <div class="col-md-12 BlogPropagandas">
        <div class="BlogPropagandas-header col-md-12">
            <h1>PATROCINADOS</h1>
        </div>
        <div class="col-md-12 BlogPropagandas-body">
            <ul>
                <?php
                foreach ($patrocinios as $patrocinio) {
                    ?>
                    <li>
                        <a href="<?php echo $patrocinio->getLink(); ?>" target="_blank">
                            <img width="100%" title="<?php echo $patrocinio->getTitulo(); ?>"
                                 alt="<?php echo $patrocinio->getTitulo(); ?>"
                                 src="<?php echo \Aplicacao\Url::Base($patrocinio->getImagem()); ?>">
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
    </div>
    <?php
}