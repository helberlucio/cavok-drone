<?php

/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 06/03/2017
 * Time: 01:14
 */

namespace Api;

use Aplicacao\Ferramentas\Ajax;
use Aplicacao\Ferramentas\Log;
use Modulos\Criadores\ModulosAdministraveis\Models\Entidades\SysLogEntidade;

class ApiExeption extends \Exception
{

    private $action = 1;

    public function __construct($message = "", $action = 1, $code = 0, \Throwable $previous = null, SysLogEntidade $log = null)
    {
        $this->action = $action;
        parent::__construct($message, $code, $previous);
        if($log instanceof SysLogEntidade && $log->getLevel()){
            Log::Geral($log->getLevel(), $log->getMensagem());
        }
    }

    public function getAction()
    {
        return $this->action;
    }

    // personaliza a apresentação do objeto como string
    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }


}