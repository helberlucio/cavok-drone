<?php

/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 06/03/2017
 * Time: 01:00
 */

namespace Api\ApiHelp\Abstracao;

use Api\ApiExeption;
use Api\ApiHelp\Interfaces\ApiITRF;
use Api\ApiHelp\Models\Verificacoes\Campos;
use Api\ApiHelp\Models\Verificacoes\CamposObrigatorios;
use Api\ApiHelp\Models\Verificacoes\TipoRequet;

abstract class ApiAbs implements ApiITRF
{
    public abstract function __construct();
    public abstract function init();

    public abstract function setCamposHeader($campos_header = array());
    public abstract function setCamposObrigatorios($campos_obrigatorios =  array());
    public abstract function setCamposTipos($campos_tipos = array());
    public abstract function setEntidadesRelacionadas($entidades_relacionadas = array());
    public abstract function setTiposRequestsPermitidos($tipos_requests_permitidos = array());
    public abstract function setCamposObrigatoriosGET($campos_obrigatorios_get = array());
    public abstract function setCamposObrigatoriosPOST($campos_obrigatorios_post = array());
    public abstract function setCamposObrigatoriosPUT($campos_obrigatorios_put = array());
    public abstract function setCamposObrigatoriosDELETE($campos_obrigatorios_delete = array());

    public abstract function getCamposHeader();
    public abstract function getCamposObrigatorios();
    public abstract function getCamposTipos();
    public abstract function getEntidadesRelacionadas();
    public abstract function getTiposRequestsPermitidos();
    public abstract function getCamposObrigatoriosGET();
    public abstract function getCamposObrigatoriosPOST();
    public abstract function getCamposObrigatoriosPUT();
    public abstract function getCamposObrigatoriosDELETE();

    public abstract function verificarHeader();
    public abstract function verificarTiposRequest();
    public abstract function verificarCamposObrigatorios();
    public abstract function verificarCamposTipos();
    public abstract function verificarErroExistente();

    public abstract function methodGET();
    public abstract function methodPOST();
    public abstract function methodPUT();
    public abstract function methodDELETE();
    public abstract function methodPATH();

}