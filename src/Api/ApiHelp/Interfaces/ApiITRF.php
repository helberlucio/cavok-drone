<?php

/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 06/03/2017
 * Time: 01:01
 */
namespace Api\ApiHelp\Interfaces;
interface ApiITRF
{
    public function __construct();
    public function init();

    public function setCamposHeader($campos_header = array());
    public function setCamposObrigatorios($campos_obrigatorios =  array());
    public function setCamposTipos($campos_tipos = array());
    public function setCamposObrigatoriosGET($campos_obrigatorios_get = array());
    public function setCamposObrigatoriosPOST($campos_obrigatorios_post = array());
    public function setCamposObrigatoriosPUT($campos_obrigatorios_put = array());
    public function setCamposObrigatoriosDELETE($campos_obrigatorios_delete = array());
    public function setEntidadesRelacionadas($entidades_relacionadas = array());
    public function setTiposRequestsPermitidos($tipos_requests_permitidos = array());

    public function getTiposRequestsPermitidos();
    public function getCamposObrigatorios();
    public function getCamposTipos();
    public function getCamposHeader();
    public function getEntidadesRelacionadas();
    public function getCamposObrigatoriosGET();
    public function getCamposObrigatoriosPOST();
    public function getCamposObrigatoriosPUT();
    public function getCamposObrigatoriosDELETE();

    public function verificarHeader();
    public function verificarTiposRequest();
    public function VerificarCamposObrigatorios();
    public function verificarCamposTipos();

    public function methodGET();
    public function methodPOST();
    public function methodPUT();
    public function methodDELETE();
    public function methodPATH();
}