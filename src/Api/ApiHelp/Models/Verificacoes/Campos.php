<?php
/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 06/03/2017
 * Time: 02:00
 */

namespace Api\ApiHelp\Models\Verificacoes;


use Api\ApiExeption;
use Aplicacao\Ferramentas\Response;
use Aplicacao\Ferramentas\ResponseCode;

class Campos
{
    private $erros = array();

    public function verificarCamposTipos($campos = array())
    {
        foreach ($campos as $campo => $tipoValidacao) {
            if(!$tipoValidacao){
                continue;
            }
            $verificacao = 'is_' . $tipoValidacao;
            if (input($campo)) {
                if (!$verificacao(input($campo))) {
                    $this->erros[] = 'O campo '.$campo.' deve ser do tipo '.$tipoValidacao;
                }
            }
        }
        try{
            if(!empty($this->erros)){
                throw new ApiExeption(implode('; ', $this->erros));
            }
        } catch (ApiExeption $e) {
            $json = array(
                'msg' => $e->getMessage(),
                'acao' => $e->getAction(),
                'erro' => true
            );
            return Response::json($json, ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }

        return true;
    }
}