<?php

/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 06/03/2017
 * Time: 00:46
 */
namespace Api\ApiHelp\Models\Verificacoes;
use Api\ApiExeption;
use Aplicacao\Ferramentas\Ajax;
use Aplicacao\Ferramentas\Identificadores;
use Aplicacao\Ferramentas\Response;
use Aplicacao\Ferramentas\ResponseCode;

class TipoRequet
{

    /**
     * @param array $tiposRequestPermitidos
     * @return string
     */
    public function verificar($tiposRequestPermitidos = array())
    {
        $permissaoDoRequest = false;
        $request = Ajax::getRequestTipo();

        foreach($tiposRequestPermitidos as $requestPermitido){
            if($request == $requestPermitido){
                $permissaoDoRequest = true;
            }
        }

        try{
            if(!$permissaoDoRequest){
                throw new ApiExeption('Api nao suporta o tipo de request '.Ajax::getRequestTipo(), 1,ResponseCode::HTTP_BAD_REQUEST);
            }
        }catch (ApiExeption $e) {
            $json = array(
                'msg' => $e->getMessage(),
                'acao' => $e->getAction(),
                'erro' => true
            );
            return Response::json($json, $e->getCode());
        }
        return true;
    }

}