<?php

/**
 * Created by PhpStorm.
 * User: Gabriel
 * Date: 06/03/2017
 * Time: 00:57
 */

namespace Api\ApiHelp;

use Api\ApiExeption;
use Api\ApiHelp\Abstracao\ApiAbs;
use Api\ApiHelp\Models\Verificacoes\Campos;
use Api\ApiHelp\Models\Verificacoes\CamposObrigatorios;
use Api\ApiHelp\Models\Verificacoes\Header;
use Api\ApiHelp\Models\Verificacoes\TipoRequet;
use Aplicacao\Ferramentas\Ajax;
use Aplicacao\Ferramentas\Response;
use Aplicacao\Ferramentas\ResponseCode;
use Aplicacao\Url;

class ApiHP extends ApiAbs
{
    public $classe_api                  =   __CLASS__;
    public $error                       =   array();
    public $campos_header               =   array();
    public $campos_obrigatorios         =   array();
    public $campos_obrigatorios_get     =   array();
    public $campos_obrigatorios_post    =   array();
    public $campos_obrigatorios_put     =   array();
    public $campos_obrigatorios_delete  =   array();
    public $campos_tipos                =   array();
    public $entidades_relacionadas      =   array();
    public $requests_permitidos         =   array();

    public function __construct()
    {
        $this->setTiposRequestsPermitidos();
        $this->setCamposHeader();
        $this->setCamposObrigatorios();
        $this->setCamposTipos();
        $this->setCamposObrigatoriosGET();
        $this->setCamposObrigatoriosPOST();
        $this->setCamposObrigatoriosPUT();
        $this->setCamposObrigatoriosDELETE();
        $this->setEntidadesRelacionadas();

        $this->verificarTiposRequest();
        $this->verificarHeader();
        $this->verificarCamposObrigatorios();
        $this->verificarCamposTipos();
        $this->verificarErroExistente();
    }

    public function init()
    {
        $metodo = 'method' . Ajax::getRequestTipo();
        try {
            if (!method_exists($this, $metodo)) {
                throw new ApiExeption('Metodo: ' . $metodo . ' não implementado na classe de api: ' . $this->classe_api, 1, Ajax::HTTP_NOT_FOUND);
            }
        } catch (ApiExeption $e) {
            $json = array(
                'msg' => $e->getMessage(),
                'acao' => $e->getAction(),
                'erro' => true
            );
            Response::json($json, $e->getCode());
        }
        $this->{$metodo}();
    }

    public function setCamposHeader($campos_header = array())
    {
        $this->campos_header = $campos_header;
    }

    public function setCamposTipos($campos_tipos = array())
    {
        $camposObrigatorios = $this->getCamposObrigatorios();
        $camposValidacao = $campos_tipos;
        $this->campos_tipos = array_merge($camposObrigatorios, $camposValidacao);
    }

    public function setEntidadesRelacionadas($entidades_relacionadas = array())
    {
        if (empty($entidades_relacionadas)) {
            $entidades_relacionadas = array('website_geral' => 'controlador_geral_site', 'cms_usuarios' => 'usuarios_admin');
        }
        $this->entidades_relacionadas = $entidades_relacionadas;
    }

    public function setTiposRequestsPermitidos($requests_permitidos = array())
    {
        if (empty($requests_permitidos)) {
            $requests_permitidos = array(Ajax::GET, Ajax::POST, Ajax::PUT, Ajax::DELETE, Ajax::PATCH);
        }
        header('Access-Control-Allow-Methods: '.implode(', ', $requests_permitidos));
        $this->requests_permitidos = $requests_permitidos;
    }

    public function setCamposObrigatorios($campos_obrigatorios = array())
    {
        $this->campos_obrigatorios = $campos_obrigatorios;
    }

    public function setCamposObrigatoriosGET($campos_obrigatorios_get = array())
    {
        $this->campos_obrigatorios_get = $campos_obrigatorios_get;
    }

    public function setCamposObrigatoriosPOST($campos_obrigatorios_post = array())
    {
        $this->campos_obrigatorios_post = $campos_obrigatorios_post;
    }

    public function setCamposObrigatoriosPUT($campos_obrigatorios_put = array())
    {
        $this->campos_obrigatorios_put = $campos_obrigatorios_put;
    }

    public function setCamposObrigatoriosDELETE($campos_obrigatorios_delete = array())
    {
        $this->campos_obrigatorios_delete = $campos_obrigatorios_delete;
    }

    public function getCamposHeader()
    {
        return $this->campos_header;
    }

    public function getCamposObrigatorios()
    {
        return $this->campos_obrigatorios;
    }

    public function getCamposTipos()
    {
        return $this->campos_tipos;
    }

    public function getEntidadesRelacionadas()
    {
        return $this->entidades_relacionadas;
    }

    public function getTiposRequestsPermitidos()
    {
        return $this->requests_permitidos;
    }

    public function getCamposObrigatoriosGET()
    {
        return $this->campos_obrigatorios_get;
    }

    public function getCamposObrigatoriosPOST()
    {
        return $this->campos_obrigatorios_post;
    }

    public function getCamposObrigatoriosPUT()
    {
        return $this->campos_obrigatorios_put;
    }

    public function getCamposObrigatoriosDELETE()
    {
        return $this->campos_obrigatorios_delete;
    }

    public function verificarHeader()
    {
        (new Header())->verificarCampos($this->getCamposHeader());
    }

    public function verificarTiposRequest()
    {
        $tiposRequestPermitidos = $this->getTiposRequestsPermitidos();
        $requestErros = (new TipoRequet())->verificar($tiposRequestPermitidos);
        $this->error[] = $requestErros;

    }

    public function verificarCamposObrigatorios()
    {
        $camposObrigatorios = $this->getCamposObrigatorios();
        $metodo ='getCamposObrigatorios'.strtoupper(Ajax::getRequestTipo());
        $camposObrigatorios = array_merge($camposObrigatorios, $this->{$metodo}());
        $erros = (new CamposObrigatorios())->verificar($camposObrigatorios);
        $this->error[] = $erros;
    }

    public function verificarCamposTipos()
    {
        $campos = $this->getCamposTipos();
        $metodo ='getCamposObrigatorios'.strtoupper(Ajax::getRequestTipo());
        $campos = array_merge($campos, $this->{$metodo}());
        $erros = (new Campos())->verificarCamposTipos($campos);
        $this->error[] = $erros;
    }

    public function verificarErroExistente()
    {
        foreach ($this->error as $erro) {
            if (isset($erro->erro)) {
                Ajax::retorno($erro);
            }
        }

        try {
            if (!empty($this->erro)) {
                throw new ApiExeption(implode('; ', $this->erro));
            }
        } catch (ApiExeption $e) {
            $json = array(
                'msg' => $e->getMessage(),
                'acao' => $e->getAction(),
                'erro' => true
            );
            Response::json($json, ResponseCode::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function methodGET()
    {
        Response::json(array('Metodo' => 'GET'), ResponseCode::HTTP_ACCEPTED);
    }

    public function methodPOST()
    {
        Response::json(array('Metodo' => 'POST'), ResponseCode::HTTP_ACCEPTED);
    }

    public function methodPUT()
    {
        Response::json(array('Metodo' => 'PUT'), ResponseCode::HTTP_ACCEPTED);
    }

    public function methodDELETE()
    {
        Response::json(array('Metodo' => 'DELETE'), ResponseCode::HTTP_ACCEPTED);
    }

    public function methodPATH()
    {
        Response::json(array('Metodo' => 'PATH'), ResponseCode::HTTP_ACCEPTED);
    }

}