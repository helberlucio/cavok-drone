<?php
/*
 * @author Gabriel
 * @description inicializar projeto
 * @date 05/11/2016 09:26:48
 */
//require_once 'system/helpers/FuncoesHelper.php';
//try{
//    throw new Exception('Module RisortPark not founded', 500);
//}catch(Exception $e){
//    show_array($e);
//}
//exit;
ini_set( 'session.cookie_httponly', 1 );
date_default_timezone_set('America/Sao_Paulo');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, OPTIONS, PUT');

require_once 'Config.php';
require_once 'vendor/autoload.php';
require_once 'src/helpers/FuncoesHelper.php';
session_start();
$route = new \Aplicacao\RotearCamadas();